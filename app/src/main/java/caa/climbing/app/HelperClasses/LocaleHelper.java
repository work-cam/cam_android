package caa.climbing.app.HelperClasses;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import caa.climbing.app.Activities.MainActivity;
import caa.climbing.app.Screens.Settings;

import java.util.Locale;

/**
 * Helper class to handle changes of the localisation.
 */
public class LocaleHelper {
    /**
     * Update the context to a new locale.
     *
     * @param context  - Context for which the Locale should be changed
     * @param language - Language to change to
     * @return - Context with updated locale configuration
     */
    public static Context setLocale(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        configuration.setLayoutDirection(locale);

        return context.createConfigurationContext(configuration);
    }

    /**
     * Update the context to use the saved preferred locale.
     *
     * @param context  - Context for which the Locale should be changed
     * @return - Context with updated locale configuration
     */
    public static Context setLocale(Context context) {
        String language = Settings.getDefaultLanguageCode(context);
        return setLocale(context, language);
    }

    /**
     * Update the language of the app if it was changed due to the settings.
     *
     * @param activity - Activity to update.
     */
    static public void updateAppLanguage(Activity activity, String oldLanguageCode) {
        if (!(activity instanceof MainActivity)) return;

        String newLanguageCode = Settings.getDefaultLanguageCode(activity);
        if (oldLanguageCode.equals(newLanguageCode)) return;

        Locale locale = new Locale(newLanguageCode);
        Locale.setDefault(locale);

        Intent intent = new Intent(activity, activity.getClass());
        intent.putExtra(MainActivity.ACTIVE_SCREEN_KEY, ((MainActivity) activity).getActiveScreenKey());

        activity.finish();
        activity.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        activity.startActivity(intent);
    }

    /**
     * Get the language codes for a list of languages.
     *
     * @param languageOptions - Array of language strings
     * @return - Array of matching language codes
     */
    static public String[] getLanguageCodes(String[] languageOptions) {
        int languageCount = languageOptions.length;
        String[] languageCodes = new String[languageCount];
        for (int i = 0; i < languageCount; i++) {
            languageCodes[i] = languageToLocale(languageOptions[i]);
        }
        return languageCodes;
    }

    /**
     * Get the matching language code for a language string.
     *
     * @param language - Language string for which to get the code
     * @return - Matching language code
     */
    static private String languageToLocale(String language) {
        language = language.toLowerCase(Locale.ROOT);
        if (language.equals("english") || language.equals("englisch")) return "en";
        else return "de";
    }
}
