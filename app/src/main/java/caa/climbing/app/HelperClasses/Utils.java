package caa.climbing.app.HelperClasses;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import caa.climbing.app.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

/**
 * A class that provides general utility functions
 */
public class Utils {
    /**
     * Adds the elements of two arrays of float numbers element-wise and returns a new array with
     * the result. Only works if both arrays have the same size.
     *
     * @param first  - First array
     * @param second - Second array
     * @return - New Array with element-wise added elements, or null for arrays of different sizes
     */
    public static float[] addFloatArrays(float[] first, float[] second) {
        if (first == null || second == null || first.length != second.length) return null;

        int length = first.length;
        float[] sum = new float[length];

        for (int i = 0; i < length; i++) {
            sum[i] = first[i] + second[i];
        }

        return sum;
    }

    /**
     * Adds the elements of two arrays of float numbers element-wise and returns a new array with
     * the result. Only works if both arrays have the same size.
     *
     * @param first  - First array
     * @param second - Second array
     * @return - New Array with element-wise added elements, or null for arrays of different sizes
     */
    public static int[] addIntArrays(int[] first, int[] second) {
        if (first == null || second == null || first.length != second.length) return null;

        int length = first.length;
        int[] sum = new int[length];

        for (int i = 0; i < length; i++) {
            sum[i] = first[i] + second[i];
        }

        return sum;
    }

    /**
     * Compare the size of two floating point numbers
     *
     * @param a - First number
     * @param b - Second number
     * @return 1 is a > b
     * -1 is a < b
     * 0 is a == b
     */
    public static int compareFloat(float a, float b) {
        int ta = Math.round(a * 1000000);
        int tb = Math.round(b * 1000000);
        return Integer.compare(ta, tb);
    }

    /**
     * Convert a Density pixel value to a pixel value
     *
     * @param context - Context from which the view was created
     * @param dpValue - dp value to convert
     * @return - Value in px
     */
    public static int dp2px(Context context, float dpValue) {
        if (context == null || compareFloat(0f, dpValue) == 0) return 0;
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f) / 2;
    }

    /**
     * Convert a Density pixel value to a pixel value
     *
     * @param context - Context from which the view was created
     * @param spValue - sp value to convert
     * @return - Value in px
     */
    public static float sp2px(Context context, float spValue) {
        if (context == null || compareFloat(0f, spValue) == 0) return 0;
        float scale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue / scale);
    }

    /**
     * Styles the view.
     *
     * @param context    - Context from which the view was created
     * @param view       - The view to style
     * @param attrs      - Attributes that are passed to the view
     * @param background - Optional background for the view
     */
    public static void setStyle(Context context, View view, AttributeSet attrs, @Nullable Drawable background) {
        int[] paddings = new int[]{android.R.attr.paddingLeft, android.R.attr.paddingTop, android.R.attr.paddingBottom, android.R.attr.paddingRight, android.R.attr.paddingStart, android.R.attr.paddingEnd};

        int setPaddingL = -1;
        int setPaddingR = -1;
        int setPaddingT = -1;
        int setPaddingB = -1;
        int setPaddingS = -1;
        int setPaddingE = -1;
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, paddings);

            setPaddingL = a.getDimensionPixelOffset(0, -1);
            setPaddingT = a.getDimensionPixelOffset(1, -1);
            setPaddingB = a.getDimensionPixelOffset(2, -1);
            setPaddingR = a.getDimensionPixelOffset(3, -1);
            setPaddingS = a.getDimensionPixelOffset(4, -1);
            setPaddingE = a.getDimensionPixelOffset(5, -1);

            a.recycle();
        }
        int paddingT = dp2px(context, context.getResources().getDimension(R.dimen.input_padding_top));
        int paddingB = dp2px(context, context.getResources().getDimension(R.dimen.input_padding_bottom));
        int paddingL = dp2px(context, context.getResources().getDimension(R.dimen.input_padding_left));
        int paddingR = dp2px(context, context.getResources().getDimension(R.dimen.input_padding_right));

        int finalPaddingL, finalPaddingR, finalPaddingT, finalPaddingB;

        // left padding
        finalPaddingL = paddingL;
        if (setPaddingL != -1) finalPaddingL = setPaddingL;
        if (setPaddingS != -1) finalPaddingL = setPaddingS;

        // right padding
        finalPaddingR = paddingR;
        if (setPaddingR != -1) finalPaddingR = setPaddingR;
        if (setPaddingE != -1) finalPaddingR = setPaddingE;

        // top padding
        finalPaddingT = paddingT;
        if (setPaddingT != -1) finalPaddingT = setPaddingT;

        // bottom padding
        finalPaddingB = paddingB;
        if (setPaddingB != -1) finalPaddingB = setPaddingB;

        view.setPadding(finalPaddingL, finalPaddingT, finalPaddingR, finalPaddingB);

        if (background != null) view.setBackground(background);
    }

    /**
     * Sets the text style of a view.
     *
     * @param context - Context from which the view was created
     * @param view    - The view to style
     * @param attrs   - Attributes that are passed to the view
     */
    public static void setTextStyle(Context context, TextView view, AttributeSet attrs) {
        int[] textAttrs = new int[]{android.R.attr.textColor, android.R.attr.textAlignment, android.R.attr.textSize};
        int textColor = ResourcesCompat.getColor(context.getResources(), R.color.white, null);
        int textAlignment = View.TEXT_ALIGNMENT_CENTER;
        float textSize = sp2px(context, context.getResources().getDimension(R.dimen.fontsize_big));

        if (attrs != null) {
            TypedArray a_txt = context.obtainStyledAttributes(attrs, textAttrs);
            textColor = a_txt.getColor(0, ResourcesCompat.getColor(context.getResources(), R.color.white, null));
            textAlignment = a_txt.getInt(1, View.TEXT_ALIGNMENT_CENTER);
            textSize = a_txt.getDimension(2, sp2px(context, context.getResources().getDimension(R.dimen.fontsize_big)));

            a_txt.recycle();
        }

        view.setTextColor(textColor);
        view.setTextAlignment(textAlignment);
        view.setTextSize(textSize);
        view.setTypeface(Typeface.DEFAULT_BOLD);
    }

    /**
     * Hide the soft keyboard of the device.
     *
     * @param context - Context from which to call the method.
     */
    public static void hideKeyboard(Context context) {

        Activity activity = getActivity(context);

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        // Find the currently focused view in order to grab the correct window token from it.
        View view = activity.getCurrentFocus();

        // If no view currently has focus, create a new one in order to grab a window token from it.
        if (view == null) {
            view = new View(activity);
        }

        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Get the owning activity based on the context.
     *
     * @param context - Context from which to call the method.
     * @return - The owning activity
     */
    public static AppCompatActivity getActivity(Context context) {

        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (AppCompatActivity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }

    /**
     * Get the usable height of the screen.
     *
     * @param activity - Activity displayed on the screen
     * @return - Usable height of the screen
     */
    private static int getUsableHeight(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    /**
     * Get the height of the status bar.
     *
     * @param activity - Activity of the status bar
     * @return - Height of the status bar
     */
    private static int getStatusBarHeight(Activity activity) {
        Rect rectangle = new Rect();
        Window window = activity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        return rectangle.top;
    }

    /**
     * Calculate the distance between the bottom edge of a view and the bottom edge of the screen.
     *
     * @param view     - View to use for the calculation
     * @param activity - Activity where the view is displayed.
     * @return - Distance between the bottom edge of a view and the bottom edge of the screen
     */
    public static int getDistanceToBottomScreen(View view, Activity activity) {
        if (activity == null) return 0;

        int usableHeight = getUsableHeight(activity);
        int statusBarHeight = getStatusBarHeight(activity);

        int[] topViewLocation = new int[2];
        view.getLocationOnScreen(topViewLocation);
        int bottomEdgeTopView = topViewLocation[1] + view.getMeasuredHeight();

        return usableHeight + statusBarHeight - bottomEdgeTopView;
    }

    /**
     * Set the calendar time so that no information about units smaller than the day are present.
     *
     * @param calendar - Calendar to reset
     */
    public static void resetCalendar(Calendar calendar) {
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }
}
