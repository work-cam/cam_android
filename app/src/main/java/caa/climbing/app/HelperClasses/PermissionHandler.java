package caa.climbing.app.HelperClasses;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import caa.climbing.app.Dialogs.InformDialog.InformDialog;
import caa.climbing.app.R;

import java.util.Map;

/**
 * Class to handle permission requests.
 */
public class PermissionHandler extends Fragment {
    static private final String TAG = "PERMISSION";

    private ActivityResultLauncher<String> requestPermissionLauncher;
    private ActivityResultLauncher<String[]> requestMultiplePermissionLauncher;

    private final Context context;
    private final FragmentManager fragmentManager;
    private final PermissionCallback callBack;
    private final int type;

    public final static int CAMERA = 0;
    public final static int READ = 1;
    public final static int WRITE = 2;
    public final static int CALENDAR = 3;

    /**
     * Creates a new {@link PermissionHandler}.
     */
    public PermissionHandler() {
        this.context = null;
        this.fragmentManager = null;
        this.callBack = null;
        this.type = -1;
    }

    /**
     * Creates a new {@link PermissionHandler}.
     *
     * @param context         - Context from which to handle permissions
     * @param fragmentManager - Fragment manager to show dialogs
     * @param callBack        - Callback to execute if permission is granted
     * @param permissionType  - Type of the permission handler, either for camera or storage access
     */
    public PermissionHandler(Context context, FragmentManager fragmentManager, PermissionCallback callBack, int permissionType) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.callBack = callBack;
        this.type = permissionType;

        this.registerPermissionLauncher();

        if (this.fragmentManager == null) return;

        this.fragmentManager.beginTransaction().add(this, TAG).commit();
    }

    /**
     * Register the Launcher to handle the result from the respective permission requests.
     */
    private void registerPermissionLauncher() {
        switch (this.type) {
            case CAMERA:
                this.registerCameraPermissionLauncher();
                break;
            case READ:
                this.registerReadStoragePermissionLauncher();
                break;
            case WRITE:
                this.registerWriteStoragePermissionLauncher();
                break;
            case CALENDAR:
                this.registerCalendarPermissionLauncher();
                break;
            default:
                break;
        }
    }

    /**
     * Register the Launcher to handle the result from the camera permission requests.
     */
    private void registerCameraPermissionLauncher() {
        this.requestMultiplePermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), permissions -> {
            boolean allPermissionsGranted = true;
            for (Map.Entry<String, Boolean> entry : permissions.entrySet()) {
                if (!entry.getValue()) allPermissionsGranted = false;
            }

            if (allPermissionsGranted) {
                if (this.callBack != null) this.callBack.startAction();
            } else {
                this.showCameraPermissionNecessaryDialog();
            }
        });
    }

    /**
     * Register the Launcher to handle the result from the permission requests to read from the storage.
     */
    private void registerReadStoragePermissionLauncher() {
        this.requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
            if (isGranted) {
                if (this.callBack != null) this.callBack.startAction();
            } else {
                this.showReadStoragePermissionNecessaryDialog();
            }
        });
    }

    /**
     * Register the Launcher to handle the result from permission requests to write to the storage.
     */
    private void registerWriteStoragePermissionLauncher() {
        this.requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
            if (isGranted) {
                if (this.callBack != null) this.callBack.startAction();
            } else {
                this.showWriteStoragePermissionNecessaryDialog();
            }
        });
    }

    /**
     * Register the Launcher to handle the result from the calendar permission requests.
     */
    private void registerCalendarPermissionLauncher() {
        this.requestMultiplePermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), permissions -> {
            boolean allPermissionsGranted = true;
            for (Map.Entry<String, Boolean> entry : permissions.entrySet()) {
                if (!entry.getValue()) allPermissionsGranted = false;
            }

            if (allPermissionsGranted) {
                if (this.callBack != null) this.callBack.startAction();
            } else {
                this.showCalendarPermissionNecessaryDialog();
            }
        });
    }

    /**
     * Shows a dialog to the user to inform them that the app needs permission to access the camera in order to take photos.
     */
    private void showCameraPermissionNecessaryDialog() {
        InformDialog.openDialog(this.fragmentManager, getString(R.string.camera_missing_permission));
    }

    /**
     * Shows a dialog to the user to inform them that the app needs permission to read from the
     * device's storage in order to select a photo.
     */
    private void showReadStoragePermissionNecessaryDialog() {
        InformDialog.openDialog(this.fragmentManager, getString(R.string.gallery_missing_permission));
    }

    /**
     * Shows a dialog to the user to inform them that the app needs permission to write to the
     * device's storage.
     */
    private void showWriteStoragePermissionNecessaryDialog() {
        InformDialog.openDialog(this.fragmentManager, getString(R.string.storage_missing_permission));
    }

    /**
     * Shows a dialog to the user to inform them that the app needs permission read from and to write to the
     * device's calendar.
     */
    private void showCalendarPermissionNecessaryDialog() {
        InformDialog.openDialog(this.fragmentManager, getString(R.string.calendar_missing_permission));
    }

    /**
     * Starts an intent based on the type of the permission handler.
     */
    public void startActionWithPermission() {
        switch (this.type) {
            case CAMERA:
                this.startCameraIntentWithPermission();
                break;
            case READ:
                this.startReadStorageIntentWithPermission();
                break;
            case WRITE:
                this.startWriteStorageIntentWithPermission();
                break;
            case CALENDAR:
                this.startCalendarActionWithPermission();
            default:
                break;
        }
    }

    /**
     * Starts an intent if all necessary permissions for camera access have been granted.
     * If permissions were not granted, ask for permission.
     */
    private void startCameraIntentWithPermission() {
        if (this.hasWriteStorageAndCameraPermissions()) {
            if (this.callBack != null) this.callBack.startAction();
        } else {
            String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            this.requestMultiplePermissionLauncher.launch(permissions);
        }
    }

    /**
     * Starts an intent if the necessary permission for reading from the storage has been granted.
     * If permissions were not granted, ask for permission.
     */
    private void startReadStorageIntentWithPermission() {
        if (this.hasReadStoragePermission()) {
            if (this.callBack != null) this.callBack.startAction();
        } else {
            String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
            this.requestPermissionLauncher.launch(permission);
        }
    }

    /**
     * Starts an intent if all necessary permissions for writing access have been granted.
     * If permissions were not granted, ask for permission.
     */
    private void startWriteStorageIntentWithPermission() {
        if (this.hasWriteStoragePermission()) {
            if (this.callBack != null) this.callBack.startAction();
        } else {
            String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            this.requestPermissionLauncher.launch(permission);
        }
    }

    /**
     * If the app has permission to access the calendar, the callback is executed.
     * If permissions were not granted, ask for permission.
     */
    private void startCalendarActionWithPermission() {
        if (this.hasCalendarPermissions()) {
            if (this.callBack != null) this.callBack.startAction();
        } else {
            String[] permissions = new String[]{Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR};
            this.requestMultiplePermissionLauncher.launch(permissions);
        }
    }

    /**
     * Check whether the app has the permissions to read files from the device storage.
     *
     * @return - Whether the permissions are granted
     */
    private boolean hasReadStoragePermission() {
        if (this.context == null) return false;

        return ContextCompat.checkSelfPermission(this.context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Check whether the app has the permissions to write to the device storage.
     *
     * @return - Whether the permissions are granted
     */
    private boolean hasWriteStoragePermission() {
        if (this.context == null) return false;

        return ContextCompat.checkSelfPermission(this.context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Check whether the app has the permissions to use the device's camera and save files to the device storage.
     *
     * @return - Whether the permissions are granted
     */
    private boolean hasWriteStorageAndCameraPermissions() {
        if (this.context == null) return false;

        boolean hasCameraPermission = ContextCompat.checkSelfPermission(this.context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        boolean hasStoragePermission = ContextCompat.checkSelfPermission(this.context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

        return hasCameraPermission && hasStoragePermission;
    }

    /**
     * Check whether the app has the permissions to read from and write to the calendar.
     *
     * @return - Whether the permissions are granted
     */
    private boolean hasCalendarPermissions() {
        if (this.context == null) return false;

        boolean hasReadCalendarPermission = ContextCompat.checkSelfPermission(this.context, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED;
        boolean hasWriteCalendarPermission = ContextCompat.checkSelfPermission(this.context, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED;

        return hasReadCalendarPermission && hasWriteCalendarPermission;

    }

    /**
     * Wrapper class to pass callback function to the PermissionHandler.
     */
    public interface PermissionCallback {
        void startAction();
    }
}
