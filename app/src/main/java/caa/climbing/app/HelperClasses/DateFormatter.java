package caa.climbing.app.HelperClasses;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A class to format dates in a consistent way.
 */
public class DateFormatter {
    final static private String datePatternLong = "dd/MM/yyyy";
    final static private String datePatternShort = "dd/MM/yy";

    // milliseconds * sec * min * hours
    // 1000 * 60 * 60 * 24 = 86400000
    final static private long MILLISECONDS_PER_DAY = 86400000L;

    /**
     * Create a simple date formatter to format dates.
     *
     * @param shortFormat - Whether to use to short format {@link DateFormatter#datePatternShort} or the long format {@link DateFormatter#datePatternLong}
     * @return - Formatter to format dates
     */
    static private SimpleDateFormat sdf(boolean shortFormat) {
        return shortFormat ? new SimpleDateFormat(datePatternShort, Locale.getDefault()) : new SimpleDateFormat(datePatternLong, Locale.getDefault());
    }

    /**
     * Format a date to get its string representation.
     *
     * @param date        - Date to format
     * @param shortFormat - Whether to use to short format {@link DateFormatter#datePatternShort} or the long format {@link DateFormatter#datePatternLong}
     * @return - Formatted date string
     */
    static public String format(Date date, boolean shortFormat) {
        SimpleDateFormat sdf = sdf(shortFormat);
        return sdf.format(date);
    }

    /**
     * Parse a date from a string.
     *
     * @param dateString  - String to parsed
     * @param shortFormat - Whether to use to short format {@link DateFormatter#datePatternShort} or the long format {@link DateFormatter#datePatternLong}
     * @return - Parsed date
     * @throws ParseException - Throws an exception if the string cannot be parsed
     */
    static public Date parse(String dateString, boolean shortFormat) throws ParseException {
        SimpleDateFormat sdf = sdf(shortFormat);
        return sdf.parse(dateString);
    }

    /**
     * Get the number of days since January 1, 1970, 00:00:00 GTM from a given date.
     *
     * @param date - Date to use
     * @return - Number of days since January 1, 1970, 00:00:00 GTM
     * @link {https://stackoverflow.com/a/24683935/12755273}
     */
    static public int DateToDays(Date date) {
        long millis = date.getTime();
        long days = millis / MILLISECONDS_PER_DAY;
        return (int) days;
    }

    /**
     * Convert a number of days to a date string.
     *
     * @param days        - Days since January 1, 1970, 00:00:00 GTM
     * @param shortFormat - Whether to use to short format {@link DateFormatter#datePatternShort} or the long format {@link DateFormatter#datePatternLong}
     * @return - String representation of the date
     */
    static public String DaysToDate(int days, boolean shortFormat) {
        long millis = (long) days * MILLISECONDS_PER_DAY;
        Date date = new Date(millis);
        return sdf(shortFormat).format(date);
    }
}
