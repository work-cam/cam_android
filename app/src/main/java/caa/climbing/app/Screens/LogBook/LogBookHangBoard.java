package caa.climbing.app.Screens.LogBook;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.ViewOutlineProvider;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import caa.climbing.app.Adapters.RecyclerAdapter_HangBoardSessionDisplay;
import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.DatabaseModels.HangboardSession;
import caa.climbing.app.R;

/**
 * A Fragment to display hangboard sessions as content of a tab bar.
 */
public class LogBookHangBoard extends Fragment {
    private RecyclerView sessionContainer;

    /**
     * Creates a new {@link LogBookHangBoard} Fragment
     */
    public LogBookHangBoard() {
    }

    /**
     * Inflates the view
     *
     * @param inflater           - Inflater to inflate the view
     * @param container          - Container that contains the views
     * @param savedInstanceState - Previous Instance
     * @return - The inflated view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.logbook_hangboard, container, false);
    }

    /**
     * Called after the view was created.
     *
     * @param view               - The view that was created
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.initViews(view);
        this.loadData();
    }

    /**
     * Initializes the views inside the Fragment
     *
     * @param rootView - Root view that contains the rest of the views
     */
    private void initViews(View rootView) {
        this.sessionContainer = rootView.findViewById(R.id.hangboard_session_container);

        Context context = getContext();
        if (context == null) return;

        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.divider, null);
        if (drawable != null) {
            itemDecoration.setDrawable(drawable);
            this.sessionContainer.addItemDecoration(itemDecoration);
        }
    }


    /**
     * Loads the logged sessions from the database and displays them.
     */
    private void loadData() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(getContext());
        int lastTestEdgeSize = dbHelper.getLastHangboardTestEdgeSize(dbHelper.getReadableDatabase());

        ArrayList<HangboardSession> sessions = dbHelper.getHangboardSession(dbHelper.getReadableDatabase(), lastTestEdgeSize, true);

        // use linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        this.sessionContainer.setLayoutManager(layoutManager);

        this.sessionContainer.setOutlineProvider(ViewOutlineProvider.BACKGROUND);
        this.sessionContainer.setClipToOutline(true);

        RecyclerAdapter_HangBoardSessionDisplay adapter = new RecyclerAdapter_HangBoardSessionDisplay(getContext(), sessions);

        this.sessionContainer.setAdapter(adapter);
        this.sessionContainer.setHasFixedSize(true);
    }

}
