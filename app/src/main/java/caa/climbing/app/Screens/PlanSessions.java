package caa.climbing.app.Screens;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.Dialogs.SelectDialog.SelectDialog;
import caa.climbing.app.R;
import caa.climbing.app.Views.EventCalendar.CalendarEvent;
import caa.climbing.app.Views.EventCalendar.EventCalendar;

import java.util.ArrayList;
import java.util.Date;

/**
 * A Fragment to display future sessions and already completed ones.
 */
public class PlanSessions extends Fragment {

    private EventCalendar sessionCalendar;
    private ArrayList<LabelledInt> sessionOptions;

    /**
     * Creates a new {@link PlanSessions} instance.
     */
    public PlanSessions() {
        super(R.layout.plan_sessions_screen);
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param rootView           - Inflated View
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View rootView, Bundle savedInstanceState) {
        this.sessionCalendar = rootView.findViewById(R.id.session_calendar);

        this.sessionOptions = new ArrayList<>();
        this.sessionOptions.add(new LabelledInt(getString(R.string.plan_session_indoor), 0));
        this.sessionOptions.add(new LabelledInt(getString(R.string.plan_session_outdoor), 1));
        this.sessionOptions.add(new LabelledInt(getString(R.string.plan_session_hangboard), 2));

        this.sessionCalendar.setAddEventButtonListener((view) -> this.showSessionSelectDialog());
    }

    /**
     * Show the dialog to select which kind of session should be saved.
     */
    private void showSessionSelectDialog() {
        Context context = getContext();
        if (context == null) return;

        SelectDialog select = new SelectDialog(context.getString(R.string.plan_session_select_title), -1, sessionOptions);

        View.OnClickListener clickListener = (v) -> {
            LabelledInt selectedSession = select.getSelectedOption();
            Date selectedDate = this.sessionCalendar.getSelectedDate();
            CalendarEvent newEvent = new CalendarEvent(selectedSession.label, selectedDate);

            this.sessionCalendar.saveCalendarEvent(newEvent);
        };

        select.setOkClick(clickListener);

        select.show(getChildFragmentManager(), "select");
    }

}
