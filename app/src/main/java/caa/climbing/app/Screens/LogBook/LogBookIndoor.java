package caa.climbing.app.Screens.LogBook;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.IntSummaryStatistics;

import caa.climbing.app.Adapters.RecyclerAdapter_IndoorSessionDisplay;
import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.DatabaseModels.IndoorSession;
import caa.climbing.app.HelperClasses.DateFormatter;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import caa.climbing.app.Views.Filter.FilterIndoorSession;
import caa.climbing.app.Views.Slider.OnRangeChangedListener;
import caa.climbing.app.Views.Slider.RangeSlider;

/**
 * A Fragment to display indoor sessions as content of a tab bar.
 */
public class LogBookIndoor extends Fragment {
    private RecyclerView sessionContainer;
    private RecyclerAdapter_IndoorSessionDisplay adapter;
    private FilterIndoorSession filter;
    private ArrayList<IndoorSession> sessions;

    /**
     * Creates a new {@link LogBookIndoor} Fragment
     */
    public LogBookIndoor() {
    }

    /**
     * Inflates the view
     *
     * @param inflater           - Inflater to inflate the view
     * @param container          - View that contains the view
     * @param savedInstanceState - Previous Instance
     * @return - The inflated view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.logbook_indoor, container, false);
    }

    /**
     * Called after the view was created.
     *
     * @param view               - The view that was created
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.initViews(view);
        this.loadData();
        this.setDateFilterRange();
        this.addEventListener();
    }

    /**
     * Initializes the views inside the Fragment
     *
     * @param rootView - Root view that contains the rest of the views
     */
    private void initViews(View rootView) {
        this.sessionContainer = rootView.findViewById(R.id.indoor_session_container);

        Context context = getContext();
        if (context == null) return;

        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.divider, null);
        if (drawable != null) itemDecoration.setDrawable(drawable);
        this.sessionContainer.addItemDecoration(itemDecoration);

        this.filter = rootView.findViewById(R.id.filter_indoor_session);

        ViewTreeObserver observer = rootView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                setSessionContainerHeight();
            }
        });
    }

    /**
     * Set the height of the session container view, so that it fills the screen.
     */
    private void setSessionContainerHeight() {
        ViewGroup.LayoutParams layoutParams = this.sessionContainer.getLayoutParams();
        layoutParams.height = Utils.getDistanceToBottomScreen(this.filter, getActivity());
        this.sessionContainer.setLayoutParams(layoutParams);
    }

    /**
     * Set the range of the seekbar for the date filter. The range should only
     * include dates between the earliest and the latest session.
     */
    private void setDateFilterRange() {
        int[] dates = this.getIndoorSessionDates(this.sessions);
        IntSummaryStatistics stats = Arrays.stream(dates).summaryStatistics();
        int min = stats.getMin();
        int max = stats.getMax();

        if (dates.length == 0 || min == max) {
            this.filter.setDateRangeDisplay("");
            return;
        }

        String minDate = DateFormatter.DaysToDate(min, true);
        String maxDate = DateFormatter.DaysToDate(max, true);

        this.filter.setDateRangeDisplay(minDate + " - " + maxDate);
        this.filter.setDateRange(min, max);
    }

    /**
     * Adds event listener for the filter.
     */
    private void addEventListener() {
        View.OnClickListener clickListener = (view) -> filterSessions();

        OnRangeChangedListener rangeChangedListener = new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSlider view, float leftValue, float rightValue, boolean isFromUser) {
                String minDate = DateFormatter.DaysToDate((int) leftValue, true);
                String maxDate = DateFormatter.DaysToDate((int) rightValue, true);
                filter.setDateRangeDisplay(minDate + " - " + maxDate);
            }

            @Override
            public void onStartTrackingTouch(RangeSlider view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSlider view, boolean isLeft) {
                filterSessions();
            }
        };

        TextView.OnEditorActionListener actionListener = (textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                filterSessions();
                return true;
            }
            return false;
        };

        this.filter.setSearchFieldActionListener(actionListener);
        this.filter.setSearchButtonClick(clickListener);
        this.filter.setSortingOrderClick(clickListener);
        this.filter.setBoulderCheckBoxClickListener(clickListener);
        this.filter.setRouteCheckBoxClickListener(clickListener);
        this.filter.setOnRangeChangedListener(rangeChangedListener);
    }

    /**
     * Filter the sessions based on the inputs in the filter and update
     * the view with the filtered data.
     */
    private void filterSessions() {
        ArrayList<IndoorSession> filtered = filter.filter(sessions);
        if (filtered.size() > 0) this.filter.collapse();
        this.adapter = new RecyclerAdapter_IndoorSessionDisplay(getContext(), filtered);
        this.sessionContainer.swapAdapter(adapter, true);
        Utils.hideKeyboard(getContext());
    }

    /**
     * Loads the logged sessions from the database and displays them.
     */
    private void loadData() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(getContext());

        this.sessions = dbHelper.getIndoorSessions(dbHelper.getReadableDatabase(), getContext());

        // use linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        this.sessionContainer.setLayoutManager(layoutManager);

        this.adapter = new RecyclerAdapter_IndoorSessionDisplay(getContext(), sessions);

        this.sessionContainer.setAdapter(this.adapter);
        this.sessionContainer.setHasFixedSize(true);

        this.filter.setSearchFieldGymSuggestions(dbHelper.getGyms(dbHelper.getReadableDatabase()));
    }

    /**
     * Get the date for each {@link IndoorSession} in a list.
     *
     * @param sessions - List of sessions to get the dates from
     * @return - Array of dates
     */
    private int[] getIndoorSessionDates(ArrayList<IndoorSession> sessions) {
        int[] dates = new int[sessions.size()];
        for (int i = 0; i < sessions.size(); i++) {
            dates[i] = DateFormatter.DateToDays(sessions.get(i).date);
        }

        return dates;
    }
}
