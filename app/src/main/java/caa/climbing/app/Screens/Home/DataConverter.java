package caa.climbing.app.Screens.Home;

import caa.climbing.app.Database.DatabaseModels.HangboardSession;
import caa.climbing.app.Database.DatabaseModels.IndoorSession;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;

import caa.climbing.app.Database.DatabaseModels.OutdoorAscent;
import caa.climbing.app.HelperClasses.DateFormatter;
import caa.climbing.app.HelperClasses.Utils;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.*;

/**
 * Class to convert raw Data from the Resources.Database to usable forms
 */
class DataConverter {

    /**
     * Convert the data from the DB to a form that can be used to plot inside a LineChart
     *
     * @param hangboardSessions - Hangboard Sessions as they are retrieved from the DB
     * @return - Hangboard data for plotting
     */
    public static HangboardData hangBoardToLine(ArrayList<HangboardSession> hangboardSessions) {

        // Data for the LineChart
        ArrayList<Entry> dataLeft = new ArrayList<>();
        ArrayList<Entry> dataRight = new ArrayList<>();
        ArrayList<Entry> dataRelStrength = new ArrayList<>();

        // iterate over the Sessions and extract the information from each session
        // the info is then added to the corresponding data for the LineChart
        for (HangboardSession session : hangboardSessions) {
            int days = DateFormatter.DateToDays(session.date);

            if (session.bodyWeight != 0 && session.maxStrengthLeft != 0 && session.maxStrengthRight != 0) {
                Entry left = new Entry(days, session.maxStrengthLeft);
                Entry right = new Entry(days, session.maxStrengthRight);
                float rs = session.bodyWeight / (session.maxStrengthLeft + session.maxStrengthRight);
                Entry relStrength = new Entry(days, rs);

                dataLeft.add(left);
                dataRight.add(right);
                dataRelStrength.add(relStrength);
            }

        }

        // construct the data for the LineChart and return the result
        LineDataSet lineDataLeft = new LineDataSet(dataLeft, "");
        LineDataSet lineDataRight = new LineDataSet(dataRight, "");
        LineDataSet lineDataRelStrength = new LineDataSet(dataRelStrength, "");

        return new HangboardData(lineDataLeft, lineDataRight, lineDataRelStrength);
    }

    /**
     * Finds the index of the first {@link Entry} in a list with the given x-Value.
     * If no element matches, -1 is returned.
     *
     * @param entries - List of {@link Entry}-objects
     * @param xValue  - x-Value to search for
     * @return - The index of the matching {@link BarEntry}, or -1
     */
    private static <T extends Entry> int getEntryIndexByX(ArrayList<T> entries, float xValue) {
        for (int i = 0; i < entries.size(); i++) {
            Entry entry = entries.get(i);
            if (entry != null && entry.getX() == xValue) {
                return i;
            }
        }
        return -1;// not in list
    }

    /**
     * Reduces a list of {@link Entry}-objects to a list where each element has a unique x-value.
     * The y-values of elements with non-unique x-values are added together element-wise.
     *
     * @param data - Data to reduce
     * @return - Reduced List
     */
    public static <T extends Entry> ArrayList<T> reduceData(Class<T> type, ArrayList<T> data, boolean cumulative) {

        ArrayList<T> copy = new ArrayList<>(data);
        ArrayList<T> result = new ArrayList<>();
        ArrayList<T> cumulated = new ArrayList<>();

        while (copy.size() > 0) {
            T element = copy.get(0);

            while (getEntryIndexByX(copy, element.getX()) != -1) {
                int index = getEntryIndexByX(copy, element.getX());
                Entry current = copy.get(index);

                if (element instanceof BarEntry && current instanceof BarEntry) {
                    float[] addedValues = Utils.addFloatArrays(((BarEntry) element).getYVals(), ((BarEntry) current).getYVals());
                    if (!current.equals(element) && addedValues != null) {
                        element = type.cast(element);
                        ((BarEntry) element).setVals(addedValues);
                    }
                } else {
                    float addedValues = element.getY() + current.getY();
                    if (!current.equals(element)) {
                        element = type.cast(element);
                        element.setY(addedValues);
                    }
                }
                copy.remove(index);
            }
            result.add(element);
        }

        if (cumulative) {
            float total = 0;
            for (Entry entry : result) {
                total += entry.getY();
                cumulated.add(type.cast(new Entry(entry.getX(), total)));
            }
            return cumulated;
        }
        return result;
    }

    /**
     * Convert the data from the DB to a form that can be used to plot inside a BarChart
     *
     * @param indoorSessions - Indoor Sessions as they are retrieved from the DB
     * @return - A Bundle with data for plotting
     */
    public static IndoorDataWrapper indoorToBar(ArrayList<IndoorSession> indoorSessions, boolean cumulative) {
        ArrayList<BarEntry> dataPointsBoulder = new ArrayList<>();
        ArrayList<BarEntry> dataPointsRoutes = new ArrayList<>();

        if (cumulative) {
            IndoorDataWrapper cumulatedAscents = getCumulatedIndoorAscents(indoorSessions);
            dataPointsBoulder = cumulatedAscents.boulders;
            dataPointsRoutes = cumulatedAscents.routes;
        } else {
            for (IndoorSession session : indoorSessions) {

                Date date = session.date;

                addIndoorDataToBar(date, session.boulders, dataPointsBoulder);
                addIndoorDataToBar(date, session.routes, dataPointsRoutes);
            }
        }

        ArrayList<BarEntry> reducedBoulders = reduceData(BarEntry.class, dataPointsBoulder, false);
        ArrayList<BarEntry> reducedRoutes = reduceData(BarEntry.class, dataPointsRoutes, false);

        return new IndoorDataWrapper(reducedBoulders, reducedRoutes);
    }

    /**
     * Get the total amount of ascents per grade
     *
     * @param indoorSessions - List of indoor sessions
     * @return - List of total ascents per grade
     */
    private static IndoorDataWrapper getCumulatedIndoorAscents(ArrayList<IndoorSession> indoorSessions) {
        int[] resultBoulders = {0, 0, 0, 0, 0};
        int[] resultRoutes = {0, 0, 0, 0, 0, 0};
        ArrayList<BarEntry> dataPointsBoulder = new ArrayList<>();
        ArrayList<BarEntry> dataPointsRoutes = new ArrayList<>();

        for (IndoorSession session : indoorSessions) {
            resultBoulders = addIndoorAscents(session.boulders, resultBoulders);
            resultRoutes = addIndoorAscents(session.routes, resultRoutes);
        }

        if (Arrays.stream(resultBoulders).sum() > 0) {
            for (int i = 0; i < resultBoulders.length; i++) {
                dataPointsBoulder.add(new BarEntry(i, resultBoulders[i]));
            }
        }

        if (Arrays.stream(resultRoutes).sum() > 0) {
            for (int i = 0; i < resultRoutes.length; i++) {
                dataPointsRoutes.add(new BarEntry(i, resultRoutes[i]));
            }
        }

        return new IndoorDataWrapper(dataPointsBoulder, dataPointsRoutes);
    }

    /**
     * Add ascents of different sessions together to get the total number of ascents per grade
     *
     * @param ascents - Ascents to add
     * @param result  - Array to add to
     * @return - Cumulated list of ascents per grade
     */
    private static int[] addIndoorAscents(ArrayList<LabelledInt> ascents, int[] result) {
        int[] list = LabelledInt.getIntValues(ascents);
        int sum = Arrays.stream(list).sum();
        int[] temp = sum > 0 && list.length == result.length ? Utils.addIntArrays(list, result) : result;

        if (temp != null) result = temp;

        return result;
    }

    /**
     * Add a single {@link BarEntry} of ascents to a list of ascents.
     *
     * @param date     - Date of the session
     * @param ascents  - Ascents to add
     * @param dataList - List to add the ascents to
     */
    private static void addIndoorDataToBar(Date date, ArrayList<LabelledInt> ascents, ArrayList<BarEntry> dataList) {
        if (!LabelledInt.allZero(ascents)) {
            int days = DateFormatter.DateToDays(date);
            BarEntry entry = new BarEntry(days, LabelledInt.getFloatValues(ascents));
            dataList.add(entry);
        }
    }

    /**
     * Convert the data from the DB to a form that can be used to plot inside a BarChart
     *
     * @param ascents - Outdoor Ascents as they are retrieved from the DB
     * @return - A Bundle with data for plotting
     */
    public static ArrayList<BarEntry> outdoorToBar(ArrayList<OutdoorAscent> ascents) {

        ArrayList<BarEntry> dataPoints = new ArrayList<>();
        HashMap<Integer, float[]> ascentsPerGrade = getOutdoorAscentsPerGrade(ascents);
        addOutdoorDataToBar(ascentsPerGrade, dataPoints);

        return reduceData(BarEntry.class, dataPoints, false);
    }

    /**
     * Convert the data from the DB to a form that can be used to plot inside a LineChart.
     *
     * @param ascents - Outdoor Ascents as they are retrieved from the DB
     * @return - Data to plot
     */
    public static LineDataSet outdoorToLine(ArrayList<OutdoorAscent> ascents) {
        ArrayList<Entry> ascentScores = new ArrayList<>();

        for (OutdoorAscent ascent : ascents) {
            int days = DateFormatter.DateToDays(ascent.date);

            int score = ascent.score;

            ascentScores.add(new Entry(days, score));
        }

        ArrayList<Entry> reduced = reduceData(Entry.class, ascentScores, true);

        return new LineDataSet(reduced, "");
    }

    /**
     * Add a single {@link BarEntry} of ascents to a list of ascents.
     *
     * @param ascents    - Ascents to add
     * @param dataPoints - List to add the ascents to
     */
    private static void addOutdoorDataToBar(HashMap<Integer, float[]> ascents, ArrayList<BarEntry> dataPoints) {
        for (Map.Entry<Integer, float[]> entry : ascents.entrySet()) {
            Integer key = entry.getKey();
            float[] counts = entry.getValue();

            if (key == null) continue;

            BarEntry barEntry = new BarEntry(key, counts);
            dataPoints.add(barEntry);
        }
    }

    /**
     * Get the number of on-sights, flashes and red-points for each grade from a list of {@link OutdoorAscent}s.
     *
     * @param ascents - List of ascents
     * @return - Number of on-sights, flashes and red-points for each grade
     */
    private static HashMap<Integer, float[]> getOutdoorAscentsPerGrade(ArrayList<OutdoorAscent> ascents) {
        HashMap<Integer, float[]> ascentsPerGrade = new HashMap<>();

        for (OutdoorAscent ascent : ascents) {
            Integer gradeId = ascent.gradeId;
            int idx = -1;
            switch (ascent.method) {
                case OutdoorAscent.ON_SIGHT:
                    idx = 0;
                    break;
                case OutdoorAscent.FLASH:
                    idx = 1;
                    break;
                case OutdoorAscent.RED_POINT:
                    idx = 2;
                    break;
            }

            if (idx == -1) continue;

            float[] counts = ascentsPerGrade.get(gradeId);
            if (counts == null) counts = new float[]{0, 0, 0};

            counts[idx]++;
            ascentsPerGrade.put(gradeId, counts);
        }

        return ascentsPerGrade;
    }

    /**
     * Wrapper class for hangboard data.
     */
    public static class HangboardData {
        final public LineDataSet dataLeft, dataRight, dataRelStrength;

        /**
         * Create a new {@link  HangboardData} object.
         *
         * @param dataLeft        - Max strength data for left hand
         * @param dataRight       - Max strength data for right hand
         * @param dataRelStrength - Max strength relative to body weight
         */
        public HangboardData(LineDataSet dataLeft, LineDataSet dataRight, LineDataSet dataRelStrength) {
            this.dataLeft = dataLeft;
            this.dataRight = dataRight;
            this.dataRelStrength = dataRelStrength;
        }

        /**
         * Get the size of the data.
         *
         * @return - Size of datasets or -1 if sizes are not equal
         */
        public int size() {
            if (this.dataLeft == null || this.dataRight == null || this.dataRelStrength == null) return -1;
            int sizeLeft = this.dataLeft.getEntryCount();
            int sizeRight = this.dataRight.getEntryCount();
            int sizeRelStrength = this.dataRelStrength.getEntryCount();

            if (sizeLeft == sizeRight && sizeLeft == sizeRelStrength) {
                return sizeLeft;
            }

            return -1;
        }
    }

    /**
     * Wrapper class to pass data
     */
    public static class IndoorDataWrapper {
        protected ArrayList<BarEntry> boulders;
        protected ArrayList<BarEntry> routes;

        /**
         * @param boulders - List of boulders
         * @param routes   - List of routes
         */
        public IndoorDataWrapper(ArrayList<BarEntry> boulders, ArrayList<BarEntry> routes) {
            this.boulders = boulders;
            this.routes = routes;
        }
    }
}
