package caa.climbing.app.Screens;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import androidx.preference.PreferenceManager;
import androidx.fragment.app.FragmentManager;
import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.SqliteExporter;
import caa.climbing.app.Dialogs.ConfirmDialog.ConfirmDialog;
import caa.climbing.app.Dialogs.InformDialog.InformDialog;
import caa.climbing.app.HelperClasses.LocaleHelper;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;

/**
 * A Fragment to display selectable option that are stored as global settings.
 */
public class Settings extends Fragment {

    final public static String KEY_BOULDER_GRADE = "boulder_grade";
    final public static String KEY_ROUTE_GRADE = "route_grade";
    final public static String KEY_LANGUAGE = "language";

    RadioGroup boulderGradesGroup, routeGradesGroup, languageGroup;
    FlatButton saveSettingsBtn, exportDatabaseButton;

    private Context context;

    /**
     * Creates an Instance of a {@link Settings}-Fragment.
     */
    public Settings() {
        super(R.layout.settings_screen);
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param rootView           - Inflated View
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View rootView, Bundle savedInstanceState) {
        this.context = getActivity();

        this.initViews(rootView);
        this.addEventListener();
    }

    /**
     * Initializes the views inside the Fragment
     *
     * @param rootView - Root view that contains the rest of the views
     */
    private void initViews(View rootView) {
        this.boulderGradesGroup = rootView.findViewById(R.id.grade_boulder);
        this.routeGradesGroup = rootView.findViewById(R.id.grade_route);
        this.languageGroup = rootView.findViewById(R.id.language_setting);
        this.saveSettingsBtn = rootView.findViewById(R.id.save_settings_btn);
        this.exportDatabaseButton = rootView.findViewById(R.id.export_db_button);

        Resources resources = getResources();
        String[] languageOptions = resources.getStringArray(R.array.languages);
        this.createRadioButtons(languageOptions, languageGroup, getDefaultLanguageCode(this.context), LocaleHelper.getLanguageCodes(languageOptions));
        this.createRadioButtons(resources.getStringArray(R.array.boulder_grades), boulderGradesGroup, getGradeSystem(this.context, KEY_BOULDER_GRADE), DatabaseHelper.getGradeSystemList(this.context, true));
        this.createRadioButtons(resources.getStringArray(R.array.route_grades), routeGradesGroup, getGradeSystem(this.context, KEY_ROUTE_GRADE), DatabaseHelper.getGradeSystemList(this.context, false));
    }

    /**
     * Add listeners to the interactive elements.
     */
    private void addEventListener() {
        this.saveSettingsBtn.setOnClickListener((view) -> saveSettings());

        this.exportDatabaseButton.setOnClickListener((view) -> {
            SqliteExporter sqliteExporter = new SqliteExporter(getActivity());
            View.OnClickListener clickListener = (v) -> sqliteExporter.export();
            ConfirmDialog dialog = new ConfirmDialog(getString(R.string.sqlite_exporter_choose_folder), clickListener, null);

            FragmentManager fragmentManager = getChildFragmentManager();

            dialog.show(fragmentManager, "confirm");
        });
    }

    /**
     * Retrieves the preferred grade system.
     *
     * @param context - Context of the Fragment
     * @param key     - Identifier to specify which type (route/boulder) of grade system to get
     * @return - Preferred grade system
     */
    public static String getGradeSystem(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String defaultGrade;
        switch (key) {
            case KEY_BOULDER_GRADE:
                defaultGrade = context.getResources().getString(R.string.default_boulder_grade);
                break;
            case KEY_ROUTE_GRADE:
                defaultGrade = context.getResources().getString(R.string.default_route_grade);
                break;
            default:
                defaultGrade = "";
        }

        return prefs.getString(key, defaultGrade);

    }

    /**
     * Retrieves the code of the preferred language.
     *
     * @param context - Context of the Fragment
     * @return - Preferred language code
     */
    static public String getDefaultLanguageCode(Context context) {
        if (context == null) return "";

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String defaultLanguageCode = context.getString(R.string.default_language_code);

        return prefs.getString(KEY_LANGUAGE, defaultLanguageCode);
    }

    /**
     * Saves the preference for a specific setting.
     *
     * @param option - Identifier of the grading system
     * @param key    - Key of the preference
     */
    private void savePreference(String option, String key) {
        FragmentActivity activity = this.getActivity();
        if (activity == null) return;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, option);
        editor.apply();
    }

    /**
     * Retrieves the selected preferences from the selected options and saves them.
     */
    private void saveSettings() {
        String oldLanguageCode = getDefaultLanguageCode(this.context);

        saveOptionAsPreference(routeGradesGroup, KEY_ROUTE_GRADE);
        saveOptionAsPreference(boulderGradesGroup, KEY_BOULDER_GRADE);
        saveOptionAsPreference(languageGroup, KEY_LANGUAGE);

        InformDialog.CloseCallBack closeCallBack = () -> LocaleHelper.updateAppLanguage(getActivity(), oldLanguageCode);

        InformDialog.openDialog(getParentFragmentManager(), getString(R.string.settings_info_saving), closeCallBack);
    }

    /**
     * Get the checked option from a group of radio buttons and save that option as the preference.
     *
     * @param options - RadioGroup to check
     * @param key     - Key under which to save the preference
     */
    private void saveOptionAsPreference(RadioGroup options, String key) {
        int numberOfOptions = options.getChildCount();

        for (int i = 0; i < numberOfOptions; i++) {
            RadioButton option = (RadioButton) options.getChildAt(i);
            if (option.isChecked()) {
                savePreference(option.getTag().toString(), key);
                break;
            }
        }
    }

    /**
     * Populates a RadioGroup dynamically with RadioButtons
     *
     * @param options       - Selectable options in the RadioGroup
     * @param radioGroup    - RadioGroup to add the RadioButtons to
     * @param defaultOption - Option that is selected per default
     * @param tags          - Tags for the options
     */
    private void createRadioButtons(String[] options, RadioGroup radioGroup, String defaultOption, @Nullable String[] tags) {
        if (tags == null || tags.length != options.length) tags = options;

        float scale = getResources().getDisplayMetrics().density;
        float textSize_sp = getResources().getDimension(R.dimen.fontsize_standard);
        float textSize_px = textSize_sp / scale;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1
        );

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_enabled}, // disabled
                        new int[]{android.R.attr.state_enabled}, // enabled
                },
                new int[]{
                        ResourcesCompat.getColor(getResources(), R.color.black, null), // disabled
                        ResourcesCompat.getColor(getResources(), R.color.white, null) // enabled
                }
        );

        radioGroup.removeAllViewsInLayout();

        int optionsCount = options.length;
        for (int i = 0; i < optionsCount; i++) {
            RadioButton radioButton = new RadioButton(this.context);
            radioButton.setText(options[i]);
            radioButton.setTag(tags[i]);

            radioButton.setLayoutParams(params);
            radioButton.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
            radioButton.setTextSize(textSize_px);
            radioButton.setButtonTintList(colorStateList);

            radioGroup.addView(radioButton);
        }

        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            final String tag = tags[i];
            ((RadioButton) radioGroup.getChildAt(i)).setChecked(tag.equals(defaultOption));
        }
    }

}
