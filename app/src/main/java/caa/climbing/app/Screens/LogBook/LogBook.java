package caa.climbing.app.Screens.LogBook;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import caa.climbing.app.R;
import caa.climbing.app.Views.TabBar.TabBar;
import caa.climbing.app.Views.TabBar.TabConfig;

/**
 * Class to display saved sessions.
 */
public class LogBook extends Fragment {

    static final public int INDEX_OUTDOOR = 0;
    static final public int INDEX_INDOOR = 1;
    static final public int INDEX_HANGBOARD = 2;

    private int activeTabIndex;

    /**
     * Creates a new {@link LogBook} Fragment.
     */
    public LogBook() {
        super(R.layout.logbook_screen);
        this.activeTabIndex = 0;
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param view               - Inflated View
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        TabBar tabBar = view.findViewById(R.id.logbook_tabs);
        tabBar.createTabs(getChildFragmentManager(), getLifecycle(), createTabConfig());
        tabBar.setActiveTab(this.activeTabIndex);
    }

    /**
     * Generates a list of configuration objects to create the tabs from.
     */
    private ArrayList<TabConfig> createTabConfig() {
        ArrayList<TabConfig> config = new ArrayList<>();
        LogBookOutdoor outdoorTabContent = new LogBookOutdoor();
        LogBookIndoor indoorTabContent = new LogBookIndoor();
        LogBookHangBoard hangBoardTabContent = new LogBookHangBoard();

        config.add(new TabConfig(getString(R.string.logbook_tab_outdoor), outdoorTabContent));
        config.add(new TabConfig(getString(R.string.logbook_tab_indoor), indoorTabContent));
        config.add(new TabConfig(getString(R.string.logbook_tab_hangboard), hangBoardTabContent));

        return config;
    }

    /**
     * Set the index of the tab for which the information in the logbook should be displayed.
     *
     * @param index - Index of the tab to activate
     */
    public void setActiveTab(int index) {
        this.activeTabIndex = index;
    }
}
