package caa.climbing.app.Screens.LogBook;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.*;

import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import caa.climbing.app.Adapters.RecyclerAdapter_AscentInputOutdoor;
import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.Database.DatabaseModels.OutdoorAscent;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import caa.climbing.app.Views.TableHeader;

/**
 * A Fragment to display outdoor ascents as content of a tab bar.
 */
public class LogBookOutdoor extends Fragment {

    private RecyclerView inputContainer;

    private TableHeader tableHeaderBoulders;
    private TableHeader tableHeaderRoutes;

    private TextView noResultsElement;

    private ArrayList<OutdoorAscent> outdoorAscentsBoulder, outdoorAscentsRoutes;
    private ArrayList<LabelledInt> gradesBoulder, gradesRoutes;
    private ArrayList<LabelledInt> methods;

    private int[] defaultTableHeaderPadding;
    private boolean displayBoulders;

    /**
     * Creates a new instance of a {@link LogBookOutdoor} fragment.
     */
    public LogBookOutdoor() {
    }

    /**
     * Inflates the view
     *
     * @param inflater           - Inflater to inflate the view
     * @param container          - View that contains the view
     * @param savedInstanceState - Previous Instance
     * @return - The inflated view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.logbook_outdoor, container, false);
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param view               - Inflated View
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.displayBoulders = true;
        this.initViews(view);
        this.setInputContainerDisplay();
        this.loadData();
        this.setData();
        this.setActiveTableHeader();
        this.setTableHeaderTitles();
        this.addEventListener(view);
    }

    /**
     * Initializes the views inside the Fragment
     *
     * @param rootView - Root view that contains the rest of the views
     */
    private void initViews(View rootView) {
        this.inputContainer = rootView.findViewById(R.id.ascents_outdoor);
        this.noResultsElement = rootView.findViewById(R.id.no_ascents_element);

        this.tableHeaderBoulders = rootView.findViewById(R.id.tableHeader_boulder);
        this.tableHeaderRoutes = rootView.findViewById(R.id.tableHeader_routes);

        this.defaultTableHeaderPadding = this.getDefaultTableHeaderPadding();
    }

    /**
     * Get the default padding of a table header.
     *
     * @return - Default padding of the table header
     */
    private int[] getDefaultTableHeaderPadding() {
        int top = tableHeaderBoulders.getPaddingTop();
        int right = tableHeaderBoulders.getPaddingRight();
        int left = tableHeaderBoulders.getPaddingLeft();
        int bottom = tableHeaderBoulders.getPaddingBottom();

        return new int[]{left, top, right, bottom};
    }

    /**
     * Initialize the display parameters of the input container in order to display the ascents in a list.
     */
    private void setInputContainerDisplay() {
        Context context = getContext();

        if (context == null) return;
        DividerItemDecoration itemDecoration = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);

        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.divider, null);
        if (drawable != null) {
            itemDecoration.setDrawable(drawable);
            this.inputContainer.addItemDecoration(itemDecoration);
        }

        this.inputContainer.setOutlineProvider(ViewOutlineProvider.BACKGROUND);
        this.inputContainer.setClipToOutline(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        this.inputContainer.setLayoutManager(layoutManager);
        this.inputContainer.setNestedScrollingEnabled(true);
        this.inputContainer.setHasFixedSize(false);
    }

    /**
     * Loads the logged sessions from the database and displays them.
     */
    private void loadData() {
        Context context = this.getContext();
        if (context == null) return;

        DatabaseHelper dbHelper = DatabaseHelper.getInstance(context);

        this.gradesBoulder = dbHelper.getGrades(true, dbHelper.getReadableDatabase());
        this.gradesRoutes = dbHelper.getGrades(false, dbHelper.getReadableDatabase());
        this.methods = OutdoorAscent.getMethods(context);

        this.outdoorAscentsBoulder = dbHelper.getOutdoorSessionsBoulder(dbHelper.getReadableDatabase(), getContext());

        this.outdoorAscentsRoutes = dbHelper.getOutdoorSessionsRoutes(dbHelper.getReadableDatabase(), getContext());
    }

    /**
     * Set the titles of the table headers so that they show which ascents are shown (routes or boulders)
     * as well as how many ascents are displayed.
     */
    private void setTableHeaderTitles() {
        int numberOfBoulders = this.outdoorAscentsBoulder.size();
        int numberOfRoutes = this.outdoorAscentsRoutes.size();

        this.tableHeaderBoulders.setText(getString(R.string.logbook_outdoor_table_header_boulders, numberOfBoulders));
        this.tableHeaderRoutes.setText(getString(R.string.logbook_outdoor_table_header_routes, numberOfRoutes));

        float smallFontSize = Utils.sp2px(getContext(), getResources().getDimension(R.dimen.fontsize_small));
        this.tableHeaderRoutes.setTextSize(smallFontSize);
        this.tableHeaderBoulders.setTextSize(smallFontSize);
    }

    /**
     * Update the display of the table headers in order to reflect which one is currently active.
     */
    private void setActiveTableHeader() {
        int smallPaddingTop = this.defaultTableHeaderPadding[1];
        int smallPaddingBottom = (int) (this.defaultTableHeaderPadding[3] * 0.5);
        int paddingLeft = this.defaultTableHeaderPadding[0];
        int paddingTop = this.defaultTableHeaderPadding[1];
        int paddingRight = this.defaultTableHeaderPadding[2];
        int paddingBottom = this.defaultTableHeaderPadding[3];

        if (this.displayBoulders) {
            this.tableHeaderBoulders.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
            this.tableHeaderRoutes.setPadding(paddingLeft, smallPaddingTop, paddingRight, smallPaddingBottom);
        } else {
            this.tableHeaderRoutes.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
            this.tableHeaderBoulders.setPadding(paddingLeft, smallPaddingTop, paddingRight, smallPaddingBottom);
        }
    }

    /**
     * Display the loaded data.
     */
    private void setData() {
        ArrayList<OutdoorAscent> ascents = this.displayBoulders ? this.outdoorAscentsBoulder : this.outdoorAscentsRoutes;
        ArrayList<LabelledInt> grades = this.displayBoulders ? this.gradesBoulder : this.gradesRoutes;

        if (ascents.size() > 0) {
            RecyclerAdapter_AscentInputOutdoor adapter = new RecyclerAdapter_AscentInputOutdoor(getContext(), this.displayBoulders, ascents, grades, this.methods, true, true);

            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    super.onChanged();
                    onAdapterDataChanged();
                }
            });

            this.inputContainer.setAdapter(adapter);
            this.inputContainer.setVisibility(View.VISIBLE);
            this.noResultsElement.setVisibility(View.GONE);
        } else {
            this.inputContainer.setVisibility(View.GONE);
            this.noResultsElement.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Update the display when the data of the input container changes.
     */
    private void onAdapterDataChanged() {
        RecyclerAdapter_AscentInputOutdoor adapter = (RecyclerAdapter_AscentInputOutdoor) this.inputContainer.getAdapter();

        if (adapter == null) return;

        if (adapter.getItemCount() == 0) {
            this.inputContainer.setVisibility(View.GONE);
            this.noResultsElement.setVisibility(View.VISIBLE);
        }

        if (this.displayBoulders) {
            this.outdoorAscentsBoulder = adapter.getData();
        } else {
            this.outdoorAscentsRoutes = adapter.getData();
        }

        this.setTableHeaderTitles();
    }

    /**
     * Set the input of the input container view, so that it fills the screen.
     */
    private void setInputContainerHeight() {
        ViewGroup.LayoutParams layoutParams = inputContainer.getLayoutParams();
        layoutParams.height = Utils.getDistanceToBottomScreen(tableHeaderBoulders, getActivity());
        this.inputContainer.setLayoutParams(layoutParams);
    }

    /**
     * Set Listener on the table headers to toggle between the display of boulder and route ascents.
     * Additionally, set a LayoutListener on the root view, so that the height of the input container
     * can be calculated when the root view has been drawn.
     */
    private void addEventListener(View rootView) {
        this.tableHeaderBoulders.setOnClickListener((view) -> {
            this.displayBoulders = true;
            this.setData();
            this.setActiveTableHeader();
        });

        this.tableHeaderRoutes.setOnClickListener((view) -> {
            this.displayBoulders = false;
            this.setData();
            this.setActiveTableHeader();
        });

        ViewTreeObserver observer = rootView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                setInputContainerHeight();
            }
        });
    }
}
