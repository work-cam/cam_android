package caa.climbing.app.Screens;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Date;

import caa.climbing.app.Activities.MainActivity;
import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.Dialogs.InformDialog.InformDialog;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;
import caa.climbing.app.Views.Inputs.DatePicker.DatePicker;
import caa.climbing.app.Views.Inputs.LabelInput.LabelInput;
import caa.climbing.app.Views.IndoorSessionTable.IndoorSessionTable;

/**
 * A Fragment to input ascents for an indoor session.
 */
public class IndoorSession extends Fragment {

    private DatePicker datePicker;
    private LabelInput gymInput;
    private FlatButton saveSessionBtn;
    private LinearLayout container;

    private IndoorSessionTable boulderTable;
    private IndoorSessionTable routeTable;

    private DatabaseHelper dbHelper;

    /**
     * Creates an instance of an {@link IndoorSession}-Fragment.
     */
    public IndoorSession() {
        super(R.layout.indoorsession_screen);
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param view               - Inflated View
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        this.datePicker = view.findViewById(R.id.indoor_session_datepicker);
        this.gymInput = view.findViewById(R.id.selectGym);
        this.container = view.findViewById(R.id.indoorTableContainer);
        this.saveSessionBtn = view.findViewById(R.id.save_btn_indoorsession);

        this.dbHelper = DatabaseHelper.getInstance(getContext());

        this.loadData();
        this.loadGymSuggestions();
        this.addEventListener();
    }

    /**
     * Load the data on the bracketing for the grades.
     * After the data is loaded, the tables are updated accordingly.
     */
    private void loadData() {
        Context context = this.getContext();

        ArrayList<LabelledInt> boulderBracketsLabelled = this.dbHelper.getGradeBrackets(true, this.dbHelper.getReadableDatabase());
        ArrayList<LabelledInt> routeBracketsLabelled = this.dbHelper.getGradeBrackets(false, this.dbHelper.getReadableDatabase());

        this.boulderTable = new IndoorSessionTable(context, getString(R.string.general_climb_type_boulders), boulderBracketsLabelled, false);
        this.routeTable = new IndoorSessionTable(context, getString(R.string.general_climb_type_routes), routeBracketsLabelled, true);

        this.container.addView(this.boulderTable);
        this.container.addView(this.routeTable);
    }

    /**
     * Load a list of gyms where indoor sessions were previously completed in order
     * to display them as suggestions in the gym input.
     */
    private void loadGymSuggestions() {
        String[] gymSuggestions = this.dbHelper.getGyms(this.dbHelper.getReadableDatabase());
        this.gymInput.setSuggestions(gymSuggestions);
    }

    /**
     * Mark all the inputs as valid that can be validated.
     */
    private void markAllInputsValid() {
        this.datePicker.markAsValid();
        this.gymInput.markAsValid();

        this.boulderTable.markAllAsValid();
        this.routeTable.markAllAsValid();
    }

    /**
     * Check if all inputs are valid.
     *
     * @return - True if all inputs are valid, false otherwise
     */
    private boolean validateInputs() {
        markAllInputsValid();

        if (gymInput.getValue().equals("")) {
            gymInput.markAsInvalid();
            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.indoor_session_info_gym_required));
            return false;
        }
        if (datePicker.getValue() == null) {
            datePicker.markAsInvalid();
            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.indoor_session_info_date_required));
            return false;
        }

        if (boulderTable.allEmpty() && routeTable.allEmpty()) {
            boulderTable.markAllAsInvalid();
            routeTable.markAllAsInvalid();
            boulderTable.focusFirst();

            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.indoor_session_info_nothing_to_save));
            return false;
        }

        return true;
    }

    /**
     * Add listeners to the interactive elements.
     */
    private void addEventListener() {
        saveSessionBtn.setOnClickListener((view) -> this.saveSession());
    }

    /**
     * Save the current session to the database.
     */
    private void saveSession() {
        Date date = datePicker.getValue();
        String gym = gymInput.getValue();

        ArrayList<LabelledInt> boulderGrades = boulderTable.getNumberOfAscents();
        ArrayList<LabelledInt> routeGrades = routeTable.getNumberOfAscents();


        // check that required fields are not empty
        if (!validateInputs()) return;

        caa.climbing.app.Database.DatabaseModels.IndoorSession ascents;
        ascents = new caa.climbing.app.Database.DatabaseModels.IndoorSession(gym, date, boulderGrades, routeGrades);

        boolean success = dbHelper.saveIndoorSession(ascents, dbHelper.getWritableDatabase());

        if (success) {
            gymInput.clearText();
            boulderTable.emptyInputs();
            routeTable.emptyInputs();
            this.loadGymSuggestions();

            MainActivity parentActivity = (MainActivity) getActivity();
            if (parentActivity != null) parentActivity.scrollToTop();

            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.indoor_session_info_save_successful));

        }
    }
}
