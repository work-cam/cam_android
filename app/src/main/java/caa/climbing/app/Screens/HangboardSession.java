package caa.climbing.app.Screens;

import android.os.Bundle;
import android.view.View;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.*;

import caa.climbing.app.R;
import caa.climbing.app.Views.Hangboard.HangboardForm;
import caa.climbing.app.Views.Hangboard.HangboardTimer;

/**
 * A Fragment to display the necessary inputs to start a fragment and a timer to time the session.
 */
public class HangboardSession extends Fragment {
    private final HangboardTimer hangboardTimer;
    private HangboardForm hangboardForm;

    /**
     * Create a new {@link HangboardSession} instance.
     */
    public HangboardSession() {
        super(R.layout.hangboardsession_screen);

        this.hangboardTimer = new HangboardTimer();
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param rootView           - Inflated View
     * @param savedInstanceState - previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View rootView, Bundle savedInstanceState) {

        if (savedInstanceState == null) {

            this.hangboardForm = new HangboardForm();

            this.showActiveView(this.hangboardForm);

            // add listeners
            this.hangboardForm.setStartSessionListener((view) -> openTimer());

            this.hangboardTimer.setFinishSessionListener((view) -> closeTimer());

            this.setTimerBackPress();
        }
    }

    /**
     * Add a backPress callback to the hangboard timer in order to return to the hangboard form instead of closing the app.
     */
    private void setTimerBackPress() {
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                closeTimer();
            }
        };
        this.hangboardTimer.setOnBackPress(callback);
    }

    /**
     * Change the active view.
     *
     * @param activeView - View to change to
     */
    private void showActiveView(Fragment activeView) {
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        if (activeView == null) {
            // Show an empty screen
            for (Fragment fragment : fragmentManager.getFragments()) {
                transaction.remove(fragment);
            }
        } else {
            // replace the current screen with the active screen
            transaction
                    .replace(R.id.fragment_container_view, activeView, null)
                    .setReorderingAllowed(true);
        }

        transaction.commit();
    }

    /**
     * Open the timer to start the session.
     */
    private void openTimer() {
        if (this.hangboardForm.checkTimerArguments()) {
            Bundle args = this.hangboardForm.getTimerArguments();
            this.hangboardTimer.setArguments(args);

            showActiveView(this.hangboardTimer);
        }
    }

    /**
     * Close the timer and return to the input form.
     */
    private void closeTimer() {
        this.hangboardTimer.finish();
        showActiveView(this.hangboardForm);
    }
}
