package caa.climbing.app.Screens.Home;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import caa.climbing.app.Activities.MainActivity;
import caa.climbing.app.Database.DatabaseModels.OutdoorAscent;
import caa.climbing.app.Screens.LogBook.LogBook;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.DatabaseModels.HangboardSession;
import caa.climbing.app.Database.DatabaseModels.IndoorSession;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.HelperClasses.DateFormatter;
import caa.climbing.app.R;
import caa.climbing.app.Views.SessionStatistics.SessionStatistics;

/**
 * A Fragment to display an overview of finished sessions in the form of charts.
 */
public class Home extends Fragment {
    private final static ValueFormatter dateFormatterLong = new ValueFormatter() {
        @Override
        public String getAxisLabel(float value, AxisBase axis) {
            return DateFormatter.DaysToDate((int) value, false);
        }
    };
    private final static ValueFormatter dateFormatterShort = new ValueFormatter() {
        @Override
        public String getAxisLabel(float value, AxisBase axis) {
            return DateFormatter.DaysToDate((int) value, true);
        }
    };
    private final int initialRangeDays = 90;
    private DatabaseHelper dbHelper;
    private LinearLayout container;
    private SessionStatistics hangboardStatistics;

    /**
     * Creates an instance of a {@link Home}-Fragment.
     */
    public Home() {
        super(R.layout.home_screen);
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param view               - Inflated View
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        this.dbHelper = DatabaseHelper.getInstance(this.getContext());
        this.container = view.findViewById(R.id.home_screen_container);

        this.setIndoorSessions();
        this.setOutdoorSessions();
        this.setHangboardSessions();
    }

    /**
     * Creates the charts for the completed indoor sessions.
     */
    private void setIndoorSessions() {
        SessionStatistics stats = new SessionStatistics(this.getContext(), null);

        int sessionCount = dbHelper.getIndoorSessionCount(dbHelper.getReadableDatabase());

        int colorTop = ResourcesCompat.getColor(getResources(), R.color.blue400, null);
        int colorBottom = ResourcesCompat.getColor(getResources(), R.color.blue100, null);
        stats.setSessionInfo(getString(R.string.indoor_statistics_title), sessionCount, colorTop, colorBottom);
        setIndoorDataDisplay(stats, false);
        this.openLogbook(stats, LogBook.INDEX_INDOOR);

        String checkboxLabel = getString(R.string.indoor_cumulative_display);
        stats.setCheckbox(checkboxLabel, (button, isChecked) -> {
            for (int i = stats.container.getChildCount() - 1; i >= 2; i--) {
                stats.container.removeViewAt(i);
            }
            setIndoorDataDisplay(stats, isChecked);
        });

        this.container.addView(stats);
    }

    /**
     * Loads the data about outdoor sessions from the database and displays the data.
     *
     * @param stats      - Statistics to add the charts to
     * @param cumulative - Whether to display a cumulative BarChart
     */
    private void setIndoorDataDisplay(SessionStatistics stats, boolean cumulative) {
        ArrayList<IndoorSession> indoorSessions = dbHelper.getIndoorSessions(dbHelper.getReadableDatabase(), getContext());

        DataConverter.IndoorDataWrapper indoorData = DataConverter.indoorToBar(indoorSessions, cumulative);

        ArrayList<BarEntry> boulders = indoorData.boulders;
        ArrayList<BarEntry> routes = indoorData.routes;

        if (boulders != null) {
            this.setIndoorBarCharts(true, stats, boulders, cumulative);
        }
        if (routes != null) {
            this.setIndoorBarCharts(false, stats, routes, cumulative);
        }
    }

    /**
     * Add BarCharts with indoor data to the screen.
     *
     * @param boulder    - Whether to display data for boulders or routes
     * @param stats      - View to add the chart to
     * @param data       - Data to display
     * @param cumulative - Whether to display the data cumulated
     */
    private void setIndoorBarCharts(boolean boulder, @NonNull SessionStatistics stats, @NonNull ArrayList<BarEntry> data, boolean cumulative) {
        ArrayList<LabelledInt> gradeBrackets = this.dbHelper.getGradeBrackets(boulder, this.dbHelper.getReadableDatabase());

        ValueFormatter gradeFormatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                int index = (int) value;
                if (index < 0 || index > gradeBrackets.size() - 1) return "";
                return gradeBrackets.get(index).label;
            }
        };

        String title = getString(boulder ? R.string.general_climb_type_boulders : R.string.general_climb_type_routes);
        int initialRangeX = cumulative ? gradeBrackets.size() : this.initialRangeDays;
        float maxX = cumulative ? gradeBrackets.size() - 0.5f : this.getNow();
        ValueFormatter axisFormatter = cumulative ? gradeFormatter : Home.dateFormatterLong;
        stats.setBarChart(data, title, LabelledInt.getLabels(gradeBrackets), false, initialRangeX, maxX, axisFormatter, gradeFormatter);
    }

    /**
     * Get the current time in days.
     *
     * @return - Current time in days
     */
    private float getNow() {
        long now = DateFormatter.DateToDays(new Date());
        return now + 0.5f;
    }

    /**
     * Creates the charts for the completed outdoor sessions.
     */
    private void setOutdoorSessions() {
        SessionStatistics stats = new SessionStatistics(this.getContext(), null);

        int sessionCount = dbHelper.getOutdoorSessionCount(dbHelper.getReadableDatabase());
        int colorTop = ResourcesCompat.getColor(getResources(), R.color.blue300, null);
        int colorBottom = ResourcesCompat.getColor(getResources(), R.color.blue200, null);

        stats.setSessionInfo(getString(R.string.outdoor_statistics_title), sessionCount, colorTop, colorBottom);
        this.setOutDoorDataDisplay(stats, true, true);
        this.setOutDoorDataDisplay(stats, false, false);

        this.setOutdoorScoreDisplay(stats);

        this.openLogbook(stats, LogBook.INDEX_OUTDOOR);

        this.container.addView(stats);
    }

    /**
     * Loads the data about outdoor sessions from the database and displays the data.
     *
     * @param stats                - Statistics to add the charts to
     * @param boulder              - Whether the data is for boulders or routes
     * @param removePreviousCharts - Whether to remove previous charts
     */
    private void setOutDoorDataDisplay(SessionStatistics stats, boolean boulder, boolean removePreviousCharts) {
        String title = getString(boulder ? R.string.general_climb_type_boulders : R.string.general_climb_type_routes);

        ArrayList<LabelledInt> grades = dbHelper.getGrades(boulder, dbHelper.getReadableDatabase());
        ArrayList<OutdoorAscent> ascents = dbHelper.getOutdoorSessions(dbHelper.getReadableDatabase(), boulder, getContext());

        ArrayList<BarEntry> barData = DataConverter.outdoorToBar(ascents);

        ValueFormatter formatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                LabelledInt closestGrade = dbHelper.getClosestGrade(grades, (int) value);

                return closestGrade == null ? "" : closestGrade.label;
            }
        };

        ArrayList<String> methods = OutdoorAscent.getMethodStrings(this.getContext());

        stats.setBarChart(barData, title, methods, removePreviousCharts, -1, -1, formatter, formatter);
    }

    /**
     * Add a graph that displays the cumulated score for all outdoor ascents.
     *
     * @param stats - Statistics to add the charts to
     */
    private void setOutdoorScoreDisplay(SessionStatistics stats) {
        ArrayList<OutdoorAscent> boulders = dbHelper.getOutdoorSessions(dbHelper.getReadableDatabase(), true, false, getContext());
        ArrayList<OutdoorAscent> routes = dbHelper.getOutdoorSessions(dbHelper.getReadableDatabase(), false, false, getContext());

        ArrayList<LineDataSet> outdoorScores = new ArrayList<>();
        outdoorScores.add(DataConverter.outdoorToLine(boulders));
        outdoorScores.add(DataConverter.outdoorToLine(routes));

        ArrayList<String> categories = new ArrayList<>();
        categories.add(getString(R.string.general_climb_type_boulders));
        categories.add(getString(R.string.general_climb_type_routes));

        stats.setLineChart(outdoorScores, getString(R.string.outdoor_chart_title_outdoor_score), categories, false, this.initialRangeDays, this.getNow(), Home.dateFormatterLong, Home.dateFormatterShort);
    }

    /**
     * Creates the charts for the completed hangboard sessions.
     */
    private void setHangboardSessions() {
        String[] testEdgeSizes = dbHelper.getHangboardTestEdgeSizes(dbHelper.getReadableDatabase());
        Integer lastTestEdgeSize = dbHelper.getLastHangboardTestEdgeSize(dbHelper.getReadableDatabase());
        this.hangboardStatistics = new SessionStatistics(this.getContext(), null);

        if (testEdgeSizes == null || testEdgeSizes.length == 0 || lastTestEdgeSize == null) return;

        this.setHangboardDataDisplay(lastTestEdgeSize, testEdgeSizes, true);

        this.openLogbook(this.hangboardStatistics, LogBook.INDEX_HANGBOARD);
    }

    /**
     * Loads the data about hangboard sessions from the database and displays the data.
     *
     * @param lastTestEdgeSize - Last edge size for max strength tests
     * @param testEdgeSizes    - All previously recorded test edge sizes for max strength tests
     * @param addToView        - Whether to add the view initially or update the displays
     */
    private void setHangboardDataDisplay(int lastTestEdgeSize, String[] testEdgeSizes, boolean addToView) {
        ArrayList<HangboardSession> hangboardSessions = dbHelper.getHangboardSession(dbHelper.getReadableDatabase(), lastTestEdgeSize);

        DataConverter.HangboardData hangboardData = DataConverter.hangBoardToLine(hangboardSessions);

        if (!addToView) this.hangboardStatistics.clear();

        ArrayList<LineDataSet> dataLR = new ArrayList<>(Arrays.asList(hangboardData.dataLeft, hangboardData.dataRight));
        ArrayList<LineDataSet> dataRS = new ArrayList<>(Collections.singletonList(hangboardData.dataRelStrength));
        ArrayList<String> categories = new ArrayList<>();
        categories.add(getString(R.string.hangboard_chart_strength_left));
        categories.add(getString(R.string.hangboard_chart_strength_right));

        int colorTop = ResourcesCompat.getColor(getResources(), R.color.blue600, null);
        int colorBottom = ResourcesCompat.getColor(getResources(), R.color.blue500, null);

        this.hangboardStatistics.setSessionInfo(getString(R.string.hangboard_statistics_title), hangboardData.size(), colorTop, colorBottom);
        if (hangboardData.size() > 0) {
            float now = this.getNow();
            this.hangboardStatistics.setLineChart(dataLR, getString(R.string.hangboard_chart_title_max_strength, lastTestEdgeSize), categories, true, this.initialRangeDays, now, Home.dateFormatterLong, Home.dateFormatterShort);
            this.hangboardStatistics.setLineChart(dataRS, getString(R.string.hangboard_chart_title_relative_strength), new ArrayList<>(), false, this.initialRangeDays, now, Home.dateFormatterLong, Home.dateFormatterShort);
        }

        if (testEdgeSizes.length > 1) {
            this.addHangboardFilter(lastTestEdgeSize, testEdgeSizes);
        }

        if (addToView) this.container.addView(this.hangboardStatistics);
    }

    /**
     * Add a filter to the HangboardSession statistics to switch between sessions where the strength was tested on different edge sizes.
     *
     * @param lastTestEdgeSize - Last edge size used for max-strength testing
     * @param testEdgeSizes    - All previously used edge sizes for max-strength testing
     */
    private void addHangboardFilter(int lastTestEdgeSize, String[] testEdgeSizes) {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String editableString = s.toString();
                if (editableString == null) return;

                int newTestEdgeSize = Integer.parseInt(editableString);
                setHangboardDataDisplay(newTestEdgeSize, testEdgeSizes, false);
            }
        };

        String label = getString(R.string.hangboard_test_edge_size);
        this.hangboardStatistics.setSessionFilter(label, testEdgeSizes, Integer.toString(lastTestEdgeSize), textWatcher);
    }

    /**
     * Opens the logbook when the session info of the session statistics is clicked to reveal more details about the
     * respective sessions.
     *
     * @param stats     - Session statistics to set the click listener on
     * @param activeTab - Which tab of the logbook to open
     */
    private void openLogbook(SessionStatistics stats, int activeTab) {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null) return;

        stats.setSessionInfoClickListener((view) -> mainActivity.openLogBook(activeTab));
    }
}
