package caa.climbing.app.Screens;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;

import caa.climbing.app.Activities.MainActivity;
import caa.climbing.app.Adapters.RecyclerAdapter_AscentInputOutdoor;
import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.Database.DatabaseModels.OutdoorAscent;
import caa.climbing.app.Dialogs.InformDialog.InformDialog;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import caa.climbing.app.Views.AscentDisplayOutdoor.AscentInputOutdoor;
import caa.climbing.app.Views.Buttons.FlatButton;
import caa.climbing.app.Views.Buttons.IconButton;

/**
 * A Fragment to input ascents for an outdoor session.
 */
public class OutdoorSession extends Fragment {

    public ArrayList<OutdoorAscent> boulders = new ArrayList<>();
    public ArrayList<OutdoorAscent> routes = new ArrayList<>();
    IconButton addBoulderBtn, addRouteBtn;
    FlatButton saveSessionBtn;
    ArrayList<LabelledInt> boulderGrades, routeGrades;
    private RecyclerView boulderAscents, routeAscents;
    private ArrayList<LabelledInt> methods;

    /**
     * Creates an instance of an {@link OutdoorSession}-Fragment.
     */
    public OutdoorSession() {
        super(R.layout.outdoorsession_screen);
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param view               - Inflated View
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        this.initViews(view);
        this.loadData();

        this.setupRecyclerView(true);
        this.setupRecyclerView(false);

        this.addEventListener();
    }

    /**
     * Initializes the views.
     *
     * @param rootView - Root view that contains the rest of the views
     */
    private void initViews(View rootView) {
        this.saveSessionBtn = rootView.findViewById(R.id.save_btn_outdoorsession);

        this.boulderAscents = rootView.findViewById(R.id.ascents_boulder);
        this.addBoulderBtn = rootView.findViewById(R.id.add_boulder_btn);

        this.routeAscents = rootView.findViewById(R.id.ascents_routes);
        this.addRouteBtn = rootView.findViewById(R.id.add_route_btn);
    }

    /**
     * Retrieve possible grades from the database.
     */
    private void loadData() {
        Context context = this.getContext();
        if (context == null) return;

        DatabaseHelper dbHelper = DatabaseHelper.getInstance(context);
        this.boulderGrades = dbHelper.getGrades(true, dbHelper.getReadableDatabase());
        this.routeGrades = dbHelper.getGrades(false, dbHelper.getReadableDatabase());
        this.methods = OutdoorAscent.getMethods(context);
    }

    /**
     * Set up the RecyclerViews displaying the added ascents.
     *
     * @param boulder - Whether to set up the boulder or route view
     */
    private void setupRecyclerView(boolean boulder) {
        RecyclerView container = boulder ? this.boulderAscents : this.routeAscents;
        ArrayList<OutdoorAscent> ascents = boulder ? this.boulders : this.routes;
        ArrayList<LabelledInt> grades = boulder ? this.boulderGrades : this.routeGrades;

        RecyclerAdapter_AscentInputOutdoor adapter = new RecyclerAdapter_AscentInputOutdoor(getContext(), boulder, ascents, grades, this.methods, true);
        container.setLayoutManager(new LinearLayoutManager(getContext()));
        container.setNestedScrollingEnabled(true);
        container.setAdapter(adapter);
    }

    /**
     * Add listeners to the interactive elements.
     */
    private void addEventListener() {
        addBoulderBtn.setOnClickListener((view) -> this.addAscentButtonClick(true));

        addRouteBtn.setOnClickListener((view) -> this.addAscentButtonClick(false));

        saveSessionBtn.setOnClickListener((view) -> this.saveSession());
    }

    /**
     * Add a new ascent when the respective button is clicked.
     *
     * @param boulder - Whether to add the ascent to the boulder or route list
     */
    private void addAscentButtonClick(boolean boulder) {
        RecyclerView container = boulder ? this.boulderAscents : this.routeAscents;
        RecyclerAdapter_AscentInputOutdoor adapter = (RecyclerAdapter_AscentInputOutdoor) container.getAdapter();
        if (adapter == null) return;
        addAscent(adapter, boulder);
        adapter.notifyItemRangeChanged(0, adapter.getItemCount());
    }

    /**
     * Creates a new {@link OutdoorAscent} and adds it as an input to add a new ascent.
     */
    private void addAscent(RecyclerAdapter_AscentInputOutdoor adapter, boolean boulder) {
        if (adapter.getData() != null) {
            int ascentKey = boulder ? OutdoorAscent.BOULDER : OutdoorAscent.ROUTE;

            OutdoorAscent ascent = new OutdoorAscent(ascentKey);
            Calendar calendar = Calendar.getInstance();
            Utils.resetCalendar(calendar);

            ascent.date = calendar.getTime();
            adapter.setData(getAscents(boulder));
            adapter.addItem(ascent);
        }
    }

    /**
     * Get a list of all ascents.
     *
     * @param boulder - Whether to get boulder or route ascents
     * @return - List of ascents
     */
    private ArrayList<OutdoorAscent> getAscents(boolean boulder) {
        ArrayList<OutdoorAscent> ascents = new ArrayList<>();

        RecyclerAdapter_AscentInputOutdoor adapter = (RecyclerAdapter_AscentInputOutdoor) (boulder ? boulderAscents.getAdapter() : routeAscents.getAdapter());
        if (adapter == null) return ascents;

        RecyclerView ascentContainer = boulder ? boulderAscents : routeAscents;

        for (int i = 0; i < adapter.getItemCount(); i++) {
            RecyclerView.ViewHolder holder = ascentContainer.findViewHolderForAdapterPosition(i);

            if (holder == null) continue;

            OutdoorAscent ascent = ((AscentInputOutdoor) holder.itemView).getAscent();

            ascents.add(ascent);
        }

        return ascents;
    }

    /**
     * Saves the added ascents.
     */
    private void saveSession() {
        boulders = getAscents(true);
        routes = getAscents(false);
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this.getContext());
        boolean successBoulders = true;
        boolean successRoutes = true;
        if (boulders.size() == 0 && routes.size() == 0) {
            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.outdoor_session_info_nothing_to_save));
            return;
        }
        if (boulders.size() > 0) {
            for (OutdoorAscent ascent : boulders) {
                if (ascent.isInvalid()) successBoulders = false;
            }
            if (successBoulders) {
                successBoulders = dbHelper.saveOutdoorSession(boulders, dbHelper.getWritableDatabase());
            }
        }
        if (routes.size() > 0) {
            for (OutdoorAscent ascent : routes) {
                if (ascent.isInvalid()) successRoutes = false;
            }
            if (successRoutes) {
                successRoutes = dbHelper.saveOutdoorSession(routes, dbHelper.getWritableDatabase());
            }
        }

        if (successBoulders && successRoutes) {
            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.outdoor_session_info_save_successful));

            boulders = new ArrayList<>();
            routes = new ArrayList<>();
            this.setupRecyclerView(true);
            this.setupRecyclerView(false);

            MainActivity parentActivity = (MainActivity) getActivity();
            if (parentActivity != null) parentActivity.scrollToTop();
        } else {
            if (!successBoulders && !successRoutes) {
                InformDialog.openDialog(getParentFragmentManager(), getString(R.string.outdoor_session_info_save_failed));
            } else {
                if (!successBoulders) {
                    InformDialog.openDialog(getParentFragmentManager(), getString(R.string.outdoor_session_info_save_failed_boulders));
                }
                if (!successRoutes) {
                    InformDialog.openDialog(getParentFragmentManager(), getString(R.string.outdoor_session_info_save_failed_routes));
                }
            }

        }

    }
}
