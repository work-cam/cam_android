package caa.climbing.app.Adapters;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.DatabaseModels.HangboardSession;
import caa.climbing.app.Dialogs.ConfirmDialog.ConfirmDialog;
import caa.climbing.app.Dialogs.InformDialog.InformDialog;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import caa.climbing.app.Views.HangboardSessionDisplay;

/**
 * An adapter to display multiple {@link HangboardSessionDisplay} views inside a RecyclerView.
 */
public class RecyclerAdapter_HangBoardSessionDisplay extends RecyclerAdapter_Expandable<HangboardSession, HangboardSessionDisplay> {

    private final String updateFailedMessage, confirmDeleteTitle;

    /**
     * Creates a new {@link RecyclerAdapter_HangBoardSessionDisplay} object.
     *
     * @param context  - Context of the owning RecyclerView
     * @param sessions - Data based on which the views inside the RecyclerView are created
     */
    public RecyclerAdapter_HangBoardSessionDisplay(Context context, ArrayList<HangboardSession> sessions) {
        super(context, sessions, () -> new HangboardSessionDisplay(context, null));

        this.updateFailedMessage = this.context.getString(R.string.hangboard_update_failed);
        this.confirmDeleteTitle = this.context.getString(R.string.hangboard_confirm_delete);
    }

    /**
     * Get element from the dataset at given position.
     * Replace the contents of the view with that element.
     *
     * @param holder   - Holder of the element
     * @param position - Index of element in dataset
     */
    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapter_Expandable.ViewHolder<HangboardSessionDisplay> holder, int position) {
        super.onBindViewHolder(holder, position);

        final HangboardSession session = this.data.get(position);
        final int viewPosition = position;

        // set text values
        holder.itemView.setSession(session);

        holder.itemView.setUpdateAction((view) -> this.updateSession(holder, viewPosition));

        holder.itemView.setDeleteAction((view) -> this.openDeleteConfirmDialog(session.id, viewPosition));

        holder.itemView.setCancelAction((view) -> this.cancelEdit(holder, position));
    }

    /**
     * Delete a session from the database and if successful, remove the corresponding view from the RecyclerView.
     * The view is then updated to reflect that change.
     *
     * @param id       - Id of the session that should be deleted
     * @param position - Position of the corresponding view in the RecyclerView
     */
    private void deleteSession(int id, int position) {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this.context);
        boolean success = dbHelper.deleteHangboardSession(id, dbHelper.getReadableDatabase());

        if (success) {
            this.data.remove(position);
            this.expandedItems.remove(position);
            notifyItemRemoved(position);
        }
    }

    /**
     * Opens a confirmation dialog to confirm the deletion of a session.
     *
     * @param id       - Id of the session that should be deleted
     * @param position - Position of the corresponding view in the RecyclerView
     */
    private void openDeleteConfirmDialog(final int id, final int position) {
        View.OnClickListener okClick = (view) -> deleteSession(id, position);
        ConfirmDialog confirm = new ConfirmDialog(this.confirmDeleteTitle, okClick, null);

        FragmentManager fragmentManager = Utils.getActivity(context).getSupportFragmentManager();
        confirm.show(fragmentManager, "confirm");
    }

    /**
     * Update a session in the database. If the database operation is successful, the view is updated as well.
     *
     * @param holder   - ViewHolder that holds the inputs for the updated session
     * @param position - Position of the corresponding view in the RecyclerView
     */
    private void updateSession(RecyclerAdapter_Expandable.ViewHolder<HangboardSessionDisplay> holder, int position) {
        HangboardSession session = holder.itemView.getSession();

        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this.context);
        boolean success = dbHelper.updateHangboardSession(session, dbHelper.getReadableDatabase());

        if (success) {
            holder.itemView.setEditMode(false);
            this.data.set(position, session);
            notifyItemChanged(position);
        } else {
            InformDialog.openDialog(Utils.getActivity(context).getSupportFragmentManager(), this.updateFailedMessage);
        }
    }

    /**
     * Cancels the editing of the inputs and resets their values.
     *
     * @param holder   - ViewHolder of the view
     * @param position - Position of the view
     */
    private void cancelEdit(RecyclerAdapter_Expandable.ViewHolder<HangboardSessionDisplay> holder, int position) {
        HangboardSession backup = holder.itemView.getBackupSession();
        if (backup == null) return;

        holder.itemView.setEditMode(false);
        this.data.set(position, backup);
        notifyItemChanged(position);
    }
}
