package caa.climbing.app.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;

import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Dialogs.ConfirmDialog.ConfirmDialog;
import caa.climbing.app.Dialogs.InformDialog.InformDialog;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.Views.AscentDisplayOutdoor.AscentInputOutdoor;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.Database.DatabaseModels.OutdoorAscent;
import caa.climbing.app.R;

import java.util.ArrayList;

/**
 * An adapter to display multiple {@link AscentInputOutdoor} views inside a RecyclerView.
 */
public class RecyclerAdapter_AscentInputOutdoor extends RecyclerAdapter_Expandable<OutdoorAscent, AscentInputOutdoor> {
    private final Context context;
    boolean boulder;
    ArrayList<LabelledInt> grades, methods;
    private final FragmentManager fragmentManager;

    private final boolean databaseBacked;
    private final boolean smallViewPort;
    private final boolean hasBottomControl;
    private String[] cragSuggestions;

    private final String updateFailedMessage, confirmDeleteTitle, gradeSelectorText, methodSelectorText;

    /**
     * Creates a new {@link RecyclerAdapter_AscentInputOutdoor} object.
     *
     * @param context          - Context of the owning RecyclerView
     * @param boulder          - Whether the ascents are boulders or routes
     * @param ascents          - List of ascents
     * @param grades           - List of grades
     * @param methods          - List of possible methods of topping
     * @param databaseBacked   - Whether the data is already stored in the database
     * @param smallViewPort    - Whether the views are displayed in a small space
     * @param hasBottomControl - Whether a control element will be displayed below the RecyclerView
     */
    public RecyclerAdapter_AscentInputOutdoor(Context context, boolean boulder, ArrayList<OutdoorAscent> ascents, ArrayList<LabelledInt> grades, ArrayList<LabelledInt> methods, boolean databaseBacked, boolean smallViewPort, boolean hasBottomControl) {
        super(context, ascents, () -> new AscentInputOutdoor(context, null, boulder ? OutdoorAscent.BOULDER : OutdoorAscent.ROUTE, grades, methods));

        this.context = context;
        this.boulder = boulder;

        this.grades = grades;
        this.methods = methods;
        this.fragmentManager = Utils.getActivity(this.context).getSupportFragmentManager();
        this.databaseBacked = databaseBacked;
        this.smallViewPort = smallViewPort;
        this.hasBottomControl = hasBottomControl;
        this.setCragSuggestions();

        this.updateFailedMessage = this.context.getString(R.string.outdoor_ascent_update_failed);
        this.confirmDeleteTitle = this.context.getString(R.string.outdoor_ascent_confirm_delete);
        this.gradeSelectorText = this.context.getString(R.string.outdoor_ascent_grade);
        this.methodSelectorText = this.context.getString(R.string.outdoor_ascent_method);
    }

    /**
     * Set the list of suggestions to show in the crag input.
     * When the data changes, that list is reloaded.
     */
    private void setCragSuggestions() {
        this.loadCragSuggestions();

        this.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                loadCragSuggestions();
            }
        });
    }

    /**
     * Load a list of crags from previously completed sessions from the database.
     */
    private void loadCragSuggestions() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(context);
        this.cragSuggestions = dbHelper.getCrags(dbHelper.getReadableDatabase());
    }

    /**
     * Creates a new {@link RecyclerAdapter_AscentInputOutdoor} object.
     *
     * @param context        - Context of the owning RecyclerView
     * @param boulder        - Whether the ascents are boulders or routes
     * @param ascents        - List of ascents
     * @param grades         - List of grades
     * @param methods        - List of possible methods of topping
     * @param databaseBacked - Whether the data is already stored in the database
     * @param smallViewPort  - Whether the views are displayed in a small space
     */
    public RecyclerAdapter_AscentInputOutdoor(Context context, boolean boulder, ArrayList<OutdoorAscent> ascents, ArrayList<LabelledInt> grades, ArrayList<LabelledInt> methods, boolean databaseBacked, boolean smallViewPort) {
        this(context, boulder, ascents, grades, methods, databaseBacked, smallViewPort, false);
    }

    /**
     * Creates a new {@link RecyclerAdapter_AscentInputOutdoor} object.
     *
     * @param context - Context of the owning RecyclerView
     * @param boulder - Whether the ascents are boulders or routes
     * @param ascents - List of ascents
     * @param grades  - List of grades
     * @param methods - List of possible methods of topping
     */
    public RecyclerAdapter_AscentInputOutdoor(Context context, boolean boulder, ArrayList<OutdoorAscent> ascents, ArrayList<LabelledInt> grades, ArrayList<LabelledInt> methods, boolean hasBottomControl) {
        this(context, boulder, ascents, grades, methods, false, false, hasBottomControl);
    }

    /**
     * Get element from the dataset at this position.
     * Replace the contents of the view with that element.
     *
     * @param holder   - Holder of the element
     * @param position - Index of element in ArrayList
     */
    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        super.onBindViewHolder(holder, position);

        this.setInputDisplay(holder);
        this.setValues(holder, position);
        this.setGrade(holder, position);
        this.setMethod(holder, position);
        this.setViewBackground(holder, position);
        this.setImage(holder, position);
        this.setDataButtonActions(holder, position);
    }

    /**
     * Modify the display of the inputs based on whether the data is
     * persisted in the database and whether it should be displayed
     * in a small space.
     *
     * @param holder - Holder of the element
     */
    private void setInputDisplay(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder) {
        holder.itemView.setToggleEditEnabled(this.databaseBacked);
        holder.itemView.setSmallViewPort(this.smallViewPort);
        holder.itemView.setCragSuggestions(this.cragSuggestions);
        if (this.databaseBacked) {
            holder.itemView.setEditMode(false);
        }
    }

    /**
     * Set the values for the text inputs and the {@link AscentInputOutdoor} view itself.
     *
     * @param holder   - Holder of the element
     * @param position - Index of element in ArrayList
     */
    private void setValues(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        holder.itemView.setAscent(this.data.get(position));
        holder.itemView.ascentCrag.setValue(this.data.get(position).ascentCrag);
        holder.itemView.ascentName.setValue(this.data.get(position).ascentName);
        holder.itemView.datePicker.setValue(this.data.get(position).date);
    }

    /**
     * Set the display text of the grade-select button.
     *
     * @param holder   - ViewHolder of the view
     * @param position - Position of the view
     */
    private void setGrade(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        int gradeId = this.data.get(position).gradeId;
        if (gradeId != -1) {
            String gradeLabel = this.data.get(position).grade.label;
            holder.itemView.gradeSelectorBtn.setText(gradeLabel);
        } else {
            holder.itemView.gradeSelectorBtn.setText(this.gradeSelectorText);
        }
    }

    /**
     * Set the display text of the method-select button.
     *
     * @param holder   - ViewHolder of the view
     * @param position - Position of the view
     */
    private void setMethod(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        final int methodIdx = this.data.get(position).method;
        if (methodIdx != -1) {
            String method = methods.get(methodIdx).label;
            holder.itemView.methodSelectorBtn.setText(method);
        } else {
            holder.itemView.methodSelectorBtn.setText(this.methodSelectorText);
        }
    }

    /**
     * Set background for the views in the list.
     *
     * @param holder   - ViewHolder of the view
     * @param position - Position of the view
     */
    private void setViewBackground(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        if (this.hasBottomControl) {
            this.setViewBackgroundWithBottomControls(holder, position);
        }
    }

    /**
     * Set background gray for every uneven entry and white for every even entry.
     *
     * @param holder   - ViewHolder of the view
     * @param position - Position of the view
     */
    private void setViewBackgroundWithBottomControls(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        Drawable background;
        if ((getItemCount() - position - 1) % 2 == 0) {
            background = ResourcesCompat.getDrawable(this.context.getResources(), R.drawable.box_accentlight, this.context.getTheme());
        } else {
            background = ResourcesCompat.getDrawable(this.context.getResources(), R.drawable.box_white, this.context.getTheme());
        }
        holder.itemView.setBackground(background);
    }

    /**
     * If a photo was taken for an ascent, read the image from the file storage and display it.
     *
     * @param holder   - ViewHolder of the view
     * @param position - Position of the view
     */
    private void setImage(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        String photoUriString = this.data.get(position).photoUriString;
        if (photoUriString != null) {
            try {
                holder.itemView.setImageView(photoUriString);
            } catch (Exception e) {
                holder.itemView.emptyImageView();
                e.printStackTrace();
            }
        } else {
            holder.itemView.emptyImageView();
        }
    }

    /**
     * Set Listener for updating and deleting an ascent.
     *
     * @param holder   - ViewHolder of the view
     * @param position - Position of the view
     */
    private void setDataButtonActions(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        holder.itemView.setUpdateAction((view) -> this.updateAscent(holder, position));

        holder.itemView.setDeleteAction((view) -> this.openDeleteConfirmDialog(position));

        holder.itemView.setCancelAction((view) -> this.cancelEdit(holder, position));
    }

    /**
     * Update an ascent with new data and save the changes to the database.
     *
     * @param holder   - ViewHolder of the view
     * @param position - Position of the view
     */
    private void updateAscent(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(this.context);
        boolean inputsInvalid = holder.itemView.inputsInvalid();

        if (inputsInvalid) {
            InformDialog.openDialog(this.fragmentManager, this.updateFailedMessage);
            return;
        }

        OutdoorAscent ascent = this.data.get(position);
        boolean success = dbHelper.updateOutdoorAscent(ascent, dbHelper.getReadableDatabase());
        if (success) {
            holder.itemView.setEditMode(false);
            this.data.set(position, ascent);
            notifyItemChanged(position);
        } else {
            InformDialog.openDialog(this.fragmentManager, this.updateFailedMessage);
            holder.itemView.setEditMode(false);
        }
    }

    /**
     * Opens a confirmation dialog to confirm the deletion of an ascent.
     *
     * @param position - Position of the corresponding view in the RecyclerView
     */
    private void openDeleteConfirmDialog(final int position) {
        @SuppressLint("NotifyDataSetChanged")
        View.OnClickListener okClick = (view) -> {
            if (this.databaseBacked) {
                DatabaseHelper dbHelper = DatabaseHelper.getInstance(this.context);
                int ascentId = this.data.get(position).id;
                boolean success = dbHelper.deleteOutdoorAscent(ascentId, dbHelper.getReadableDatabase());

                if (success) {
                    this.data.remove(position);
                    notifyDataSetChanged();
                }
            } else {
                this.data.remove(position);
                notifyDataSetChanged();
            }
        };
        ConfirmDialog confirm = new ConfirmDialog(this.confirmDeleteTitle, okClick, null);

        confirm.show(this.fragmentManager, "confirm");
    }

    /**
     * Cancels the editing of the inputs and resets their values.
     *
     * @param holder   - ViewHolder of the view
     * @param position - Position of the view
     */
    private void cancelEdit(RecyclerAdapter_Expandable.ViewHolder<AscentInputOutdoor> holder, int position) {
        OutdoorAscent backup = holder.itemView.getBackupAscent();
        if (backup == null) return;

        holder.itemView.setEditMode(false);
        holder.itemView.setAscent(backup);
        this.data.set(position, backup);
    }

}
