package caa.climbing.app.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import caa.climbing.app.Dialogs.CalendarEventDialog.CalendarEventDialog;
import caa.climbing.app.Dialogs.ConfirmDialog.ConfirmDialog;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import caa.climbing.app.Views.EventCalendar.CalendarEvent;
import caa.climbing.app.Views.EventCalendar.CalendarEventDetail;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * An adapter that holds data to display a list of {@link CalendarEventDetail} views.
 */
public class RecyclerAdapter_CalendarEventDetail extends RecyclerView.Adapter<RecyclerAdapter_CalendarEventDetail.ViewHolder> {
    final private ArrayList<CalendarEvent> data;
    final private FragmentManager fragmentManager;
    final private Consumer<CalendarEvent> deleteCallback;
    final private CalendarEventDialog parentDialog;
    final private String confirmDeleteMessage;

    /**
     * Creates a new {@link RecyclerAdapter_CalendarEventDetail} object
     *
     * @param data - Data to create views from
     */
    public RecyclerAdapter_CalendarEventDetail(Context context, ArrayList<CalendarEvent> data, Consumer<CalendarEvent> deleteCallback, CalendarEventDialog parentDialog) {
        this.data = data;
        this.fragmentManager = Utils.getActivity(context).getSupportFragmentManager();
        this.deleteCallback = deleteCallback;
        this.parentDialog = parentDialog;
        this.confirmDeleteMessage = context.getString(R.string.calendar_confirm_delete_event);
    }

    /**
     * Create new views (invoked by the layout manager)
     *
     * @param parent   - Parent view
     * @param viewType - Type of the view
     * @return - A new ViewHolder
     */
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create new view item
        CalendarEventDetail eventDetail = new CalendarEventDetail(parent.getContext(), null);
        eventDetail.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        return new ViewHolder(eventDetail);
    }

    /**
     * Replace the contents of a view (invoked by the layout manager)
     *
     * @param holder   - ViewHolder that holds the data
     * @param position - Position of the data in the data set to create a view from
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.eventDetail.setEvent(data.get(position));
        holder.eventDetail.setDeleteAction((view) -> this.openDeleteConfirmDialog(position));
    }

    /**
     * Get the number of entries in the data
     *
     * @return - Size of the data
     */
    @Override
    public int getItemCount() {
        return data.size();
    }

    /**
     * Open a dialog to confirm whether the deleteCallback should be executed and the item should be
     * removed from the data list.
     *
     * @param position - Position of the item to delete
     */
    private void openDeleteConfirmDialog(final int position) {
        @SuppressLint("NotifyDataSetChanged")
        View.OnClickListener okClick = (view) -> {
            CalendarEvent event = this.data.get(position);
            this.deleteCallback.accept(event);

            this.data.remove(position);
            if (this.data.size() == 0) {
                this.parentDialog.dismiss();
            }

            notifyDataSetChanged();
        };
        ConfirmDialog confirm = new ConfirmDialog(this.confirmDeleteMessage, okClick, null);
        confirm.show(this.fragmentManager, "confirm");
    }

    /**
     * Provide a reference to the views for each data item
     * Complex data items may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder
     */
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public CalendarEventDetail eventDetail;

        public ViewHolder(CalendarEventDetail itemView) {
            super(itemView);
            eventDetail = itemView;
        }
    }
}
