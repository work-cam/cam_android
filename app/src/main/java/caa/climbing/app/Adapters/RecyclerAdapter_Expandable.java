package caa.climbing.app.Adapters;

import android.content.Context;
import android.view.ViewGroup;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import caa.climbing.app.Views.ExpandableConstraintLayout;

import java.util.ArrayList;
import java.util.function.Supplier;

/**
 * An adapter to display multiple {@link ExpandableConstraintLayout} views inside a RecyclerView.
 *
 * @param <T> - Type of the data
 * @param <V> - Type of the displayed views
 */
public class RecyclerAdapter_Expandable<T, V extends ExpandableConstraintLayout> extends RecyclerView.Adapter<RecyclerAdapter_Expandable.ViewHolder<V>> {

    private final Supplier<V> supplier;
    protected final Context context;
    protected ArrayList<T> data;
    protected final ArrayList<Boolean> expandedItems;
    private int currentActivePosition;

    /**
     * Creates a new {@link RecyclerAdapter_Expandable} object.
     *
     * @param context  - Context of the owning RecyclerView
     * @param data     - Data based on which the views inside the RecyclerView are created
     * @param supplier - Supplier to create a new view
     */
    public RecyclerAdapter_Expandable(Context context, ArrayList<T> data, Supplier<V> supplier) {
        this.context = context;
        this.data = data;
        this.supplier = supplier;

        this.expandedItems = new ArrayList<>();
        for (T ignored : data) {
            this.expandedItems.add(false);
        }

        this.currentActivePosition = -1;
    }

    /**
     * Get the data of the adapter.
     *
     * @return - Data list
     */
    public ArrayList<T> getData() {
        return this.data;
    }

    /**
     * Set the data of the adapter.
     *
     * @param newData - New data set
     */
    public void setData(ArrayList<T> newData) {
        this.data = newData;
    }

    /**
     * Add an expanded item to the data set.
     *
     * @param item - Item to add
     */
    public void addItem(T item) {
        this.data.add(item);
        this.expandedItems.add(true);
    }

    /**
     * Toggle the expansion of the container.
     *
     * @param position- Position of the view
     */
    private void toggleExpansion(int position) {
        this.expandedItems.set(position, !this.expandedItems.get(position));
        if (this.expandedItems.get(position)) this.currentActivePosition = position;
        notifyItemChanged(position);
    }

    /**
     * Called when RecyclerView needs a new ViewHolder of the given type to represent an item.
     * The new ViewHolder will be used to display items of the adapter.
     *
     * @param parent   - ViewGroup into which the new View will be added after it is bound to an adapter position
     * @param viewType - The view type of the new View
     * @return - A new ViewHolder to display an item of the adapter
     */
    @NonNull
    @Override
    public ViewHolder<V> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        V view = supplier.get();

        view.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));

        return new ViewHolder<>(view);
    }

    /**
     * Get element from the dataset at given position.
     * Replace the contents of the view with that element.
     *
     * @param holder   - Holder of the element
     * @param position - Index of element in dataset
     */
    @CallSuper
    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter_Expandable.ViewHolder<V> holder, int position) {
        holder.itemView.toggleExpansion(this.expandedItems.get(position));
        holder.itemView.setActivated(position == this.currentActivePosition);

        holder.itemView.setToggleAction((view) -> this.toggleExpansion(position));
    }

    /**
     * Get the total number of entries in the data on which the display is based.
     *
     * @return - Number of items in the data
     */
    @Override
    public int getItemCount() {
        return this.data.size();
    }

    /**
     * A ViewHolder describes an item view and metadata about its place within the RecyclerView.
     */
    public static class ViewHolder<V extends ExpandableConstraintLayout> extends RecyclerView.ViewHolder {
        public V itemView;

        /**
         * Creates a new {@link RecyclerAdapter_HangBoardSessionDisplay.ViewHolder} object.
         *
         * @param itemView - View that is held by the ViewHolder
         */
        public ViewHolder(V itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }
}
