package caa.climbing.app.Adapters;

import android.text.Editable;
import android.text.TextWatcher;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.ViewGroup;

import caa.climbing.app.Views.Inputs.NumberInput.NumberInput;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;

import java.util.ArrayList;

/**
 * An adapter that holds data to display a list of {@link NumberInput} views.
 */
public class RecyclerAdapter_NumberInput extends RecyclerView.Adapter<RecyclerAdapter_NumberInput.ViewHolder> {
    private final ArrayList<LabelledInt> data;
    private final boolean smallViewPort;
    private int labelColor;
    private boolean editMode;

    /**
     * Creates a new {@link RecyclerAdapter_NumberInput} object
     *
     * @param data - Data to create views from
     */
    public RecyclerAdapter_NumberInput(ArrayList<LabelledInt> data, boolean editMode, boolean smallViewPort) {
        this.data = data;
        this.editMode = editMode;
        this.smallViewPort = smallViewPort;
    }

    /**
     * Create new views (invoked by the layout manager)
     *
     * @param parent   - Parent view
     * @param viewType - Type of the view
     * @return - A new ViewHolder
     */
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create new view item
        NumberInput numberInput = new NumberInput(parent.getContext(), null);
        numberInput.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        numberInput.setLabelColor(labelColor);
        return new ViewHolder(numberInput);
    }

    /**
     * Replace the contents of a view (invoked by the layout manager)
     *
     * @param holder   - ViewHolder that holds the data
     * @param position - Position of the data in the data set to create a view from
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.numberInput.setLabel(data.get(position).label);
        holder.numberInput.setValue(data.get(position).value);
        holder.numberInput.setTag(data.get(position).tag);
        holder.numberInput.setEditMode(this.editMode);
        holder.numberInput.setSmallViewPort(this.smallViewPort);
        holder.numberInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int idx = holder.getAdapterPosition();
                int oldValue = data.get(idx).value;
                String stringValue = s.toString();
                try {
                    data.get(idx).value = Integer.parseInt(stringValue);
                } catch (Exception e) {
                    data.get(idx).value = oldValue;
                }
            }
        });
    }

    /**
     * Get the number of entries in the data
     *
     * @return - Size of the data
     */
    @Override
    public int getItemCount() {
        return data.size();
    }

    /**
     * Sets the color of the labels
     *
     * @param color - New label color
     */
    public void setLabelColor(int color) {
        labelColor = color;
    }

    /**
     * Set the editMode of the adapter.
     *
     * @param editMode - Value to set the editMode to
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * Get the data of the adapter.
     *
     * @return - Data of the adapter
     */
    public ArrayList<LabelledInt> getData() {
        return this.data;
    }

    /**
     * Reset all input values to zero.
     */
    public void resetInputs() {
        for (int i = 0; i < this.data.size(); i++) {
            LabelledInt entry = this.data.get(i);
            entry.value = 0;
            this.notifyItemChanged(i);
        }
    }

    /**
     * Provide a reference to the views for each data item
     * Complex data items may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public NumberInput numberInput;

        public ViewHolder(NumberInput itemView) {
            super(itemView);
            numberInput = itemView;
        }
    }

}
