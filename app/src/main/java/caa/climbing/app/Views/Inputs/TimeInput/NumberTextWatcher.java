package caa.climbing.app.Views.Inputs.TimeInput;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class NumberTextWatcher implements TextWatcher {

    EditText editText;
    String formattedValue;

    public NumberTextWatcher(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // do nothing
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //Toast.makeText(editText.getContext(), s, Toast.LENGTH_SHORT).show();
        formattedValue = s.toString();
        while (formattedValue.startsWith("0")) {
            formattedValue = formattedValue.substring(1);
        }
        if (formattedValue.equals("")) {
            formattedValue = "00";
        }
        if (formattedValue.length()==1) {
            formattedValue = "0" + formattedValue;
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            editText.removeTextChangedListener(this);

            editText.setText(formattedValue);
            editText.setSelection(editText.getText().length());

            editText.addTextChangedListener(this);
            return;
        } catch (Exception e) {
            editText.addTextChangedListener(this);
        }
    }
}
