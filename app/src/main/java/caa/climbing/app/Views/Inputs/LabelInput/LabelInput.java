package caa.climbing.app.Views.Inputs.LabelInput;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import caa.climbing.app.R;
import caa.climbing.app.Views.Inputs.Input;

import androidx.annotation.Nullable;

import java.text.DecimalFormatSymbols;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A custom view for text input. The input can optionally display suggestions.
 */
public class LabelInput extends Input<String> {

    private int decimalPlaces;

    /**
     * Creates a new {@link LabelInput} object.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public LabelInput(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setInputAttributes(context, attrs);
        this.setDecimalPlaces(context, attrs);
        this.setSmallViewPort(this.smallViewPort);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getResourceId() {
        return R.layout.label_input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ViewGroup getRoot() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setValue(String value) {
        this.setTextValue(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue() {
        return this.getTextValue();
    }

    /**
     * Set the number of allowed decimal places if the input is a numerical decimal input
     * and an attribute for the number of allowed decimal places was set.
     *
     * @param attrs - Attributes defined in the *.xml file
     */
    private void setDecimalPlaces(Context context, AttributeSet attrs) {
        this.decimalPlaces = -1;
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.LabelInput, 0, 0);
            if (this.input.getInputType() == (InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER)) {
                this.decimalPlaces = a.getInt(R.styleable.LabelInput_decimals, 2);
            }
            a.recycle();
        }

        if (this.decimalPlaces > -1) {
            this.setFillDecimalsOnFocusLost();
            this.addTextChangedListener(new DecimalDigitsInputFilter((EditText) this.input, decimalPlaces));
        }
    }

    /**
     * When the input loses focus, the decimal places are filled with zeroes until the length of the
     * decimal places string matches the amount of allowed decimal places.
     */
    private void setFillDecimalsOnFocusLost() {
        this.input.setOnFocusChangeListener((view, hasFocus) -> {
            if (!hasFocus) {
                String textValue = this.getTextValue();
                char decimalSeparator = DecimalFormatSymbols.getInstance().getDecimalSeparator();
                Pattern pattern = Pattern.compile("(\\d+)?(.)?(\\d+)?");
                Matcher matcher = pattern.matcher(textValue);

                if (matcher.matches()) {
                    String integerPart = matcher.group(1);
                    String decimalPartString = matcher.group(3);
                    StringBuilder decimalPart = decimalPartString == null ? new StringBuilder() : new StringBuilder(decimalPartString);
                    while (decimalPart.length() < decimalPlaces) {
                        decimalPart.append("0");
                    }
                    if (integerPart == null || integerPart.equals("")) integerPart = "0";

                    this.setTextValue(integerPart + decimalSeparator + decimalPart);
                }
            }
        });
    }

    /**
     * Set the suggestions that should be displayed for the input after the user
     * has typed a certain amount of characters.
     *
     * @param suggestions - List of suggestions
     * @param threshold   - Specifies the minimum number of characters before suggestions are shown
     */
    public void setSuggestions(String[] suggestions, int threshold) {
        if (suggestions == null) return;

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, suggestions);
        ((AutoCompleteTextView) this.input).setAdapter(adapter);
        ((AutoCompleteTextView) this.input).setThreshold(threshold);
    }

    /**
     * Set the suggestions that should be displayed for the input after the user
     * has typed at least two characters.
     *
     * @param suggestions - List of suggestions
     */
    public void setSuggestions(String[] suggestions) {
        this.setSuggestions(suggestions, 1);
    }
}
