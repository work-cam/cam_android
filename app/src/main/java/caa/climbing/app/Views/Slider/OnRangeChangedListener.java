package caa.climbing.app.Views.Slider;

public interface OnRangeChangedListener {
    void onRangeChanged(RangeSlider view, float leftValue, float rightValue, boolean isFromUser);

    void onStartTrackingTouch(RangeSlider view, boolean isLeft);

    void onStopTrackingTouch(RangeSlider view, boolean isLeft);
}
