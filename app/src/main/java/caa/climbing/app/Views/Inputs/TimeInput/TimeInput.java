package caa.climbing.app.Views.Inputs.TimeInput;

import android.content.Context;

import androidx.annotation.Nullable;

import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import caa.climbing.app.R;
import caa.climbing.app.Views.Inputs.Input;

/**
 * View to allow the input of a time in a mm:ss format.
 */
public class TimeInput extends Input<Integer> {

    private EditText minutesInput, secondsInput;
    private TextView inputSeparator;

    final private String minutesSelector;
    final private String secondsSelector;

    /**
     * Creates a new {@link TimeInput} object.
     *
     * @param context - Context from which the view is created
     * @param attrs   - Optional Attributes that can be applied to the view
     */
    public TimeInput(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.minutesSelector = context.getString(R.string.selector_input_minutes);
        this.secondsSelector = context.getString(R.string.selector_input_seconds);

        this.initViews();
        this.setSmallViewPort(this.smallViewPort);
        this.addEventListener();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getResourceId() {
        return R.layout.time_input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ViewGroup getRoot() {
        return this;
    }

    /**
     * Set the total time.
     *
     * @param value - New total input time in seconds
     */
    @Override
    public void setValue(Integer value) {
        int mm = (int) (value / 60.0);
        int ss = value % 60;

        minutesInput.setText(String.valueOf(mm));
        secondsInput.setText(String.valueOf(ss));
    }

    /**
     * Get the total time from the minutes input and seconds input.
     *
     * @return - Total input time in seconds
     */
    @Override
    public Integer getValue() {
        String mm = minutesInput.getText().toString();
        String ss = secondsInput.getText().toString();

        // remove leading zeros
        if (mm.startsWith("0")) mm = mm.substring(1);
        if (ss.startsWith("0")) ss = ss.substring(1);

        int minutes, seconds;
        if (mm.equals("")) {
            minutes = 0;
        } else {
            minutes = Integer.parseInt(mm);
        }
        if (ss.equals("")) {
            seconds = 0;
        } else {
            seconds = Integer.parseInt(ss);
        }

        return 60 * minutes + seconds;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSmallViewPort(boolean smallViewPort) {
        super.setSmallViewPort(smallViewPort);

        float fontSize = this.smallViewPort ? this.smallFontSize : this.standardFontSize;

        this.inputSeparator.setTextSize(fontSize);
    }

    /**
     * Initializes the views.
     */
    private void initViews() {
        inputSeparator = findViewById(R.id.input_separator);
        minutesInput = (EditText) this.inputs.stream().filter((input) -> input.getTag().equals(this.minutesSelector)).findAny().orElse(null);
        secondsInput = (EditText) this.inputs.stream().filter((input) -> input.getTag().equals(this.secondsSelector)).findAny().orElse(null);
    }

    /**
     * Add Listeners to the inputs
     */
    private void addEventListener() {
        minutesInput.setFilters(new InputFilter[]{new InputFilterMinMax(0, 99)});
        secondsInput.setFilters(new InputFilter[]{new InputFilterMinMax(0, 59)});

        minutesInput.addTextChangedListener(new NumberTextWatcher(minutesInput));
        secondsInput.addTextChangedListener(new NumberTextWatcher(secondsInput));

        minutesInput.setSelection(minutesInput.getText().length());
        secondsInput.setSelection(secondsInput.getText().length());
    }
}
