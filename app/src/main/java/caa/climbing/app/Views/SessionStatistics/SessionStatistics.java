package caa.climbing.app.Views.SessionStatistics;

import android.content.Context;

import android.text.TextWatcher;
import android.widget.CompoundButton;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import caa.climbing.app.Views.Chart.ChartMarkerView;

import caa.climbing.app.Views.Inputs.SelectInput.SelectInput;
import caa.climbing.app.Views.Inputs.ToggleCheckbox.ToggleCheckbox;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.*;

import caa.climbing.app.R;
import caa.climbing.app.Views.Chart.ChartLegend;

import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarLineScatterCandleBubbleDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * View to display statistics about a certain type of climbing/training session.
 */
public class SessionStatistics extends ConstraintLayout {

    public final LinearLayout container;
    private final int chartHeight;
    private final int padding_px;
    private final ArrayList<Integer> colors;
    private final Context context;
    private final int textColor;
    private final float axisFontSize = 12f;
    private SessionInfo sessionInfo;

    /**
     * Creates a new {@link SessionStatistics} object.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public SessionStatistics(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        inflate(context, R.layout.session_statistics, this);

        this.container = findViewById(R.id.sessionContainer);
        float scale = getContext().getResources().getDisplayMetrics().density;
        this.padding_px = (int) getResources().getDimension(R.dimen.card_padding);
        this.chartHeight = (int) (250 * scale + 0.5f);

        this.colors = new ArrayList<>();
        this.colors.add(ResourcesCompat.getColor(getResources(), R.color.graph_color_1, null));
        this.colors.add(ResourcesCompat.getColor(getResources(), R.color.graph_color_2, null));
        this.colors.add(ResourcesCompat.getColor(getResources(), R.color.graph_color_3, null));
        this.colors.add(ResourcesCompat.getColor(getResources(), R.color.graph_color_4, null));
        this.colors.add(ResourcesCompat.getColor(getResources(), R.color.graph_color_5, null));
        this.colors.add(ResourcesCompat.getColor(getResources(), R.color.graph_color_6, null));

        this.textColor = ResourcesCompat.getColor(getResources(), R.color.gray800, null);
        // style the container
        this.container.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.card_white__backdrop, null));
        this.container.setPadding(this.padding_px, this.padding_px, this.padding_px, this.padding_px);
    }

    /**
     * Style the SessionInfo View and define the information to be displayed
     *
     * @param sessionName  - Type of the Session, displayed in the top box of SessionInfo
     * @param sessionCount - Number of completed sessions, displayed in the bottom box of SessionInfo
     * @param colorTop     - Color for the top box of SessionInfo
     * @param colorBottom  - Color for the bottom box of SessionInfo
     */
    public void setSessionInfo(String sessionName, int sessionCount, int colorTop, int colorBottom) {
        // create header
        this.sessionInfo = new SessionInfo(this.context, null);

        // add header to container
        this.container.addView(this.sessionInfo, 0);

        this.sessionInfo.setName(sessionName);
        this.sessionInfo.setCount(sessionCount);
        this.sessionInfo.style(colorTop, colorBottom);
    }

    /**
     * Add a Select input to switch displayed sessions based on the selected options
     *
     * @param options     - Options to choose from
     * @param selected    - Pre-selected option
     * @param textWatcher - TextWatcher to update the view when the select changes its value
     */
    public void setSessionFilter(String label, String[] options, String selected, @Nullable TextWatcher textWatcher) {
        SelectInput selectInput = new SelectInput(getContext(), null);
        selectInput.setSuggestions(options);
        selectInput.setLabel(label);
        selectInput.setLabelColor(this.textColor);

        if (selected != null && selected.length() > 0) {
            selectInput.setValue(selected);
        }

        if (textWatcher != null) selectInput.addTextChangedListener(textWatcher);

        this.container.addView(selectInput, 1);
    }

    /**
     * Add a checkbox to the display.
     *
     * @param label    - Label for the checkbox
     * @param listener - ChangeListener to add to the checkbox
     */
    public void setCheckbox(String label, CompoundButton.OnCheckedChangeListener listener) {
        ToggleCheckbox checkbox = new ToggleCheckbox(this.context);
        checkbox.addEventListener(listener);
        checkbox.setLabel(label);
        this.container.addView(checkbox, 1);
    }

    /**
     * Create a Line Chart and populate it with data
     * Set the styling of the chart
     * Define zoom behavior and show MarkerView on Value clicked
     * Add chart to view
     *
     * @param dataSets             - Data to be displayed in the chart
     * @param title                - Title that will be displayed above the chart
     * @param categories           - Strings that will be displayed in the legend below the chart
     * @param removePreviousCharts - if true, all charts in the container will be removed before the new one is added
     * @param initialRangeX        - initial range of the x-axis
     * @param maxX                 - Max value of the x-axis
     * @param axisFormatter        - Optional formatter for the labels on the x-axis below the respective Bar
     * @param markerFormatter      - Optional formatter for the marker when a value is highlighted
     */
    public void setLineChart(ArrayList<LineDataSet> dataSets, String title, ArrayList<String> categories, boolean removePreviousCharts, int initialRangeX, float maxX, @Nullable ValueFormatter axisFormatter, @Nullable ValueFormatter markerFormatter) {
        // create line chart
        final LineChart chart = new LineChart(this.context);

        List<ILineDataSet> iDataSets = new ArrayList<>();

        for (int i = 0; i < dataSets.size(); i++) {
            LineDataSet lineDataSet = dataSets.get(i);
            int color = this.colors.get(i % this.colors.size());

            lineDataSet.setColor(color);
            lineDataSet.setCircleColor(color);
            lineDataSet.setDrawCircles(true);

            lineDataSet.setHighLightColor(ResourcesCompat.getColor(getResources(), R.color.white, null));

            lineDataSet.setCircleRadius(4f);
            lineDataSet.setDrawCircleHole(false);

            lineDataSet.setDrawValues(false);

            lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
            lineDataSet.setLineWidth(4f);

            iDataSets.add(lineDataSet);
        }
        LineData lineData = new LineData(iDataSets);

        chart.setData(lineData);

        this.styleLineChart(chart, initialRangeX, maxX, axisFormatter, markerFormatter);

        this.addChartToView(chart, title, categories, removePreviousCharts);
    }

    /**
     * Create a Bar Chart and populate it with data.
     * Set the styling of the chart.
     * Add chart to view.
     *
     * @param dataSets             - Data to be displayed in the chart
     * @param title                - Title that will be displayed above the chart
     * @param categories           - Strings that will be displayed in the legend below the chart
     * @param removePreviousCharts - if true, all charts in the container will be removed before the new one is added
     * @param initialRangeX        - initial range of the x-axis
     * @param maxX                 - Max value of the x-axis
     * @param axisFormatter        - Optional formatter for the labels on the x-axis below the respective Bar
     * @param markerFormatter      - Optional formatter for the marker when a value is highlighted
     */
    public void setBarChart(ArrayList<BarEntry> dataSets, String title, ArrayList<String> categories, boolean removePreviousCharts, int initialRangeX, float maxX, @Nullable ValueFormatter axisFormatter, @Nullable ValueFormatter markerFormatter) {
        if (dataSets.size() > 0) {
            // create bar chart
            BarChart chart = new BarChart(this.context);
            // create line data set that contains the data points
            BarDataSet barDataSet = new BarDataSet(dataSets, "");

            barDataSet.setDrawIcons(false);
            barDataSet.setDrawValues(false);

            float[] yValues = dataSets.get(0).getYVals();
            ArrayList<Integer> selectColors = new ArrayList<>();

            if (yValues != null) {
                // case of stacked bars
                // create list of colors that matches the length of the values in a single bar
                // if the standard colors list is passed, bar color may appear out of order
                for (int i = 0; i < yValues.length; i++) {
                    int color = this.colors.get(i % this.colors.size());
                    selectColors.add(color);
                }
            } else {
                // case for single bars
                for (int i = 0; i < dataSets.size(); i++) {
                    int color = this.colors.get(i % this.colors.size());
                    selectColors.add(color);
                }
            }
            if (selectColors.size() > 0) barDataSet.setColors(selectColors);
            else barDataSet.setColors(this.colors);

            BarData barData = new BarData(barDataSet);
            barData.setValueTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
            chart.setData(barData);

            this.styleBarChart(chart, initialRangeX, maxX, axisFormatter, markerFormatter);

            addChartToView(chart, title, categories, removePreviousCharts);
        }
    }

    /**
     * Handle Gestures to reset the zoom and highlighted value of a chart.
     *
     * @param chart         - Chart for which the zoom will be reset
     * @param initialRangeX - Range of the x-axis to which the chart will be reset
     * @param maxX          - Max value of the x-axis
     * @param <S>           - Type of the Entry
     * @param <T>           - Type of the ChartDataSet
     * @param <U>           - Type of the ChartData
     * @param <V>           - Type of the Chart
     */
    private <S extends Entry,
            T extends IBarLineScatterCandleBubbleDataSet<S>,
            U extends BarLineScatterCandleBubbleData<T>,
            V extends BarLineChartBase<U>>
    void handleZoomReset(V chart, int initialRangeX, float maxX) {
        //double tap should reset zoom, therefore zoom on double tap needs to be disabled here
        chart.setDoubleTapToZoomEnabled(false);

        // events for resetting the zoom and hiding the marker view
        chart.setOnChartGestureListener(new OnChartGestureListener() {
            @Override
            public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
            }

            @Override
            public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
            }

            @Override
            public void onChartLongPressed(MotionEvent me) {
                chart.highlightValue(null);
            }

            @Override
            public void onChartDoubleTapped(MotionEvent me) {
                setInitialZoom(chart, initialRangeX, maxX);
                chart.highlightValue(null);
            }

            @Override
            public void onChartSingleTapped(MotionEvent me) {
            }

            @Override
            public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
                chart.highlightValue(null);
            }

            @Override
            public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
                chart.highlightValue(null);
            }

            @Override
            public void onChartTranslate(MotionEvent me, float dX, float dY) {
                chart.highlightValue(null);
            }
        });
    }

    /**
     * Set the initial view so that only a limited x-range is initially visible.
     *
     * @param chart         - Chart to set the zoom on
     * @param desiredRangeX - initial range of the x-axis
     * @param maxX          - Max value of the x-axis
     * @param <S>           - Type of the Entry
     * @param <T>           - Type of the ChartDataSet
     * @param <U>           - Type of the ChartData
     * @param <V>           - Type of the Chart
     */
    private <S extends Entry,
            T extends IBarLineScatterCandleBubbleDataSet<S>,
            U extends BarLineScatterCandleBubbleData<T>,
            V extends BarLineChartBase<U>>
    void setInitialZoom(V chart, int desiredRangeX, float maxX) {
        ViewPortHandler handler = chart.getViewPortHandler();

        MPPointD topLeft = chart.getValuesByTouchPoint(handler.contentLeft(), handler.contentTop(), YAxis.AxisDependency.LEFT);
        MPPointD bottomRight = chart.getValuesByTouchPoint(handler.contentRight(), handler.contentBottom(), YAxis.AxisDependency.LEFT);

        float xMin = (float) topLeft.x;
        float xMax = (float) bottomRight.x;

        float yMin = chart.getYChartMin();
        float yMax = chart.getYChartMax();

        float currentRangeX = xMax - xMin;

        float currentRangeY = (float) topLeft.y - (float) bottomRight.y;
        float desiredRangeY = yMax - yMin;
        float scaleX = currentRangeX / desiredRangeX;
        float scaleY = currentRangeY / desiredRangeY;

        chart.zoom(scaleX, scaleY, 0, 0);

        chart.getXAxis().setAxisMaximum(maxX);
        chart.moveViewToX(maxX - desiredRangeX);
    }


    /**
     * Define Styling specific for the LineChart.
     *
     * @param chart           - Chart to be styled
     * @param initialRangeX   - initial range of the x-axis
     * @param maxX            - Max value of the x-axis
     * @param axisFormatter   - Optional formatter for the labels on the x-axis below the respective Bar
     * @param markerFormatter - Optional formatter for the marker when a value is highlighted
     */
    private void styleLineChart(LineChart chart, int initialRangeX, float maxX, @Nullable ValueFormatter axisFormatter, @Nullable ValueFormatter markerFormatter) {
        this.styleChart(chart, initialRangeX, maxX, axisFormatter, markerFormatter);
        // set text color of y-axis
        chart.getAxisLeft().setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        // hide grid
        chart.getAxisLeft().setDrawGridLines(false);

        // y axis
        YAxis yAxisRight = chart.getAxisRight();
        YAxis yAxisLeft = chart.getAxisLeft();
        yAxisLeft.setTextSize(this.axisFontSize);
        yAxisRight.setEnabled(false);
    }

    /**
     * Define Styling specific for the BarChart.
     *
     * @param chart           - Chart to be styled
     * @param initialRangeX   - initial range of the x-axis
     * @param maxX            - Max value of the x-axis
     * @param axisFormatter   - Optional formatter for the labels on the x-axis below the respective Bar
     * @param markerFormatter - Optional formatter for the marker when a value is highlighted
     */
    private void styleBarChart(BarChart chart, int initialRangeX, float maxX, @Nullable ValueFormatter axisFormatter, @Nullable ValueFormatter markerFormatter) {
        this.styleChart(chart, initialRangeX, maxX, axisFormatter, markerFormatter);
        // set text color of y-axis
        chart.getAxisLeft().setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        // hide grid
        chart.getAxisLeft().setDrawGridLines(false);

        // y axis
        YAxis yAxisRight = chart.getAxisRight();
        YAxis yAxisLeft = chart.getAxisLeft();
        yAxisRight.setEnabled(false);

        yAxisLeft.setAxisMinimum(0);
    }

    /**
     * Define general styling for all chart types (line and bar).
     *
     * @param chart           - Chart to be styled
     * @param initialRangeX   - initial range of the x-axis
     * @param maxX            - Max value of the x-axis
     * @param axisFormatter   - Optional formatter for the labels on the x-axis below the respective Bar
     * @param markerFormatter - Optional formatter for the marker when a value is highlighted
     * @param <S>             - Type of the Entry
     * @param <T>             - Type of the ChartDataSet
     * @param <U>             - Type of the ChartData
     * @param <V>             - Type of the Chart
     */
    private <S extends Entry,
            T extends IBarLineScatterCandleBubbleDataSet<S>,
            U extends BarLineScatterCandleBubbleData<T>,
            V extends BarLineChartBase<U>>
    void styleChart(V chart, int initialRangeX, float maxX, @Nullable ValueFormatter axisFormatter, @Nullable ValueFormatter markerFormatter) {
        if (markerFormatter == null) markerFormatter = axisFormatter;

        // set initial limits of x-Axis
        if (initialRangeX > 0 && maxX > 0) {
            this.setInitialZoom(chart, initialRangeX, maxX);
            this.handleZoomReset(chart, initialRangeX, maxX);
        }

        ChartMarkerView chartMarkerView = new ChartMarkerView(this.context, R.layout.chart_marker_view, markerFormatter);
        chart.setMarker(chartMarkerView);

        chart.setMinimumHeight(this.chartHeight);

        chart.getDescription().setEnabled(false);

        // no default legend
        chart.getLegend().setEnabled(false);

        // define spacing of chart area
        chart.setExtraBottomOffset(14f);
        chart.setExtraLeftOffset(12f);
        chart.setExtraRightOffset(25f);

        // set background
        chart.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.card_primary__smallcorner, null));
        // set text color on axes
        chart.getXAxis().setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        // hide grid
        chart.getXAxis().setDrawGridLines(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(this.axisFontSize);

        // fix interval of x-axis
        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(1f);

        if (axisFormatter != null) {
            xAxis.setValueFormatter(axisFormatter);
        }
        xAxis.setLabelRotationAngle(-45);
    }

    /**
     * Add a chart to the view.
     *
     * @param chart                - Chart that will be added to the container view
     * @param title                - Title that will be displayed above the chart
     * @param categories           - Strings that will be displayed in the legend below the chart
     * @param removePreviousCharts - If true, all previous charts in the container are removed before the new chart is added
     * @param <S>                  - Type of the Entry
     * @param <T>                  - Type of the ChartDataSet
     * @param <U>                  - Type of the ChartData
     * @param <V>                  - Type of the Chart
     */
    private <S extends Entry,
            T extends IBarLineScatterCandleBubbleDataSet<S>,
            U extends BarLineScatterCandleBubbleData<T>,
            V extends BarLineChartBase<U>>
    void addChartToView(V chart, String title, ArrayList<String> categories, boolean removePreviousCharts) {
        float scale = this.context.getResources().getDisplayMetrics().scaledDensity;

        TextView chartTitle = new TextView(this.context);
        chartTitle.setText(title);
        chartTitle.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        chartTitle.setTextSize(getResources().getDimension(R.dimen.fontsize_medium) / scale);
        chartTitle.setPadding(0, 0, 0, (int) getResources().getDimension(R.dimen.chart_title_padding_bottom));
        chartTitle.setTextColor(this.textColor);

        if (removePreviousCharts) {
            // remove old charts
            for (int i = 1; i < this.container.getChildCount(); i++) {
                this.container.removeViewAt(i);
            }
        }

        // create chart container
        LinearLayout chartContainer = new LinearLayout(this.context);
        chartContainer.setOrientation(LinearLayout.VERTICAL);
        chartContainer.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.card_white__backdrop, null));
        chartContainer.setPadding(this.padding_px, this.padding_px, this.padding_px, this.padding_px);

        // add title to chart container
        if (!title.equals("")) chartContainer.addView(chartTitle);

        // add chart to chart container
        chartContainer.addView(chart);

        // create legend
        ChartLegend chartLegend = new ChartLegend(this.context, null);
        int[] colors = chart.getData().getColors();
        chartLegend.setLegend(colors, categories);
        chartContainer.addView(chartLegend);

        // add chart container to container
        this.container.addView(chartContainer);
    }

    /**
     * Remove all views from the container.
     */
    public void clear() {
        this.container.removeAllViews();
    }

    /**
     * Set a click listener for the session info.
     *
     * @param clickListener - New listener for the view
     */
    public void setSessionInfoClickListener(OnClickListener clickListener) {
        this.sessionInfo.setOnClickListener(clickListener);
    }
}
