package caa.climbing.app.Views.TabBar;

import android.content.Context;
import android.util.AttributeSet;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;

import caa.climbing.app.R;

/**
 * View to display a tab bar with respective content.
 */
public class TabBar extends ConstraintLayout {

    private final TabLayout tabContainer;
    private final ViewPager2 viewPager;

    /**
     * Creates a new {@link TabBar} object.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public TabBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        ConstraintLayout rootView = (ConstraintLayout) inflate(context, R.layout.tabbar, this);

        this.tabContainer = rootView.findViewById(R.id.tab_container);
        this.viewPager = rootView.findViewById(R.id.pager);
        this.viewPager.setSaveEnabled(false);
    }

    /**
     * Creates the tabs with a title and a fragment as content.
     *
     * @param fragmentManager - FragmentManager of ViewPager2 host
     * @param lifecycle       - LifeCycle of ViewPager2 host
     * @param config          - List of configuration objects for the tabs
     */
    public void createTabs(FragmentManager fragmentManager, Lifecycle lifecycle, final ArrayList<TabConfig> config) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(fragmentManager, lifecycle, config);

        this.viewPager.setAdapter(adapter);

        new TabLayoutMediator(tabContainer, viewPager, (tab, position) -> tab.setText(config.get(position).getTabTitle())).attach();
    }

    /**
     * Activate a tab of the tab bar.
     *
     * @param index - Index of the tab to activate
     */
    public void setActiveTab(int index) {
        this.viewPager.setVisibility(INVISIBLE);
        this.viewPager.postDelayed(() -> {
            this.viewPager.setCurrentItem(index);
            this.viewPager.setVisibility(VISIBLE);
        }, 50);
    }

}
