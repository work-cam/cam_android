package caa.climbing.app.Views;

import android.content.Context;
import android.util.AttributeSet;

import caa.climbing.app.R;

import androidx.constraintlayout.widget.ConstraintLayout;

public class HangboardSessionsFilter extends ConstraintLayout {

    public HangboardSessionsFilter(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context, R.layout.hangboard_sessions_filter, this);
    }
}
