package caa.climbing.app.Views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;

import androidx.appcompat.widget.AppCompatTextView;

public class FramedTextView extends AppCompatTextView {
    private Context context;

    public FramedTextView(Context context) {
        super(context);
        this.context = context;
        setStyle(null);
    }

    public FramedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setStyle(attrs);

    }

    public FramedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        setStyle(attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /**
     * Styles the view.
     *
     * @param attrs - Attributes that are passed to the view
     */
    private void setStyle(AttributeSet attrs) {
        Drawable background = getResources().getDrawable(R.drawable.textfield_background);
        Utils.setStyle(context, this, attrs, background);
    }


}
