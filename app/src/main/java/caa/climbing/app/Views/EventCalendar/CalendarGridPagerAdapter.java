package caa.climbing.app.Views.EventCalendar;

import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Consumer;

/**
 * An adapter to display multiple {@link CalendarGrid} views inside a {@link androidx.viewpager2.widget.ViewPager2}.
 */
class CalendarGridPagerAdapter extends RecyclerView.Adapter<CalendarGridPagerAdapter.ViewHolder> {

    final private ArrayList<Date> months;
    final private Consumer<CalendarEvent> deleteCallback;

    private int currentPosition;
    private long calendarId;

    /**
     * Create a new {@link CalendarGridPagerAdapter} object.
     *
     * @param calendarId     - ID of the calendar to use
     * @param deleteCallback - Callback to execute when the delete button of the detail dialog was pressed
     */
    public CalendarGridPagerAdapter(Consumer<CalendarEvent> deleteCallback, long calendarId) {
        this.currentPosition = 0;
        this.months = new ArrayList<>();
        this.calendarId = calendarId;
        this.deleteCallback = deleteCallback;

        this.initMonths();
    }

    /**
     * Initialize the list of dates that determine the {@link CalendarGrid} views to display.
     */
    private void initMonths() {
        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);
        int currentYear = calendar.get(Calendar.YEAR);

        calendar.set(EventCalendar.MIN_YEAR, 0, 1, 0, 0, 0);

        int totalMonths = (EventCalendar.MAX_YEAR - EventCalendar.MIN_YEAR) * 12;

        for (int i = 0; i < totalMonths; i++) {
            this.months.add(calendar.getTime());
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);

            if (year == currentYear && month == currentMonth) {
                this.currentPosition = i;
            }

            calendar.add(Calendar.MONTH, 1);
        }
    }

    /**
     * Set the ID of the calendar to read events from.
     *
     * @param calendarId - ID of the calendar to use
     */
    protected void setCalendarId(long calendarId) {
        this.calendarId = calendarId;
    }

    /**
     * Create new views (invoked by the layout manager)
     *
     * @param parent   - Parent view
     * @param viewType - Type of the view
     * @return - A new ViewHolder
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CalendarGrid calendarGrid = new CalendarGrid(parent.getContext(), null, this.deleteCallback, this.calendarId);

        calendarGrid.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        ));

        return new ViewHolder(calendarGrid);
    }

    /**
     * Get the date from the dataset at the given position.
     * Set the date of the calendar grid to that date.
     *
     * @param holder   - Holder of the element
     * @param position - Index of element in dataset
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.calendarGrid.setDate(this.getDate(position));
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return this.months.size();
    }

    /**
     * Get the date from the dataset at the given position.
     *
     * @param position - Index of element in dataset
     * @return - Date at the given position
     */
    @Nullable
    public Date getDate(int position) {
        if (position < 0 || position > this.getItemCount() - 1) return null;

        return this.months.get(position);
    }

    /**
     * Get the position of the current month in the list of dates.
     *
     * @return - Position of the current month
     */
    public int getCurrentPosition() {
        return this.currentPosition;
    }

    /**
     * A ViewHolder describes an item view and metadata about its place within the RecyclerView.
     */
    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public CalendarGrid calendarGrid;

        /**
         * Creates a new {@link ViewHolder} object.
         *
         * @param calendarGrid - View that is held by the ViewHolder
         */
        public ViewHolder(CalendarGrid calendarGrid) {
            super(calendarGrid);
            this.calendarGrid = calendarGrid;
        }
    }
}
