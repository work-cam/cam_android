package caa.climbing.app.Views.EventCalendar;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.GridView;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import caa.climbing.app.Dialogs.CalendarEventDialog.CalendarEventDialog;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

/**
 * A view to list the dates of a given month as a grid.
 */
public class CalendarGrid extends GridView {
    static final private int MAX_NUMBER_DAYS = 42;

    final private Calendar calendar;
    final private Context context;
    final private long calendarId;
    final private FragmentManager fragmentManager;
    final private ArrayList<Date> dates;
    final private Consumer<CalendarEvent> deleteCallback;

    private int indexOfFirstDayOfMonth, activePosition;

    private CalendarGridAdapter calendarGridAdapter;

    /**
     * Creates a new {@link CalendarGrid} instance.
     *
     * @param context - Context from which the view was created
     */
    public CalendarGrid(Context context) {
        this(context, null, null, -1);
    }

    /**
     * Creates a new {@link CalendarGrid} instance.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public CalendarGrid(Context context, AttributeSet attrs) {
        this(context, attrs, null, -1);
    }

    /**
     * Creates a new {@link CalendarGrid} instance.
     *
     * @param context        - Context from which the view was created
     * @param attrs          - Attributes that are passed to the view
     * @param deleteCallback - Callback to execute when the delete button of the detail dialog was pressed
     * @param calendarId     - ID of the calendar to use
     */
    public CalendarGrid(Context context, AttributeSet attrs, Consumer<CalendarEvent> deleteCallback, long calendarId) {
        super(context, attrs);
        this.context = context;
        this.deleteCallback = deleteCallback;
        this.calendarId = calendarId;

        this.calendar = Calendar.getInstance(TimeZone.getDefault());
        this.calendar.setFirstDayOfWeek(Calendar.MONDAY);

        this.fragmentManager = Utils.getActivity(context).getSupportFragmentManager();
        this.indexOfFirstDayOfMonth = -1;
        this.activePosition = -1;

        this.dates = new ArrayList<>();

        this.setNumColumns(7);
        this.setChoiceMode(CHOICE_MODE_SINGLE);
        this.setOnItemClickListener((parent, view, position, id) -> this.itemClick(position));

        this.setupCalendar();
    }

    /**
     * Check whether two dates fall onto the same day.
     *
     * @param first  - First day to check
     * @param second - Second day to check
     * @return - True if the dates have the same day
     */
    static protected boolean isSameDay(@Nullable Date first, @Nullable Date second) {
        if (first == null || second == null) return false;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(first);
        Utils.resetCalendar(calendar);
        int firstDay = calendar.get(Calendar.DATE);
        int firstMonth = calendar.get(Calendar.MONTH);
        int firstYear = calendar.get(Calendar.YEAR);

        calendar.setTime(second);
        Utils.resetCalendar(calendar);

        int secondDay = calendar.get(Calendar.DATE);
        int secondMonth = calendar.get(Calendar.MONTH);
        int secondYear = calendar.get(Calendar.YEAR);

        return firstDay == secondDay && firstMonth == secondMonth && firstYear == secondYear;
    }

    /**
     * Update the UI to reflect the currently selected month.
     */
    private void setupCalendar() {
        this.setupDatesForMonth();

        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();
        int position = this.getGridPosition(currentDate);
        this.activePosition = position > -1 && position < MAX_NUMBER_DAYS ? position : this.indexOfFirstDayOfMonth;
        this.calendarGridAdapter = new CalendarGridAdapter(this.context, this.dates, this.calendar, this.activePosition);
        this.setAdapter(this.calendarGridAdapter);
        try {
            this.readPublicCalendarEvents(this.calendarId);
        } catch (SecurityException e) {
            Log.i("caa_", "Cannot read calendar");
        }
    }

    /**
     * Set up the list that contains the dates that should be displayed in the current view.
     */
    private void setupDatesForMonth() {
        this.dates.clear();
        Calendar monthCalendar = (Calendar) this.calendar.clone();
        monthCalendar.set(Calendar.DAY_OF_MONTH, 1);
        Utils.resetCalendar(monthCalendar);

        // Week day of the first day of the month
        int dayOfWeek = monthCalendar.get(Calendar.DAY_OF_WEEK);
        // Which day the week starts with
        int firstDayOfWeek = monthCalendar.getFirstDayOfWeek();
        // How many days of the last month are in the first week of the current month
        int daysOfPreviousMonth = firstDayOfWeek - dayOfWeek;
        // Offset to display the week days correctly
        int offsetDays = daysOfPreviousMonth > 0 ? daysOfPreviousMonth - 7 : daysOfPreviousMonth;

        monthCalendar.add(Calendar.DAY_OF_MONTH, offsetDays);

        while (this.dates.size() < MAX_NUMBER_DAYS) {
            this.dates.add(monthCalendar.getTime());
            monthCalendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        this.indexOfFirstDayOfMonth = Math.abs(offsetDays);
    }

    /**
     * Handles the click on an item in the grid.
     *
     * @param position - Position of the view in the grid
     */
    private void itemClick(int position) {
        Date selectedDate = this.calendarGridAdapter.getItem(position);
        if (selectedDate == null) return;

        if (this.activePosition == position) {
            ArrayList<CalendarEvent> eventsForDate = this.calendarGridAdapter.getEventsForDate(selectedDate);
            if (eventsForDate.size() > 0) this.showDialog(eventsForDate);
        }

        this.activePosition = position;
        this.highlightSelectedDate();
    }

    /**
     * Highlight a view in the calendar grid.
     */
    private void highlightSelectedDate() {
        this.calendarGridAdapter.setActivePosition(this.activePosition);
        this.calendarGridAdapter.notifyDataSetChanged();
    }

    /**
     * Show a dialog that displays a list of events for the given day.
     *
     * @param events - List of events to display
     */
    private void showDialog(ArrayList<CalendarEvent> events) {
        Date currentDate = this.getSelectedDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd.MM.yyyy", Locale.getDefault());
        String title = this.context.getString(R.string.calendar_dialog_title, dateFormat.format(currentDate));
        CalendarEventDialog dialog = new CalendarEventDialog(title, events, this.deleteCallback);

        dialog.show(this.fragmentManager, "events");
    }

    /**
     * Get the currently selected date.
     *
     * @return - The selected date
     */
    public Date getSelectedDate() {
        return this.calendarGridAdapter.getItem(this.activePosition);
    }

    /**
     * Read the events that are stored in the public calendar for the current month
     * and update the UI to display them.
     *
     * @throws SecurityException - Throw an exception if the app does not have permission to access the calendar
     */
    public void readPublicCalendarEvents(long calendarId) throws SecurityException {
        ArrayList<CalendarEvent> events = new ArrayList<>();
        ContentResolver contentResolver = this.context.getContentResolver();
        Uri uri = CalendarContract.Events.CONTENT_URI;
        String[] projection = new String[]{
                CalendarContract.Calendars._ID,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.UID_2445
        };

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.dates.get(0));
        Utils.resetCalendar(calendar);
        Date startDate = calendar.getTime();
        calendar.add(Calendar.DATE, MAX_NUMBER_DAYS);
        Date endDate = calendar.getTime();

        String selection = String.format("((%s >= ?) AND (%s <= ?) AND (%s = ?) AND (%s = ?))",
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.DTEND,
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.UID_2445);

        String[] selectionArguments = new String[]{String.valueOf(startDate.getTime()), String.valueOf(endDate.getTime()), String.valueOf(calendarId), CalendarIntentFragment.UID};

        Cursor cursor = contentResolver.query(uri, projection, selection, selectionArguments, CalendarContract.Events.TITLE + " ASC");

        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(CalendarContract.Events.TITLE));
            long dtStart = Long.parseLong(cursor.getString(cursor.getColumnIndex(CalendarContract.Events.DTSTART)));
            long id = Long.parseLong(cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars._ID)));

            events.add(new CalendarEvent(name, new Date(dtStart), id));

        }
        cursor.close();

        this.calendarGridAdapter.setEvents(events);
        this.calendarGridAdapter.notifyDataSetChanged();
    }

    /**
     * Set the date of the calendar.
     *
     * @param date - Date to change to
     */
    public void setDate(Date date) {
        this.calendar.setTime(date);
        this.setupCalendar();
    }

    /**
     * Find the position of a given date in the calendar grid
     *
     * @param date - Date to search
     * @return - Position of the date in the grid or -1
     */
    private int getGridPosition(Date date) {
        for (int i = 0; i < this.dates.size(); i++) {
            if (isSameDay(date, this.dates.get(i))) return i;
        }

        return -1;
    }
}
