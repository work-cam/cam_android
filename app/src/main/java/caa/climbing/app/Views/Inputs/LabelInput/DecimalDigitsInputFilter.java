package caa.climbing.app.Views.Inputs.LabelInput;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

class DecimalDigitsInputFilter implements TextWatcher {

    private int MAX_DECIMAL_PLACES;
    private EditText editText;
    String formattedValue;
    int currPos;

    public DecimalDigitsInputFilter(EditText editText, int maxDecimalPlaces) {
        MAX_DECIMAL_PLACES = maxDecimalPlaces;
        this.editText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        formattedValue = formatDecimal(s.toString());

        if (s.length()==2 && s.charAt(0)=='0') {
            currPos = 0;
        } else if (before > count) {
            currPos = start;
        } else if (before < count){
            currPos = start+1;
        }
//        currPos = Math.max(0,currPos);

        // POSSIBLE SCENARIOS

        //



//        Toast.makeText(editText.getContext(), "start: " + start, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            editText.removeTextChangedListener(this);

            editText.setText(formattedValue);
//            editText.setSelection(editText.getText().length());
            int pos = Math.min(formattedValue.length(), currPos+1);
            editText.setSelection(currPos);

            editText.addTextChangedListener(this);
            return;
        } catch (Exception e) {
            editText.addTextChangedListener(this);
        }

//        String str = editText.getText().toString();
////        if (str.isEmpty()) return;
//        String formatted = formatDecimal(str);
//
//        if (!formatted.equals(str)) {
//            editText.setText(formatted);
//            int pos = editText.getText().length();
//            editText.setSelection(pos);
//        }
    }

    private String formatDecimal(String decimalStr) {

        if (decimalStr.isEmpty()) return decimalStr;

        // remove leadig zeroes
        while (decimalStr.startsWith("0") && decimalStr.length()>1 && decimalStr.charAt(1)!='.') {
            decimalStr = decimalStr.substring(1);
        }
        // add zero before decimal point
        if (decimalStr.startsWith(".")) decimalStr = "0" + decimalStr;

        int max = decimalStr.length();

        String rFinal = "";
        boolean after = false;
        int i=0, up = 0, decimal = 0;
        char t;
        while (i<max) {
            t = decimalStr.charAt(i);
            if (t != '.' && !after) {
                up++;
//                if (up > )
            } else if (t == '.') {
                after = true;
            } else {
                decimal++;
                if (decimal > MAX_DECIMAL_PLACES) {
                    return rFinal;
                }
            }
            rFinal += t;
            i++;
        }
        return rFinal;
    }
}
