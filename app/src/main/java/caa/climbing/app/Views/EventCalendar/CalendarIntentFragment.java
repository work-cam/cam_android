package caa.climbing.app.Views.EventCalendar;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.provider.CalendarContract;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import caa.climbing.app.HelperClasses.PermissionHandler;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * A fragment to save events to the device calendar.
 */
public class CalendarIntentFragment extends Fragment {

    final static private String TAG = "CALENDAR";
    final static public String UID = "caa";

    final private FragmentManager fragmentManager;
    final private PermissionHandler permissionHandler;
    final private FragmentActivity activity;

    private ActivityResultCallback<ActivityResult> activityResultCallback;
    private CalendarEvent event;
    private long calendarId;
    private boolean delete;

    /**
     * Creates a new {@link CalendarIntentFragment} object.
     */
    public CalendarIntentFragment() {
        this(null, null);
    }

    /**
     * Creates a new {@link CalendarIntentFragment} object.
     *
     * @param activity               - Activity the fragment is attached to
     * @param activityResultCallback - Callback for when the activity result is received
     */
    public CalendarIntentFragment(Activity activity, @Nullable ActivityResultCallback<ActivityResult> activityResultCallback) {
        super();
        this.event = null;
        this.calendarId = -1;
        this.delete = false;
        this.activity = activity == null ? getActivity() : (FragmentActivity) activity;
        this.fragmentManager = this.activity == null ? null : this.activity.getSupportFragmentManager();
        this.activityResultCallback = activityResultCallback;
        this.permissionHandler = new PermissionHandler(activity, this.fragmentManager, this::startCalendarAction, PermissionHandler.CALENDAR);
    }

    /**
     * Read from or write to the calendar if permission is granted to do so.
     */
    private void startCalendarAction() {
        if (this.event != null) {
            if (this.delete) this.deleteFromCalendar();
            else this.writeToCalendar();
        }
        if (this.activityResultCallback != null) this.activityResultCallback.onActivityResult(null);
    }

    /**
     * Save an event to the default calendar.
     */
    private void writeToCalendar() {
        if (this.event == null || this.calendarId == -1) return;

        ContentResolver contentResolver = this.activity.getContentResolver();
        ContentValues values = new ContentValues();

        TimeZone timeZoneUTC = TimeZone.getTimeZone("UTC");
        TimeZone timeZoneDefault = TimeZone.getDefault();

        Calendar calendar = Calendar.getInstance(timeZoneUTC);
        Date startDate = this.event.getDate();

        // In order to store events as all day events, the time zone must be set to UTC.
        // However, dates should be displayed in the default time zone. Therefore, the offset between
        // UTC and default time zone is added to the hour field of the date.
        long offset = timeZoneDefault.getOffset(startDate.getTime()) - timeZoneUTC.getOffset(startDate.getTime());

        calendar.setTime(startDate);
        calendar.add(Calendar.HOUR, (int) (offset / 1000 / 60 / 60));
        values.put(CalendarContract.Events.DTSTART, calendar.getTimeInMillis());

        values.put(CalendarContract.Events.DTEND, calendar.getTimeInMillis());
        values.put(CalendarContract.Events.TITLE, this.event.getTitle());
        values.put(CalendarContract.Events.DESCRIPTION, "");
        values.put(CalendarContract.Events.UID_2445, UID);

        // Custom Calendar
        values.put(CalendarContract.Events.CALENDAR_ID, this.calendarId);

        values.put(CalendarContract.Events.HAS_ALARM, 1);

        // If allDay is set to 1 eventTimezone must be "UTC" and the time must correspond to a midnight boundary.
        values.put(CalendarContract.Events.ALL_DAY, 1);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZoneUTC.getID());

        contentResolver.insert(CalendarContract.Events.CONTENT_URI, values);

        // Reset the event
        this.event = null;
    }

    /**
     * Delete an event from the calendar.
     */
    private void deleteFromCalendar() {
        if (this.event == null || this.calendarId == -1) return;

        ContentResolver contentResolver = this.activity.getContentResolver();

        String where = String.format("(%s = ?)", CalendarContract.Events._ID);
        String[] selectionArguments = new String[]{String.valueOf(this.event.getId())};

        contentResolver.delete(CalendarContract.Events.CONTENT_URI, where, selectionArguments);
    }

    /**
     * Perform an action on a given event, either deleting or saving it.
     *
     * @param event      - Event to perform the action on
     * @param calendarId - ID of the calendar to use
     * @param delete     - Whether to delete or save the event
     */
    private void calendarAction(CalendarEvent event, long calendarId, boolean delete) {
        this.event = event;
        this.calendarId = calendarId;
        this.delete = delete;

        this.start();
    }

    /**
     * Delete an event from the calendar
     *
     * @param event      - Event to delete
     * @param calendarId - ID of the calendar to use
     */
    public void deleteCalendarEvent(CalendarEvent event, long calendarId) {
        this.calendarAction(event, calendarId, true);
    }

    /**
     * Save an event to the calendar.
     *
     * @param event      - Event to save
     * @param calendarId - ID of the calendar to use
     */
    protected void saveCalendarEvent(CalendarEvent event, long calendarId) {
        this.calendarAction(event, calendarId, false);
    }

    /**
     * Starts the {@link PermissionHandler} to ask for permission to access the calendar
     * or directly carry out calendar operations if the permission was already granted.
     */
    protected void start() {
        boolean fragmentAlreadyAdded = this.fragmentManager.getFragments().contains(this);

        if (!fragmentAlreadyAdded) {
            this.fragmentManager.beginTransaction().add(this, TAG).commitNow();
            this.fragmentManager.executePendingTransactions();
        }

        this.permissionHandler.startActionWithPermission();
    }

    /**
     * Remove the callback that is executed after permission was granted to access the calendar.
     */
    protected void removeCallBack() {
        this.activityResultCallback = null;
    }
}
