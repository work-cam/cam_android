package caa.climbing.app.Views.Hangboard;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Date;
import java.util.Locale;

import caa.climbing.app.Activities.MainActivity;
import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.DatabaseModels.HangboardSession;
import caa.climbing.app.Dialogs.InformDialog.InformDialog;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;
import caa.climbing.app.Views.Inputs.DatePicker.DatePicker;
import caa.climbing.app.Views.Inputs.LabelInput.LabelInput;
import caa.climbing.app.Views.Inputs.NumberInput.NumberInput;
import caa.climbing.app.Views.RangeSeekBar;
import caa.climbing.app.Views.Slider.OnRangeChangedListener;
import caa.climbing.app.Views.Slider.RangeSlider;
import caa.climbing.app.Views.Inputs.TimeInput.TimeInput;

/**
 * A Fragment to display the inputs for setting up a hangboard session
 */
public class HangboardForm extends Fragment {

    DatePicker datePicker;
    LabelInput bodyWeight_input, extraWeight_input, maxStrengthLeft, maxStrengthRight, testEdgeSizeInput;
    RangeSeekBar intensitySlider;
    TimeInput hangTimeInput, restTimeInput, pauseTimeInput;
    NumberInput setsInput, repsInput;
    private DatabaseHelper dbHelper;
    private FlatButton saveSessionBtn, startSessionBtn;
    private View.OnClickListener startSessionListener;

    /**
     * Creates an instance of a {@link HangboardForm}-Fragment.
     */
    public HangboardForm() {
        super(R.layout.hangboard_form);
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param view               - Inflated View
     * @param savedInstanceState - Previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

        if (savedInstanceState == null) {
            this.dbHelper = DatabaseHelper.getInstance(this.getContext());

            this.initViews(view);
            this.setInitialInputValues();

            this.addEventListener();
        }
    }

    /**
     * Initializes the inputs inside the fragment
     *
     * @param view - Inflated View
     */
    private void initViews(View view) {
        this.datePicker = view.findViewById(R.id.hangboard_input_date);
        this.bodyWeight_input = view.findViewById(R.id.hangboard_input_bodyweight_kg);
        this.extraWeight_input = view.findViewById(R.id.hangboard_input_extra_weight_kg);
        this.maxStrengthLeft = view.findViewById(R.id.hangboard_input_maxstrength_left_kg);
        this.maxStrengthRight = view.findViewById(R.id.hangboard_input_maxstrength_right_kg);
        this.testEdgeSizeInput = view.findViewById(R.id.hangboard_test_edge_size_mm);
        this.intensitySlider = view.findViewById(R.id.hanngboard_slider_intensity_setting);

        this.hangTimeInput = view.findViewById(R.id.hang_time_input);
        this.restTimeInput = view.findViewById(R.id.rest_time_input);
        this.pauseTimeInput = view.findViewById(R.id.pause_time_input);
        this.setsInput = view.findViewById(R.id.sets_input);
        this.repsInput = view.findViewById(R.id.reps_input);

        this.saveSessionBtn = view.findViewById(R.id.save_btn_hangboardsession);
        this.startSessionBtn = view.findViewById(R.id.start_hangboard_session_btn);
    }

    /**
     * Sets the initial values of the inputs.
     * If a previous session was saved, the values from that session are used.
     * Otherwise, the inputs are filled with zeroes.
     */
    private void setInitialInputValues() {
        HangboardSession lastSession = this.dbHelper.getLastHangboardSession(this.dbHelper.getReadableDatabase());
        String[] testEdgeSizes = this.dbHelper.getHangboardTestEdgeSizes(this.dbHelper.getReadableDatabase());

        int hang_s = 0;
        int rest_s = 0;
        int pause_s = 0;
        int reps = 0;
        int sets = 0;
        float extraWeight = 0;
        int testEdgeSize = 0;

        if (lastSession != null) {
            hang_s = lastSession.hangTime;
            rest_s = lastSession.restTime;
            pause_s = lastSession.pauseTime;
            reps = lastSession.reps;
            sets = lastSession.sets;
            extraWeight = lastSession.extraWeight;
            testEdgeSize = lastSession.testEdgeSize;
        }

        this.hangTimeInput.setValue(hang_s);
        this.restTimeInput.setValue(rest_s);
        this.pauseTimeInput.setValue(pause_s);
        this.repsInput.setValue(reps);
        this.setsInput.setValue(sets);

        this.extraWeight_input.setValue(String.valueOf(extraWeight));
        this.testEdgeSizeInput.setValue(testEdgeSize == 0 ? "" : String.valueOf(testEdgeSize));
        this.testEdgeSizeInput.setSuggestions(testEdgeSizes);
    }

    /**
     * Create a {@link TextWatcher} that updates the values of the intensity slider.
     *
     * @param updateExtraWeightInput - Listener to update the extraWeight-input
     * @return - TextWatcher that updates the intensity slider
     */
    private TextWatcher updateIntensitySlider(final OnRangeChangedListener updateExtraWeightInput) {

        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                HangboardSession session = getSession();

                if (session.maxStrengthLeft == 0 || session.maxStrengthRight == 0) return;
                float intensity = (session.bodyWeight + session.extraWeight) / (session.maxStrengthLeft + session.maxStrengthRight) * 100;

                intensitySlider.removeOnRangeChangedListener();
                intensitySlider.setProgress(intensity);
                intensitySlider.setProgressDisplay(String.format(Locale.ENGLISH, "%.0f", intensity) + " %");
                intensitySlider.setOnRangeChangeListener(updateExtraWeightInput);
            }
        };
    }

    /**
     * Create a {@link OnRangeChangedListener} that updates the values of the extraWeight-input
     *
     * @return - Listener that updates the values of the extraWeight-input
     */
    private OnRangeChangedListener updateExtraWeightInput() {
        final OnRangeChangedListener defaultListener = intensitySlider.getOnRangeChangeListener();

        return new OnRangeChangedListener() {

            @Override
            public void onRangeChanged(RangeSlider view, float leftValue, float rightValue, boolean isFromUser) {
                defaultListener.onRangeChanged(view, leftValue, rightValue, isFromUser);
                float percent = Math.max(leftValue, rightValue) / 100;

                HangboardSession session = getSession();

                float intensity = percent * (session.maxStrengthLeft + session.maxStrengthRight);
                float extraWeight = intensity - session.bodyWeight;

                extraWeight_input.setValue(String.format(Locale.ENGLISH, "%.1f", extraWeight));
            }

            @Override
            public void onStartTrackingTouch(RangeSlider view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSlider view, boolean isLeft) {

            }
        };
    }

    /**
     * Collect the data from the inputs and return them inside a {@link HangboardSession}-object.
     *
     * @return - The collected data from the inputs.
     */
    private HangboardSession getSession() {
        Date date = datePicker.getValue();
        String bw = bodyWeight_input.getValue();
        String ew = extraWeight_input.getValue();
        String left = maxStrengthLeft.getValue();
        String right = maxStrengthRight.getValue();
        String testEdgeSize = testEdgeSizeInput.getValue();

        float bodyWeight = bw.equals("") ? 0 : Float.parseFloat(bw.replace(",", "."));
        float extraWeight = ew.equals("") ? 0 : Float.parseFloat(ew.replace(",", "."));
        float msLeft = left.equals("") ? 0 : Float.parseFloat(left.replace(",", "."));
        float msRight = right.equals("") ? 0 : Float.parseFloat(right.replace(",", "."));
        int testEdgeSize_mm = testEdgeSize == null || testEdgeSize.equals("") ? 0 : Integer.parseInt(testEdgeSize);

        int hangTime = this.hangTimeInput.getValue();
        int restTime = this.restTimeInput.getValue();
        int pause = this.pauseTimeInput.getValue();
        int sets = this.setsInput.getValue();
        int reps = this.repsInput.getValue();

        return new HangboardSession(date, sets, pause, reps, restTime, hangTime, msLeft, msRight, testEdgeSize_mm, bodyWeight, extraWeight);
    }

    /**
     * Creates a {@link Bundle} containing the values from the timer inputs.
     * This bundle can then be passed to a {@link HangboardTimer}-Fragment.
     *
     * @return - A Bundle containing the necessary times to start the timer
     */
    public Bundle getTimerArguments() {
        Bundle args = new Bundle();
        HangboardSession session = getSession();

        args.putInt(HangboardTimer.HANG_TIME, session.hangTime);
        args.putInt(HangboardTimer.REST_TIME, session.restTime);
        args.putInt(HangboardTimer.PAUSE_TIME, session.pauseTime);
        args.putInt(HangboardTimer.SETS, session.sets);
        args.putInt(HangboardTimer.REPS, session.reps);

        return args;
    }

    /**
     * Sets the listener for the Start button
     *
     * @param clickListener - Listener for the start button
     */
    public void setStartSessionListener(View.OnClickListener clickListener) {
        this.startSessionListener = clickListener;
    }

    /**
     * Creates a listener to save the session
     *
     * @return - Listener for the save button
     */
    private View.OnClickListener saveSession() {
        return (view) -> {
            DatabaseHelper dbHelper = DatabaseHelper.getInstance(getContext());

            // check that required fields are not empty
            HangboardSession session = getSession();

            if (session.date == null) {
                datePicker.markAsInvalid();
                InformDialog.openDialog(getParentFragmentManager(), getString(R.string.hangboard_session_info_date_required));
                return;
            }

            boolean success = dbHelper.saveHangboardSession(dbHelper.getWritableDatabase(), session);

            if (success) {
                MainActivity parentActivity = (MainActivity) getActivity();
                if (parentActivity != null) parentActivity.scrollToTop();

                InformDialog.openDialog(getParentFragmentManager(), getString(R.string.hangboard_session_info_save_successful));
            } else {
                InformDialog.openDialog(getParentFragmentManager(), getString(R.string.hangboard_session_info_save_failed));
            }

        };
    }

    /**
     * Checks if the input values are valid to start a timer.
     *
     * @return - The validity of the inputs
     */
    public boolean checkTimerArguments() {
        HangboardSession session = getSession();

        markAllInputsValid();

        if (session.hangTime == 0) {
            hangTimeInput.markAsInvalid();
            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.hangboard_session_info_hang_required));
            return false;
        }
        if (session.reps > 1 && session.restTime == 0) {
            restTimeInput.markAsInvalid();
            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.hangboard_session_info_rest_required));
            return false;
        }
        if (session.reps == 0) {
            this.repsInput.markAsInvalid();
            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.hangboard_session_info_repetitions_required));
            return false;
        }
        if (session.sets > 1 && session.pauseTime == 0) {
            pauseTimeInput.markAsInvalid();
            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.hangboard_session_info_pause_required));
            return false;
        }
        if (session.sets == 0) {
            setsInput.markAsInvalid();
            InformDialog.openDialog(getParentFragmentManager(), getString(R.string.hangboard_session_info_sets_required));
            return false;
        }

        return true;
    }

    /**
     * Mark all the inputs as valid that can be validated.
     */
    private void markAllInputsValid() {
        this.hangTimeInput.markAsValid();
        this.restTimeInput.markAsValid();
        this.pauseTimeInput.markAsValid();
        this.repsInput.markAsValid();
        this.setsInput.markAsValid();
        this.datePicker.markAsValid();
    }

    /**
     * Add Listeners to the relevant inputs.
     */
    private void addEventListener() {
        final OnRangeChangedListener updateExtraWeightInput = this.updateExtraWeightInput();

        TextWatcher updateIntensitySlider = this.updateIntensitySlider(updateExtraWeightInput);

        bodyWeight_input.addTextChangedListener(updateIntensitySlider);
        maxStrengthLeft.addTextChangedListener(updateIntensitySlider);
        maxStrengthRight.addTextChangedListener(updateIntensitySlider);
        extraWeight_input.addTextChangedListener(updateIntensitySlider);

        intensitySlider.setOnRangeChangeListener(updateExtraWeightInput);

        startSessionBtn.setOnClickListener(this.startSessionListener);
        saveSessionBtn.setOnClickListener(this.saveSession());
    }
}
