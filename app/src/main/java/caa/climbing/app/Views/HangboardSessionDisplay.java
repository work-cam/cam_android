package caa.climbing.app.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import caa.climbing.app.Database.DatabaseModels.HangboardSession;
import caa.climbing.app.HelperClasses.DateFormatter;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;
import caa.climbing.app.Views.Buttons.IconButton;
import caa.climbing.app.Views.Inputs.DatePicker.DatePicker;
import caa.climbing.app.Views.Inputs.LabelInput.LabelInput;
import caa.climbing.app.Views.Inputs.NumberInput.NumberInput;
import caa.climbing.app.Views.Inputs.TimeInput.TimeInput;

import java.util.Date;

/**
 * A view to display the data from a single hangboard session
 */
public class HangboardSessionDisplay extends ExpandableConstraintLayout {

    private final View rootView;
    boolean editMode;
    private ConstraintLayout inputContainer;
    private FlatButton toggleButton;
    private DatePicker date;
    private LabelInput testEdgeSize, maxStrengthLeft, maxStrengthRight, bodyWeight, extraWeight;
    private TimeInput hangTime, restTime, pauseTime;
    private NumberInput reps, sets;
    private IconButton editBtn, deleteBtn, saveBtn, cancelBtn;
    private Integer sessionId;
    private HangboardSession backupSession;

    /**
     * Creates a new {@link HangboardSessionDisplay} object
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public HangboardSessionDisplay(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.rootView = inflate(context, R.layout.hangboard_session_display, this);
        this.sessionId = null;

        this.initViews();
        this.setEditMode(false);
        this.addEventListener();
    }

    /**
     * Creates a new {@link HangboardSessionDisplay} object
     *
     * @param context - Context from which the view was created
     */
    public HangboardSessionDisplay(@NonNull Context context) {
        this(context, null);
    }

    /**
     * Initialize the view.
     */
    private void initViews() {
        this.toggleButton = this.rootView.findViewById(R.id.toggle_button);
        this.inputContainer = this.rootView.findViewById(R.id.input_container);
        this.date = this.rootView.findViewById(R.id.hangboard_input_date);

        this.maxStrengthLeft = this.rootView.findViewById(R.id.hangboard_input_maxstrength_left_kg);
        this.maxStrengthRight = this.rootView.findViewById(R.id.hangboard_input_maxstrength_right_kg);
        this.bodyWeight = this.rootView.findViewById(R.id.hangboard_input_bodyweight_kg);
        this.extraWeight = this.rootView.findViewById(R.id.hangboard_input_extra_weight_kg);
        this.testEdgeSize = this.rootView.findViewById(R.id.hangboard_test_edge_size_mm);

        this.hangTime = this.rootView.findViewById(R.id.hang_time_input);
        this.restTime = this.rootView.findViewById(R.id.rest_time_input);
        this.pauseTime = this.rootView.findViewById(R.id.pause_time_input);

        this.reps = this.rootView.findViewById(R.id.reps_input);
        this.sets = this.rootView.findViewById(R.id.sets_input);

        this.editBtn = this.rootView.findViewById(R.id.edit_btn);
        this.saveBtn = this.rootView.findViewById(R.id.save_btn);
        this.deleteBtn = this.rootView.findViewById(R.id.delete_btn);
        this.cancelBtn = this.rootView.findViewById(R.id.cancel_edit_btn);

        this.setToggleButton(this.toggleButton);
        this.setToggleContainer(this.inputContainer);
        this.setHideViewOnExpansion(this.date);
    }

    /**
     * Get a {@link HangboardSession} object from the inputs.
     *
     * @return - Object that represents the session
     */
    public HangboardSession getSession() {
        Date date = this.date.getValue();
        int sets = this.sets.getValue();
        int pauseTime = this.pauseTime.getValue();
        int reps = this.reps.getValue();
        int restTime = this.restTime.getValue();
        int hangTime = this.hangTime.getValue();
        float maxStrengthLeft = Float.parseFloat(this.maxStrengthLeft.getValue());
        float maxStrengthRight = Float.parseFloat(this.maxStrengthRight.getValue());
        float bodyWeight = Float.parseFloat(this.bodyWeight.getValue());
        float extraWeight = Float.parseFloat(this.extraWeight.getValue());
        int testEdgeSize = Integer.parseInt(this.testEdgeSize.getValue());

        return new HangboardSession(this.sessionId, date, sets, pauseTime, reps, restTime, hangTime, maxStrengthLeft, maxStrengthRight, testEdgeSize, bodyWeight, extraWeight);
    }

    /**
     * Set the display values of the fields based on a hangboard session from the database.
     *
     * @param session - Session to display
     */
    public void setSession(HangboardSession session) {
        this.backupSession = session;

        this.sessionId = session.id;
        this.date.setValue(session.date);
        this.toggleButton.setText(DateFormatter.format(session.date, false));

        this.bodyWeight.setValue(Float.toString(session.bodyWeight));
        this.extraWeight.setValue(Float.toString(session.extraWeight));
        this.maxStrengthLeft.setValue(Float.toString(session.maxStrengthLeft));
        this.maxStrengthRight.setValue(Float.toString(session.maxStrengthRight));
        this.testEdgeSize.setValue(Integer.toString(session.testEdgeSize));
        this.reps.setValue(session.reps);
        this.sets.setValue(session.sets);
        this.hangTime.setValue(session.hangTime);
        this.restTime.setValue(session.restTime);
        this.pauseTime.setValue(session.pauseTime);
    }

    /**
     * Get the backup {@link HangboardSession} from before entering the edit mode.
     *
     * @return - Object that contains the information about the session before editing
     */
    public HangboardSession getBackupSession() {
        return this.backupSession;
    }

    /**
     * Set the editMode of the inputs. If editMode is enabled, inputs can be edited.
     * If it is disabled, inputs functions as readonly fields.
     *
     * @param editMode - True if inputs can be edited, false otherwise
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
        date.setEditMode(editMode);

        bodyWeight.setEditMode(editMode);
        extraWeight.setEditMode(editMode);
        maxStrengthLeft.setEditMode(editMode);
        maxStrengthRight.setEditMode(editMode);
        testEdgeSize.setEditMode(editMode);

        hangTime.setEditMode(editMode);
        restTime.setEditMode(editMode);
        pauseTime.setEditMode(editMode);
        reps.setEditMode(editMode);
        sets.setEditMode(editMode);

        if (editMode) {
            this.backupSession = this.getSession();
            this.saveBtn.setVisibility(VISIBLE);
            this.editBtn.setVisibility(GONE);
            this.deleteBtn.setVisibility(GONE);
            this.cancelBtn.setVisibility(VISIBLE);

            this.date.setVisibility(VISIBLE);
            this.toggleButton.setVisibility(GONE);
        } else {
            this.saveBtn.setVisibility(GONE);
            this.editBtn.setVisibility(VISIBLE);
            this.deleteBtn.setVisibility(VISIBLE);
            this.cancelBtn.setVisibility(GONE);

            this.date.setVisibility(GONE);
            this.toggleButton.setVisibility(VISIBLE);
        }

        this.recalculateInputContainerHeight();
    }

    /**
     * Recalculate the height of the input container.
     * This should be called when the visibility of individual inputs is set to gone,
     * otherwise the buttons at the bottom may be cut off.
     */
    private void recalculateInputContainerHeight() {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) this.inputContainer.getLayoutParams();
        params.height = LayoutParams.WRAP_CONTENT;
        this.inputContainer.setLayoutParams(params);
    }

    /**
     * Add Listeners to the buttons.
     */
    private void addEventListener() {
        this.editBtn.setOnClickListener((view) -> this.setEditMode(true));
    }

    /**
     * Set a clickListener for the delete button.
     *
     * @param clickListener - Listener for the delete button
     */
    public void setDeleteAction(OnClickListener clickListener) {
        this.deleteBtn.setOnClickListener(clickListener);
    }

    /**
     * Set a clickListener for the cancel button.
     *
     * @param clickListener - Listener for the cancel button
     */
    public void setCancelAction(OnClickListener clickListener) {
        this.cancelBtn.setOnClickListener(clickListener);
    }

    /**
     * Set a clickListener for the save button.
     *
     * @param clickListener - Listener for the save button
     */
    public void setUpdateAction(OnClickListener clickListener) {
        this.saveBtn.setOnClickListener(clickListener);
    }
}
