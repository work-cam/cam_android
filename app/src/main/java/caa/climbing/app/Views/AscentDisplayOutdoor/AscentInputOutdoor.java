package caa.climbing.app.Views.AscentDisplayOutdoor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentManager;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.Database.DatabaseModels.OutdoorAscent;
import caa.climbing.app.Dialogs.ConfirmDialog.ConfirmDialog;
import caa.climbing.app.Dialogs.SelectDialog.SelectDialog;
import caa.climbing.app.HelperClasses.*;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;
import caa.climbing.app.Views.Buttons.IconButton;
import caa.climbing.app.Views.ExpandableConstraintLayout;
import caa.climbing.app.Views.Inputs.DatePicker.DatePicker;
import caa.climbing.app.Views.Inputs.LabelInput.LabelInput;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * A view to add a new ascent from an outdoor session.
 */
public class AscentInputOutdoor extends ExpandableConstraintLayout {

    final private FragmentManager fragmentManager;
    final private Context context;
    final private ArrayList<LabelledInt> grades, methods;
    public LabelInput ascentName, ascentCrag;
    public IconButton cameraBtn, galleryBtn;
    public ImageView imageView;
    public DatePicker datePicker;
    public FlatButton methodSelectorBtn, gradeSelectorBtn;
    public IconButton editBtn, saveBtn, deleteBtn, cancelEditBtn;
    private ConstraintLayout background;
    private OutdoorAscent ascent;
    private OutdoorAscent backupAscent;
    private boolean editMode;

    /**
     * Creates a new {@link AscentInputOutdoor} object.
     *
     * @param context   - Context from which the view was created
     * @param attrs     - Attributes that are passed to the view
     * @param climbType - Type of the climb, either boulder or route
     * @param grades    - Possible grades of the climb
     * @param methods   - Possible methods of topping
     */
    public AscentInputOutdoor(Context context, AttributeSet attrs, int climbType, ArrayList<LabelledInt> grades, ArrayList<LabelledInt> methods) {
        super(context, attrs);
        setSaveEnabled(true);
        inflate(context, R.layout.ascent_input_outdoor, this);

        this.editMode = true;
        this.context = context;
        this.grades = grades;
        this.methods = methods;
        this.fragmentManager = Utils.getActivity(context).getSupportFragmentManager();

        this.ascent = new OutdoorAscent(climbType);

        this.initViews();
        this.addEventListener();
    }

    /**
     * Creates a new {@link AscentInputOutdoor} object.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public AscentInputOutdoor(Context context, AttributeSet attrs) {
        this(context, attrs, -1, null, null);
    }

    /**
     * Initializes the views.
     */
    private void initViews() {
        this.background = findViewById(R.id.ascent_input_background);
        FlatButton toggleButton = findViewById(R.id.toggle_button);
        ConstraintLayout toggleContainer = findViewById(R.id.ascent_input_container);
        this.setToggleContainer(toggleContainer);
        this.setToggleButton(toggleButton);

        this.ascentName = findViewById(R.id.ascent_name);
        this.ascentCrag = findViewById(R.id.ascent_crag);
        this.cameraBtn = findViewById(R.id.add_photo_from_camera);
        this.galleryBtn = findViewById(R.id.add_photo_from_gallery);
        this.imageView = findViewById(R.id.ascent_img);

        this.datePicker = findViewById(R.id.outdoor_ascent_datepicker);
        this.gradeSelectorBtn = findViewById(R.id.outdoor_ascent_grade_selector_btn);
        this.methodSelectorBtn = findViewById(R.id.outdoor_ascent_method_selector_btn);
        this.editBtn = findViewById(R.id.edit_ascent_btn);
        this.saveBtn = findViewById(R.id.save_ascent_btn);
        this.cancelEditBtn = findViewById(R.id.cancel_edit_ascent_btn);
        this.deleteBtn = findViewById(R.id.delete_ascent_btn);

        this.setHideViewOnExpansion(this.ascentName, this.gradeSelectorBtn);
    }

    /**
     * Add Listeners to the buttons.
     */
    private void addEventListener() {
        this.editBtn.setOnClickListener((view) -> this.setEditMode(true));
        this.setCameraAction();
        this.setGalleryAction();
        this.addTextChangedListener();
        this.gradeSelectorBtn.setOnClickListener((view) -> this.openGradeSelectDialog());
        this.methodSelectorBtn.setOnClickListener((view) -> this.openMethodSelectDialog());
    }

    /**
     * Set the value of the input for the ascent name.
     *
     * @param name - Name of the boulder/route
     */
    public void setName(String name) {
        ascentName.setValue(name);
    }

    /**
     * Loads an image file from a Uri and displays that image.
     *
     * @param uriString - String representation of the Uri of the image to display
     */
    public void setImageView(String uriString) {
        if (uriString == null) return;
        Uri photoUri = Uri.parse(uriString);

        this.setImageClickAction(photoUri);

        Picasso.get()
                .load(photoUri)
                .placeholder(R.drawable.box_white__border)
                .error(R.drawable.box_white__border)
                .fit()
                .centerCrop()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("Picasso Error", e.toString());
                    }
                });
    }

    /**
     * Add a click listener to the image view to show that image.
     *
     * @param photoUri - Uri to the image
     */
    private void setImageClickAction(@NonNull Uri photoUri) {
        Activity activity = Utils.getActivity(context);
        ViewIntentFragment viewIntentFragment = new ViewIntentFragment(photoUri, activity);
        this.imageView.setOnClickListener((view) -> viewIntentFragment.openImage());
        this.imageView.setOnLongClickListener((view) -> {
            if (this.editMode) this.openDeleteImageConfirmDialog();
            return false;
        });

    }

    /**
     * Opens a confirmation dialog to confirm that the photo should be removed from the ascent.
     * The photo will NOT be deleted from the device.
     */
    private void openDeleteImageConfirmDialog() {
        View.OnClickListener okClick = v -> {
            this.ascent.photoUriString = null;
            this.emptyImageView();
        };
        ConfirmDialog confirm = new ConfirmDialog(this.context.getString(R.string.outdoor_ascent_confirm_delete_image), okClick, null);

        confirm.show(this.fragmentManager, "confirm");
    }

    /**
     * Clears the ImageView.
     */
    public void emptyImageView() {
        this.imageView.setOnClickListener(null);
        this.imageView.setOnLongClickListener(null);

        Picasso.get()
                .load(R.drawable.box_white__border)
                .error(R.drawable.box_white__border)
                .fit()
                .centerCrop()
                .into(imageView);
    }

    /**
     * Set a clickListener for the delete button.
     *
     * @param clickListener - Listener for the delete button
     */
    public void setDeleteAction(OnClickListener clickListener) {
        this.deleteBtn.setOnClickListener(clickListener);
    }

    /**
     * Set a clickListener for the cancel button.
     *
     * @param clickListener - Listener for the cancel button
     */
    public void setCancelAction(OnClickListener clickListener) {
        this.cancelEditBtn.setOnClickListener(clickListener);
    }

    /**
     * Opens a {@link SelectDialog} to select the grade of the route/boulder.
     */
    private void openGradeSelectDialog() {
        SelectDialog select = new SelectDialog(this.context.getString(R.string.outdoor_ascent_select_grade), this.ascent.gradeId, this.grades);

        View.OnClickListener okClick = (view) -> {
            LabelledInt selectedGrade = select.getSelectedOption();
            this.ascent.grade = selectedGrade;
            this.ascent.gradeId = selectedGrade.value;
            this.gradeSelectorBtn.setText(selectedGrade.label);
        };

        select.setOkClick(okClick);

        select.show(this.fragmentManager, "select");
    }

    /**
     * Opens a {@link SelectDialog} to select the method of topping of the route/boulder.
     */
    private void openMethodSelectDialog() {
        SelectDialog select = new SelectDialog(this.context.getString(R.string.outdoor_ascent_select_method), this.ascent.method, this.methods);

        View.OnClickListener okClick = (view) -> {
            LabelledInt selectedMethod = select.getSelectedOption();
            this.ascent.method = selectedMethod.value;
            this.methodSelectorBtn.setText(selectedMethod.label);
        };

        select.setOkClick(okClick);

        select.show(this.fragmentManager, "select");
    }

    /**
     * Set the action for the camera button, so that a photo can be taken with the camera.
     */
    private void setCameraAction() {
        Activity activity = Utils.getActivity(this.context);
        ActivityResultCallback<ActivityResult> activityResultCallback = (result) -> {
            if (result.getResultCode() == RESULT_OK) {
                Intent data = result.getData();
                if (data != null && data.getData() != null) {
                    Uri photoUri = data.getData();
                    String uriString = photoUri == null ? null : photoUri.toString();
                    ascent.photoUriString = uriString;
                    this.setImageView(uriString);
                }
            }
        };

        CameraIntentFragment cameraIntentFragment = new CameraIntentFragment(activity, activityResultCallback);

        this.cameraBtn.setOnClickListener((view) -> cameraIntentFragment.openCamera());
    }

    /**
     * Set the action for the gallery button, so that a picture can be selected from the device storage.
     */
    private void setGalleryAction() {
        Activity activity = Utils.getActivity(context);
        ActivityResultCallback<ActivityResult> activityResultCallback = (result) -> {
            if (result.getResultCode() == RESULT_OK) {
                Intent data = result.getData();
                Uri photoUri = data == null ? null : data.getData();
                String uriString = photoUri == null ? null : photoUri.toString();
                ascent.photoUriString = uriString;
                this.setImageView(uriString);
            }
        };

        GalleryIntentFragment galleryIntentFragment = new GalleryIntentFragment(activity, activityResultCallback);

        this.galleryBtn.setOnClickListener((view) -> galleryIntentFragment.openGallery());
    }

    /**
     * Add TextChangedListener to the text inputs in order to keep the GUI and data in sync.
     */
    private void addTextChangedListener() {
        this.ascentName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                ascent.ascentName = s.toString();
            }
        });

        this.ascentCrag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                ascent.ascentCrag = s.toString();
            }
        });

        this.datePicker.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                ascent.date = datePicker.getValue();
            }
        });
    }

    /**
     * Set a clickListener for the save button.
     *
     * @param clickListener - Listener for the save button
     */
    public void setUpdateAction(OnClickListener clickListener) {
        this.saveBtn.setOnClickListener(clickListener);
    }

    /**
     * Set whether the view can be toggled between input and readonly mode.
     * When enabled, additional buttons for editing and updating are displayed.
     *
     * @param enabled - Whether the view can be toggled between input and readonly mode
     */
    public void setToggleEditEnabled(boolean enabled) {
        if (enabled) {
            this.enableToggling();
        } else {
            this.disableToggling();
            this.saveBtn.setVisibility(GONE);
            this.editBtn.setVisibility(GONE);
            this.cancelEditBtn.setVisibility(GONE);

            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) this.deleteBtn.getLayoutParams();
            layoutParams.startToEnd = R.id.guideline_middle;
            layoutParams.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID;

            this.deleteBtn.setLayoutParams(layoutParams);
        }
    }

    /**
     * Enable toggling the expansion/collapse of the input container by showing the toggle button
     * and hiding the input for the ascent name and grade.
     */
    private void enableToggling() {
        this.toggleButton.setVisibility(VISIBLE);
        this.ascentName.setVisibility(GONE);
        this.gradeSelectorBtn.setVisibility(GONE);

        this.recalculateInputContainerHeight();
    }

    /**
     * Disable toggling the expansion/collapse of the input container by hiding the toggle button
     * and showing the input for the ascent name and grade.
     */
    private void disableToggling() {
        this.toggleButton.setVisibility(GONE);
        this.ascentName.setVisibility(VISIBLE);
        this.gradeSelectorBtn.setVisibility(VISIBLE);

        this.recalculateInputContainerHeight();
    }

    /**
     * Recalculate the height of the input container.
     * This should be called when the visibility of individual inputs is set to gone,
     * otherwise the image view may be cut off at the bottom.
     */
    private void recalculateInputContainerHeight() {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) this.toggleContainer.getLayoutParams();
        params.height = LayoutParams.WRAP_CONTENT;
        this.toggleContainer.setLayoutParams(params);
    }

    /**
     * Enable the editMode for the inputs.
     * If editing is allowed, toggling of the input container is disabled.
     */
    private void enable() {
        this.editMode = true;

        this.backupAscent = new OutdoorAscent(this.ascent);

        Drawable defaultBackground = ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.textfield_background, getContext().getTheme());

        this.disableToggling();

        ascentCrag.setEditMode(true);
        ascentName.setEditMode(true);
        datePicker.setEditMode(true);
        cameraBtn.setVisibility(VISIBLE);
        galleryBtn.setVisibility(VISIBLE);
        gradeSelectorBtn.setEnabled(true);
        gradeSelectorBtn.setBackground(defaultBackground);
        methodSelectorBtn.setEnabled(true);
        methodSelectorBtn.setBackground(defaultBackground);

        this.editBtn.setVisibility(GONE);
        this.saveBtn.setVisibility(VISIBLE);

        this.deleteBtn.setVisibility(GONE);
        this.cancelEditBtn.setVisibility(VISIBLE);

    }

    /**
     * Disable the editMode for the inputs.
     * If editing is disabled, toggling of the input container is enabled.
     */
    private void disable() {
        this.editMode = false;

        this.backupAscent = null;

        this.enableToggling();

        ascentCrag.setEditMode(false);
        ascentName.setEditMode(false);
        datePicker.setEditMode(false);
        cameraBtn.setVisibility(GONE);
        galleryBtn.setVisibility(GONE);
        gradeSelectorBtn.setEnabled(false);
        gradeSelectorBtn.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        gradeSelectorBtn.setBackground(null);
        methodSelectorBtn.setEnabled(false);
        methodSelectorBtn.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        methodSelectorBtn.setBackground(null);

        this.editBtn.setVisibility(VISIBLE);
        this.saveBtn.setVisibility(GONE);

        this.deleteBtn.setVisibility(VISIBLE);
        this.cancelEditBtn.setVisibility(GONE);
    }

    /**
     * Set the editMode of the inputs. If editMode is enabled, inputs can be edited.
     * If it is disabled, inputs functions as readonly fields.
     *
     * @param editMode - True if inputs can be edited, false otherwise
     */
    public void setEditMode(boolean editMode) {
        if (editMode) enable();
        else disable();
    }

    /**
     * Optimize the view for display in narrow spaces.
     *
     * @param smallViewPort - True if view is displayed in a narrow space, false otherwise
     */
    public void setSmallViewPort(boolean smallViewPort) {
        ascentName.setSmallViewPort(smallViewPort);
        ascentCrag.setSmallViewPort(smallViewPort);
        datePicker.setSmallViewPort(smallViewPort);

        float smallFontSize = Utils.sp2px(context, getResources().getDimension(R.dimen.fontsize_small));
        float standardFontSize = Utils.sp2px(context, getResources().getDimension(R.dimen.fontsize_standard));
        float setFontSize = smallViewPort ? smallFontSize : standardFontSize;

        float paddingTopSmall = getResources().getDimension(R.dimen.ascent_display_padding_top__small);
        float paddingTopStandard = getResources().getDimension(R.dimen.ascent_display_padding_top);
        int setPaddingTop = (int) (smallViewPort ? paddingTopSmall : paddingTopStandard);

        float paddingBottomSmall = getResources().getDimension(R.dimen.ascent_display_padding_bottom__small);
        float paddingBottomStandard = getResources().getDimension(R.dimen.ascent_display_padding_bottom);
        int setPaddingBottom = (int) (smallViewPort ? paddingBottomSmall : paddingBottomStandard);

        int paddingLeft = (int) getResources().getDimension(R.dimen.ascent_display_padding_left);
        int paddingRight = (int) getResources().getDimension(R.dimen.ascent_display_padding_right);

        this.background.setPadding(paddingLeft, setPaddingTop, paddingRight, setPaddingBottom);

        if (smallViewPort) {
            this.toggleContainer.setPadding(0, 0, 0, (int) paddingBottomStandard);
        }

        this.toggleButton.setTextSize(setFontSize);
        this.gradeSelectorBtn.setTextSize(setFontSize);
        this.methodSelectorBtn.setTextSize(setFontSize);

    }

    /**
     * Marks all inputs as valid.
     */
    private void markAllAsValid() {
        Drawable defaultBackground = ResourcesCompat.getDrawable(getResources(), R.drawable.textfield_background, getContext().getTheme());

        this.ascentName.markAsValid();
        this.ascentName.markAsValid();
        this.datePicker.markAsValid();
        this.gradeSelectorBtn.setBackground(defaultBackground);
        this.methodSelectorBtn.setBackground(defaultBackground);
    }

    /**
     * Check for invalid inputs.
     * If any are found, they are marked accordingly.
     *
     * @return - Whether any input contains invalid data
     */
    public boolean inputsInvalid() {
        boolean inputsInvalid = false;
        this.markAllAsValid();

        Drawable errorBackground = ResourcesCompat.getDrawable(getResources(), R.drawable.textfield_background__invalid, getContext().getTheme());

        if (ascent.ascentName == null || ascent.ascentName.replaceAll("\\s", "").equals("")) {
            inputsInvalid = true;
            this.ascentName.markAsInvalid();
        }
        if (ascent.ascentCrag == null || ascent.ascentCrag.replaceAll("\\s", "").equals("")) {
            inputsInvalid = true;
            this.ascentCrag.markAsInvalid();
        }
        if (ascent.date == null) {
            inputsInvalid = true;
            this.datePicker.markAsInvalid();
        }
        if (ascent.gradeId == -1) {
            inputsInvalid = true;
            this.gradeSelectorBtn.setBackground(errorBackground);
        }
        if (ascent.method == -1) {
            inputsInvalid = true;
            this.methodSelectorBtn.setBackground(errorBackground);
        }

        return inputsInvalid;
    }

    /**
     * Set the suggestions that should be displayed for the crag input.
     *
     * @param suggestions - Suggestions to show in the crag input
     */
    public void setCragSuggestions(String[] suggestions) {
        this.ascentCrag.setSuggestions(suggestions);
    }

    /**
     * Get a {@link OutdoorAscent} object from the inputs.
     *
     * @return - Object that contains the information about the ascent
     */
    public OutdoorAscent getAscent() {
        return this.ascent;
    }

    /**
     * Set the value of this view, which is an {@link OutdoorAscent}.
     *
     * @param ascent - New value
     */
    public void setAscent(OutdoorAscent ascent) {
        this.ascent = ascent;

        String toggleButtonText = ascent.grade == null ? ascent.ascentName : ascent.ascentName + ", " + ascent.grade.label;

        this.toggleButton.setText(toggleButtonText);
    }

    /**
     * Get the backup {@link OutdoorAscent} from before entering the edit mode.
     *
     * @return - Object that contains the information about the ascent before editing
     */
    public OutdoorAscent getBackupAscent() {
        return this.backupAscent;
    }
}
