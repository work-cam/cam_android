package caa.climbing.app.Views;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.IconButton;

/**
 *
 */

public class TopBar extends ConstraintLayout{
    final private IconButton menu_btn;
    final private TextView topBarTitle;

    /**
     *
     * @param context - Context where the topBar was created
     * @param attributes - Optional attributes
     */
    public TopBar(Context context, @Nullable AttributeSet attributes) {
        super(context, attributes);
        inflate(context, R.layout.top_bar, this);
        menu_btn = findViewById(R.id.hamburgerMenuBtn);
        topBarTitle = findViewById(R.id.topBarTitle);


        TypedArray a = context.obtainStyledAttributes(attributes, R.styleable.Topbar, 0, 0);
        try {
            String title = a.getString(R.styleable.Topbar_title);
            topBarTitle.setText(title);
        } finally {
            a.recycle();
        }

    }

    /**
     * Add a clickListener to the menu button.
     *
     * @param clickListener - ClickListener
     */
    public void setMenuButtonClick(View.OnClickListener clickListener) {
        menu_btn.setOnClickListener(clickListener);
    }

    /**
     * Set the title for the topBar.
     *
     * @param title - New Title for the topBar
     */
    public void setTitle(String title) {
        this.topBarTitle.setText(title);
    }
}









