package caa.climbing.app.Views.Chart;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import caa.climbing.app.R;

import androidx.constraintlayout.widget.ConstraintLayout;

public class LegendEntry extends ConstraintLayout {

    View colorDisplay;
    TextView label;

    public LegendEntry(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.legend_entry, this);

        colorDisplay = findViewById(R.id.color_display);
        label = findViewById(R.id.label);
        label.setTextColor(ResourcesCompat.getColor(getResources(), R.color.gray800, null));

    }

    public void setColor(int color) {
        colorDisplay.setBackgroundColor(color);
    }
    public void setLabel(String label) {
        this.label.setText(label);
    }
}
