package caa.climbing.app.Views.Slider;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

public class SavedSliderState extends View.BaseSavedState {

    public float minValue;
    public float maxValue;
    public float rangeInterval;
    public int tickNumber;
    public float currSelectedMin;
    public float currSelectedMax;

    public SavedSliderState(Parcelable superState) {
        super(superState);
    }

    private SavedSliderState(Parcel in) {
        super(in);
        minValue = in.readFloat();
        maxValue = in.readFloat();
        rangeInterval = in.readFloat();
        tickNumber = in.readInt();
        currSelectedMin = in.readFloat();
        currSelectedMax = in.readFloat();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeFloat(minValue);
        out.writeFloat(maxValue);
        out.writeFloat(rangeInterval);
        out.writeInt(tickNumber);
        out.writeFloat(currSelectedMin);
        out.writeFloat(currSelectedMax);
    }

    public static final Parcelable.Creator<SavedSliderState> CREATOR = new Parcelable.Creator<SavedSliderState>() {
        public SavedSliderState createFromParcel(Parcel in) {
            return new SavedSliderState(in);
        }

        public SavedSliderState[] newArray(int size) {
            return new SavedSliderState[size];
        }
    };
}
