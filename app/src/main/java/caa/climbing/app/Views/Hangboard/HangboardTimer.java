package caa.climbing.app.Views.Hangboard;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;
import caa.climbing.app.Views.Buttons.IconButton;

/**
 * A Fragment to display a timer for hangboard session.
 */
public class HangboardTimer extends Fragment {
    static final public String HANG_TIME = "hangTime";
    static final public String REST_TIME = "restTime";
    static final public String PAUSE_TIME = "pauseTime";
    static final public String SETS = "sets";
    static final public String REPS = "reps";

    static private final int VIBRATION_SHORT = 120;
    static private final int VIBRATION_LONG = 350;

    static private final int SHORT_INTERVAL = 10;
    static private final int GET_READY_TIME_S = 10;

    private String getReadyMessage, hangMessage, pauseMessage, restMessage, finishedMessage;

    private boolean playSound = true;
    private SoundPool soundPool;
    private int tickSound, finishSound;

    private IconButton startStopButton, resetButton, volumeOnOffButton;
    private FlatButton finishBtn;
    private TextView activityDisplay, setsCompleted, repsCompleted, timerDisplay;

    private int hangTime_s, rest_s, pause_s;
    private int sets2Complete, reps2Complete;

    private boolean isRunning;
    private boolean started;
    private CountDownTimerPausable restTimer, pauseTimer, hangTimer, getReadyTimer;

    private Vibrator vibrator;
    private AudioManager audioManager;
    private int currentVolume;

    private int completedReps, completedSets;

    private ContentObserver audioObserver;

    private View.OnClickListener finishListener;
    private FragmentActivity activity;

    private OnBackPressedCallback backPressedCallback;

    /**
     * Create a new {@link HangboardTimer} instance.
     */
    public HangboardTimer() {
        super(R.layout.hangoard_timer);
        this.activity = getActivity();
    }

    /**
     * Initializes the functionality after the view of the component was inflated.
     *
     * @param view               - Inflated View
     * @param savedInstanceState - previous Instance
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        this.activity = getActivity();

        this.getReadyMessage = getString(R.string.hangboard_session_message_get_ready);
        this.hangMessage = getString(R.string.hangboard_session_message_hang);
        this.restMessage = getString(R.string.hangboard_session_message_rest);
        this.pauseMessage = getString(R.string.hangboard_session_message_pause);
        this.finishedMessage = getString(R.string.hangboard_session_message_finished);

        this.isRunning = false;
        this.started = false;

        this.completedReps = 0;

        Bundle args = getArguments();
        if (args != null) {
            this.hangTime_s = args.getInt(HANG_TIME, 0);
            this.rest_s = args.getInt(REST_TIME, 0);
            this.pause_s = args.getInt(PAUSE_TIME, 0);
            this.sets2Complete = args.getInt(SETS, 0);
            this.reps2Complete = args.getInt(REPS, 0);

            this.initAudio();
            this.initViews(view);
            this.initTimers();
            this.addEventListener();
        }
    }

    /**
     * Initialize the audio/vibration.
     */
    private void initAudio() {
        if (this.activity == null) return;

        this.vibrator = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
        this.audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
        this.currentVolume = this.audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        if (this.currentVolume == 0) this.playSound = false;

        this.audioObserver = new ContentObserver(new Handler()) {
            // https://stackoverflow.com/a/30441548/12755273
            @Override
            public boolean deliverSelfNotifications() {
                return false;
            }

            @Override
            public void onChange(boolean selfChange) {
                onAudioObserverChange();
            }
        };

        this.activity.getApplicationContext().getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this.audioObserver);

        this.soundPool = new SoundPool.Builder().build();

        this.tickSound = this.soundPool.load(this.getContext(), R.raw.beep_tick, 1);
        this.finishSound = this.soundPool.load(this.getContext(), R.raw.beep_finish, 1);
    }

    /**
     * Callback when the audio observer changes.
     * Toggle whether the audio is played (and at which volume) or the device is vibrated on timer ticks.
     */
    private void onAudioObserverChange() {
        if (this.audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0) {
            this.playSound = false;
            this.volumeOnOffButton.switchIcon(IconButton.MUTE);
        } else {
            this.currentVolume = this.audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            this.playSound = true;
            this.volumeOnOffButton.switchIcon(IconButton.UNMUTE);
        }
    }

    /**
     * Initializes the views.
     *
     * @param view - Inflated root view
     */
    private void initViews(View view) {
        this.startStopButton = view.findViewById(R.id.start_stop_timer_btn);
        this.resetButton = view.findViewById(R.id.reset_timer_btn);
        this.volumeOnOffButton = view.findViewById(R.id.volume_on_off_btn);
        this.activityDisplay = view.findViewById(R.id.activity_display);
        this.setsCompleted = view.findViewById(R.id.sets_completed);
        this.repsCompleted = view.findViewById(R.id.reps_completed);
        this.timerDisplay = view.findViewById(R.id.timer_display);
        this.finishBtn = view.findViewById(R.id.finish_hangboard_timer_btn);

        if (this.playSound) this.volumeOnOffButton.switchIcon(IconButton.UNMUTE);
        else this.volumeOnOffButton.switchIcon(IconButton.MUTE);
    }

    /**
     * Common callback for ticks of a timer
     *
     * @param timer               - Timer to apply the callback to
     * @param millisUntilFinished - Milliseconds until the time finishes
     */
    private void onTickTimer(CountDownTimerPausable timer, long millisUntilFinished, boolean onlyDuringPreparation, @Nullable String message) {

        boolean preparationTime = millisUntilFinished <= GET_READY_TIME_S * 1000;

        int currentSeconds = (int) (millisUntilFinished / 1000) % 60;

        if (currentSeconds < timer.prevSeconds && timer.prevSeconds > 0 && millisUntilFinished < timer.getMillisInFuture() - 1000) {
            if (preparationTime) {
                if (playSound) soundPool.play(tickSound, 1, 1, 1, 0, 1);
                vibrate(VIBRATION_SHORT);
            }
            if (onlyDuringPreparation && preparationTime) {
                if (message != null) activityDisplay.setText(message);
            }
        }
        this.timerDisplay.setText(formatMillisToTime(millisUntilFinished));
        timer.prevSeconds = currentSeconds;
    }

    /**
     * Common callback to call when a timer finishes
     *
     * @param timer - Timer to apply the callback to
     */
    private void onFinishTimer(CountDownTimerPausable timer) {
        if (this.playSound) this.soundPool.play(this.finishSound, 1, 1, 1, 0, 1);
        vibrate(VIBRATION_LONG);
        timer.cancel();
    }

    /**
     * Initialize the individual timers for getting ready, hanging, resting, and pausing.
     */
    private void initTimers() {
        this.updateSetsAndRepsDisplay();
        this.activityDisplay.setText(this.getReadyMessage);
        this.timerDisplay.setText(formatMillisToTime(GET_READY_TIME_S * 1000));

        this.initGetReadyTimer();
        this.initHangTimer();
        this.initRestTimer();
        this.initPauseTimer();
    }

    /**
     * Initialize the timer for getting ready.
     */
    private void initGetReadyTimer() {
        this.getReadyTimer = new CountDownTimerPausable(GET_READY_TIME_S * 1000, SHORT_INTERVAL) {

            @Override
            public void onTick(long millisUntilFinished) {
                onTickTimer(this, millisUntilFinished, false, null);
            }

            @Override
            public void onFinish() {
                onFinishTimer(this);
                activityDisplay.setText(hangMessage);
                hangTimer.start();
            }
        };
    }

    /**
     * Initialize the timer for hanging.
     */
    private void initHangTimer() {
        this.hangTimer = new CountDownTimerPausable((long) 1000 * this.hangTime_s, SHORT_INTERVAL) {

            @Override
            public void onTick(long millisUntilFinished) {
                onTickTimer(this, millisUntilFinished, false, null);
            }

            @Override
            public void onFinish() {
                completedReps++;
                updateSetsAndRepsDisplay();

                onFinishTimer(this);

                if (completedReps < reps2Complete) {
                    activityDisplay.setText(restMessage);
                    restTimer.start();
                } else {
                    completedSets++;
                    updateSetsAndRepsDisplay();
                    if (completedSets < sets2Complete) {
                        activityDisplay.setText(pauseMessage);
                        pauseTimer.start();
                    } else {
                        reset(true);
                    }
                }
            }
        };
    }

    /**
     * Initialize the timer for resting.
     */
    private void initRestTimer() {
        this.restTimer = new CountDownTimerPausable((long) 1000 * this.rest_s, SHORT_INTERVAL) {

            @Override
            public void onTick(long millisUntilFinished) {
                onTickTimer(this, millisUntilFinished, true, null);
            }

            @Override
            public void onFinish() {
                onFinishTimer(this);
                if (completedSets < sets2Complete) {
                    activityDisplay.setText(hangMessage);
                    hangTimer.start();
                }
            }
        };
    }

    /**
     * Initialize the timer for pausing.
     */
    private void initPauseTimer() {
        this.pauseTimer = new CountDownTimerPausable((long) 1000 * pause_s, SHORT_INTERVAL) {

            @Override
            public void onTick(long millisUntilFinished) {
                onTickTimer(this, millisUntilFinished, true, getReadyMessage);
            }

            @Override
            public void onFinish() {
                onFinishTimer(this);
                completedReps = 0;
                updateSetsAndRepsDisplay();

                if (completedSets < sets2Complete) {
                    activityDisplay.setText(hangMessage);
                    hangTimer.start();
                }
            }
        };
    }

    /**
     * Pause/restart the currently running timer.
     */
    private void startStopButtonClick() {
        this.isRunning = !this.isRunning;
        if (this.getReadyTimer.isPaused() && !this.getReadyTimer.isCancelled()) {
            this.getReadyTimer.resume();
            this.startStopButton.switchIcon(IconButton.PAUSE);
        } else if (this.getReadyTimer.isRunning() && !this.getReadyTimer.isCancelled()) {
            this.getReadyTimer.pause();
            this.startStopButton.switchIcon(IconButton.PLAY);
        }
        if (this.restTimer.isPaused() && !this.restTimer.isCancelled()) {
            this.restTimer.resume();
            this.startStopButton.switchIcon(IconButton.PAUSE);
        } else if (this.restTimer.isRunning() && !this.restTimer.isCancelled()) {
            this.restTimer.pause();
            this.startStopButton.switchIcon(IconButton.PLAY);
        }
        if (this.hangTimer.isPaused() && !this.hangTimer.isCancelled()) {
            this.hangTimer.resume();
            this.startStopButton.switchIcon(IconButton.PAUSE);
        } else if (this.hangTimer.isRunning() && !this.hangTimer.isCancelled()) {
            this.hangTimer.pause();
            this.startStopButton.switchIcon(IconButton.PLAY);
        }
        if (this.pauseTimer.isPaused() && !this.pauseTimer.isCancelled()) {
            this.pauseTimer.resume();
            this.startStopButton.switchIcon(IconButton.PAUSE);
        } else if (this.pauseTimer.isRunning() && !this.pauseTimer.isCancelled()) {
            this.pauseTimer.pause();
            this.startStopButton.switchIcon(IconButton.PLAY);
        }

        if (!this.started) {
            if (this.playSound) this.soundPool.play(this.tickSound, 1, 1, 1, 0, 1);
            vibrate(VIBRATION_SHORT);
            this.getReadyTimer.start();
            this.started = true;
            this.startStopButton.switchIcon(IconButton.PAUSE);
        }
    }

    /**
     * Reset the timers and update the UI.
     *
     * @param finished - Indicates whether the session was completed
     */
    private void reset(boolean finished) {
        this.resetTimers();

        String message;
        long time;

        if (finished) {
            message = this.finishedMessage;
            time = 0;
        } else {
            message = this.getReadyMessage;
            time = (long) GET_READY_TIME_S * 1000;

            this.completedReps = 0;
            this.completedSets = 0;
            this.updateSetsAndRepsDisplay();
        }

        this.timerDisplay.setText(formatMillisToTime(time));
        this.activityDisplay.setText(message);
    }

    /**
     * Reset the timers and update the UI.
     */
    private void reset() {
        this.reset(false);
    }

    /**
     * Reset the individual timers for getting ready, hanging, resting, and pausing.
     */
    private void resetTimers() {
        this.isRunning = false;
        this.started = false;

        this.startStopButton.switchIcon(IconButton.PLAY);

        this.getReadyTimer.cancel();
        this.restTimer.cancel();
        this.hangTimer.cancel();
        this.pauseTimer.cancel();
    }

    /**
     * Update the display of the completed sets and reps.
     */
    private void updateSetsAndRepsDisplay() {
        String formatString = "%s / %s";
        this.setsCompleted.setText(String.format(formatString, this.completedSets, this.sets2Complete));
        this.repsCompleted.setText(String.format(formatString, this.completedReps, this.reps2Complete));
    }

    /**
     * Finish the timer and reset all individual timers.
     */
    public void finish() {
        reset();
        this.activity.getApplicationContext().getContentResolver().unregisterContentObserver(this.audioObserver);
    }

    /**
     * Set the OnClickListener for the finish button.
     */
    public void setFinishSessionListener(View.OnClickListener clickListener) {
        this.finishListener = clickListener;
    }

    /**
     * Set a callback that should be executed instead of the default action when the back button is pressed.
     *
     * @param callback - Callback to execute
     */
    public void setOnBackPress(OnBackPressedCallback callback) {
        this.backPressedCallback = callback;
    }

    /**
     * Add EventListener to the Buttons.
     */
    private void addEventListener() {
        this.volumeOnOffButton.setOnClickListener(this::toggleMute);

        this.startStopButton.setOnClickListener((view) -> startStopButtonClick());

        this.resetButton.setOnClickListener((view) -> reset());

        this.finishBtn.setOnClickListener(this.finishListener);

        if (this.backPressedCallback != null)
            this.activity.getOnBackPressedDispatcher().addCallback(this, this.backPressedCallback);
    }

    /**
     * Toggle the volume on/off.
     *
     * @param view - Button for toggling
     */
    private void toggleMute(View view) {
        if (this.playSound) this.playSound = false;
        else if (this.currentVolume > 0) this.playSound = true;

        if (this.playSound) {
            this.volumeOnOffButton.switchIcon(IconButton.UNMUTE);
            this.audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, this.currentVolume, 0);
        } else {
            this.volumeOnOffButton.switchIcon(IconButton.MUTE);
            this.audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
        }
    }

    /**
     * Format milliseconds to a time string in the format mm:ss:ms
     *
     * @param milliSeconds - Time in milliseconds
     * @return - Formatted String in the form mm:ss:ms
     */
    private String formatMillisToTime(long milliSeconds) {
        int millis = (int) (milliSeconds % 1000) / 10;
        int seconds = (int) (milliSeconds / 1000) % 60;
        int min = (int) (milliSeconds / (1000 * 60)) % 60;

        String mm = min < 10 ? "0" + min : String.valueOf(min);
        String ss = seconds < 10 ? "0" + seconds : String.valueOf(seconds);
        String ms = millis < 10 ? "0" + millis : String.valueOf(millis);

        return mm + ":" + ss + ":" + ms;
    }

    /**
     * Vibrate the device.
     *
     * @param duration - Duration of the vibration in ms
     */
    private void vibrate(long duration) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(duration);
        }
    }
}
