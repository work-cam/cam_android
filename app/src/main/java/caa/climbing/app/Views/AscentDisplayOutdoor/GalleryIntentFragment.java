package caa.climbing.app.Views.AscentDisplayOutdoor;

import android.app.Activity;
import android.content.Intent;
import android.provider.MediaStore;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import androidx.fragment.app.Fragment;
import caa.climbing.app.HelperClasses.PermissionHandler;
import caa.climbing.app.R;

/**
 * A Fragment to open the gallery in order to select a photo.
 * When the photo was selected successfully, the uri of the file can be passed back for further processing.
 */
public class GalleryIntentFragment extends Fragment {

    static private final String TAG = "GALLERY";
    private final FragmentManager fragmentManager;

    FragmentActivity activity;

    private final PermissionHandler permissionHandler;
    private ActivityResultLauncher<Intent> activityResultLauncher;
    private ActivityResultCallback<ActivityResult> activityResultCallback;

    /**
     * Creates a new {@link GalleryIntentFragment} object.
     */
    public GalleryIntentFragment() {
        super();
        this.activity = getActivity();
        this.fragmentManager = this.activity == null ? null : this.activity.getSupportFragmentManager();
        this.permissionHandler = new PermissionHandler(getContext(), this.fragmentManager, null, PermissionHandler.CAMERA);
    }

    /**
     * Creates a new {@link GalleryIntentFragment} object.
     *
     * @param activity               - Activity the fragment is attached to
     * @param activityResultCallback - Callback for when the activity result is received
     */
    public GalleryIntentFragment(Activity activity, ActivityResultCallback<ActivityResult> activityResultCallback) {
        super();
        this.activity = (FragmentActivity) activity;
        this.fragmentManager = this.activity == null ? null : this.activity.getSupportFragmentManager();
        this.activityResultCallback = activityResultCallback;
        this.permissionHandler = new PermissionHandler(getContext(), this.fragmentManager, this::startGalleryIntent, PermissionHandler.READ);
        this.registerActivityResultLauncher();
    }

    /**
     * Opens the gallery if the app has permission to do so.
     */
    public void openGallery() {
        boolean fragmentAlreadyAdded = this.fragmentManager.getFragments().contains(this);

        if (!fragmentAlreadyAdded) {
            this.fragmentManager.beginTransaction().add(this, TAG).commitNow();
//            this.fragmentManager.executePendingTransactions();
        }

        this.permissionHandler.startActionWithPermission();
    }

    /**
     * Open the gallery to select a picture.
     */
    private void startGalleryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK);
        galleryIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");

        Intent chooseIntent = Intent.createChooser(galleryIntent, getString(R.string.gallery_choose_prompt));
        this.activityResultLauncher.launch(chooseIntent);
    }

    /**
     * Register the Launcher to handle the result from the gallery activity.
     */
    private void registerActivityResultLauncher() {
        ActivityResultContracts.StartActivityForResult startActivityForResult = new ActivityResultContracts.StartActivityForResult();
        this.activityResultLauncher = registerForActivityResult(startActivityForResult, this.activityResultCallback);
    }
}
