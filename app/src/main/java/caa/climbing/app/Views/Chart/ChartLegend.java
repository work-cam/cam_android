package caa.climbing.app.Views.Chart;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import caa.climbing.app.R;

import com.google.android.flexbox.FlexboxLayout;

import java.util.ArrayList;

public class ChartLegend extends FlexboxLayout {

    Context context;
    FlexboxLayout container;

    public ChartLegend(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.chart_legend, this);
        this.context = context;

        container = findViewById(R.id.legend_container);
    }

    public void setLegend(int[] colors, ArrayList<String> labels) {
        if (colors.length != labels.size()) return;

        container.removeAllViews();

        for (int i = 0; i < labels.size(); i++) {
            LegendEntry legendEntry = new LegendEntry(context, null);
            legendEntry.setColor(colors[i]);
            legendEntry.setLabel(labels.get(i));

            container.addView(legendEntry);
        }

        for (int i = 0; i < container.getChildCount(); i++) {
            View view = container.getChildAt(i);
            FlexboxLayout.LayoutParams params = (FlexboxLayout.LayoutParams) view.getLayoutParams();
            params.setFlexBasisPercent(0.5f);

            view.setLayoutParams(params);
        }


    }
}
