package caa.climbing.app.Views.EventCalendar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * An Adapter for {@link EventCalendar} to display the dates of a given month in a grid view.
 */
public class CalendarGridAdapter extends ArrayAdapter<Date> {
    static private final int MAX_NUMBER_EVENTS = 3;
    static private final int EVENT_DISPLAY_MAX_LINES = 1;
    private final LayoutInflater inflater;
    private final ArrayList<Date> dates;
    private final Calendar calendar;
    private final int colorCurrentMonth, colorOtherMonth;
    private final Context context;
    private final Drawable backgroundTodayActive, backgroundOtherActive;
    private int activePosition;

    private ArrayList<CalendarEvent> events;

    /**
     * Creates a new {@link CalendarGridAdapter} instance.
     *
     * @param context        - Context of the corresponding view
     * @param dates          - List of dates to display
     * @param calendar       - Calendar to use
     * @param activePosition - Currently selected position in the grid
     */
    public CalendarGridAdapter(@NonNull Context context, ArrayList<Date> dates, Calendar calendar, int activePosition) {
        super(context, R.layout.calendar_tile);
        this.context = context;
        this.dates = dates;
        this.calendar = calendar;
        this.inflater = LayoutInflater.from(context);
        this.colorCurrentMonth = ResourcesCompat.getColor(context.getResources(), R.color.white, null);
        this.colorOtherMonth = ResourcesCompat.getColor(context.getResources(), R.color.gray300, null);

        this.backgroundTodayActive = ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.calendar_tile__selected, null);
        this.backgroundOtherActive = ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.calendar_tile__activated, null);

        this.activePosition = activePosition;
        this.events = new ArrayList<>();
    }

    /**
     * Get the view at a given position and update its appearance with respect to the corresponding data.
     *
     * @param position    - Position of the view
     * @param convertView - View to handle.
     * @param parent      - Parent view.
     * @return - The updated view
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.calendar_tile, parent, false);
        }

        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(this.dates.get(position));

        TextView calendarDay = convertView.findViewById(R.id.calendar_day);
        LinearLayout calendarEvents = convertView.findViewById(R.id.calendar_events);
        calendarDay.setText(String.valueOf(dateCalendar.get(Calendar.DAY_OF_MONTH)));

        int displayMonth = dateCalendar.get(Calendar.MONTH);
        int displayYear = dateCalendar.get(Calendar.YEAR);
        int currentMonth = this.calendar.get(Calendar.MONTH);
        int currentYear = this.calendar.get(Calendar.YEAR);

        if (displayMonth == currentMonth && displayYear == currentYear) {
            convertView.setBackgroundColor(this.colorCurrentMonth);
        } else {
            convertView.setBackgroundColor(this.colorOtherMonth);
        }
        this.setActiveTileBackground(calendarDay, position);

        this.setEventsForDate(this.dates.get(position), calendarEvents);

        return convertView;
    }

    /**
     * Get the number of displayed dates.
     *
     * @return - Total number of displayed dates
     */
    @Override
    public int getCount() {
        return this.dates.size();
    }

    /**
     * Get the corresponding data for a given position.
     *
     * @param position - Position for which to get the item
     * @return - Underlying data for the given position
     */
    @Nullable
    @Override
    public Date getItem(int position) {
        return dates.get(position);
    }

    /**
     * Get the position of an item in the underlying data structure.
     *
     * @param item - Item for which to get the position
     * @return - Position of the item
     */
    @Override
    public int getPosition(@Nullable Date item) {
        return this.dates.indexOf(item);
    }

    /**
     * Update the display of events for a given date.
     *
     * @param date           - Date for which to add events
     * @param calendarEvents - Events for the date
     */
    private void setEventsForDate(Date date, LinearLayout calendarEvents) {
        calendarEvents.removeAllViews();

        ArrayList<CalendarEvent> eventsForDate = this.getEventsForDate(date);

        int totalEventsForDate = eventsForDate.size();
        if (totalEventsForDate < MAX_NUMBER_EVENTS) {
            for (CalendarEvent event : eventsForDate) {
                TextView eventDisplay = createEventDisplay(true);
                eventDisplay.setText(event.getTitle());
                calendarEvents.addView(eventDisplay);
            }
        } else {
            TextView eventCount = createEventDisplay(false);
            eventCount.setText(String.valueOf(totalEventsForDate));
            calendarEvents.addView(eventCount);
        }
    }

    /**
     * Get the list of events associated with a date.
     *
     * @param date - Date for which to get the events
     * @return - List of events
     */
    protected ArrayList<CalendarEvent> getEventsForDate(Date date) {
        Calendar eventCalendar = Calendar.getInstance();

        ArrayList<CalendarEvent> eventsForDate = new ArrayList<>();
        for (CalendarEvent event : this.events) {
            Date eventDate = event.getDate();

            eventCalendar.setTime(eventDate);
            int eventYear = eventCalendar.get(Calendar.YEAR);
            int eventMonth = eventCalendar.get(Calendar.MONTH);
            int eventDay = eventCalendar.get(Calendar.DATE);

            eventCalendar.setTime(date);
            int currentYear = eventCalendar.get(Calendar.YEAR);
            int currentMonth = eventCalendar.get(Calendar.MONTH);
            int currentDay = eventCalendar.get(Calendar.DATE);

            if (currentDay == eventDay && currentMonth == eventMonth && currentYear == eventYear) {
                eventsForDate.add(event);
            }
        }

        return eventsForDate;
    }

    /**
     * Create a TextView to display details of a single event.
     *
     * @param matchParent - Whether to match the parent width
     * @return - TextView to display event details
     */
    private TextView createEventDisplay(boolean matchParent) {
        TextView eventDisplay = new TextView(this.context);
        float smallFontSize = Utils.sp2px(getContext(), this.context.getResources().getDimension(R.dimen.fontsize_xsmall));
        eventDisplay.setTextSize(smallFontSize);
        eventDisplay.setMaxLines(EVENT_DISPLAY_MAX_LINES);
        eventDisplay.setEllipsize(TextUtils.TruncateAt.END);
        eventDisplay.setBackground(ResourcesCompat.getDrawable(this.context.getResources(), R.drawable.card_blue100, null));

        int padding = (int) this.context.getResources().getDimension(R.dimen.calendar_tile_event_padding);
        eventDisplay.setPadding(padding, 0, padding, 0);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                matchParent ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        eventDisplay.setLayoutParams(layoutParams);

        return eventDisplay;
    }

    /**
     * Set the events that should be displayed.
     *
     * @param events - List of events to display
     */
    public void setEvents(ArrayList<CalendarEvent> events) {
        this.events = events;
    }

    /**
     * Set the currently active position
     *
     * @param position - New active position
     */
    public void setActivePosition(int position) {
        this.activePosition = position;
    }

    /**
     * Set the background for the active Tile.
     *
     * @param view     - View for which to change the background
     * @param position - Position of the view in the grid
     */
    private void setActiveTileBackground(View view, int position) {
        if (this.activePosition > -1 && this.activePosition == position) {
            Date currentDate = new Date();
            boolean isSameDay = CalendarGrid.isSameDay(currentDate, this.dates.get(position));
            if (isSameDay) {
                view.setBackground(this.backgroundTodayActive);
            } else {
                view.setBackground(this.backgroundOtherActive);
            }
        } else {
            view.setBackground(null);
        }
    }
}
