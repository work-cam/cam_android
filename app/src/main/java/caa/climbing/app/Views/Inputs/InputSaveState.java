package caa.climbing.app.Views.Inputs;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/**
 * A simple class to store the state of views that only have a single typed value as state.
 */
public class InputSaveState<T> extends View.BaseSavedState {
    private T value;

    /**
     * Creates a new {@link InputSaveState} object.
     *
     * @param superState - State of the extended view
     */
    public InputSaveState(Parcelable superState) {
        super(superState);
    }

    /**
     * Creates a new {@link InputSaveState} object from a Parcel.
     *
     * @param in - Parcel from which the instance is created.
     */
    private InputSaveState(Parcel in) {
        super(in);
    }

    /**
     * Sets the saved value in the state.
     *
     * @param value - New value
     */
    public void setValue(T value) {
        this.value = value;
    }

    /**
     * Get the saved value in the state.
     *
     * @return - The saved value
     */
    public T getValue() {
        return this.value;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param out   - The Parcel in which the object should be written
     * @param flags - Additional flags about how the object should be written
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeValue(value);
    }

    /**
     * Generates instances of the Parcelable class from a Parcel
     */
    public static final Creator<InputSaveState<?>> CREATOR
            = new Creator<InputSaveState<?>>() {

        /**
         * Create a new instance of the Parcelable class, instantiating it from
         * the given Parcel whose data had previously been written by writeToParcel()
         *
         * @param in - Previously created Parcel
         * @return - New instance of the Parcelable class
         */
        public InputSaveState<?> createFromParcel(Parcel in) {
            return new InputSaveState<>(in);
        }

        /**
         * Create a new array of the Parcelable class.
         *
         * @param size - Size of the new Array
         * @return - New array of the Parcelable class
         */
        public InputSaveState<?>[] newArray(int size) {
            return new InputSaveState[size];
        }
    };
}