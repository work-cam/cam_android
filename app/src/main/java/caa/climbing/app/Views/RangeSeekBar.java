package caa.climbing.app.Views;

import android.content.Context;

import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import caa.climbing.app.Views.Slider.OnRangeChangedListener;
import caa.climbing.app.Views.Slider.RangeSlider;
import caa.climbing.app.R;


/**
 * Created by Christian A on 26.09.2020.
 */

public class RangeSeekBar extends ConstraintLayout{
    Context context;
    TextView textView, label;
    RangeSlider rsb;

    private CharSequence[] labels;
    private float maxValue, minValue;

    public RangeSeekBar(Context context) {
        super(context);
        inflate(context, R.layout.range_seek_bar, this);
        textView = findViewById(R.id.text_view);
        rsb = findViewById(R.id.rsb);
        this.context = context;

        rsb.setSeekBarMode(RangeSlider.SEEKBAR_MODE_RANGE);

    }

    public RangeSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.range_seek_bar, this);
        textView = findViewById(R.id.text_view);
        label = findViewById(R.id.label);
        rsb = findViewById(R.id.rsb);
        this.context = context;

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RangeSlider);

        int seekbar_mode = RangeSlider.SEEKBAR_MODE_RANGE;
        int steps = 100;
        minValue = 0;
        maxValue = 100;
        float interval = 1;
        boolean showTicks = false;
        int textColor = getResources().getColor(R.color.white);
        String slider_label = "Hallo";
        if (a != null) {

            seekbar_mode = a.getInt(R.styleable.RangeSlider_rsb_mode, RangeSlider.SEEKBAR_MODE_RANGE);
            steps = a.getInt(R.styleable.RangeSlider_rsb_steps, 100);
            minValue = a.getFloat(R.styleable.RangeSlider_rsb_min, 0);
            maxValue = a.getFloat(R.styleable.RangeSlider_rsb_max, 100);
            interval = a.getFloat(R.styleable.RangeSlider_rsb_min_interval, 1);
            showTicks = a.getBoolean(R.styleable.RangeSlider_rsb_show_ticks, false);
            textColor = a.getColor(R.styleable.RangeSlider_rsb_tick_mark_text_color, getResources().getColor(R.color.white));
            slider_label = a.getString(R.styleable.RangeSlider_slider_label);

            a.recycle();
        }

        rsb.setSeekBarMode(seekbar_mode);
        rsb.setSteps(steps);
        rsb.setRange(minValue, maxValue, interval);
        rsb.setShowTicks(showTicks);
        rsb.setTickMarkTextColor(textColor);
        textView.setTextColor(textColor);
        label.setTextColor(textColor);
        label.setText(slider_label);


        if (seekbar_mode == RangeSlider.SEEKBAR_MODE_SINGLE) {
            rsb.setOnRangeChangedListener(singleModeListener);
            float percent = Math.max(rsb.getProgressLeft(), rsb.getProgressRight());///maxValue;
            String text = String.format("%.0f", percent) + " %";
            textView.setText(text);
        }

    }

    private OnRangeChangedListener singleModeListener = new OnRangeChangedListener() {
        @Override
        public void onRangeChanged(RangeSlider view, float leftValue, float rightValue, boolean isFromUser) {
            float percent = Math.max(leftValue, rightValue);///maxValue;
            String text = String.format("%.0f", percent) + " %";
            textView.setText(text);
        }

        @Override
        public void onStartTrackingTouch(RangeSlider view, boolean isLeft) {

        }

        @Override
        public void onStopTrackingTouch(RangeSlider view, boolean isLeft) {

        }
    };

    public void setOnRangeChangeListener (OnRangeChangedListener listener) {
        rsb.setOnRangeChangedListener(listener);
    }

    public OnRangeChangedListener getOnRangeChangeListener() {
        return rsb.getOnRangeChangedListener();
    }
    public void removeOnRangeChangedListener() {
        rsb.removeOnRangeChangedListener();
    }

    public void setProgress(float progess) {
        rsb.setProgress(progess);
    }
    public void setProgressDisplay(String text) {
        textView.setText(text);
    }
    public void setLabel(String label) {
        this.label.setText(label);
    }

    public void setTickLabels(CharSequence[] values, int displayNLabels) {
        rsb.setSteps(values.length-1);
        this.labels = new CharSequence[displayNLabels];

        final CharSequence[] grades = {"4C+", "5A", "5A+", "5B", "5B+", "5C", "5C+", "6A", "6A+", "6B", "6B+", "6C", "6C+", "7A", "7A+", "7B", "7B+", "7C", "7C+", "8A", "8A+", "8B", "8B+", "8C", "8C+"};
        int downsize = 2;

        labels = new CharSequence[Math.round((float)grades.length/downsize)];
        for (int i=0; i<grades.length; i++) {
            if (i%downsize==0) labels[i/downsize] = grades[i];
        }
        textView.setText(grades[0].toString() + " - " + grades[grades.length-1].toString());

        rsb.setRange(0, grades.length-1,1);
        rsb.setProgressLeft(0);
        rsb.setProgressRight(grades.length-1);
        rsb.setSteps(labels.length-1);

        rsb.setTickMarkMode(RangeSlider.TICK_MARK_MODE_OTHER);

        rsb.setTickMarkTextColor(getResources().getColor(R.color.white));
        rsb.setTickMarkTextArray(labels);

        rsb.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSlider view, float leftValue, float rightValue, boolean isFromUser) {
                int i1 = (int) leftValue;
                int i2 = (int) rightValue;

                String grade1 = grades[i1].toString();
                String grade2 = grades[i2].toString();

                textView.setText(grade1 + " - " + grade2);
            }

            @Override
            public void onStartTrackingTouch(RangeSlider view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSlider view, boolean isLeft) {

            }
        });


    }

    /**
     * Get the minimum value for a range slider.
     *
     * @return - Minimum value of the slider
     */
    public float getMin() {
        return rsb.getLeftSeekBar().getProgress();
    }

    /**
     * Get the maximum value for a range slider.
     *
     * @return - Maximum value of the slider
     */
    public float getMax() {
        return rsb.getRightSeekBar().getProgress();
    }

    /**
     * Set the range of the slider.
     *
     * @param min - Minimum value of the range
     * @param max - Maximum value of the range
     */
    public void setRange(float min, float max) {
        rsb.setRange(min, max, 1);
        rsb.setProgress(min, max);
    }

    /**
     * Set the display text.
     *
     * @param value - Text to display
     */
    public void setRangeDisplay(String value) {
        this.textView.setText(value);
    }
}
