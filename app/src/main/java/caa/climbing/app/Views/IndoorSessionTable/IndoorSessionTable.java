package caa.climbing.app.Views.IndoorSessionTable;

import android.annotation.SuppressLint;
import android.content.Context;

import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

import caa.climbing.app.Adapters.RecyclerAdapter_NumberInput;
import caa.climbing.app.Animations.DropdownAnimation;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.R;
import caa.climbing.app.Views.Inputs.NumberInput.NumberInput;
import caa.climbing.app.Views.TableHeader;

import java.util.ArrayList;

/**
 * View to display inputs to save the ascents of an indoor session.
 */
public class IndoorSessionTable extends ConstraintLayout {

    private final RecyclerView gradeInputContainer;
    private final TableHeader tableHeader;

    private boolean smallViewPort;
    private boolean isCollapsed;
    private ArrayList<LabelledInt> gradeBracketsLabelled;
    private Drawable arrowDown, arrowUp;

    /**
     * Creates a new {@link IndoorSessionTable} object
     *
     * @param context - Context where the view is created
     */
    public IndoorSessionTable(Context context) {
        this(context, null);
    }

    /**
     * Creates a new {@link IndoorSessionTable} object
     *
     * @param context - Context where the view is created
     * @param attrs   - Attributes that are passed to the view
     */
    public IndoorSessionTable(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context, R.layout.indoor_session_table, this);
        this.tableHeader = findViewById(R.id.tableHeader);
        this.gradeInputContainer = findViewById(R.id.gradeInputContainer);

        this.styleGradeInputContainer(context, attrs);
    }

    /**
     * Creates a new {@link IndoorSessionTable} object
     *
     * @param context               - Context where the view is created
     * @param header                - Header text
     * @param gradeBracketsLabelled - Data from which the entries are created
     */
    public IndoorSessionTable(Context context, String header, ArrayList<LabelledInt> gradeBracketsLabelled, boolean isCollapsed) {
        this(context, null);

        this.gradeBracketsLabelled = gradeBracketsLabelled;
        this.tableHeader.setText(header);
        this.fillTable(context, this.gradeBracketsLabelled, true);

        this.arrowUp = ResourcesCompat.getDrawable(getResources(), R.drawable.triangle__up, null);
        this.arrowDown = ResourcesCompat.getDrawable(getResources(), R.drawable.triangle__down, null);

        this.isCollapsed = isCollapsed;

        if (this.isCollapsed) {
            this.collapse(null);
            this.tableHeader.setBackgroundRounded(true);
        } else {
            this.setToggleArrow(false);
        }

        this.tableHeader.setOnClickListener((view) -> this.toggle());
    }


    /**
     * Style the gradeInputContainer depending on the passed attributes.
     *
     * @param context - Context where the view is created
     * @param attrs   - Attributes that are passed to the view
     */
    private void styleGradeInputContainer(Context context, @Nullable AttributeSet attrs) {
        this.smallViewPort = false;
        if (attrs != null) {
            @SuppressLint("CustomViewStyleable")
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Input);
            this.smallViewPort = a.getBoolean(R.styleable.Input_smallViewPort, false);
            a.recycle();
        }

        int spacing = this.smallViewPort ? getResources().getDimensionPixelSize(R.dimen.list_element_spacing__xsmall) : getResources().getDimensionPixelSize(R.dimen.list_element_spacing__medium);
        gradeInputContainer.addItemDecoration(new VerticalSpaceItemDecoration(spacing));
    }

    /**
     * Fills the table with inputs according to the data.
     *
     * @param context - Context where the view is created
     * @param data    - Data based on which the table is filled
     */
    public void fillTable(Context context, ArrayList<LabelledInt> data, boolean editMode) {
        this.gradeBracketsLabelled = data;

        // use linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        this.gradeInputContainer.setLayoutManager(layoutManager);

        RecyclerAdapter_NumberInput adapter = new RecyclerAdapter_NumberInput(data, editMode, this.smallViewPort);
        adapter.setLabelColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
        this.gradeInputContainer.setAdapter(adapter);

        this.gradeInputContainer.setHasFixedSize(true);
    }

    /**
     * Count the number of ascents in each grade bracket
     *
     * @return - Number of ascents per grade bracket
     */
    public ArrayList<LabelledInt> getNumberOfAscents() {
        RecyclerAdapter_NumberInput adapter = (RecyclerAdapter_NumberInput) gradeInputContainer.getAdapter();
        if (adapter == null) return new ArrayList<>();
        return adapter.getData();
    }

    /**
     * Focus the first input
     */
    public void focusFirst() {
        RecyclerAdapter_NumberInput.ViewHolder vh = (RecyclerAdapter_NumberInput.ViewHolder) gradeInputContainer.findViewHolderForAdapterPosition(0);
        if (vh != null) vh.numberInput.requestFocus();
    }

    /**
     * Marks all inputs as invalid
     */
    public void markAllAsInvalid() {
        RecyclerAdapter_NumberInput adapter = (RecyclerAdapter_NumberInput) gradeInputContainer.getAdapter();

        if (adapter == null) return;

        for (int i = 0; i < adapter.getItemCount(); i++) {
            RecyclerAdapter_NumberInput.ViewHolder vh = (RecyclerAdapter_NumberInput.ViewHolder) gradeInputContainer.findViewHolderForAdapterPosition(i);
            if (vh != null) vh.numberInput.markAsInvalid();
        }
    }

    /**
     * Marks all inputs as valid
     */
    public void markAllAsValid() {
        RecyclerAdapter_NumberInput adapter = (RecyclerAdapter_NumberInput) gradeInputContainer.getAdapter();

        if (adapter == null) return;

        for (int i = 0; i < adapter.getItemCount(); i++) {
            RecyclerAdapter_NumberInput.ViewHolder vh = (RecyclerAdapter_NumberInput.ViewHolder) gradeInputContainer.findViewHolderForAdapterPosition(i);
            if (vh != null) vh.numberInput.markAsValid();
        }
    }

    /**
     * Checks if all inputs are equal to zero or empty
     *
     * @return - True if all inputs are zero or empty, false otherwise
     */
    public boolean allEmpty() {
        return LabelledInt.allZero(getNumberOfAscents());
    }

    /**
     * Empties all inputs.
     */
    public void emptyInputs() {
        RecyclerAdapter_NumberInput adapter = (RecyclerAdapter_NumberInput) gradeInputContainer.getAdapter();
        if (adapter == null) return;
        adapter.resetInputs();
    }

    /**
     * Sets the editMode of the {@link NumberInput}s inside the table.
     *
     * @param editMode - True if inputs can be edited, false otherwise
     */
    public void setEditMode(boolean editMode) {
        RecyclerAdapter_NumberInput adapter = (RecyclerAdapter_NumberInput) gradeInputContainer.getAdapter();

        if (adapter == null) return;

        adapter.setEditMode(editMode);

        for (int i = 0; i < adapter.getItemCount(); i++) {
            RecyclerView.LayoutManager layoutManager = gradeInputContainer.getLayoutManager();

            if (layoutManager == null) continue;

            View view = layoutManager.findViewByPosition(i);
            if (view instanceof NumberInput) {
                ((NumberInput) view).setEditMode(editMode);
            }
        }
    }

    /**
     * Sets the title of the table header
     *
     * @param title - Title of the table header
     */
    public void setTableHeader(String title) {
        this.tableHeader.setText(title);
    }

    /**
     * Adds a gray border around the body of the table.
     */
    public void addTableBorder() {
        gradeInputContainer.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.box_white__border_rounded, null));
    }

    /**
     * Toggle the container with the inputs.
     */
    private void toggle() {
        DropdownAnimation.AnimationCallback callback = new DropdownAnimation.AnimationCallback() {
            @Override
            public void onAnimationEnd() {
                if (isCollapsed) tableHeader.setBackgroundRounded(true);
            }

            @Override
            public void onAnimationStart() {
                if (!isCollapsed) tableHeader.setBackgroundRounded(false);
            }
        };

        if (this.isCollapsed) {
            this.expand(callback);
        } else {
            this.collapse(callback);
        }

        this.isCollapsed = !this.isCollapsed;
    }

    /**
     * Expand the container with the inputs.
     *
     * @param callback - Optional callback for the animation
     */
    private void expand(@Nullable DropdownAnimation.AnimationCallback callback) {
        DropdownAnimation.expand(this.gradeInputContainer, callback);
        this.setToggleArrow(false);
    }

    /**
     * Collapse the container with the inputs.
     *
     * @param callback - Optional callback for the animation
     */
    private void collapse(@Nullable DropdownAnimation.AnimationCallback callback) {
        DropdownAnimation.collapse(this.gradeInputContainer, callback);
        this.setToggleArrow(true);
    }

    /**
     * Set the icon of the TableHeader to indicate that it can be toggled.
     *
     * @param isCollapsed - Whether the container with the inputs is collapsed.
     */
    private void setToggleArrow(boolean isCollapsed) {
        if (isCollapsed) {
            this.tableHeader.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, arrowDown, null);
        } else {
            this.tableHeader.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, arrowUp, null);
        }
    }
}
