package caa.climbing.app.Views.TabBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;

/**
 * Adapter for listening to fragment lifecycle changes that happen inside the adapter.
 */
public class ViewPagerAdapter extends FragmentStateAdapter {

    private final ArrayList<TabConfig> configList;

    /**
     * Creates a new {@link ViewPagerAdapter} object.
     *
     * @param fragmentManager - FragmentManager of ViewPager2 host
     * @param lifecycle       - LifeCycle of ViewPager2 host
     * @param configList      - A list of configuration items for the tabBar
     */
    public ViewPagerAdapter(@NonNull FragmentManager fragmentManager, Lifecycle lifecycle, ArrayList<TabConfig> configList) {
        super(fragmentManager, lifecycle);
        this.configList = configList;
    }

    /**
     * Get the fragment that corresponds to the content of the tab at a given position.
     *
     * @param position - Position of the corresponding tab
     * @return - The content fragment for the specific tab
     */
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return this.configList.get(position).getTabBarContent();
    }

    /**
     * Get the total number of items in the list that can be displayed.
     *
     * @return - The total number of items that can be displayed
     */
    @Override
    public int getItemCount() {
        return this.configList.size();
    }
}
