package caa.climbing.app.Views.AscentDisplayOutdoor;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import caa.climbing.app.HelperClasses.PermissionHandler;

/**
 * A Fragment to open a photo.
 */
public class ViewIntentFragment extends Fragment {

    static private final String TAG = "VIEW";
    private final FragmentManager fragmentManager;

    private final PermissionHandler permissionHandler;
    private final Uri photoUri;

    /**
     * Creates a new {@link ViewIntentFragment} object.
     */
    public ViewIntentFragment() {
        this(null, null);
    }

    /**
     * Creates a new {@link ViewIntentFragment} object.
     */
    public ViewIntentFragment(Uri photoUri, Activity activity) {
        super();
        FragmentActivity activity1 = (FragmentActivity) activity;
        this.fragmentManager = activity1 == null ? null : activity1.getSupportFragmentManager();
        this.permissionHandler = new PermissionHandler(getContext(), this.fragmentManager, this::startViewIntent, PermissionHandler.READ);
        this.photoUri = photoUri;
    }

    /**
     * Opens the image if the app has permission to do so.
     */
    public void openImage() {
        boolean fragmentAlreadyAdded = this.fragmentManager.getFragments().contains(this);

        if (!fragmentAlreadyAdded) {
            this.fragmentManager.beginTransaction().add(this, TAG).commit();
            this.fragmentManager.executePendingTransactions();
        }

        this.permissionHandler.startActionWithPermission();
    }

    /**
     * Open the photo.
     */
    private void startViewIntent() {
        if (this.photoUri == null) return;

        Intent viewIntent = new Intent(Intent.ACTION_VIEW);
        viewIntent.setDataAndType(this.photoUri, "image/*");

        startActivity(viewIntent);
    }
}
