package caa.climbing.app.Views.Buttons;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.core.content.res.ResourcesCompat;
import caa.climbing.app.R;

/**
 * A class for displaying icon only buttons.
 */
public class IconButton extends FlatButton {

    private String iconName;

    /**
     * Icon Names
     */
    final public static String CAMERA = "camera";
    final public static String CONFIRM = "confirm";
    final public static String DELETE = "delete";
    final public static String EDIT = "edit";
    final public static String GALLERY = "gallery";
    final public static String MINUS = "minus";
    final public static String PAUSE = "pause";
    final public static String PLAY = "play";
    final public static String PLUS = "plus";
    final public static String RESET = "reset";
    final public static String SEARCH = "search";
    final public static String MUTE = "mute";
    final public static String UNMUTE = "unmute";
    final public static String CANCEL = "cancel";
    final public static String MENU = "menu";
    final public static String NEXT = "next";
    final public static String PREVIOUS = "previous";

    /**
     * Creates a new {@link IconButton} object.
     *
     * @param context - Context where the button was created
     */
    public IconButton(Context context) {
        this(context, null);
    }

    /**
     * Creates a new {@link IconButton} object.
     *
     * @param context - Context where the button was created
     * @param attrs   - Attributes that can be passed to the view
     */
    public IconButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.IconButton, 0, 0);
            this.iconName = a.getString(R.styleable.IconButton_iconName);
            a.recycle();
        } else {
            this.iconName = null;
        }

        this.setIcon();

    }

    /**
     * Get the corresponding resource id to the icon name.
     *
     * @return - Matching resource id
     */
    private Integer getIconResource() {
        if (this.iconName == null) return null;

        switch (this.iconName) {
            case CAMERA:
                return R.drawable.camera_btn;
            case CONFIRM:
                return R.drawable.confirm_btn;
            case DELETE:
                return R.drawable.delete_btn;
            case EDIT:
                return R.drawable.edit_btn;
            case GALLERY:
                return R.drawable.gallery_btn;
            case MINUS:
                return R.drawable.minus_btn;
            case PAUSE:
                return R.drawable.pause_btn;
            case PLAY:
                return R.drawable.play_btn;
            case PLUS:
                return R.drawable.plus_btn;
            case RESET:
                return R.drawable.reset_btn;
            case SEARCH:
                return R.drawable.search_btn;
            case MUTE:
                return R.drawable.speaker_off_btn;
            case UNMUTE:
                return R.drawable.speaker_on_btn;
            case CANCEL:
                return R.drawable.x_btn;
            case MENU:
                return R.drawable.hamburger;
            case NEXT:
                return R.drawable.next_btn;
            case PREVIOUS:
                return R.drawable.previous_btn;
            default:
                return null;
        }
    }

    /**
     * Set the background of the button to the icon matching the icon name.
     */
    private void setIcon() {
        Integer iconResource = this.getIconResource();

        if (iconResource == null) return;

        this.setBackground(ResourcesCompat.getDrawable(getResources(), iconResource, null));
    }

    /**
     * Switch the displayed Icon.
     *
     * @param newIconName - New icon to display
     */
    public void switchIcon(String newIconName) {
        this.iconName = newIconName;
        this.setIcon();
    }
}
