package caa.climbing.app.Views.EventCalendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.IconButton;

/**
 * A class to display the title of an event and give the user the chance to delete it.
 */
public class CalendarEventDetail extends ConstraintLayout {

    private TextView eventTitle;
    private IconButton deleteButton;

    /**
     * Creates a new {@link CalendarEventDetail} instance.
     *
     * @param context - Context from which the view was created
     */
    public CalendarEventDetail(@NonNull Context context) {
        this(context, null);
    }

    /**
     * Creates a new {@link CalendarEventDetail} instance.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public CalendarEventDetail(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        View rootView = inflate(context, R.layout.calendar_event_detail, this);

        this.initViews(rootView);
    }

    /**
     * Initializes the individual components of the view.
     *
     * @param rootView - Root view that contains the rest of the views
     */
    private void initViews(View rootView) {
        this.eventTitle = rootView.findViewById(R.id.event_title);
        this.deleteButton = rootView.findViewById(R.id.event_delete_btn);
    }

    /**
     * Set the event to display.
     *
     * @param event - Event to display
     */
    public void setEvent(CalendarEvent event) {
        this.eventTitle.setText(event.getTitle());
    }

    /**
     * Add a click listeners to the delete button.
     */
    public void setDeleteAction(OnClickListener clickListener) {
        this.deleteButton.setOnClickListener(clickListener);
    }
}
