package caa.climbing.app.Views.AscentDisplayOutdoor;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import caa.climbing.app.HelperClasses.PermissionHandler;
import caa.climbing.app.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * A Fragment to open the camera in order to take and save photos.
 * When the photo was taken successfully, the uri of the file can be passed back for further processing.
 */
public class CameraIntentFragment extends Fragment {

    static private final String TAG = "CAMERA";
    private final FragmentActivity activity;

    private final PermissionHandler permissionHandler;
    private ActivityResultLauncher<Intent> activityResultLauncher;
    private ActivityResultCallback<ActivityResult> activityResultCallback;

    private final FragmentManager fragmentManager;

    private Uri currentPhotoUri;

    /**
     * Creates a new {@link CameraIntentFragment} object.
     */
    public CameraIntentFragment() {
        super();
        this.activity = getActivity();
        this.fragmentManager = this.activity == null ? null : this.activity.getSupportFragmentManager();
        this.permissionHandler = new PermissionHandler(getContext(), this.fragmentManager, null, PermissionHandler.CAMERA);
    }

    /**
     * Creates a new {@link CameraIntentFragment} object.
     *
     * @param activity               - Activity the fragment is attached to
     * @param activityResultCallback - Callback for when the activity result is received
     */
    public CameraIntentFragment(Activity activity, ActivityResultCallback<ActivityResult> activityResultCallback) {
        super();
        this.activity = (FragmentActivity) activity;
        this.fragmentManager = this.activity.getSupportFragmentManager();
        this.activityResultCallback = activityResultCallback;
        this.permissionHandler = new PermissionHandler(getContext(), this.fragmentManager, this::startCameraIntent, PermissionHandler.CAMERA);
        this.registerActivityResultLauncher();
    }

    /**
     * Register the Launcher to handle the result from the camera activity.
     */
    private void registerActivityResultLauncher() {
        ActivityResultContracts.StartActivityForResult startActivityForResult = new ActivityResultContracts.StartActivityForResult();
        this.activityResultLauncher = registerForActivityResult(startActivityForResult, this::handleCameraResult);
    }

    /**
     * Handle the result from the camera activity.
     * If a photo was taken successfully, pass the Uri of that image to the callback for further processing.
     * Otherwise, try to delete the empty image.
     *
     * @param result - Result from the camera activity
     */
    private void handleCameraResult(ActivityResult result) {
        Intent data = result.getData() == null ? new Intent() : result.getData();
        if (result.getResultCode() == RESULT_OK) {
            data.setData(currentPhotoUri);
            ActivityResult newResult = new ActivityResult(result.getResultCode(), data);

            if (this.activityResultCallback == null) return;
            this.activityResultCallback.onActivityResult(newResult);

        } else {
            if (activity == null) return;

            ContentResolver resolver = activity.getContentResolver();
            resolver.delete(this.currentPhotoUri, null, null);
        }
    }

    /**
     * Opens the camera to take a photo.
     */
    private void startCameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoUri = this.createPhotoUri();
        this.currentPhotoUri = photoUri;

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        this.activityResultLauncher.launch(takePictureIntent);
    }

    /**
     * Opens the camera if the app has the permission to do so.
     * Otherwise, ask the user for permission to access the camera.
     */
    public void openCamera() {
        boolean fragmentAlreadyAdded = this.fragmentManager.getFragments().contains(this);

        if (!fragmentAlreadyAdded) {
            this.fragmentManager.beginTransaction().add(this, TAG).commit();
            this.fragmentManager.executePendingTransactions();
        }

        this.permissionHandler.startActionWithPermission();
    }

    /**
     * Create a file for the image to be saved and save it using the MediaStore.
     *
     * @return - The Uri for the file
     */
    private Uri createPhotoUri() {
        String appName = getString(R.string.app_name);
        String relativeLocation = Environment.DIRECTORY_PICTURES + File.separator + appName;

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imgFileName = "JPEG_" + timeStamp + "_.jpg";

        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, imgFileName);
        contentValues.put(MediaStore.Images.Media.TITLE, imgFileName);
        contentValues.put(MediaStore.Images.Media.DESCRIPTION, imgFileName);
        contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        contentValues.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis() / 1000);
        contentValues.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());

        // Save the images in a subfolder for SDK-Versions >= 29
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, relativeLocation);
        }
        return this.activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

    }
}
