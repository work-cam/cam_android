package caa.climbing.app.Views.Inputs.SelectInput;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import caa.climbing.app.R;
import caa.climbing.app.Views.Inputs.Input;

/**
 * A custom view for selecting options from a dropdown.
 */
public class SelectInput extends Input<String> {
    private boolean dropDownVisible;

    /**
     * Creates a new {@link Input} object.
     *
     * @param context - Context from which the view is created
     * @param attrs   - Optional Attributes that can be applied to the view
     */
    public SelectInput(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.dropDownVisible = false;

        this.addEventListener();
    }

    /**
     * Set the options to choose from.
     *
     * @param options - Options to show in the dropdown
     */
    public void setSuggestions(String[] options) {
        NoFilterArrayAdapter adapter = new NoFilterArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, options);
        ((AutoCompleteTextView) this.input).setAdapter(adapter);
    }

    /**
     * Get the id of the resource that should be inflated.
     *
     * @return - ID of the resource that should be inflated
     */
    @Override
    protected int getResourceId() {
        return R.layout.select_input;
    }

    /**
     * Get the root view to attach the instance to.
     *
     * @return - Root view to attach the instance to
     */
    @Override
    protected ViewGroup getRoot() {
        return this;
    }

    /**
     * Set the value of the input.
     *
     * @param value - New value of the input
     */
    @Override
    public void setValue(String value) {
        this.setTextValue(value);
    }

    /**
     * Get the value of the input.
     */
    @Override
    public String getValue() {
        return this.getTextValue();
    }

    /**
     * Add Listeners for showing/hiding the dropdown.
     */
    private void addEventListener() {
        this.input.setOnClickListener((view) -> this.toggleDropDown());

        this.input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                dropDownVisible = false;
            }
        });
    }

    /**
     * Toggle the display of the dropdown.
     */
    private void toggleDropDown() {
        this.dropDownVisible = !this.dropDownVisible;

        if (this.dropDownVisible) {
            ((AutoCompleteTextView) this.input).showDropDown();
        } else {
            ((AutoCompleteTextView) this.input).dismissDropDown();
        }
    }

    /**
     * An Adapter for the {@link AutoCompleteTextView} that allows the display of the dropdown without user input.
     */
    protected static class NoFilterArrayAdapter extends ArrayAdapter<String> {
        /**
         * Create a new {@link NoFilterArrayAdapter}.
         *
         * @param context  - Context where the adapter is created
         * @param resource - Resource to style items
         * @param data     - Data to display
         */
        public NoFilterArrayAdapter(@NonNull Context context, int resource, @NonNull String[] data) {
            super(context, resource, data);
        }

        /**
         * Returns a filter that can be used to constrain data with a filtering pattern.
         *
         * @return - Filter used to constrain data
         */
        @NonNull
        @Override
        public Filter getFilter() {
            return new Filter() {
                /**
                 * Filter the data according to the constraint. Here, no filtering is applied in order to show data immediately.
                 *
                 * @param constraint - Sequence to filter for
                 * @return - Filtered data
                 */
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    return null;
                }

                /**
                 * Invoked to display filter results. Do nothing in this method to show data immediately without applying the filter.
                 *
                 * @param constraint - Sequence to filter for
                 * @param results    - Results of the filtering operation
                 */
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {

                }
            };
        }
    }
}
