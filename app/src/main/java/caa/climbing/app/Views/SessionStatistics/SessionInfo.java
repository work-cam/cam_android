package caa.climbing.app.Views.SessionStatistics;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import caa.climbing.app.R;

/**
 * Created by Christian A on 01.09.2020.
 */

public class SessionInfo extends ConstraintLayout {

    private TextView sessionName;
    private TextView sessionCount;

    public void setName(String name) {
        sessionName.setText(name);
    }
    public void setCount(int count) {
        sessionCount.setText(String.valueOf(count));
    }
    public void style(int colorTop, int colorBottom) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        int cornerRadius_px = (int) getResources().getDimension(R.dimen.borderradius);

        GradientDrawable backgroundTop = new GradientDrawable();
        float[] radiiTop = {cornerRadius_px,cornerRadius_px,cornerRadius_px,cornerRadius_px,0,0,0,0};
        backgroundTop.setColor(colorTop);
        backgroundTop.setStroke(1, colorTop);
        backgroundTop.setCornerRadii(radiiTop);
        sessionName.setBackground(backgroundTop);

        GradientDrawable backgroundBtm = new GradientDrawable();
        float[] radiiBtm = {0,0,0,0,cornerRadius_px,cornerRadius_px,cornerRadius_px,cornerRadius_px};
        backgroundBtm.setColor(colorBottom);
        backgroundBtm.setStroke(1, colorBottom);
        backgroundBtm.setCornerRadii(radiiBtm);
        sessionCount.setBackground(backgroundBtm);


    }

    public SessionInfo(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.session_info, this);
        sessionName = findViewById(R.id.sessionName);
        sessionCount = findViewById(R.id.sessionCount);

        // get Attributes and apply them
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SessionInfo, 0, 0);
        String name = "";
        int count = 0;
        int topColor;
        int bottomColor;


        try {
            name = a.getString(R.styleable.SessionInfo_name);
            count = a.getInt(R.styleable.SessionInfo_count, 0);
            topColor = a.getColor(R.styleable.SessionInfo_topColor, getResources().getColor(R.color.white));
            bottomColor = a.getColor(R.styleable.SessionInfo_bottomColor, getResources().getColor(R.color.white));

            setName(name);
            setCount(count);
            style(topColor, bottomColor);

//            sessionName.setWidth(width);
//            sessionCount.setWidth(width);
//
//            GradientDrawable top = new GradientDrawable();
//            float[] topRadii = {cornerRadius,cornerRadius,cornerRadius,cornerRadius,0,0,0,0};
//            float[] btmRadii = {0,0,0,0,cornerRadius,cornerRadius,cornerRadius,cornerRadius};
//            top.setColor(topColor);
//            top.setStroke(1, topColor);
//            top.setCornerRadii(topRadii);
//            sessionName.setBackground(top);
//
//            GradientDrawable btm = new GradientDrawable();
//            btm.setColor(bottomColor);
//            btm.setStroke(1, bottomColor);
//            btm.setCornerRadii(btmRadii);
//            sessionCount.setBackground(btm);


        } finally {


            a.recycle();
        }



    }

}
