package caa.climbing.app.Views.Buttons;

import android.content.Context;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import caa.climbing.app.Animations.AlphaAnimation;

/**
 * A class to add a fading animation to buttons with a flat background.
 */
public class FlatButton extends androidx.appcompat.widget.AppCompatButton {

    /**
     * Creates a new {@link FlatButton} object.
     *
     * @param context - Context where the button was created
     */
    public FlatButton(@NonNull Context context) {
        super(context);
    }

    /**
     * Creates a new {@link FlatButton} object.
     *
     * @param context - Context where the button was created
     * @param attrs   - Attributes that can be passed to the view
     */
    public FlatButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Set a clickListener on the button for normal clicks.
     * Additionally, the view is faded in/out to give visual feedback on the click event.
     *
     * @param clickListener - Listener to add
     */
    @Override
    public void setOnClickListener(OnClickListener clickListener) {
        OnClickListener newClickListener = (view) -> {
            AlphaAnimation.fadeIn(this);
            clickListener.onClick(this);
        };
        super.setOnClickListener(newClickListener);
    }

    /**
     * Set a clickListener on the button for long clicks.
     * Additionally, the view is faded in/out to give visual feedback on the click event.
     *
     * @param clickListener - Listener to add
     */
    @Override
    public void setOnLongClickListener(OnLongClickListener clickListener) {
        OnLongClickListener newClickListener = (view) -> {
            AlphaAnimation.fadeIn(this);
            clickListener.onLongClick(this);
            return false;
        };
        super.setOnLongClickListener(newClickListener);
    }
}
