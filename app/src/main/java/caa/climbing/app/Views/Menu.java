package caa.climbing.app.Views;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.Map;
import java.util.Set;

import androidx.core.content.res.ResourcesCompat;
import caa.climbing.app.Animations.DropdownAnimation;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import caa.climbing.app.Activities.MainActivity;
import caa.climbing.app.Views.Buttons.FlatButton;

/**
 * View to display buttons to switch between screens.
 */
public class Menu extends ConstraintLayout {

    private Map<Integer, String> screens;
    private Integer activeScreenKey;
    private View topReference;

    final private Context context;
    final private Activity activity;
    final private LinearLayout menuButtonContainer;

    private boolean isExpanded;

    /**
     * Creates a new {@link Menu} object.
     *
     * @param context - Context where the menu was created
     * @param attrs   - Attributes that can be passed to the view
     */
    public Menu(final Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        this.activity = Utils.getActivity(this.context);

        inflate(context, R.layout.menu, this);

        this.menuButtonContainer = findViewById(R.id.menuButtonContainer);

        this.isExpanded = getVisibility() == VISIBLE;
    }

    /**
     * Set the Map of available screens.
     *
     * @param screens - Map of the Screens
     */
    public void setScreens(Map<Integer, String> screens) {
        this.screens = screens;
    }

    /**
     * Set the currently active screen.
     *
     * @param activeScreenKey - Unique key of the active screen
     */
    public void setActiveScreenKey(int activeScreenKey) {
        this.activeScreenKey = activeScreenKey;
    }

    /**
     * Set the view to which the menu is constrained to at the top.
     *
     * @param view - View to which the menu is constrained to at the top
     */
    public void setTopReference(View view) {
        this.topReference = view;
    }

    /**
     * Hide the menu.
     */
    public void hide() {
        this.isExpanded = false;
        DropdownAnimation.collapse(this);
    }

    /**
     * Show the menu.
     */
    public void show() {
        this.isExpanded = true;
        Utils.hideKeyboard(getContext());
        render();
        DropdownAnimation.expandFull(this, this.topReference, this.activity);
    }

    /**
     * Check if the menu is visible.
     *
     * @return - Visibility of the menu
     */
    public boolean isVisible() {
        return this.isExpanded;
    }

    /**
     * Hide or show the menu without animating the change in visibility.
     *
     * @param visible - Whether the menu is visible
     */
    public void setVisibility(boolean visible) {
        this.isExpanded = visible;
        if (visible) {
            setVisibility(VISIBLE);
        } else {
            setVisibility(GONE);
        }
    }

    /**
     * Render the menu with buttons.
     */
    public void render() {
        Set<Integer> screenKeys = this.screens.keySet();

        this.menuButtonContainer.removeAllViewsInLayout();

        for (final int screenKey : screenKeys) {
            FlatButton menuButton = createMenuButton(screenKey);
            menuButton.setOnClickListener((view) -> this.onMenuButtonClick(screenKey));

            this.menuButtonContainer.addView(menuButton);
        }

    }

    /**
     * Creates a button do display in the menu.
     * Highlight the button corresponding to the currently active screen.
     *
     * @param screenKey - Key for changing to the correct screen
     * @return - A button to display in the menu
     */
    private FlatButton createMenuButton(int screenKey) {
        FlatButton menuButton = new FlatButton(this.context);
        final String text = this.screens.get(screenKey);
        int backgroundColor = this.activeScreenKey == screenKey ? R.color.interactive : R.color.accent;
        LinearLayout.LayoutParams layoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParameters.weight = 1;

        menuButton.setText(text);
        menuButton.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        menuButton.setBackgroundColor(ResourcesCompat.getColor(getResources(), backgroundColor, null));
        menuButton.setStateListAnimator(null);
        menuButton.setLayoutParams(layoutParameters);

        return menuButton;
    }

    /**
     * ClickListener for the menu buttons.
     * Changes the active screen when the button is clicked.
     *
     * @param screenKey - Key for changing to the correct screen
     */
    private void onMenuButtonClick(int screenKey) {
        MainActivity parentActivity = (MainActivity) Utils.getActivity(getContext());

        if (parentActivity != null) parentActivity.setActiveScreen(screenKey, true);
        this.activeScreenKey = screenKey;

        this.hide();
    }

}
