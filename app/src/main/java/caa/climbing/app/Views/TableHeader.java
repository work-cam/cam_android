package caa.climbing.app.Views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.core.content.res.ResourcesCompat;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * A view that is used as the header for a table.
 */
public class TableHeader extends AppCompatTextView {

    private final Context context;
    private final Drawable backgroundFullRounded, backgroundTopRounded;

    /**
     * @param context - Context from which the view was created
     */
    public TableHeader(Context context) {
        this(context, null);
    }

    /**
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public TableHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.backgroundTopRounded = ResourcesCompat.getDrawable(getResources(), R.drawable.table_header, null);
        this.backgroundFullRounded = ResourcesCompat.getDrawable(getResources(), R.drawable.card_blue500, null);
        this.setStyle(attrs);
    }

    /**
     * Styles the view.
     *
     * @param attrs - Attributes that are passed to the view
     */
    private void setStyle(AttributeSet attrs) {
        Utils.setStyle(this.context, this, attrs, this.backgroundTopRounded);
        Utils.setTextStyle(this.context, this, attrs);
    }

    /**
     * Set the background of the view
     *
     * @param fullRounded - Whether the table header should be rounded on every corner
     */
    public void setBackgroundRounded(boolean fullRounded) {
        Drawable background = fullRounded ? this.backgroundFullRounded : this.backgroundTopRounded;
        this.setBackground(background);
    }
}
