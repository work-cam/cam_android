package caa.climbing.app.Views.Filter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;

import androidx.core.content.res.ResourcesCompat;
import caa.climbing.app.Database.DatabaseModels.IndoorSession;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.HelperClasses.DateFormatter;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;
import caa.climbing.app.Views.Buttons.IconButton;
import caa.climbing.app.Views.Inputs.LabelInput.LabelInput;
import caa.climbing.app.Views.RangeSeekBar;
import caa.climbing.app.Views.Slider.OnRangeChangedListener;

/**
 * A view to filter indoor sessions inside the logbook.
 */
public class FilterIndoorSession extends FilterBase {

    private final View rootView;
    private CheckBox onlyBouldersCheckbox;
    private CheckBox onlyRoutesCheckbox;
    private IconButton searchGymButton;
    private LabelInput searchFieldGym;
    private FlatButton toggleSortingOrderButton;
    private RangeSeekBar dateRangeSeekbar;

    private boolean orderAscending;

    /**
     * Creates a new {@link FilterIndoorSession} object.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public FilterIndoorSession(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.rootView = inflate(context, R.layout.filter_indoor_session, this);
        this.orderAscending = true;

        this.initView();
    }

    /**
     * Initialize the view.
     */
    private void initView() {
        super.init(this.rootView);

        this.onlyBouldersCheckbox = this.rootView.findViewById(R.id.only_boulder_checkbox);
        this.onlyRoutesCheckbox = this.rootView.findViewById(R.id.only_routes_checkbox);
        this.searchFieldGym = this.rootView.findViewById(R.id.searchfield_gyms);
        this.searchGymButton = this.rootView.findViewById(R.id.search_gym_btn);
        this.toggleSortingOrderButton = this.rootView.findViewById(R.id.toggle_sorting_order_btn);
        this.dateRangeSeekbar = this.rootView.findViewById(R.id.seekbar_indoor_dates);
    }

    /**
     * Set the click listener for the button that toggles the sorting order between ascending and descending.
     *
     * @param clickListener - New Listener for the button
     */
    public void setSortingOrderClick(final OnClickListener clickListener) {
        this.toggleSortingOrderButton.setOnClickListener((view) -> {
            clickListener.onClick(view);
            toggleSortingOrder();
        });
    }

    /**
     * Toggles the sorting order of the sessions between ascending and descending.
     */
    private void toggleSortingOrder() {
        this.orderAscending = !this.orderAscending;
        Drawable triangleUp = ResourcesCompat.getDrawable(getResources(), R.drawable.triangle__up, null);
        Drawable triangleDown = ResourcesCompat.getDrawable(getResources(), R.drawable.triangle__down, null);

        if (this.orderAscending) {
            toggleSortingOrderButton.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, triangleDown, null);
        } else {
            toggleSortingOrderButton.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, triangleUp, null);
        }
    }

    /**
     * Filter for sessions which contain the search string in their respective field for the gym.
     *
     * @param sessions - List of sessions to filter
     * @return - Filtered result list
     */
    private ArrayList<IndoorSession> filterForGymName(ArrayList<IndoorSession> sessions) {
        ArrayList<IndoorSession> filtered = new ArrayList<>();

        String searchString = this.searchFieldGym.getValue().toLowerCase();

        if (searchString.equals("")) return sessions;

        for (IndoorSession session : sessions) {
            String gymName = session.gym.toLowerCase();
            if (gymName.contains(searchString)) {
                filtered.add(session);
            }
        }

        return filtered;
    }

    /**
     * Filter for sessions that contain only boulder ascents but zero routes.
     *
     * @param sessions - List of sessions to filter
     * @return - Filtered result list
     */
    private ArrayList<IndoorSession> filterOnlyBoulder(ArrayList<IndoorSession> sessions) {
        ArrayList<IndoorSession> filtered = new ArrayList<>();

        if (!onlyBouldersCheckbox.isChecked()) return sessions;

        for (IndoorSession session : sessions) {
            if (LabelledInt.allZero(session.routes)) {
                filtered.add(session);
            }
        }

        return filtered;
    }

    /**
     * Filter for sessions that contain only route ascents but zero boulders.
     *
     * @param sessions - List of sessions to filter
     * @return - Filtered result list
     */
    private ArrayList<IndoorSession> filterOnlyRoutes(ArrayList<IndoorSession> sessions) {
        ArrayList<IndoorSession> filtered = new ArrayList<>();

        if (!onlyRoutesCheckbox.isChecked()) return sessions;

        for (IndoorSession session : sessions) {
            if (LabelledInt.allZero(session.boulders)) {
                filtered.add(session);
            }
        }

        return filtered;
    }

    /**
     * Filter for sessions that took place in a certain date range.
     *
     * @param sessions - List of sessions to filter
     * @return - Filtered result list
     */
    private ArrayList<IndoorSession> filterDates(ArrayList<IndoorSession> sessions) {
        ArrayList<IndoorSession> filtered = new ArrayList<>();

        int min = (int) this.dateRangeSeekbar.getMin();
        int max = (int) this.dateRangeSeekbar.getMax();

        for (IndoorSession session : sessions) {
            int sessionDays = DateFormatter.DateToDays(session.date);
            if (min <= sessionDays && max >= sessionDays) {
                filtered.add(session);
            }
        }

        return filtered;
    }

    /**
     * Filter for sessions that match the filter criteria for gym name, date range, and whether
     * only boulders or routes were climbed.
     *
     * @param sessions - List of sessions to filter
     * @return - Filtered result list
     */
    public ArrayList<IndoorSession> filter(ArrayList<IndoorSession> sessions) {
        ArrayList<IndoorSession> filtered = filterForGymName(sessions);
        filtered = filterOnlyBoulder(filtered);
        filtered = filterOnlyRoutes(filtered);

        Collections.sort(filtered);
        if (this.orderAscending) Collections.reverse(filtered);

        filtered = filterDates(filtered);

        return filtered;

    }

    /**
     * Sets the click listener for the search button next to the search field.
     *
     * @param clickListener - New listener for the button
     */
    public void setSearchButtonClick(OnClickListener clickListener) {
        this.searchGymButton.setOnClickListener(clickListener);
    }

    /**
     * Sets the click listener for the boulder checkbox.
     *
     * @param clickListener - New listener for the checkbox
     */
    public void setBoulderCheckBoxClickListener(OnClickListener clickListener) {
        this.onlyBouldersCheckbox.setOnClickListener(clickListener);
    }

    /**
     * Sets the click listener for the route checkbox.
     *
     * @param clickListener - New listener for the checkbox
     */
    public void setRouteCheckBoxClickListener(OnClickListener clickListener) {
        this.onlyRoutesCheckbox.setOnClickListener(clickListener);
    }

    /**
     * Sets the date range of the seekbar for limiting the sessions based on their date.
     *
     * @param min - Minimum value of the range
     * @param max - Maximum value of the range
     */
    public void setDateRange(int min, int max) {
        this.dateRangeSeekbar.setRange(min, max);
    }

    /**
     * Set the display text of the date range slider.
     *
     * @param value - Display text
     */
    public void setDateRangeDisplay(String value) {
        this.dateRangeSeekbar.setRangeDisplay(value);
    }

    /**
     * Set the {@link OnRangeChangedListener} for the date range slider.
     *
     * @param changeListener - New Listener for the slider
     */
    public void setOnRangeChangedListener(OnRangeChangedListener changeListener) {
        this.dateRangeSeekbar.setOnRangeChangeListener(changeListener);
    }

    /**
     * Sets an action listener for the search field. Should be used to handle action done events,
     * i.e. submits via the soft keyboard.
     *
     * @param actionListener - New Listener for the search field
     */
    public void setSearchFieldActionListener(TextView.OnEditorActionListener actionListener) {
        this.searchFieldGym.setOnEditorActionListener(actionListener);
    }

    /**
     * Set suggestions for the gym search field.
     *
     * @param suggestions - Suggestions for gyms
     */
    public void setSearchFieldGymSuggestions(String[] suggestions) {
        this.searchFieldGym.setSuggestions(suggestions);
    }
}
