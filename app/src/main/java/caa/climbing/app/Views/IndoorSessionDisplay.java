package caa.climbing.app.Views;

import android.content.Context;
import android.util.AttributeSet;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;
import java.util.Date;

import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Database.DatabaseModels.IndoorSession;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.HelperClasses.DateFormatter;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;
import caa.climbing.app.Views.Buttons.IconButton;
import caa.climbing.app.Views.IndoorSessionTable.IndoorSessionTable;
import caa.climbing.app.Views.Inputs.DatePicker.DatePicker;
import caa.climbing.app.Views.Inputs.LabelInput.LabelInput;

/**
 * A view to display the data from a single indoor sessions.
 */
public class IndoorSessionDisplay extends ExpandableConstraintLayout {

    public IndoorSessionTable boulderTable;
    public IndoorSessionTable routeTable;
    LabelInput gymName;
    DatePicker date;
    boolean editMode;
    private IconButton editBtn, cancelEditBtn, deleteBtn, saveBtn;
    private Integer sessionId;

    private IndoorSession backupSession;

    /**
     * Creates a new {@link IndoorSessionDisplay} object.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public IndoorSessionDisplay(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.indoor_session_display, this);
        this.sessionId = null;

        this.initViews();
        this.fillTables(context);
        this.loadGymSuggestions();
        this.setEditMode(false);
        this.addEventListener();

    }

    /**
     * Initialize the view.
     */
    private void initViews() {
        FlatButton toggleButton = findViewById(R.id.toggle_button);
        ConstraintLayout toggleContainer = findViewById(R.id.toggle_container);
        this.gymName = findViewById(R.id.gym_name);
        this.date = findViewById(R.id.date);
        this.boulderTable = findViewById(R.id.boulder_table);
        this.routeTable = findViewById(R.id.route_table);
        this.editBtn = findViewById(R.id.edit_btn);
        this.cancelEditBtn = findViewById(R.id.cancel_edit_btn);
        this.deleteBtn = findViewById(R.id.delete_btn);
        this.saveBtn = findViewById(R.id.save_btn);

        this.boulderTable.setTableHeader(getContext().getString(R.string.general_climb_type_boulders));
        this.routeTable.setTableHeader(getContext().getString(R.string.general_climb_type_boulders));

        this.boulderTable.addTableBorder();
        this.routeTable.addTableBorder();

        this.setToggleButton(toggleButton);
        this.setToggleContainer(toggleContainer);
    }

    /**
     * Fills the tables for boulder and route ascents with data.
     *
     * @param context - Context from which the view was created
     */
    private void fillTables(Context context) {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(context);
        ArrayList<LabelledInt> gradeBracketsBoulder = dbHelper.getGradeBrackets(true, dbHelper.getReadableDatabase());
        ArrayList<LabelledInt> gradeBracketsRoute = dbHelper.getGradeBrackets(false, dbHelper.getReadableDatabase());

        boulderTable.fillTable(context, gradeBracketsBoulder, false);
        routeTable.fillTable(context, gradeBracketsRoute, false);
    }

    /**
     * Load a list of gyms where indoor sessions were previously completed in order
     * to display them as suggestions in the gym input.
     */
    public void loadGymSuggestions() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(getContext());
        String[] gymSuggestions = dbHelper.getGyms(dbHelper.getReadableDatabase());
        this.gymName.setSuggestions(gymSuggestions);
    }

    /**
     * Set the id of the session.
     *
     * @param sessionId - ID of the session
     */
    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Set the editMode of the inputs. If editMode is enabled, inputs can be edited.
     * If it is disabled, inputs functions as readonly fields.
     *
     * @param editMode - True if inputs can be edited, false otherwise
     */
    public void setEditMode(boolean editMode) {
        this.editMode = editMode;

        this.gymName.setEditMode(this.editMode);
        this.date.setEditMode(this.editMode);

        this.boulderTable.setEditMode(this.editMode);
        this.routeTable.setEditMode(this.editMode);

        if (this.editMode) {
            this.backupSession = this.getSession();
            this.saveBtn.setVisibility(VISIBLE);
            this.editBtn.setVisibility(GONE);
            this.cancelEditBtn.setVisibility(VISIBLE);
            this.deleteBtn.setVisibility(GONE);

            this.toggleButton.setVisibility(GONE);
            this.gymName.setVisibility(VISIBLE);
            this.date.setVisibility(VISIBLE);
        } else {
            this.backupSession = null;
            this.saveBtn.setVisibility(GONE);
            this.editBtn.setVisibility(VISIBLE);
            this.cancelEditBtn.setVisibility(GONE);
            this.deleteBtn.setVisibility(VISIBLE);

            this.toggleButton.setVisibility(VISIBLE);
            this.gymName.setVisibility(GONE);
            this.date.setVisibility(GONE);
        }

    }

    /**
     * Add Listeners to the buttons.
     */
    private void addEventListener() {
        this.editBtn.setOnClickListener((view) -> setEditMode(true));
    }

    /**
     * Get a {@link IndoorSession} object from the inputs.
     *
     * @return - Object that contains the information about the session
     */
    public IndoorSession getSession() {
        Date date = this.date.getValue();
        String gym = this.gymName.getValue();
        ArrayList<LabelledInt> boulders = this.boulderTable.getNumberOfAscents();
        ArrayList<LabelledInt> routes = this.routeTable.getNumberOfAscents();

        return new IndoorSession(this.sessionId, gym, date, boulders, routes);
    }

    /**
     * Get the backup {@link IndoorSession} from before entering the edit mode.
     *
     * @return - Object that contains the information about the ascent before editing
     */
    public IndoorSession getBackupSession() {
        return this.backupSession;
    }

    /**
     * Set the text of the elements in order to display information about a completed session.
     *
     * @param session  - Indoor session to display
     * @param editMode - Whether inputs are in readonly mode or can be edited
     * @param context  - Context of the view
     */
    public void setSession(IndoorSession session, boolean editMode, Context context) {
        this.gymName.setValue(session.gym);
        this.date.setValue(session.date);
        this.toggleButton.setText(String.format("%s,\n%s", session.gym, DateFormatter.format(session.date, false)));
        this.boulderTable.fillTable(context, session.boulders, editMode);
        this.routeTable.fillTable(context, session.routes, editMode);
        this.setSessionId(session.id);
    }

    /**
     * Set a clickListener for the delete button.
     *
     * @param clickListener - Listener for the delete button
     */
    public void setDeleteAction(OnClickListener clickListener) {
        this.deleteBtn.setOnClickListener(clickListener);
    }

    /**
     * Set a clickListener for the save button.
     *
     * @param clickListener - Listener for the save button
     */
    public void setUpdateAction(OnClickListener clickListener) {
        this.saveBtn.setOnClickListener(clickListener);
    }

    /**
     * Set a clickListener for the cancel button.
     *
     * @param clickListener - Listener for the cancel button
     */
    public void setCancelAction(OnClickListener clickListener) {
        this.cancelEditBtn.setOnClickListener(clickListener);
    }
}
