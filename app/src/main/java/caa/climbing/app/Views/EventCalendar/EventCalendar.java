package caa.climbing.app.Views.EventCalendar;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.Dialogs.SelectDialog.SelectDialog;
import caa.climbing.app.HelperClasses.LocaleHelper;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * A custom calendar view. Only displays months.
 */
public class EventCalendar extends ConstraintLayout {
    static protected final int MIN_YEAR = 1900;
    static protected final int MAX_YEAR = 2200;

    final private String accountName;
    final private SimpleDateFormat monthFormat, yearFormat;
    final private CalendarGridPagerAdapter pagerAdapter;
    final private CalendarIntentFragment calendarIntentFragment;
    final private Context context;
    final private FragmentManager fragmentManager;

    private ArrayList<LabelledInt> years;
    private long calendarId;
    private FlatButton previousMonth, nextMonth, addEventButton;
    private TextView currentMonthDisplay, currentYearDisplay;
    private ViewPager2 gridPager;

    /**
     * Creates a new {@link EventCalendar} instance.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public EventCalendar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context = LocaleHelper.setLocale(context);
        this.fragmentManager = Utils.getActivity(context).getSupportFragmentManager();
        this.accountName = this.context.getString(R.string.account_name);
        this.calendarId = -1;

        this.calendarIntentFragment = new CalendarIntentFragment(Utils.getActivity(context), (result -> this.onActionCallBack()));

        Locale defaultLocale = Locale.getDefault();
        this.monthFormat = new SimpleDateFormat("MMMM", defaultLocale);
        this.yearFormat = new SimpleDateFormat("yyyy", defaultLocale);

        View rootView = inflate(context, R.layout.event_calendar, this);
        this.pagerAdapter = new CalendarGridPagerAdapter(this::deleteCalendarEvent, this.calendarId);

        this.initViews(rootView);
        this.initYears();
        this.setDisplayText();
        this.addEventListener();
    }

    /**
     * Callback to execute after the permission to access the calendar was granted.
     * Will be removed once executed, otherwise it will be called continuously.
     */
    private void onActionCallBack() {
        this.createCalendar();
        this.calendarId = getCalendarId();
        this.pagerAdapter.setCalendarId(this.calendarId);
        this.updateCurrentMonth();
        this.calendarIntentFragment.removeCallBack();
    }

    /**
     * Creates a new {@link EventCalendar} instance.
     *
     * @param context - Context from which the view was created
     */
    public EventCalendar(@NonNull Context context) {
        this(context, null);
    }

    /**
     * Initializes the individual components of the view.
     *
     * @param rootView - Root view that contains the rest of the views
     */
    private void initViews(View rootView) {
        this.previousMonth = rootView.findViewById(R.id.previous_month);
        this.nextMonth = rootView.findViewById(R.id.next_month);
        this.currentMonthDisplay = rootView.findViewById(R.id.current_month);
        this.currentYearDisplay = rootView.findViewById(R.id.current_year);
        this.gridPager = rootView.findViewById(R.id.grid_pager);
        this.addEventButton = rootView.findViewById(R.id.add_event_button);

        this.gridPager.setAdapter(this.pagerAdapter);
        int currentPosition = this.pagerAdapter.getCurrentPosition();
        this.gridPager.setCurrentItem(currentPosition, false);
        this.gridPager.setOffscreenPageLimit(1);
    }

    /**
     * Initialize a list of possible years to select from.
     */
    private void initYears() {
        this.years = new ArrayList<>();

        for (int year = MIN_YEAR; year < MAX_YEAR + 1; year++) {
            this.years.add(new LabelledInt(String.valueOf(year), year));
        }
    }

    /**
     * Adds Listener to the buttons for moving between months.
     */
    private void addEventListener() {
        this.previousMonth.setOnClickListener((view) -> this.goToPreviousMonth());
        this.nextMonth.setOnClickListener((view) -> this.goToNextMonth());
        this.gridPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setDisplayText();
                updateCurrentMonth();
            }
        });
        this.currentYearDisplay.setOnClickListener((view) -> this.openSelectYearDialog());
    }

    /**
     * Update the display text to show the currently selected month and year.
     */
    private void setDisplayText() {
        int currentPosition = this.gridPager.getCurrentItem();
        Date currentDate = this.pagerAdapter.getDate(currentPosition);

        if (currentDate == null) return;

        String currentMonth = this.monthFormat.format(currentDate);
        String currentYear = this.yearFormat.format(currentDate);
        this.currentMonthDisplay.setText(currentMonth);
        this.currentYearDisplay.setText(currentYear);
    }

    /**
     * Go to the month before the currently selected one.
     */
    private void goToPreviousMonth() {
        this.gridPager.setCurrentItem(this.gridPager.getCurrentItem() - 1);
        this.setDisplayText();
        this.updateCurrentMonth();
    }

    /**
     * Go to the month after the currently selected one.
     */
    private void goToNextMonth() {
        this.gridPager.setCurrentItem(this.gridPager.getCurrentItem() + 1);
        this.setDisplayText();
        this.updateCurrentMonth();
    }

    /**
     * Open a select dialog to choose which year to display.
     */
    private void openSelectYearDialog() {
        int currentPosition = this.gridPager.getCurrentItem();
        Date currentDate = this.pagerAdapter.getDate(currentPosition);

        if (currentDate == null) return;

        int currentYear = Integer.parseInt(this.yearFormat.format(currentDate));

        SelectDialog selectDialog = new SelectDialog(this.context.getString(R.string.calendar_select_year_dialog_title), currentYear, this.years);

        OnClickListener okClick = (view) -> {
            LabelledInt selected = selectDialog.getSelectedOption();
            int offset = (selected.value - currentYear) * 12;
            this.gridPager.setCurrentItem(currentPosition + offset);
            this.setDisplayText();
            this.updateCurrentMonth();
        };

        selectDialog.setOkClick(okClick);

        selectDialog.show(this.fragmentManager, "select");
    }

    /**
     * Create a custom calendar to store events in.
     */
    private void createCalendar() {
        if (calendarExists()) return;

        String calendarName = this.context.getString(R.string.app_name);
        ContentResolver contentResolver = this.context.getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(CalendarContract.Calendars.ACCOUNT_NAME, this.accountName);
        cv.put(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
        cv.put(CalendarContract.Calendars.NAME, calendarName);
        cv.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, calendarName);
        cv.put(CalendarContract.Calendars.VISIBLE, 1);
        cv.put(CalendarContract.Calendars.CALENDAR_COLOR, ResourcesCompat.getColor(getResources(), R.color.primaryLight, null));
        cv.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_OWNER);
        cv.put(CalendarContract.Calendars.OWNER_ACCOUNT, this.accountName);
        cv.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
        cv.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, TimeZone.getDefault().getDisplayName());
        cv.put(CalendarContract.Calendars.ALLOWED_REMINDERS, String.format("%s,%s,%s", CalendarContract.Reminders.METHOD_DEFAULT, CalendarContract.Reminders.METHOD_ALARM, CalendarContract.Reminders.METHOD_ALERT));
        cv.put(CalendarContract.Calendars.ALLOWED_AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        cv.put(CalendarContract.Calendars.ALLOWED_ATTENDEE_TYPES, CalendarContract.Attendees.TYPE_NONE);

        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        Uri.Builder builder = uri.buildUpon()
                .appendQueryParameter(android.provider.CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL)
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, this.accountName);

        contentResolver.insert(builder.build(), cv);
    }

    /**
     * Check whether the calendar was already created.
     *
     * @return - Whether the calendar exists already
     */
    private boolean calendarExists() {
        return getCalendarId() != -1;
    }

    /**
     * Get the ID of the calendar to use.
     *
     * @return - ID of the calendar or -1 if the calendar does not exist
     */
    private long getCalendarId() {
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        String[] projection = new String[]{CalendarContract.Calendars._ID};
        String selection = String.format("%s = ? AND %s = ?", CalendarContract.Calendars.ACCOUNT_NAME, CalendarContract.Calendars.ACCOUNT_TYPE);
        String[] selArgs = new String[]{this.accountName, CalendarContract.ACCOUNT_TYPE_LOCAL};

        ContentResolver contentResolver = this.context.getContentResolver();

        Cursor cursor = contentResolver.query(uri, projection, selection, selArgs, null);
        if (cursor.moveToFirst()) {
            long id = cursor.getLong(0);
            cursor.close();
            return id;
        }
        return -1;
    }

    /**
     * Get the currently visible {@link CalendarGrid} view from the view pager.
     *
     * @return - Currently active calendar view
     */
    @Nullable
    private CalendarGrid getCalendarGrid() {
        int currentPosition = this.gridPager.getCurrentItem();
        RecyclerView recyclerView = (RecyclerView) this.gridPager.getChildAt(0);

        CalendarGridPagerAdapter.ViewHolder viewHolder = (CalendarGridPagerAdapter.ViewHolder) recyclerView.findViewHolderForAdapterPosition(currentPosition);
        if (viewHolder == null) return null;

        return viewHolder.calendarGrid;
    }

    /**
     * Set a click listener to the button for adding events to the calendar.
     *
     * @param clickListener - New listener for the button
     */
    public void setAddEventButtonListener(OnClickListener clickListener) {
        this.addEventButton.setOnClickListener(clickListener);
    }

    /**
     * Get the currently selected date.
     *
     * @return - Currently selected date
     */
    public Date getSelectedDate() {
        CalendarGrid calendarGrid = this.getCalendarGrid();
        if (calendarGrid == null) return null;

        return calendarGrid.getSelectedDate();
    }

    /**
     * Save a {@link CalendarEvent} to the calendar.
     *
     * @param event - Event to save
     */
    public void saveCalendarEvent(CalendarEvent event) {
        this.calendarIntentFragment.saveCalendarEvent(event, this.calendarId);
        this.updateCurrentMonth();
    }

    /**
     * Delete a {@link CalendarEvent} from the calendar.
     *
     * @param event - Event to delete
     */
    private void deleteCalendarEvent(CalendarEvent event) {
        this.calendarIntentFragment.deleteCalendarEvent(event, this.calendarId);
        this.updateCurrentMonth();
    }

    /**
     * Update the display to show newly added events.
     */
    private void updateCurrentMonth() {
        CalendarGrid calendarGrid = this.getCalendarGrid();
        if (calendarGrid == null) return;

        calendarGrid.readPublicCalendarEvents(this.calendarId);
    }

    /**
     * Called from layout when this view should assign a size and position to each of its children.
     *
     * @param changed - This is a new size or position for this view
     * @param left    - Left position, relative to parent
     * @param top     - Top position, relative to parent
     * @param right   - Right position, relative to parent
     * @param bottom  - Bottom position, relative to parent
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        this.calendarIntentFragment.start();
    }
}
