package caa.climbing.app.Views.Inputs.ToggleCheckbox;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import caa.climbing.app.R;

/**
 * Custom Checkbox with a label.
 */
public class ToggleCheckbox extends ConstraintLayout {

    private final CheckBox checkBox;
    private final TextView label;

    /**
     * Creates a new {@link ToggleCheckbox} instance.
     *
     * @param context - Context where the checkbox was created
     */
    public ToggleCheckbox(Context context) {
        super(context);
        inflate(context, R.layout.checkbox, this);

        this.checkBox = findViewById(R.id.checkbox);
        this.label = findViewById(R.id.label);
    }

    /**
     * Add a listener to the checkbox that is called when the checked-status of the checkbox changes.
     *
     * @param listener - Listener to add to the checkbox
     */
    public void addEventListener(CompoundButton.OnCheckedChangeListener listener) {
        this.checkBox.setOnCheckedChangeListener(listener);
    }

    /**
     * Set the label for the checkbox.
     *
     * @param label - Label for the checkbox
     */
    public void setLabel(String label) {
        this.label.setText(label);
    }
}
