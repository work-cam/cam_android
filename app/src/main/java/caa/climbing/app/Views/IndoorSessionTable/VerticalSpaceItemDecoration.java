package caa.climbing.app.Views.IndoorSessionTable;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Decorator to space items evenly.
 */
class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private final int verticalSeparation;

    /**
     * Create a new {@link VerticalSpaceItemDecoration}.
     *
     * @param verticalSeparation - Separation between items
     */
    public VerticalSpaceItemDecoration(int verticalSeparation) {
        this.verticalSeparation = verticalSeparation;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, RecyclerView parent, @NonNull RecyclerView.State state) {
        RecyclerView.Adapter<?> parentAdapter = parent.getAdapter();

        if (parentAdapter == null) return;

        if (parent.getChildAdapterPosition(view) != parentAdapter.getItemCount() - 1) {
            outRect.bottom = verticalSeparation;
        }
    }
}
