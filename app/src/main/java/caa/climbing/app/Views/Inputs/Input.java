package caa.climbing.app.Views.Inputs;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;
import java.util.Arrays;

import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import org.apache.commons.lang3.ArrayUtils;

/**
 * Base class for form elements that consist of an (optional) label and an input.
 */
public abstract class Input<T> extends ConstraintLayout {
    private final String SELECTOR;
    private String label;
    private String tag;
    protected boolean smallViewPort;
    protected final TextView input;

    protected View rootView;
    protected ArrayList<TextView> inputs;
    protected View inputContainer;
    private TextView labelDisplay;

    protected boolean editMode;

    final private Drawable defaultBackground;
    final private Drawable errorBackground;
    final protected float smallFontSize;
    final protected float standardFontSize;


    /**
     * Creates a new {@link Input} object.
     *
     * @param context - Context from which the view is created
     * @param attrs   - Optional Attributes that can be applied to the view
     */
    public Input(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.rootView = inflate(context, getResourceId(), getRoot());
        this.smallViewPort = false;
        this.editMode = true;
        this.SELECTOR = context.getString(R.string.selector_input);

        this.initViews();
        // Setting final fields
        this.input = this.inputs.get(0);
        this.defaultBackground = ResourcesCompat.getDrawable(getResources(), R.drawable.textfield_background, getContext().getTheme());
        this.errorBackground = ResourcesCompat.getDrawable(getResources(), R.drawable.textfield_background__invalid, getContext().getTheme());
        this.smallFontSize = Utils.sp2px(context, getResources().getDimension(R.dimen.fontsize_small));
        this.standardFontSize = Utils.sp2px(context, getResources().getDimension(R.dimen.fontsize_standard));

        this.styleViews(context, attrs);
    }

    /**
     * Get the id of the resource that should be inflated.
     *
     * @return - Id of the resource that should be inflated
     */
    protected abstract int getResourceId();

    /**
     * Get the root view to attach the instance to.
     *
     * @return - Root view to attach the instance to
     */
    protected abstract ViewGroup getRoot();

    /**
     * Set the value of the input.
     *
     * @param value - New value of the input
     */
    public abstract void setValue(T value);

    /**
     * Get the value of the input.
     */
    public abstract T getValue();

    /**
     * Pass attributes from the compound view to the input.
     *
     * @param context - Context from which the view is created
     * @param attrs   - Optional Attributes that can be applied to the view
     */
    final protected void setInputAttributes(Context context, @Nullable AttributeSet attrs) {
        if (attrs == null) return;
        int[] inputAttributes = new int[]{android.R.attr.inputType, android.R.attr.hint, android.R.attr.imeOptions, android.R.attr.singleLine, android.R.attr.maxLines, android.R.attr.textSize, android.R.attr.textAlignment};

        // IMPORTANT: Attributes must first be sorted, otherwise some attributes may not be found
        Arrays.sort(inputAttributes);

        TypedArray a = context.obtainStyledAttributes(attrs, inputAttributes);

        int indexOfInputType = ArrayUtils.indexOf(inputAttributes, android.R.attr.inputType);
        int indexOfHint = ArrayUtils.indexOf(inputAttributes, android.R.attr.hint);
        int indexOfImeOptions = ArrayUtils.indexOf(inputAttributes, android.R.attr.imeOptions);
        int indexOfSingleLine = ArrayUtils.indexOf(inputAttributes, android.R.attr.singleLine);
        int indexOfMaxLines = ArrayUtils.indexOf(inputAttributes, android.R.attr.maxLines);
        int indexOfTextSize = ArrayUtils.indexOf(inputAttributes, android.R.attr.textSize);
        int indexOfTextAlignment = ArrayUtils.indexOf(inputAttributes, android.R.attr.textAlignment);

        int inputType = a.getInt(indexOfInputType, InputType.TYPE_CLASS_TEXT);
        CharSequence hint = a.getText(indexOfHint);
        int imeOptions = a.getInt(indexOfImeOptions, EditorInfo.IME_ACTION_NEXT);

        boolean singleLine = a.getBoolean(indexOfSingleLine, true);
        int maxLines = a.getInt(indexOfMaxLines, 1);
        float textSize = a.getDimension(indexOfTextSize, -1);
        int textAlignment = a.getInt(indexOfTextAlignment, View.TEXT_ALIGNMENT_CENTER);

        this.input.setInputType(inputType);
        this.input.setHint(hint);
        this.input.setImeOptions(imeOptions);
        this.input.setSingleLine(singleLine);
        this.input.setMaxLines(maxLines);

        float defaultFontSize = this.smallViewPort ? this.smallFontSize : this.standardFontSize;
        float setFontSize = textSize == -1 ? defaultFontSize : Utils.sp2px(context, textSize);
        this.input.setTextSize(setFontSize);
        if (this.labelDisplay != null) this.labelDisplay.setTextSize(setFontSize);
        this.input.setTextAlignment(textAlignment);

        a.recycle();
    }

    /**
     * Initialize the view.
     */
    private void initViews() {
        this.inputs = this.getViewBySelector(this.getRoot(), SELECTOR);
        this.labelDisplay = rootView.findViewById(R.id.label);
        this.inputContainer = rootView.findViewById(R.id.inputContainer);
        for (TextView input : this.inputs) {
            input.setId(View.generateViewId());
            input.clearFocus();
        }
    }

    /**
     * Style the views based on the attributes that are passed.
     *
     * @param context - Context from which the view is created
     * @param attrs   - Optional Attributes that can be applied to the view
     */
    private void styleViews(Context context, @Nullable AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Input, 0, 0);
            this.label = a.getString(R.styleable.Input_label);
            this.tag = a.getString(R.styleable.Input_tag);
            this.smallViewPort = a.getBoolean(R.styleable.Input_smallViewPort, false);
            int labelColor = a.getColor(R.styleable.Input_labelColor, ContextCompat.getColor(getContext(), R.color.black));
            a.recycle();

            this.setLabel(this.label);
            this.setLabelColor(labelColor);
        }
    }

    /**
     * Get the label of the input.
     *
     * @return - Label of the input
     */
    public String getLabel() {
        return labelDisplay.getText().toString();
    }

    /**
     * Set the label of the input.
     *
     * @param label - New label of the input
     */
    public void setLabel(String label) {
        if (this.labelDisplay == null) return;

        this.label = label;
        if (label == null || label.equals("")) {
            Guideline guideline = this.rootView.findViewById(R.id.guideline_half);
            if (guideline != null) {
                guideline.setGuidelinePercent(0);
            }
            this.labelDisplay.setVisibility(GONE);

            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) this.inputContainer.getLayoutParams();
            layoutParams.startToStart = ConstraintLayout.LayoutParams.PARENT_ID;
            layoutParams.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID;
            layoutParams.leftMargin = 0;
            this.inputContainer.setLayoutParams(layoutParams);
        } else {
            this.labelDisplay.setText(label);
        }
    }

    /**
     * Set the text color of the input label.
     *
     * @param color - New color of the label text
     */
    public void setLabelColor(int color) {
        if (this.labelDisplay == null) return;
        this.labelDisplay.setTextColor(color);
    }

    /**
     * Get the displayed text of the (first) input.
     *
     * @return - Value of the input
     */
    final protected String getTextValue() {
        return this.input.getText().toString();
    }

    /**
     * Set the displayed text value in the input.
     *
     * @param value - Text to display
     */
    final protected void setTextValue(String value) {
        this.input.setText(value);
    }

    /**
     * Sets a tag that should identify the input uniquely.
     *
     * @param tag - Tag to identify the input
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * Get the tag that identifies the input.
     *
     * @return - Tag to identify the input
     */
    public String getTag() {
        return this.tag;
    }

    /**
     * Removes the input text and clears the focus.
     */
    public void clearText() {
        this.inputs.forEach(this::clearInput);
    }

    /**
     * Remove text from a single view.
     *
     * @param input - View to clear
     */
    private void clearInput(View input) {
        if (input instanceof EditText) {
            ((EditText) input).getText().clear();
        } else if (input instanceof TextView) {
            ((TextView) input).setText("");
        }
        input.clearFocus();
    }

    /**
     * Mark the input as invalid by setting the corresponding background drawable.
     */
    @CallSuper
    public void markAsInvalid() {
        this.inputContainer.setBackground(this.errorBackground);
        this.inputs.get(0).requestFocus();
    }

    /**
     * Reset the background to the default background
     */
    @CallSuper
    public void markAsValid() {
        this.inputs.get(0).requestFocus();
        this.inputContainer.setBackground(this.defaultBackground);
    }

    /**
     * Enable the editMode for the input.
     */
    @CallSuper
    protected void enable() {
        this.editMode = true;
        this.inputs.forEach((input) -> input.setEnabled(true));
        this.inputContainer.setBackground(this.defaultBackground);
        if (this.labelDisplay != null) this.labelDisplay.setTypeface(Typeface.DEFAULT);
    }

    /**
     * Disable the editMode for the input.
     */
    @CallSuper
    protected void disable() {
        this.inputs.forEach((input) -> {
            input.setEnabled(false);
            input.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        });
        this.inputContainer.setBackground(null);
        if (this.labelDisplay != null) this.labelDisplay.setTypeface(Typeface.DEFAULT_BOLD);
    }

    /**
     * Set the editMode for the input.
     *
     * @param editMode - True if input can be edited, false otherwise
     */
    final public void setEditMode(boolean editMode) {
        if (editMode) this.enable();
        else this.disable();
    }

    /**
     * Optimize the view for display in narrow spaces.
     *
     * @param smallViewPort - True if view is displayed in a narrow space, false otherwise
     */
    @CallSuper
    public void setSmallViewPort(boolean smallViewPort) {
        this.smallViewPort = smallViewPort;

        int smallPaddingHorizontal = (int) getResources().getDimension(R.dimen.input_padding_horizontal__small);
        int smallPaddingVertical = (int) getResources().getDimension(R.dimen.input_padding_vertical__small);
        int standardPaddingHorizontal = (int) getResources().getDimension(R.dimen.input_padding_horizontal);
        int standardPaddingVertical = (int) getResources().getDimension(R.dimen.input_padding_vertical);

        if (this.smallViewPort) {
            if (this.labelDisplay != null)
                this.labelDisplay.setTextSize(this.smallFontSize);
            this.inputs.forEach((input) -> input.setTextSize(this.smallFontSize));
            this.inputContainer.setPadding(smallPaddingHorizontal, smallPaddingVertical, smallPaddingHorizontal, smallPaddingVertical);
        } else {
            if (this.labelDisplay != null)
                this.labelDisplay.setTextSize(this.standardFontSize);
            this.inputs.forEach((input) -> input.setTextSize(this.standardFontSize));
            this.inputContainer.setPadding(standardPaddingHorizontal, standardPaddingVertical, standardPaddingHorizontal, standardPaddingVertical);
        }
    }

    /**
     * Finds all input elements based on a given tag.
     *
     * @return - List of matching views.
     */
    private ArrayList<TextView> getViewBySelector(ViewGroup root, String selector) {
        ArrayList<TextView> views = new ArrayList<>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewBySelector((ViewGroup) child, selector));
            }

            final Object tagObj = child.getTag();

            if (tagObj != null) {
                String childTag = tagObj.toString();

                if (childTag.startsWith(selector) && child instanceof TextView) {
                    views.add((TextView) child);
                }
            }

        }
        return views;
    }

    /**
     * Add a TextWatcher to the input field.
     *
     * @param textWatcher - Watcher for the input
     */
    public void addTextChangedListener(TextWatcher textWatcher) {
        this.input.addTextChangedListener(textWatcher);
    }

    /**
     * Add an ActionListener to the input.
     * Enables, for example, to submit a form when enter is pressed.
     *
     * @param actionListener - Listener for the input
     */
    public void setOnEditorActionListener(TextView.OnEditorActionListener actionListener) {
        this.input.setOnEditorActionListener(actionListener);
    }

    /**
     * Save the state of the view, i.e. the value of the input.
     *
     * @return - The saved state
     */
    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        InputSaveState<T> savedState = new InputSaveState<>(superState);
        savedState.setValue(getValue());
        return savedState;
    }

    /**
     * When the instance is restored, the state is also restored.
     *
     * @param state - The saved state
     */
    @Override
    public void onRestoreInstanceState(Parcelable state) {
        InputSaveState<T> savedState = (InputSaveState<T>) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setValue(savedState.getValue());
    }
}
