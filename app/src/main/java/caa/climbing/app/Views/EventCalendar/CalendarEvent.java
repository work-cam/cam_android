package caa.climbing.app.Views.EventCalendar;

import java.util.Date;

/**
 * A wrapper class for events to save in the calendar.
 */
public class CalendarEvent {

    private final String title;
    private final Date date;
    private final long id;

    /**
     * Create a new {@link CalendarEvent} object.
     *
     * @param title - Title of the event
     * @param date  - Date of the event
     * @param id    - ID of the event
     */
    public CalendarEvent(String title, Date date, long id) {
        this.title = title;
        this.date = date;
        this.id = id;
    }

    /**
     * Create a new {@link CalendarEvent} object.
     *
     * @param title - Title of the event
     * @param date  - Date of the event
     */
    public CalendarEvent(String title, Date date) {
        this.title = title;
        this.date = date;
        this.id = -1;
    }

    /**
     * Get the title of the event.
     *
     * @return - Title of the event
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get the date of the event.
     *
     * @return - Date of the event
     */
    public Date getDate() {
        return date;
    }

    /**
     * Get the ID of the event.
     *
     * @return - ID of the event
     */
    public long getId() {
        return this.id;
    }
}
