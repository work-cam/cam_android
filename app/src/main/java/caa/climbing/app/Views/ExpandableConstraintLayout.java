package caa.climbing.app.Views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A Layout that allows the expansion and collapse of a sub-container.
 */
public class ExpandableConstraintLayout extends ConstraintLayout {

    protected FlatButton toggleButton;
    protected ConstraintLayout toggleContainer;
    private final Drawable triangleDown, triangleUp;
    private final ArrayList<View> hideViewsOnExpansion;

    /**
     * Creates a new {@link ExpandableConstraintLayout} object.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public ExpandableConstraintLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.triangleUp = ResourcesCompat.getDrawable(getResources(), R.drawable.triangle__black_up, null);
        this.triangleDown = ResourcesCompat.getDrawable(getResources(), R.drawable.triangle__black_down, null);
        this.hideViewsOnExpansion = new ArrayList<>();
    }

    /**
     * Set a view that should be hidden upon expansion of the toggle container.
     *
     * @param views - Views to hide
     */
    protected void setHideViewOnExpansion(View... views) {
        this.hideViewsOnExpansion.addAll(Arrays.asList(views));
    }

    /**
     * Set the container that should be expanded/collapsed.
     *
     * @param toggleContainer - Container to expand/collapse
     */
    protected void setToggleContainer(ConstraintLayout toggleContainer) {
        this.toggleContainer = toggleContainer;
    }

    /**
     * Set the button that triggers the expansion/collapse.
     *
     * @param toggleButton - Button that triggers the expansion/collapse
     */
    protected void setToggleButton(FlatButton toggleButton) {
        this.toggleButton = toggleButton;
    }

    /**
     * Set a ClickListener on the toggle button.
     *
     * @param clickListener - Listener to add
     */
    public void setToggleAction(OnClickListener clickListener) {
        if (this.toggleButton == null) return;

        this.toggleButton.setOnClickListener(clickListener);
    }

    /**
     * Toggle the input container between its expanded and collapsed state.
     *
     * @param expand - Whether to expand or collapse the view
     */
    public void toggleExpansion(boolean expand) {
        if (this.toggleButton == null || this.toggleContainer == null) return;

        if (expand) this.expandView();
        else this.collapseView();
    }

    /**
     * Expand the container with the inputs.
     */
    private void expandView() {
        this.toggleContainer.setVisibility(VISIBLE);
        this.toggleButton.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, this.triangleUp, null);

        for (View view : this.hideViewsOnExpansion) {
            view.setVisibility(GONE);
        }
    }

    /**
     * Collapse the container with the inputs.
     */
    private void collapseView() {
        this.toggleContainer.setVisibility(GONE);
        this.toggleButton.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, this.triangleDown, null);
    }
}
