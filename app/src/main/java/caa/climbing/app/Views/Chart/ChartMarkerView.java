package caa.climbing.app.Views.Chart;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.Nullable;
import caa.climbing.app.R;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

/**
 * A view to display values of a highlighted data point in a Chart.
 */
public class ChartMarkerView extends MarkerView {

    private final TextView valueDisplay;
    private final ValueFormatter formatter;
    private MPPointF offset;

    /**
     * @param context - Context from which the view was created
     */
    public ChartMarkerView(Context context) {
        this(context, R.layout.chart_marker_view, null);
    }

    /**
     * @param context        - Context from which the view was created
     * @param layoutResource - Layout resource to use for the view
     * @param formatter      - Formatter to format the displayed x-Value
     */
    public ChartMarkerView(Context context, int layoutResource, @Nullable ValueFormatter formatter) {
        super(context, layoutResource);
        this.valueDisplay = findViewById(R.id.value_display);
        this.formatter = formatter;
    }

    /**
     * Get the offset of the marker so that it is centered horizontally and vertically.
     *
     * @return - Offset of the marker
     */
    @Override
    public MPPointF getOffset() {
        if (this.offset == null) {
            this.offset = new MPPointF(-getWidth() / 2f, -getHeight() / 2f);
        }
        return this.offset;
    }

    /**
     * Update the content of the marker every time it is redrawn.
     *
     * @param entry     - Entry the marker belongs to
     * @param highlight - The highlighted object with info about the highlighted value
     */
    @Override
    public void refreshContent(Entry entry, Highlight highlight) {
        float xValue = entry.getX();
        float yValue = highlight.getY();

        if (entry instanceof BarEntry) {
            BarEntry barEntry = (BarEntry) entry;
            boolean isStacked = highlight.isStacked();
            float[] yValues = barEntry.getYVals();
            int idx = highlight.getStackIndex();

            if (yValues != null && isStacked && idx > -1 && idx < yValues.length - 1) {
                xValue = idx;
                yValue = yValues[idx];
            }
        }

        String xDisplay = this.formatter == null ? String.valueOf(xValue) : formatter.getAxisLabel(xValue, null);
        String yDisplay = String.valueOf(yValue);

        this.valueDisplay.setText(String.format("%s\n%s", yDisplay, xDisplay));

        super.refreshContent(entry, highlight);
    }
}
