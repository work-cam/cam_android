package caa.climbing.app.Views.Inputs.DatePicker;

import android.app.DatePickerDialog;
import android.content.Context;

import androidx.annotation.Nullable;

import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.ViewGroup;

import caa.climbing.app.HelperClasses.DateFormatter;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import caa.climbing.app.Views.Inputs.Input;

import java.util.Calendar;
import java.util.Date;

/**
 * A view to display and select a date from a calendar.
 */
public class DatePicker extends Input<Date> {
    private final Calendar calendar = Calendar.getInstance();

    /**
     * Creates a new {@link DatePicker} object.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public DatePicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.initViews();
        Utils.setStyle(context, this.inputContainer, attrs, null);
        this.addEventListener();
        this.setSmallViewPort(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getResourceId() {
        return R.layout.date_picker;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ViewGroup getRoot() {
        return this;
    }

    /**
     * Initialize the view.
     */
    private void initViews() {
        // Since the input element is an EditText, the default font size must be set to match that of other input elements
        float fontSize = this.smallViewPort ? this.smallFontSize : this.standardFontSize;

        this.input.setTextSize(fontSize);

        this.setValue(calendar.getTime());
    }

    /**
     * Add Listeners to the input.
     */
    private void addEventListener() {
        final DatePickerDialog.OnDateSetListener date = (datePicker, year, month, day) -> {
            this.calendar.set(Calendar.YEAR, year);
            this.calendar.set(Calendar.MONTH, month);
            this.calendar.set(Calendar.DAY_OF_MONTH, day);
            Utils.resetCalendar(this.calendar);
            setTextValue(DateFormatter.format(this.calendar.getTime(), false));
        };

        this.input.setOnClickListener((view) -> {
            if (editMode) {
                Utils.hideKeyboard(getContext());

                // show date picker dialog
                DatePickerDialog dialog = new DatePickerDialog(getContext(), R.style.DateDialogStyle, date, this.calendar.get(Calendar.YEAR), this.calendar.get(Calendar.MONTH), this.calendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setFirstDayOfWeek(Calendar.MONDAY);
                dialog.show();
            }
        });
    }

    /**
     * Set the value of the input. If the passed date string cannot be parsed,
     * the current date is used instead.
     *
     * @param value - Date string
     */
    @Override
    public void setValue(Date value) {
        String date = DateFormatter.format(value, false);
        this.setTextValue(date);
        this.calendar.setTime(value);
        Utils.resetCalendar(this.calendar);
    }

    /**
     * Get the value of the input.
     *
     * @return - Value of the input
     */
    @Override
    public Date getValue() {
        Utils.resetCalendar(this.calendar);
        return this.calendar.getTime();
    }

    /**
     * Add a TextWatcher to the datePicker.
     *
     * @param textWatcher - TextWatcher to add
     */
    public void addTextChangedListener(TextWatcher textWatcher) {
        this.input.addTextChangedListener(textWatcher);
    }
}
