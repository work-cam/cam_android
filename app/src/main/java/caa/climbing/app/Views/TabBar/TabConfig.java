package caa.climbing.app.Views.TabBar;

import androidx.fragment.app.Fragment;

/**
 * A simple class to configure the display of a tab.
 */
public class TabConfig {
    private final String tabTitle;
    private final Fragment tabBarContent;

    /**
     * Creates a new {@link TabConfig} object
     *
     * @param tabTitle      - Text displayed in the tab
     * @param tabBarContent - Content fragment for the tab
     */
    public TabConfig(String tabTitle, Fragment tabBarContent) {
        this.tabTitle = tabTitle;
        this.tabBarContent = tabBarContent;
    }

    /**
     * Get the title of the tab.
     *
     * @return - The title of the tab.
     */
    public String getTabTitle() {
        return this.tabTitle;
    }

    /**
     * Get the content fragment of the tab.
     *
     * @return - The content fragment of the tab
     */
    public Fragment getTabBarContent() {
        return this.tabBarContent;
    }

}
