package caa.climbing.app.Views.Filter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import caa.climbing.app.Animations.DropdownAnimation;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;

/**
 * Base class for specific filter components inside of the log book.
 * Specific filter components should extend this class.
 */
public abstract class FilterBase extends ConstraintLayout {
    private FlatButton filterToggleBtn;
    private ConstraintLayout filterContainer;
    private boolean filterExpanded;
    private final Context context;

    /**
     * Creates a new {@link FilterBase} object.
     *
     * @param context - Context from which the view was created
     * @param attrs   - Attributes that are passed to the view
     */
    public FilterBase(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    /**
     * Initialize the base functionality of the filter, i.e. the toggle button
     *
     * @param view - Root view of the specific filter component
     */
    public void init(View view) {
        this.initView(view);
        addEventListener();
    }

    /**
     * Initializes the filter toggle button.
     */
    private void initView(View view) {
        this.filterToggleBtn = view.findViewById(R.id.filter_toggle_btn);
        this.filterContainer = view.findViewById(R.id.filter_container);

        this.filterExpanded = false;
        this.filterContainer.getLayoutParams().height = 0;
        this.filterContainer.setVisibility(View.GONE);
    }

    /**
     * Adds event listener to the toggle button in order to expand/collapse the filter container.
     */
    private void addEventListener() {
        if (this.filterToggleBtn == null) return;

        this.filterToggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterExpanded) {
                    collapse();
                } else {
                    expand();
                }
            }
        });
    }

    /**
     * Collapse the filter container.
     */
    public void collapse() {
        if (this.filterToggleBtn == null) return;

        Drawable triangleDown = getResources().getDrawable(R.drawable.triangle__down);
        DropdownAnimation.collapse(this.filterContainer);
        this.filterToggleBtn.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, triangleDown, null);
        this.filterExpanded = false;
    }

    /**
     * Expand the filter container.
     */
    public void expand() {
        if (this.filterToggleBtn == null) return;

        Drawable triangleUp = getResources().getDrawable(R.drawable.triangle__up);
        DropdownAnimation.expand(this.filterContainer);
        this.filterToggleBtn.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, triangleUp, null);
        this.filterExpanded = true;
    }
}
