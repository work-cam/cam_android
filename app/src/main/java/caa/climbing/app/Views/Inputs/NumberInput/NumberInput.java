package caa.climbing.app.Views.Inputs.NumberInput;

import android.annotation.SuppressLint;
import android.content.Context;

import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;

import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.IconButton;
import caa.climbing.app.Views.Inputs.Input;

/**
 * View to allow the selection of a numeric value by pressing plus/minus buttons.
 */
public class NumberInput extends Input<Integer> {

    private IconButton plusBtn;
    private IconButton minusBtn;

    private boolean plusButtonIsPressed;
    private boolean minusButtonIsPressed;
    private final Handler longPressHandler;
    private final long REPEAT_INTERVAL_MS = 200;

    private String value;

    /**
     * Creates a new {@link NumberInput} object.
     *
     * @param context - Context from which the view is created
     * @param attrs   - Optional Attributes that can be applied to the view
     */
    public NumberInput(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.minusButtonIsPressed = false;
        this.plusButtonIsPressed = false;

        this.longPressHandler = new Handler();

        this.initViews();
        this.setSmallViewPort(this.smallViewPort);

        this.addEventListener();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getResourceId() {
        return R.layout.number_input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ViewGroup getRoot() {
        return this;
    }

    /**
     * Set the value of the input.
     *
     * @param value - New value of the input
     */
    @Override
    public void setValue(Integer value) {
        this.setTextValue(String.valueOf(value));
    }

    /**
     * Get the value of the input.
     *
     * @return - Value of the input
     */
    @Override
    public Integer getValue() {
        String value = this.getTextValue();
        if (value.equals("")) {
            return 0;
        } else {
            return Integer.parseInt(value);
        }
    }

    /**
     * Initializes the views.
     */
    private void initViews() {
        this.plusBtn = this.rootView.findViewById(R.id.roundPlusBtn);
        this.minusBtn = this.rootView.findViewById(R.id.roundMinusBtn);
    }

    /**
     * Increment the displayed value by one.
     */
    private void incrementValue() {
        value = this.input.getText().toString();
        if (value.equals("")) {
            this.input.setText("1");
        } else {
            int number = Integer.parseInt(value) + 1;
            this.input.setText(String.valueOf(number));
        }
    }

    /**
     * Decrement the displayed value by one.
     */
    private void decrementValue() {
        value = this.input.getText().toString();
        if (value.equals("")) {
            this.input.setText("0");
        } else {
            int number = Integer.parseInt(value) - 1;
            if (number < 0) {
                this.input.setText(String.valueOf(0));
            } else {
                this.input.setText(String.valueOf(number));
            }

        }
    }

    /**
     * Add Listeners to the buttons
     */
    @SuppressLint("ClickableViewAccessibility")
    private void addEventListener() {
        this.plusBtn.setOnClickListener((view) -> {
            Utils.hideKeyboard(getContext());
            this.incrementValue();
        });

        this.plusBtn.setOnLongClickListener((view) -> {
            Utils.hideKeyboard(getContext());
            this.plusButtonIsPressed = true;
            this.longPressHandler.post(new AutoIncrement());
            return false;
        });


        this.plusBtn.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP || motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                this.plusButtonIsPressed = false;
            }
            return false;
        });

        this.minusBtn.setOnClickListener((view) -> {
            Utils.hideKeyboard(getContext());
            this.decrementValue();
        });

        this.minusBtn.setOnLongClickListener((view) -> {
            Utils.hideKeyboard(getContext());
            this.minusButtonIsPressed = true;
            this.longPressHandler.post(new AutoDecrement());
            return false;
        });

        this.minusBtn.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP || motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                this.minusButtonIsPressed = false;
            }
            return false;
        });
    }

    /**
     * Class for handling continuously incrementing the displayed value.
     */
    private class AutoIncrement implements Runnable {
        @Override
        public void run() {
            if (plusButtonIsPressed) {
                incrementValue();
                longPressHandler.postDelayed(new AutoIncrement(), REPEAT_INTERVAL_MS);
            }
        }
    }

    /**
     * Class for handling continuously decrementing the displayed value.
     */
    private class AutoDecrement implements Runnable {
        @Override
        public void run() {
            if (minusButtonIsPressed) {
                decrementValue();
                longPressHandler.postDelayed(new AutoDecrement(), REPEAT_INTERVAL_MS);
            }
        }
    }

    /**
     * Enable the editMode for the input.
     */
    protected void enable() {
        super.enable();
        if (!this.smallViewPort) {
            this.plusBtn.setVisibility(VISIBLE);
            this.minusBtn.setVisibility(VISIBLE);
        }
    }

    /**
     * Disable the editMode for the input.
     */
    protected void disable() {
        super.disable();
        this.plusBtn.setVisibility(GONE);
        this.minusBtn.setVisibility(GONE);
    }

    /**
     * Optimize the view for display in narrow spaces.
     *
     * @param smallViewPort - True if view is displayed in a narrow space, false otherwise
     */
    public void setSmallViewPort(boolean smallViewPort) {
        super.setSmallViewPort(smallViewPort);
        if (this.smallViewPort) {
            this.plusBtn.setVisibility(GONE);
            this.minusBtn.setVisibility(GONE);
        } else {
            this.plusBtn.setVisibility(VISIBLE);
            this.minusBtn.setVisibility(VISIBLE);
        }
    }
}
