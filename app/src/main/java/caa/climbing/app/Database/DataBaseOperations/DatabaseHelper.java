package caa.climbing.app.Database.DataBaseOperations;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import caa.climbing.app.Database.DatabaseModels.HangboardSession;
import caa.climbing.app.Database.DatabaseModels.IndoorSession;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.Database.DatabaseModels.OutdoorAscent;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.*;


/**
 * Created by Christian A on 17.06.2020.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    // Singleton Instance --> only one instance at a time active
    private static DatabaseHelper sInstance;

    // Context where the class was instantiated
    private static Context instanceContext;

    /**
     * Constructor should be private to prevent direct instantiation.
     * Make a call to the static method "getInstance()" instead.
     *
     * @param context <Context> - Application context
     */
    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Use the application context, which will ensure that you
     * don't accidentally leak an Activity's context.
     * See this article for more information: http://bit.ly/6LRzfx
     *
     * @param context <Context> - Application Context
     * @return <DatabaseHelper> - Instance of this class
     */
    public static synchronized DatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context);
            instanceContext = context;
        }
        return sInstance;
    }

    /**
     * Get a list of unique strings from a column of a table inside the database.
     *
     * @param db     - Database to use
     * @param table  - Table to query
     * @param column - Column of the table
     * @return - List of unique strings from the column
     */
    protected static String[] getUniqueStrings(SQLiteDatabase db, String table, String column) {
        Cursor cursor = db.query(true, table, new String[]{column}, null, null, null, null, null, null);

        int resultCount = cursor.getCount();

        if (resultCount == 0) return null;

        String[] results = new String[cursor.getCount()];

        for (int i = 0; i < resultCount; i++) {
            cursor.moveToPosition(i);
            results[i] = cursor.getString(cursor.getColumnIndex(column));
        }
        cursor.close();

        results = Arrays.stream(results)
                .filter(string -> (string != null && string.length() > 0))
                .toArray(String[]::new);

        Arrays.sort(results);

        return results;
    }

    /**
     * Get the database file.
     *
     * @param context - Context from which to get the file
     * @return - Database file
     */
    public static File getDatabaseFile(Context context) {
        return context.getDatabasePath(DATABASE_NAME);
    }

    static public String[] getGradeSystemList(Context context, boolean boulder) {
        return DatabaseGrade.getGradeSystemList(context, boulder);
    }

    /**
     * Called when the database connection is being configured.
     * Configure database settings for things like foreign key support, write-ahead logging, etc.
     *
     * @param db <SQLiteDatabase> - Resources.Database to store information in
     */
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    /**
     * Called when the database is created for the FIRST time.
     * If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
     * Create the tables for Grades, Indoor-, Outdoor-, Hangboard-Sessions
     *
     * @param db <SQLiteDatabase> - Resources.Database to store information in
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        DatabaseGrade.createTable(db);
        DatabaseIndoorSession.createTable(db);
        DatabaseOutdoorSession.createTable(db);
        DatabaseHangboardSession.createTable(db);
    }

    /**
     * Called when the database needs to be upgraded.
     * This method will only be called if a database already exists on disk with the same DATABASE_NAME,
     * but the DATABASE_VERSION is different than the version of the database that exists on disk.
     *
     * @param db         <SQLiteDatabase> - Resources.Database to store information in
     * @param oldVersion <Integer> - Old version of the DB
     * @param newVersion <Integer> - New version of the DB
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    // INDOOR SESSIONS
    public boolean saveIndoorSession(IndoorSession session, SQLiteDatabase db) {
        return DatabaseIndoorSession.saveIndoorSession(session, db);
    }

    public ArrayList<IndoorSession> getIndoorSessions(SQLiteDatabase db, Context context) {
        return DatabaseIndoorSession.getIndoorSessions(db, context);
    }

    public boolean updateIndoorSession(IndoorSession session, SQLiteDatabase db) {
        return DatabaseIndoorSession.updateSession(session, db);
    }

    public boolean deleteIndoorSession(int id, SQLiteDatabase db) {
        return DatabaseIndoorSession.deleteSession(id, db);
    }

    public String[] getGyms(SQLiteDatabase db) {
        return DatabaseIndoorSession.getGyms(db);
    }

    public int getIndoorSessionCount(SQLiteDatabase db) {
        return DatabaseIndoorSession.getSessionCount(db);
    }

    // OUTDOOR SESSIONS
    public boolean saveOutdoorSession(ArrayList<OutdoorAscent> ascents, SQLiteDatabase db) {
        return DatabaseOutdoorSession.saveOutdoorSession(ascents, db);
    }

    public ArrayList<OutdoorAscent> getOutdoorSessions(SQLiteDatabase db, boolean boulders, Context context) {
        return DatabaseOutdoorSession.getOutdoorSessions(db, boulders, true, context);
    }

    public ArrayList<OutdoorAscent> getOutdoorSessions(SQLiteDatabase db, boolean boulders, boolean descending, Context context) {
        return DatabaseOutdoorSession.getOutdoorSessions(db, boulders, descending, context);
    }

    public ArrayList<OutdoorAscent> getOutdoorSessionsBoulder(SQLiteDatabase db, Context context) {
        return DatabaseOutdoorSession.getOutdoorSessions(db, true, true, context);
    }

    public ArrayList<OutdoorAscent> getOutdoorSessionsRoutes(SQLiteDatabase db, Context context) {
        return DatabaseOutdoorSession.getOutdoorSessions(db, false, true, context);
    }

    public boolean updateOutdoorAscent(OutdoorAscent ascent, SQLiteDatabase db) {
        return DatabaseOutdoorSession.updateAscent(ascent, db);
    }

    public boolean deleteOutdoorAscent(int id, SQLiteDatabase db) {
        return DatabaseOutdoorSession.deleteAscent(id, db);
    }

    public int getOutdoorSessionCount(SQLiteDatabase db) {
        return DatabaseOutdoorSession.getSessionCount(db);
    }

    public String[] getCrags(SQLiteDatabase db) {
        return DatabaseOutdoorSession.getCrags(db);
    }

    // HANGBOARD SESSIONS
    public boolean saveHangboardSession(SQLiteDatabase db, HangboardSession session) {
        return DatabaseHangboardSession.saveHangboardSession(db, session);
    }

    public ArrayList<HangboardSession> getHangboardSession(SQLiteDatabase db, int testEdgeSize, boolean descending) {
        return DatabaseHangboardSession.getHangboardSession(db, testEdgeSize, descending);
    }

    public ArrayList<HangboardSession> getHangboardSession(SQLiteDatabase db, int testEdgeSize) {
        return DatabaseHangboardSession.getHangboardSession(db, testEdgeSize, false);
    }

    public boolean updateHangboardSession(HangboardSession session, SQLiteDatabase db) {
        return DatabaseHangboardSession.updateSession(session, db);
    }

    public boolean deleteHangboardSession(int id, SQLiteDatabase db) {
        return DatabaseHangboardSession.deleteSession(id, db);
    }

    public HangboardSession getLastHangboardSession(SQLiteDatabase db) {
        return DatabaseHangboardSession.getLastHangboardSession(db);
    }

    public Integer getLastHangboardTestEdgeSize(SQLiteDatabase db) {
        return DatabaseHangboardSession.getLastTestEdgeSize(db);
    }

    public String[] getHangboardTestEdgeSizes(SQLiteDatabase db) {
        return DatabaseHangboardSession.getTestEdgeSizes(db);
    }

    // GRADES
    public ArrayList<LabelledInt> getGrades(boolean boulders, SQLiteDatabase db) {
        return DatabaseGrade.getGrades(boulders, db, instanceContext);
    }

    public ArrayList<LabelledInt> getGradeBrackets(boolean boulders, SQLiteDatabase db) {
        return DatabaseGrade.getGradeBrackets(boulders, db, instanceContext);
    }

    public LabelledInt getClosestGrade(ArrayList<LabelledInt> grades, int gradeId) {
        return DatabaseGrade.getClosestGrade(grades, gradeId);
    }
}
