package caa.climbing.app.Database.DataBaseOperations;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import caa.climbing.app.Database.DatabaseModels.HangboardSession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.*;

class DatabaseHangboardSession {

    /**
     * Creates the table for storing hangboard sessions in.
     *
     * @param db - Database to use
     */
    static protected void createTable(SQLiteDatabase db) {
        String CREATE_HANGBOARD_SESSIONS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_HANGBOARD_SESSIONS +
                " (" +
                KEY_HANGBOARD_SESSION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                KEY_HANGBOARD_SESSION_DATE + " INTEGER NOT NULL," +
                KEY_HANGBOARD_SESSION_HANG_S + " INTEGER NOT NULL," +
                KEY_HANGBOARD_SESSION_REST_BTW_HANGS_S + " INTEGER NOT NULL," +
                KEY_HANGBOARD_SESSION_PAUSE_BTW_SETS_S + " INTEGER NOT NULL," +
                KEY_HANGBOARD_SESSION_REPETITIONS + " INTEGER NOT NULL," +
                KEY_HANGBOARD_SESSION_SETS + " INTEGER NOT NULL," +
                KEY_HANGBOARD_SESSION_BODY_WEIGHT_KG + " REAL NOT NULL," +
                KEY_HANGBOARD_SESSION_EXTRA_WEIGHT_KG + " REAL NOT NULL," +
                KEY_HANGBOARD_SESSION_MAX_STRENGTH_LEFT_KG + " REAL DEFAULT NULL," +
                KEY_HANGBOARD_SESSION_MAX_STRENGTH_RIGHT_KG + " REAL DEFAULT NULL," +
                KEY_HANGBOARD_SESSION_TEST_EDGE_SIZE_MM + " INTEGER DEFAULT NULL" +
                ")";

        db.execSQL(CREATE_HANGBOARD_SESSIONS_TABLE);
    }

    /**
     * Saves the information about a hangboard session to the database.
     *
     * @param session - Session to store
     * @param db      - Database to use
     * @return - Whether the database action was successful
     */
    static protected boolean saveHangboardSession(SQLiteDatabase db, HangboardSession session) {
        boolean success = false;

        ContentValues values = createContentValues(session);

        try {
            db.beginTransaction();

            db.insertOrThrow(TABLE_HANGBOARD_SESSIONS, null, values);
            db.setTransactionSuccessful();
            success = true;
        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to save hangboard session.\n" + e);
        } finally {
            db.endTransaction();
        }

        return success;
    }

    /**
     * Reads all completed hangboard sessions from the database.
     *
     * @param db           - Database to use
     * @param testEdgeSize - Size of the edge for the strength test
     * @param descending   - Sort the entries in descending order
     * @return - A list of all completed hangboard sessions
     */
    static protected ArrayList<HangboardSession> getHangboardSession(SQLiteDatabase db, int testEdgeSize, boolean descending) {
        String SELECT_HANGBOARD_SESSIONS = "SELECT * FROM " + TABLE_HANGBOARD_SESSIONS + " WHERE " + KEY_HANGBOARD_SESSION_TEST_EDGE_SIZE_MM + "=" + testEdgeSize;
        Cursor cursor = db.rawQuery(SELECT_HANGBOARD_SESSIONS, null);

        ArrayList<HangboardSession> hangboardSessions = new ArrayList<>();

        try {
            if (cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_ID));
                    long millis = cursor.getLong(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_DATE));
                    int hang_s = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_HANG_S));
                    int rest_s = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_REST_BTW_HANGS_S));
                    int pause_s = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_PAUSE_BTW_SETS_S));
                    int repetitions = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_REPETITIONS));
                    int sets = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_SETS));
                    float bodyWeight = cursor.getFloat(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_BODY_WEIGHT_KG));
                    float extraWeight = cursor.getFloat(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_EXTRA_WEIGHT_KG));
                    float maxStrengthLeft = cursor.getFloat(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_MAX_STRENGTH_LEFT_KG));
                    float maxStrengthRight = cursor.getFloat(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_MAX_STRENGTH_RIGHT_KG));
                    HangboardSession hangboardSession = new HangboardSession(id, new Date(millis), sets, pause_s, repetitions, rest_s, hang_s, maxStrengthLeft, maxStrengthRight, testEdgeSize, bodyWeight, extraWeight);

                    hangboardSessions.add(hangboardSession);

                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to get hangboard sessions from database.\n" + e);
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        Collections.sort(hangboardSessions);

        if (descending) Collections.reverse(hangboardSessions);

        return hangboardSessions;
    }

    /**
     * Reads the last completed hangboard session from the database.
     *
     * @param db - Database to use
     * @return - Last completed hangboard session
     */
    static protected HangboardSession getLastHangboardSession(SQLiteDatabase db) {
        String SELECT_LAST_HANGBOARD_SESSION = "SELECT * FROM " + TABLE_HANGBOARD_SESSIONS + " ORDER BY " + KEY_HANGBOARD_SESSION_DATE + " DESC, " + KEY_HANGBOARD_SESSION_ID + " DESC LIMIT 1";
        Cursor cursor = db.rawQuery(SELECT_LAST_HANGBOARD_SESSION, null);

        try {
            if (cursor.moveToFirst()) {
                long millis = cursor.getLong(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_DATE));
                int hang_s = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_HANG_S));
                int rest_s = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_REST_BTW_HANGS_S));
                int pause_s = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_PAUSE_BTW_SETS_S));
                int repetitions = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_REPETITIONS));
                int sets = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_SETS));
                float extraWeight = cursor.getFloat(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_EXTRA_WEIGHT_KG));
                int testEdgeSize = cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_TEST_EDGE_SIZE_MM));

                return new HangboardSession(new Date(millis), sets, pause_s, repetitions, rest_s, hang_s, testEdgeSize, extraWeight);
            } else {
                Log.i(ERROR_TAG, "Could not get the last hangboard session");
            }
        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to get the last hangboard session from database.\n" + e);
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return null;
    }

    /**
     * Get a list of all unique test edge sizes of completed strength tests on the hangboard.
     *
     * @param db - Database to use
     * @return - List of test edge sizes
     */
    static protected String[] getTestEdgeSizes(SQLiteDatabase db) {
        return DatabaseHelper.getUniqueStrings(db, TABLE_HANGBOARD_SESSIONS, KEY_HANGBOARD_SESSION_TEST_EDGE_SIZE_MM);
    }

    /**
     * Reads the test edge size of the last completed strength test on the hangboard.
     *
     * @param db - Database to use
     * @return - Test edge size of the last completed strength test
     */
    static protected Integer getLastTestEdgeSize(SQLiteDatabase db) {
        String SELECT_LAST_HANGBOARD_SESSION = "SELECT " + KEY_HANGBOARD_SESSION_TEST_EDGE_SIZE_MM + " FROM " + TABLE_HANGBOARD_SESSIONS + " WHERE " + KEY_HANGBOARD_SESSION_TEST_EDGE_SIZE_MM + " IS NOT NULL ORDER BY " + KEY_HANGBOARD_SESSION_DATE + " DESC, " + KEY_HANGBOARD_SESSION_ID + " DESC LIMIT 1";
        Cursor cursor = db.rawQuery(SELECT_LAST_HANGBOARD_SESSION, null);

        try {
            if (cursor.moveToFirst()) {
                return cursor.getInt(cursor.getColumnIndex(KEY_HANGBOARD_SESSION_TEST_EDGE_SIZE_MM));
            } else {
                Log.i(ERROR_TAG, "Could not get the last test edge size.");
            }
        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to get the last test edge size from database.\n" + e);
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return null;
    }

    /**
     * Updates a specific hangboard session with new data.
     *
     * @param session - New data to update with
     * @param db      - Database to use
     * @return - Whether the database operation was successful
     */
    static protected boolean updateSession(HangboardSession session, SQLiteDatabase db) {
        boolean success = false;

        if (session.isInvalid()) return false;

        String whereClause = KEY_HANGBOARD_SESSION_ID + " = ?";

        ContentValues values = createContentValues(session);

        try {
            db.beginTransaction();

            db.updateWithOnConflict(TABLE_HANGBOARD_SESSIONS, values, whereClause, new String[]{Integer.toString(session.id)}, SQLiteDatabase.CONFLICT_FAIL);
            db.setTransactionSuccessful();

            success = true;
        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to update hangboard session.\n" + e);
        } finally {
            db.endTransaction();
        }

        return success;
    }

    /**
     * Delete a specific indoor session from the database.
     *
     * @param id - ID of the session that should be deleted
     * @param db - Database to use
     * @return - Whether the database operation was successful
     */
    static protected boolean deleteSession(int id, SQLiteDatabase db) {
        boolean success = false;

        String whereClause = KEY_HANGBOARD_SESSION_ID + " = ?";

        try {
            db.beginTransaction();

            db.delete(TABLE_HANGBOARD_SESSIONS, whereClause, new String[]{Integer.toString(id)});
            db.setTransactionSuccessful();

            success = true;

        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to delete hangboard session.\n" + e);
        } finally {
            db.endTransaction();
        }

        return success;
    }

    /**
     * Generate content values for putting into the database
     *
     * @param session - Session from which to create the values
     * @return - Content values for putting into the database
     */
    static private ContentValues createContentValues(HangboardSession session) {
        ContentValues values = new ContentValues();
        values.put(KEY_HANGBOARD_SESSION_DATE, session.date.getTime());
        values.put(KEY_HANGBOARD_SESSION_HANG_S, session.hangTime);
        values.put(KEY_HANGBOARD_SESSION_REST_BTW_HANGS_S, session.restTime);
        values.put(KEY_HANGBOARD_SESSION_PAUSE_BTW_SETS_S, session.pauseTime);
        values.put(KEY_HANGBOARD_SESSION_REPETITIONS, session.reps);
        values.put(KEY_HANGBOARD_SESSION_SETS, session.sets);
        values.put(KEY_HANGBOARD_SESSION_BODY_WEIGHT_KG, session.bodyWeight);
        values.put(KEY_HANGBOARD_SESSION_EXTRA_WEIGHT_KG, session.extraWeight);
        values.put(KEY_HANGBOARD_SESSION_MAX_STRENGTH_LEFT_KG, session.maxStrengthLeft);
        values.put(KEY_HANGBOARD_SESSION_MAX_STRENGTH_RIGHT_KG, session.maxStrengthRight);
        values.put(KEY_HANGBOARD_SESSION_TEST_EDGE_SIZE_MM, session.testEdgeSize);
        return values;
    }
}
