package caa.climbing.app.Database.DataBaseOperations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import caa.climbing.app.Database.DatabaseModels.IndoorSession;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.*;


/**
 * Perform database operations relating to indoor sessions.
 */
class DatabaseIndoorSession {

    /**
     * Creates the table for storing indoor sessions in.
     *
     * @param db - Database to use
     */
    static protected void createTable(SQLiteDatabase db) {
        String CREATE_INDOOR_SESSIONS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_INDOOR_SESSIONS +
                " (" +
                KEY_INDOOR_SESSION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                KEY_INDOOR_SESSION_DATE + " INTEGER NOT NULL," +
                KEY_INDOOR_SESSION_GYM + " TEXT NOT NULL," +
                KEY_INDOOR_SESSION_BOULDER_BRACKET_0 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_BOULDER_BRACKET_1 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_BOULDER_BRACKET_2 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_BOULDER_BRACKET_3 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_BOULDER_BRACKET_4 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_ROUTES_BRACKET_0 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_ROUTES_BRACKET_1 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_ROUTES_BRACKET_2 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_ROUTES_BRACKET_3 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_ROUTES_BRACKET_4 + " INTEGER DEFAULT NULL," +
                KEY_INDOOR_SESSION_ROUTES_BRACKET_5 + " INTEGER DEFAULT NULL" +
                ")";

        db.execSQL(CREATE_INDOOR_SESSIONS_TABLE);
    }

    /**
     * Saves the information about an indoor session to the database.
     *
     * @param session - Ascents in the session
     * @param db      - Database to use
     * @return - Whether the database action was successful
     */
    static protected boolean saveIndoorSession(IndoorSession session, SQLiteDatabase db) {
        boolean success = false;

        if (session.isInvalid()) return false;

        ContentValues values = createContentValues(session);

        try {
            db.beginTransaction();

            db.insertOrThrow(TABLE_INDOOR_SESSIONS, null, values);
            db.setTransactionSuccessful();

            success = true;

        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to save indoor session.\n" + e);
        } finally {
            db.endTransaction();
        }

        return success;
    }

    /**
     * Updates a specific indoor session with new data.
     *
     * @param session - New data to update with
     * @param db      - Database to use
     * @return - Whether the database operation was successful
     */
    static protected boolean updateSession(IndoorSession session, SQLiteDatabase db) {
        boolean success = false;

        if (session.isInvalid() || session.id == null) return false;

        String whereClause = KEY_INDOOR_SESSION_ID + " = ?";

        ContentValues values = createContentValues(session);

        try {
            db.beginTransaction();

            db.updateWithOnConflict(TABLE_INDOOR_SESSIONS, values, whereClause, new String[]{Integer.toString(session.id)}, SQLiteDatabase.CONFLICT_FAIL);
            db.setTransactionSuccessful();

            success = true;

        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to update indoor session.\n" + e);
        } finally {
            db.endTransaction();
        }

        return success;
    }

    /**
     * Get a list of boulder/route ascents for a single session.
     *
     * @param cursor  - A Cursor returned from querying a database
     * @param boulder - Whether the ascent was a boulder or not
     * @return - A list of ascents per session
     */
    static private ArrayList<LabelledInt> getAscents(Cursor cursor, boolean boulder, ArrayList<LabelledInt> gradeBrackets) {

        int numberOfGradeBrackets = gradeBrackets.size();
        String formatString = boulder ? FORMAT_STRING_INDOOR_BOULDER_BRACKETS : FORMAT_STRING_INDOOR_ROUTE_BRACKETS;

        ArrayList<LabelledInt> ascents = new ArrayList<>();
        for (int i = 0; i < numberOfGradeBrackets; i++) {
            String tag = String.format(formatString, i);
            int count = cursor.getInt(cursor.getColumnIndex(tag));
            String label = gradeBrackets.get(i).label;
            ascents.add(new LabelledInt(label, count, tag));
        }

        return ascents;
    }

    /**
     * Get a list of all unique gyms were indoor sessions were previously completed.
     *
     * @param db - Database to use
     * @return - List of gyms
     */
    static protected String[] getGyms(SQLiteDatabase db) {
        return DatabaseHelper.getUniqueStrings(db, TABLE_INDOOR_SESSIONS, KEY_INDOOR_SESSION_GYM);
    }

    /**
     * Reads all completed indoor sessions from the database.
     *
     * @param db - Database to use
     * @return - A list of all completed indoor sessions
     */
    static protected ArrayList<IndoorSession> getIndoorSessions(SQLiteDatabase db, Context context) {
        String SELECT_INDOOR_SESSIONS = "SELECT * FROM " + TABLE_INDOOR_SESSIONS + " ORDER BY date(" + KEY_INDOOR_SESSION_DATE + ") ASC";

        ArrayList<LabelledInt> gradeBracketsBoulder = DatabaseGrade.getGradeBrackets(true, db, context);
        ArrayList<LabelledInt> gradeBracketsRoute = DatabaseGrade.getGradeBrackets(false, db, context);

        Cursor cursor = db.rawQuery(SELECT_INDOOR_SESSIONS, null);

        ArrayList<IndoorSession> sessions = new ArrayList<>();

        try {
            if (cursor.moveToFirst()) {
                do {
                    String gym = cursor.getString(cursor.getColumnIndex(KEY_INDOOR_SESSION_GYM));
                    long millis = cursor.getLong(cursor.getColumnIndex(KEY_INDOOR_SESSION_DATE));
                    int id = cursor.getInt(cursor.getColumnIndex(KEY_INDOOR_SESSION_ID));

                    ArrayList<LabelledInt> boulders = getAscents(cursor, true, gradeBracketsBoulder);
                    ArrayList<LabelledInt> routes = getAscents(cursor, false, gradeBracketsRoute);

                    IndoorSession session = new IndoorSession(id, gym, new Date(millis), boulders, routes);

                    sessions.add(session);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to get indoor sessions from database.\n" + e);
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        Collections.sort(sessions);

        return sessions;
    }

    /**
     * Delete a specific indoor session from the database.
     *
     * @param id - ID of the session that should be deleted
     * @param db - Database to use
     * @return - Whether the database operation was successful
     */
    static protected boolean deleteSession(int id, SQLiteDatabase db) {
        boolean success = false;

        String whereClause = KEY_INDOOR_SESSION_ID + " = ?";

        try {
            db.beginTransaction();

            db.delete(TABLE_INDOOR_SESSIONS, whereClause, new String[]{Integer.toString(id)});
            db.setTransactionSuccessful();

            success = true;

        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to delete indoor session.\n" + e);
        } finally {
            db.endTransaction();
        }

        return success;
    }

    /**
     * Generate content values for putting into the database
     *
     * @param session - Session from which to create the values
     * @return - Content values for putting into the database
     */
    static private ContentValues createContentValues(IndoorSession session) {
        ContentValues values = new ContentValues();

        values.put(KEY_INDOOR_SESSION_GYM, session.gym);
        values.put(KEY_INDOOR_SESSION_DATE, session.date.getTime());

        for (LabelledInt entry : getAllAscents(session)) {
            values.put(entry.tag, entry.value);
        }

        return values;
    }

    /**
     * Get a list of the number of ascents per grade bracket (boulder and route) from an indoor session
     *
     * @param session - Session from which to get the ascents
     * @return - List of ascents per grade bracket
     */
    static private ArrayList<LabelledInt> getAllAscents(IndoorSession session) {
        ArrayList<LabelledInt> routesAndBoulders = new ArrayList<>();
        routesAndBoulders.addAll(session.boulders);
        routesAndBoulders.addAll(session.routes);

        return routesAndBoulders;
    }

    /**
     * Get the number of ascents with distinct dates as the count for indoor sessions.
     *
     * @param db - Database to use
     * @return - Number of ascents with distinct dates
     */
    static protected int getSessionCount(SQLiteDatabase db) {
        Cursor cursor = db.query(true, TABLE_INDOOR_SESSIONS, new String[]{KEY_INDOOR_SESSION_DATE}, null, null, null, null, null, null);

        int count = cursor.getCount();
        cursor.close();
        return count;
    }
}
