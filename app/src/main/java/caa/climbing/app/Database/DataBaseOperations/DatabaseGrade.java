package caa.climbing.app.Database.DataBaseOperations;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.HelperClasses.LocaleHelper;
import caa.climbing.app.R;
import caa.climbing.app.Screens.Settings;

import java.util.ArrayList;

import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.ERROR_TAG;
import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.FORMAT_STRING_INDOOR_BOULDER_BRACKETS;
import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.FORMAT_STRING_INDOOR_ROUTE_BRACKETS;
import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.KEY_GRADES_FRA_BOULDERS;
import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.KEY_GRADES_FRA_ROUTES;
import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.KEY_GRADES_ID;
import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.KEY_GRADES_SCORE;
import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.KEY_GRADES_UIAA_ROUTES;
import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.KEY_GRADES_USA_BOULDERS;
import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.TABLE_GRADES;

/**
 * Perform database operations relating to climbing grades.
 */
class DatabaseGrade {

    /**
     * Creates and populates the table where grades are stored.
     *
     * @param db - Database to use
     */
    static public void createTable(SQLiteDatabase db) {
        String CREATE_GRADES_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_GRADES + /*IF NOT EXISTS*/
                " (" +
                KEY_GRADES_ID + " INTEGER PRIMARY KEY UNIQUE," +
                KEY_GRADES_SCORE + " INTEGER DEFAULT 0," +
                KEY_GRADES_FRA_BOULDERS + " TEXT DEFAULT ''," +
                KEY_GRADES_FRA_ROUTES + " TEXT DEFAULT ''," +
                KEY_GRADES_USA_BOULDERS + " TEXT DEFAULT ''," +
                KEY_GRADES_UIAA_ROUTES + " TEXT DEFAULT ''" +
                ")";
        db.execSQL(CREATE_GRADES_TABLE);
        setUpGradesTable(db);
    }

    /**
     * Get a list of grades for either boulders or routes.
     *
     * @param boulders - Whether the grades are for boulders or for routes
     * @param db       - Database to use
     * @param context  - Application context
     * @return - List of grades
     */
    static protected ArrayList<LabelledInt> getGrades(boolean boulders, SQLiteDatabase db, Context context) {
        // Must use en-Locale, since grade systems may have different names in different languages
        Context defaultContext = LocaleHelper.setLocale(context, "en");

        String boulderSystem = Settings.getGradeSystem(defaultContext, Settings.KEY_BOULDER_GRADE);
        String routeSystem = Settings.getGradeSystem(defaultContext, Settings.KEY_ROUTE_GRADE);

        ArrayList<LabelledInt> grades = new ArrayList<>();

        String selector = null;
        if (boulders) {
            if (boulderSystem.equals(defaultContext.getString(R.string.grade_system_boulder_fb)))
                selector = KEY_GRADES_FRA_BOULDERS;
            if (boulderSystem.equals(defaultContext.getString(R.string.grade_system_boulder_v)))
                selector = KEY_GRADES_USA_BOULDERS;
        } else {
            if (routeSystem.equals(defaultContext.getString(R.string.grade_system_routes_french)))
                selector = KEY_GRADES_FRA_ROUTES;
            if (routeSystem.equals(defaultContext.getString(R.string.grade_system_routes_uiaa)))
                selector = KEY_GRADES_UIAA_ROUTES;
        }

        if (selector != null) {
            String SELECT_GRADES = "SELECT " + KEY_GRADES_ID + ", " + selector + ", " + KEY_GRADES_SCORE + " FROM " + TABLE_GRADES + " WHERE " + selector + " NOT LIKE '%/%' AND " + selector + " != ''";
            if (boulders) SELECT_GRADES += " AND " + KEY_GRADES_ID + " <58";

            Cursor cursor = db.rawQuery(SELECT_GRADES, null);

            try {
                if (cursor.moveToFirst()) {
                    do {
                        int id = cursor.getInt(cursor.getColumnIndex(KEY_GRADES_ID));
                        String grade = cursor.getString(cursor.getColumnIndex(selector));
                        int score = cursor.getInt(cursor.getColumnIndex(KEY_GRADES_SCORE));
                        if (selector.equals(KEY_GRADES_UIAA_ROUTES)) {
                            grade = grade.replace("+", "\u207A");
                            grade = grade.replace("-", "\u207B");
                        }
                        grades.add(new LabelledInt(grade, id, score));
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                Log.e(ERROR_TAG, "Error while trying to get grades from database.\n" + e);
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        }
        return grades;
    }

    /**
     * Get a list of grade systems, independent of localization.
     *
     * @param context - Context for which to get the list
     * @param boulder - Whether to get the system for routes or boulders
     * @return - List of grade systems in
     */
    static protected String[] getGradeSystemList(Context context, boolean boulder) {
        Context defaultContext = LocaleHelper.setLocale(context, "en");
        Resources resources = defaultContext.getResources();
        return boulder ? resources.getStringArray(R.array.boulder_grades) : resources.getStringArray(R.array.route_grades);
    }

    /**
     * Get the closest grade for a given gradeId. In some grade systems not every gradeId has a corresponding grade,
     * therefore the grade with the closest id is returned.
     *
     * @param db       - Database to use
     * @param boulders - Whether the grades are for boulders or for routes
     * @param gradeId  - Given gradeId for which to find the closest grade
     * @param context  - Application context
     * @return - The grade with an id that is closest to the given id or null if no matching grade was found
     */
    static protected LabelledInt getClosestGrade(SQLiteDatabase db, boolean boulders, int gradeId, Context context) {
        ArrayList<LabelledInt> grades = getGrades(boulders, db, context);

        return getClosestGrade(grades, gradeId);
    }

    /**
     * Get the closest grade for a given gradeId. In some grade systems not every gradeId has a corresponding grade,
     * therefore the grade with the closest id is returned.
     *
     * @param grades  - List of grades to select from
     * @param gradeId - Given gradeId for which to find the closest grade
     * @return - The grade with an id that is closest to the given id or null if no matching grade was found
     */
    static protected LabelledInt getClosestGrade(ArrayList<LabelledInt> grades, int gradeId) {
        int closest = Integer.MAX_VALUE;
        LabelledInt closestGrade = null;
        for (LabelledInt grade : grades) {
            int difference = Math.abs(gradeId - grade.value);
            if (difference < closest && !grade.label.equals("")) {
                closest = difference;
                closestGrade = grade;
            }
        }
        return closestGrade;
    }

    /**
     * Get a list of grade ranges.
     *
     * @param boulders - Whether the grades are for boulders or for routes
     * @param db       - Database to use
     * @param context  - Application context
     * @return - List of grade brackets
     */
    static protected ArrayList<LabelledInt> getGradeBrackets(boolean boulders, SQLiteDatabase db, Context context) {
        int[][] idBracketsBoulder = {{1, 15}, {16, 21}, {22, 33}, {34, 45}, {46, 57}};
        int[][] idBracketsRoutes = {{1, 15}, {16, 21}, {22, 33}, {34, 45}, {46, 57}, {58, 65}};
        int[][] idBrackets = boulders ? idBracketsBoulder : idBracketsRoutes;

        ArrayList<LabelledInt> gradeBracketsTagged = new ArrayList<>();

        ArrayList<LabelledInt> grades = getGrades(boulders, db, context);

        ArrayList<ArrayList<String>> gradesInBracket = new ArrayList<>();
        for (int[] ignored : idBrackets) {
            gradesInBracket.add(new ArrayList<>());
        }
        for (LabelledInt grade : grades) {
            for (int i = 0; i < idBrackets.length; i++) {
                if (grade.value >= idBrackets[i][0] && grade.value <= idBrackets[i][1]) {
                    gradesInBracket.get(i).add(grade.label);
                }
            }
        }

        for (int i = 0; i < gradesInBracket.size(); i++) {
            ArrayList<String> list = gradesInBracket.get(i);
            String first = list.get(0);
            String last = list.get(list.size() - 1);
            String bracket = first + " - " + last;
            String tag = String.format(boulders ? FORMAT_STRING_INDOOR_BOULDER_BRACKETS : FORMAT_STRING_INDOOR_ROUTE_BRACKETS, i);
            gradeBracketsTagged.add(new LabelledInt(bracket, tag));
        }

        return gradeBracketsTagged;
    }

    /**
     * Populate the grade table with data.
     *
     * @param db - Database to use
     */
    static private void setUpGradesTable(SQLiteDatabase db) {
        int[] scores = {25, 50, 75, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850, 875, 900, 925, 950, 975, 1000, 1025, 1050, 1075, 1100, 1125, 1150, 1175, 1200, 1225, 1250, 1275, 1300, 1325, 1350, 1375, 1400, 1425, 1450, 1475, 1500};
        //66 elements
        String[] uiaa_routes = {"2", "2+", "3-", "3", "3+", "3+/4-", "4-", "4", "4+", "4/5-", "", "5-", "5", "", "5+", "5+/6-", "6-", "6-/6", "6", "6/6+", "6+", "6+/7-", "", "7-", "", "7", "", "7+", "", "7+/8-", "", "8-", "", "8", "", "8/8+", "8+", "8+/9-", "", "9-", "", "9", "", "9/9+", "9+", "9+/10-", "", "10-", "", "10", "10/10+", "10+", "", "10+/11-", "11-", "", "11-/11", "11", "11/11+", "11+", "11+/12-", "", "12-", "", "12-/12", "12"};

        String[] fra_routes = {"2a", "2b", "2c", "3a", "3a+", "3b", "3b+", "3c", "3c+", "4a", "4a+", "4b", "4b+", "4c", "4c+", "5a", "5a+", "5b", "5b+", "5c", "5c+", "6a", "6a/+", "6a+", "6a+/6b", "6b", "6b/+", "6b+", "6b+/6c", "6c", "6c/+", "6c+", "6c+/7a", "7a", "7a/+", "7a+", "7a+/7b", "7b", "7b/+", "7b+", "7b+/7c", "7c", "7c/+", "7c+", "7c+/8a", "8a", "8a/+", "8a+", "8a+/8b", "8b", "8b/+", "8b+", "8b+/8c", "8c", "8c/+", "8c+", "8c+/9a", "9a", "9a/+", "9a+", "9a+/9b", "9b", "9b/+", "9b+", "9b+/9c", "9c"};
        String[] fra_boulders = {"2A", "2B", "2C", "3A", "3A+", "3B", "3B+", "3C", "3C+", "4A", "4A+", "4B", "4B+", "4C", "4C+", "5A", "5A+", "5B", "5B+", "5C", "5C+", "6A", "6A/+", "6A+", "6A+/6B", "6B", "6B/+", "6B+", "6B+/6C", "6C", "6C/+", "6C+", "6C+/7A", "7A", "7A/+", "7A+", "7A+/7B", "7B", "7B/+", "7B+", "7B+/7C", "7C", "7C/+", "7C+", "7C+/8A", "8A", "8A/+", "8A+", "8A+/8B", "8B", "8B/+", "8B+", "8B+/8C", "8C", "8C/+", "8C+", "8C+/9A", "9A", "9A/+", "9A+", "9A+/9B", "9B", "9B/+", "9B+", "9B+/9C", "9C"};
        String[] usa_boulders = {"", "", "", "VB", "", "", "", "V0-", "", "", "", "V0", "", "", "", "", "", "V1", "", "V2", "", "V3", "", "V3/4", "", "V4", "", "", "V4/V5", "V5", "", "V5/V6", "", "V6", "", "V7", "", "V8", "", "V8/9", "", "V9", "", "V10", "", "V11", "", "V12", "", "V13", "", "V14", "", "V15", "V15/16", "V16", "V16/17", "V17", "V17/18", "V18", "V18/19", "V19", "V19/20", "V20", "", ""};
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_GRADES, null);
        int count = cursor.getCount();

        db.beginTransaction();
        try {
            if (count == 0) {
                for (int i = 0; i < scores.length; i++) {
                    ContentValues values = new ContentValues();

                    values.put(KEY_GRADES_ID, i + 1);
                    values.put(KEY_GRADES_SCORE, scores[i]);
                    values.put(KEY_GRADES_FRA_ROUTES, fra_routes[i]);
                    values.put(KEY_GRADES_FRA_BOULDERS, fra_boulders[i]);
                    values.put(KEY_GRADES_USA_BOULDERS, usa_boulders[i].equals("") ? null : usa_boulders[i]);
                    values.put(KEY_GRADES_UIAA_ROUTES, uiaa_routes[i].equals("") ? null : uiaa_routes[i]);

                    db.insertOrThrow(TABLE_GRADES, null, values);

                }
                db.setTransactionSuccessful();
            }
        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to populate grades table.\n" + e);
        } finally {
            db.endTransaction();
            cursor.close();
        }

    }

}
