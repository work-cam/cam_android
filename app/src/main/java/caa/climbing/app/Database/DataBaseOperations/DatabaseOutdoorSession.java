package caa.climbing.app.Database.DataBaseOperations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.Database.DatabaseModels.OutdoorAscent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static caa.climbing.app.Database.DataBaseOperations.DatabaseConstants.*;

/**
 * Perform database operations relating to outdoor sessions.
 */
class DatabaseOutdoorSession {

    /**
     * Creates the table for storing outdoor ascents in.
     *
     * @param db - Database to use
     */
    static protected void createTable(SQLiteDatabase db) {
        String CREATE_OUTDOOR_SESSIONS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_OUTDOOR_ASCENTS +
                " (" +
                KEY_OUTDOOR_ASCENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                KEY_OUTDOOR_ASCENT_DATE + " INTEGER NOT NULL," +
                KEY_OUTDOOR_ASCENT_CRAG + " TEXT NOT NULL," +
                KEY_OUTDOOR_ASCENT_NAME + " TEXT NOT NULL," +
                KEY_OUTDOOR_ASCENT_IMAGE_URI_STRING + " TEXT DEFAULT NULL," +
                KEY_OUTDOOR_ASCENT_GRADE_ID + " INTEGER NOT NULL," +
                KEY_OUTDOOR_ASCENT_CLIMB_TYPE + " INTEGER NOT NULL," +
                KEY_OUTDOOR_ASCENT_METHOD + " INTEGER NOT NULL" +
                ")";
        db.execSQL(CREATE_OUTDOOR_SESSIONS_TABLE);
    }

    /**
     * Saves the information about an outdoor session to the database.
     *
     * @param ascents - Ascents in the session
     * @param db      - Database to use
     * @return - Whether the database action was successful
     */
    static protected boolean saveOutdoorSession(ArrayList<OutdoorAscent> ascents, SQLiteDatabase db) {
        boolean success = false;
        try {
            db.beginTransaction();

            for (OutdoorAscent ascent : ascents) {

                ContentValues values = createContentValues(ascent);
                db.insertOrThrow(TABLE_OUTDOOR_ASCENTS, null, values);
            }

            db.setTransactionSuccessful();
            success = true;

        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to save outdoor session.\n" + e);
        } finally {
            db.endTransaction();
        }
        return success;
    }

    /**
     * Get a list of all unique crags were outdoor sessions were previously completed.
     *
     * @param db - Database to use
     * @return - List of gyms
     */
    static protected String[] getCrags(SQLiteDatabase db) {
        return DatabaseHelper.getUniqueStrings(db, TABLE_OUTDOOR_ASCENTS, KEY_OUTDOOR_ASCENT_CRAG);
    }

    /**
     * Reads all completed boulder or route ascents from outdoor sessions from the database.
     *
     * @param db         - Database to use
     * @param boulders   - Whether to get boulders or routes
     * @param descending - Whether to sort ascends in descending order
     * @param context    - Application context
     * @return - A list of all climbed boulders/routes from outdoor sessions
     */
    static protected ArrayList<OutdoorAscent> getOutdoorSessions(SQLiteDatabase db, boolean boulders, boolean descending, Context context) {
        String condition;
        if (boulders) {
            condition = "WHERE " + KEY_OUTDOOR_ASCENT_CLIMB_TYPE + "='" + OutdoorAscent.BOULDER + "'";
        } else {
            condition = "WHERE " + KEY_OUTDOOR_ASCENT_CLIMB_TYPE + "='" + OutdoorAscent.ROUTE + "'";
        }

        String SELECT_LAST_HANGBOARD_SESSION = "SELECT * FROM " + TABLE_OUTDOOR_ASCENTS + " " + condition;
        Cursor cursor = db.rawQuery(SELECT_LAST_HANGBOARD_SESSION, null);
        ArrayList<OutdoorAscent> ascents = new ArrayList<>();

        try {
            if (cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(KEY_OUTDOOR_ASCENT_ID));
                    int climbType = cursor.getInt(cursor.getColumnIndex(KEY_OUTDOOR_ASCENT_CLIMB_TYPE));
                    String crag = cursor.getString(cursor.getColumnIndex(KEY_OUTDOOR_ASCENT_CRAG));
                    String name = cursor.getString(cursor.getColumnIndex(KEY_OUTDOOR_ASCENT_NAME));
                    String imgUrl = cursor.getString(cursor.getColumnIndex(KEY_OUTDOOR_ASCENT_IMAGE_URI_STRING));
                    long millis = cursor.getLong(cursor.getColumnIndex(KEY_OUTDOOR_ASCENT_DATE));
                    int gradeId = cursor.getInt(cursor.getColumnIndex(KEY_OUTDOOR_ASCENT_GRADE_ID));
                    int method = cursor.getInt(cursor.getColumnIndex(KEY_OUTDOOR_ASCENT_METHOD));

                    LabelledInt grade = DatabaseGrade.getClosestGrade(db, boulders, gradeId, context);
                    OutdoorAscent ascent = new OutdoorAscent(id, climbType, crag, name, imgUrl, new Date(millis), method, gradeId, grade);
                    ascents.add(ascent);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to get outdoor sessions from database.\n" + e);
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        Collections.sort(ascents);
        if (descending) {
            Collections.reverse(ascents);
        }

        return ascents;
    }

    /**
     * Updates a specific outdoor ascent with new data.
     *
     * @param ascent - New data to update with
     * @param db     - Database to use
     * @return - Whether the database operation was successful
     */
    static protected boolean updateAscent(OutdoorAscent ascent, SQLiteDatabase db) {
        boolean success = false;

        if (ascent.isInvalid() || ascent.id == null) return false;

        String whereClause = KEY_OUTDOOR_ASCENT_ID + " = ?";
        ContentValues values = createContentValues(ascent);

        try {
            db.beginTransaction();

            db.updateWithOnConflict(TABLE_OUTDOOR_ASCENTS, values, whereClause, new String[]{Integer.toString(ascent.id)}, SQLiteDatabase.CONFLICT_FAIL);
            db.setTransactionSuccessful();

            success = true;
        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to update outdoor ascent.\n" + e);
        } finally {
            db.endTransaction();
        }

        return success;
    }

    /**
     * Delete a specific outdoor ascent from the database.
     *
     * @param id - ID of the ascent that should be deleted
     * @param db - Database to use
     * @return - Whether the database operation was successful
     */
    static protected boolean deleteAscent(int id, SQLiteDatabase db) {
        boolean success = false;

        String whereClause = KEY_OUTDOOR_ASCENT_ID + " = ?";

        try {
            db.beginTransaction();

            db.delete(TABLE_OUTDOOR_ASCENTS, whereClause, new String[]{Integer.toString(id)});
            db.setTransactionSuccessful();

            success = true;
        } catch (Exception e) {
            Log.e(ERROR_TAG, "Error while trying to delete outdoor ascent.\n" + e);
        } finally {
            db.endTransaction();
        }

        return success;
    }

    /**
     * Generate content values for putting into the database
     *
     * @param ascent - Ascent from which to create the values
     * @return - Content values for putting into the database
     */
    static private ContentValues createContentValues(OutdoorAscent ascent) {
        ContentValues values = new ContentValues();
        values.put(KEY_OUTDOOR_ASCENT_CLIMB_TYPE, ascent.climbType);
        values.put(KEY_OUTDOOR_ASCENT_CRAG, ascent.ascentCrag);
        values.put(KEY_OUTDOOR_ASCENT_NAME, ascent.ascentName);
        values.put(KEY_OUTDOOR_ASCENT_DATE, ascent.date.getTime());
        values.put(KEY_OUTDOOR_ASCENT_GRADE_ID, ascent.gradeId);
        values.put(KEY_OUTDOOR_ASCENT_METHOD, ascent.method);
        values.put(KEY_OUTDOOR_ASCENT_IMAGE_URI_STRING, ascent.photoUriString);

        return values;
    }

    /**
     * Get the number of ascents with distinct dates as the count for outdoor sessions.
     *
     * @param db - Database to use
     * @return - Number of ascents with distinct dates
     */
    static protected int getSessionCount(SQLiteDatabase db) {
        Cursor cursor = db.query(true, TABLE_OUTDOOR_ASCENTS, new String[]{KEY_OUTDOOR_ASCENT_DATE}, null, null, null, null, null, null);

        int count = cursor.getCount();
        cursor.close();
        return count;
    }
}
