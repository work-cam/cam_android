package caa.climbing.app.Database.DatabaseModels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 *
 */
public class LabelledInt extends DatabaseModel implements Parcelable, Comparable<LabelledInt> {

    /**
     * Generates instances of the Parcelable class from a Parcel
     */
    public static final Creator<LabelledInt> CREATOR = new Creator<LabelledInt>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from
         * the given Parcel whose data had previously been written by writeToParcel()
         *
         * @param in - Previously created Parcel
         * @return - New instance of the Parcelable class
         */
        @Override
        public LabelledInt createFromParcel(Parcel in) {
            return new LabelledInt(in);
        }

        /**
         * Create a new array of the Parcelable class.
         *
         * @param size - Size of the new Array
         * @return - New array of the Parcelable class
         */
        @Override
        public LabelledInt[] newArray(int size) {
            return new LabelledInt[size];
        }
    };

    public String label;
    public int value;
    public String tag;
    public int secondary;

    /**
     * Creates a new {@link LabelledInt} object
     *
     * @param label     - Label of the object
     * @param value     - Value of the object
     * @param tag       - Unique tag of the object
     * @param secondary - Secondary value
     */
    public LabelledInt(String label, int value, String tag, int secondary) {
        this.label = label;
        this.value = value;
        this.tag = tag;
        this.secondary = secondary;
    }

    /**
     * Creates a new {@link LabelledInt} object
     *
     * @param label - Label of the object
     * @param value - Value of the object
     * @param tag   - Unique tag of the object
     */
    public LabelledInt(String label, int value, String tag) {
        this(label, value, tag, 0);
    }

    /**
     * Creates a new {@link LabelledInt} object
     *
     * @param label     - Label of the object
     * @param value     - Value of the object
     * @param secondary - Secondary value
     */
    public LabelledInt(String label, int value, int secondary) {
        this(label, value, "", secondary);
    }

    /**
     * Creates a new {@link LabelledInt} object
     *
     * @param label - Label of the object
     * @param tag   - Unique tag of the object
     */
    public LabelledInt(String label, String tag) {
        this(label, 0, tag, 0);
    }

    /**
     * Creates a new {@link LabelledInt} object
     *
     * @param label - Label of the object
     * @param value - Value of the object
     */
    public LabelledInt(String label, int value) {
        this(label, value, "", 0);
    }

    /**
     * Creates a new {@link LabelledInt} object.
     *
     * @param label - Label of the object
     */
    public LabelledInt(String label) {
        this(label, 0, "", 0);
    }

    /**
     * Creates a new {@link LabelledInt} object from a Parcel.
     *
     * @param in - Parcel from which the instance is created.
     */
    protected LabelledInt(Parcel in) {
        this.tag = in.readString();
        this.label = in.readString();
        this.value = in.readInt();
        this.secondary = in.readInt();
    }

    /**
     * Extract the values from an ArrayList of {@link LabelledInt} as an array of floats.
     *
     * @param list - The list from which to get the values
     * @return - An array with the extracted values
     */
    public static float[] getFloatValues(ArrayList<LabelledInt> list) {

        ArrayList<LabelledInt> copy = new ArrayList<>(list);

        float[] values = new float[copy.size()];
        for (int i = 0; i < copy.size(); i++) {
            values[i] = copy.get(i).value;
        }

        return values;
    }

    /**
     * Extract the values from an ArrayList of {@link LabelledInt} as an array of integers.
     *
     * @param list - The list from which to get the values
     * @return - An array with the extracted values
     */
    public static int[] getIntValues(ArrayList<LabelledInt> list) {

        ArrayList<LabelledInt> copy = new ArrayList<>(list);

        int[] values = new int[copy.size()];
        for (int i = 0; i < copy.size(); i++) {
            values[i] = copy.get(i).value;
        }

        return values;
    }

    /**
     * Extract the labels from an ArrayList of {@link LabelledInt} as an array of strings.
     *
     * @param list - The list from which to get the values
     * @return - An array with the extracted labels
     */
    public static ArrayList<String> getLabels(ArrayList<LabelledInt> list) {

        ArrayList<String> labels = new ArrayList<>();

        for (LabelledInt element : list) {
            labels.add(element.label);
        }

        return labels;
    }

    /**
     * Get a {@link LabelledInt} from a list based on its label.
     *
     * @param label        - Label to check for
     * @param labelledInts - List to search
     * @return - Matching {@link LabelledInt} or null
     */
    public static LabelledInt getLabelledInt(String label, ArrayList<LabelledInt> labelledInts) {
        for (LabelledInt labelledInt : labelledInts) {
            if (labelledInt.label.equals(label)) return labelledInt;
        }
        return null;
    }

    /**
     * Check whether all values of {@link LabelledInt} in a list are equal to zero
     *
     * @param list - List to check
     * @return - True if all values are zero, false otherwise
     */
    public static boolean allZero(ArrayList<LabelledInt> list) {
        for (LabelledInt element : list) {
            if (element.value != 0) return false;
        }
        return true;
    }

    /**
     * Find the index of the {@link LabelledInt} that matches the given value in a list.
     *
     * @param labelledInts - List to search
     * @param value        - Value to check for
     * @return - Index of an element in the list that matches or -1 if no element matches
     */
    public static int getIndexByValue(ArrayList<LabelledInt> labelledInts, int value) {
        for (int i = 0; i < labelledInts.size(); i++) {
            LabelledInt labelledInt = labelledInts.get(i);
            if (labelledInt != null && labelledInt.value == value) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Check if the object is invalid.
     *
     * @return - false
     */
    @Override
    public boolean isInvalid() {
        return false;
    }

    /**
     * Compare two objects to each other based on their label in order to sort them in an ArrayList.
     *
     * @param other - Other object to compare to
     * @return - A negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object
     */
    @Override
    public int compareTo(LabelledInt other) {
        return this.label.compareTo(other.label);
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable
     * instance's marshaled representation. For example, if the object will
     * include a file descriptor in the output of {@link #writeToParcel(Parcel, int)},
     * the return value of this method must include the
     * {@link #CONTENTS_FILE_DESCRIPTOR} bit.
     *
     * @return a bitmask indicating the set of special object types marshaled by this Parcelable object instance.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  - The Parcel in which the object should be written.
     * @param flags - Additional flags about how the object should be written. May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.tag);
        dest.writeString(this.label);
        dest.writeInt(this.value);
        dest.writeInt(this.secondary);
    }
}
