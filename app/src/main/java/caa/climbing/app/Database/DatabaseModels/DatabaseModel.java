package caa.climbing.app.Database.DatabaseModels;

/**
 * Base class for objects that can be stored in the Database.
 */
public abstract class DatabaseModel {
    /**
     * Check if the DatabaseModel is invalid.
     *
     * @return - True if the model is invalid, false otherwise
     */
    public abstract boolean isInvalid();
}
