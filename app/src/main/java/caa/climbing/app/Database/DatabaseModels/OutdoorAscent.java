package caa.climbing.app.Database.DatabaseModels;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import caa.climbing.app.R;

import java.util.ArrayList;
import java.util.Date;

/**
 * A class to store information about an outdoor ascent, either route or boulder.
 */
public class OutdoorAscent extends DatabaseModel implements Parcelable, Comparable<OutdoorAscent> {
    final public static int BOULDER = 0;
    final public static int ROUTE = 1;

    final public static int ON_SIGHT = 0;
    final public static int FLASH = 1;
    final public static int RED_POINT = 2;

    /**
     * Generates instances of the Parcelable class from a Parcel
     */
    public static final Creator<OutdoorAscent> CREATOR = new Creator<OutdoorAscent>() {
        /**
         * Create a new instance of the Parcelable class, instantiating it from
         * the given Parcel whose data had previously been written by writeToParcel()
         *
         * @param in - Previously created Parcel
         * @return - New instance of the Parcelable class
         */
        @Override
        public OutdoorAscent createFromParcel(Parcel in) {
            return new OutdoorAscent(in);
        }

        /**
         * Create a new array of the Parcelable class.
         *
         * @param size - Size of the new Array
         * @return - New array of the Parcelable class
         */
        @Override
        public OutdoorAscent[] newArray(int size) {
            return new OutdoorAscent[size];
        }
    };

    final private static float MULTIPLIER_ON_SIGHT = 1.5f;
    final private static float MULTIPLIER_FLASH = 1.3f;
    final private static float MULTIPLIER_RED_POINT = 1f;
    public Integer id;
    public int climbType;
    public String ascentCrag;
    public String ascentName;
    public String photoUriString;
    public Date date;
    public int gradeId;
    public int method;
    public LabelledInt grade;
    public int score;

    /**
     * Creates a new {@link OutdoorAscent} object.
     *
     * @param id             - Unique ID of the ascent
     * @param climbType      - Either a boulder or route
     * @param crag           - Crag of the boulder/route
     * @param name           - Name of the ascent
     * @param photoUriString - Optional Uri if a photo is associated to the ascent
     * @param date           - Date when the boulder/route was climbed
     * @param method         - Method of topping
     * @param gradeId        - ID of the grade
     * @param grade          - Grade of the ascent
     * @param score          - Score for the grade
     */
    public OutdoorAscent(Integer id, int climbType, String crag, String name, String photoUriString, Date date, int method, int gradeId, LabelledInt grade, int score) {
        this.id = id;
        this.setClimbType(climbType);
        this.ascentCrag = crag;
        this.ascentName = name;
        this.photoUriString = photoUriString;
        this.date = date;
        this.gradeId = gradeId;
        this.grade = grade;
        this.score = score;
        this.setMethod(method);
    }

    /**
     * Creates a new {@link OutdoorAscent} object.
     *
     * @param id             - Unique ID of the ascent
     * @param climbType      - Either a boulder or route
     * @param crag           - Crag of the boulder/route
     * @param name           - Name of the ascent
     * @param photoUriString - Optional Uri if a photo is associated to the ascent
     * @param date           - Date when the boulder/route was climbed
     * @param method         - Method of topping
     * @param gradeId        - ID of the grade
     * @param grade          - Grade of the ascent
     */
    public OutdoorAscent(Integer id, int climbType, String crag, String name, String photoUriString, Date date, int method, int gradeId, LabelledInt grade) {
        this(id, climbType, crag, name, photoUriString, date, method, gradeId, grade, 0);
    }

    /**
     * Creates a new {@link OutdoorAscent} object from a Parcel.
     *
     * @param in - Parcel from which the instance is created.
     */
    protected OutdoorAscent(Parcel in) {
        this.id = in.readInt();
        this.climbType = in.readInt();
        this.ascentCrag = in.readString();
        this.ascentName = in.readString();
        this.photoUriString = in.readString();
        this.date = new Date(in.readLong());
        this.method = in.readInt();
        this.gradeId = in.readInt();
        this.grade = in.readParcelable(LabelledInt.class.getClassLoader());
        this.score = in.readInt();
        this.setClimbType(this.climbType);
        this.setMethod(this.method);
    }

    /**
     * Creates a new {@link OutdoorAscent} object.
     *
     * @param climbType - Either a boulder or route
     */
    public OutdoorAscent(int climbType) {
        this(null, climbType, null, null, null, null, -1, -1, null);
    }

    /**
     * Creates a new {@link OutdoorAscent} object from another ascent.
     *
     * @param other - Ascent to create the copy from
     */
    public OutdoorAscent(OutdoorAscent other) {
        this(other.id, other.climbType, other.ascentCrag, other.ascentName, other.photoUriString, other.date, other.method, other.gradeId, other.grade, other.score);
    }

    /**
     * Get a list of possible methods for topping a climb.
     *
     * @return - List of possible methods for topping a climb
     */
    public static ArrayList<LabelledInt> getMethods(Context context) {
        ArrayList<LabelledInt> methods = new ArrayList<>();
        methods.add(new LabelledInt(context.getString(R.string.outdoor_ascent_method_on_sight), 0));
        methods.add(new LabelledInt(context.getString(R.string.outdoor_ascent_method_flash), 1));
        methods.add(new LabelledInt(context.getString(R.string.outdoor_ascent_method_red_point), 2));
        return methods;
    }

    /**
     * Get a list of the names of possible topping methods.
     *
     * @return - List of the names of possible topping methods
     */
    public static ArrayList<String> getMethodStrings(Context context) {
        ArrayList<LabelledInt> methods = getMethods(context);
        return LabelledInt.getLabels(methods);
    }

    /**
     * Set the type of the ascent, which is either boulder or route.
     *
     * @param climbType - Type of the ascent
     */
    public void setClimbType(int climbType) {
        switch (climbType) {
            case BOULDER:
                this.climbType = BOULDER;
                break;
            case ROUTE:
                this.climbType = ROUTE;
                break;
            default:
                this.climbType = -1;
                break;
        }
    }

    /**
     * Set the method of topping, which is either on-sight, flash or red-point.
     *
     * @param method - Method of topping
     */
    public void setMethod(int method) {
        this.score = this.getScore(method);

        switch (method) {
            case ON_SIGHT:
                this.method = ON_SIGHT;
                break;
            case RED_POINT:
                this.method = RED_POINT;
                break;
            case FLASH:
                this.method = FLASH;
                break;
            default:
                this.method = -1;
                break;
        }
    }

    /**
     * Calculate the actual score for an ascent based on the grade score and the method for ascending.
     *
     * @param method - Method for ascending
     * @return - Point score for the ascent
     */
    private int getScore(int method) {
        if (this.grade == null) return 0;

        switch (method) {
            case ON_SIGHT:
                return (int) (MULTIPLIER_ON_SIGHT * this.grade.secondary);
            case RED_POINT:
                return (int) (MULTIPLIER_RED_POINT * this.grade.secondary);
            case FLASH:
                return (int) (MULTIPLIER_FLASH * this.grade.secondary);
            default:
                return 0;
        }
    }

    /**
     * Check whether the ascent is valid for storing in the database.
     *
     * @return - True if the ascent is valid, false otherwise
     */
    @Override
    public boolean isInvalid() {
        if (ascentName == null) return true;
        else {
            if (ascentName.replaceAll("\\s", "").equals("")) return true;
        }
        if (ascentCrag == null) return true;
        else {
            if (ascentCrag.replaceAll("\\s", "").equals("")) return true;
        }
        if (date == null) return true;
        if (gradeId == -1) return true;
        if (method == -1) return true;
        return climbType == -1;
    }

    /**
     * Get a String representation of the outdoor ascent.
     *
     * @return - String representation of the object
     */
    @NonNull
    @Override
    public String toString() {
        String stringRepresentation = "";
        stringRepresentation += "Name: " + ascentName + "\n";
        stringRepresentation += "Crag: " + ascentCrag + "\n";
        stringRepresentation += "UriString: " + photoUriString + "\n";
        stringRepresentation += "Date: " + date + "\n";
        stringRepresentation += "GradeID: " + gradeId + "\n";
        stringRepresentation += "Grade: " + (grade == null ? null : grade.label) + "\n";
        stringRepresentation += "Method: " + method;

        return stringRepresentation;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable
     * instance's marshaled representation. For example, if the object will
     * include a file descriptor in the output of {@link #writeToParcel(Parcel, int)},
     * the return value of this method must include the
     * {@link #CONTENTS_FILE_DESCRIPTOR} bit.
     *
     * @return a bitmask indicating the set of special object types marshaled by this Parcelable object instance.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  - The Parcel in which the object should be written.
     * @param flags - Additional flags about how the object should be written. May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.climbType);
        dest.writeString(this.ascentCrag);
        dest.writeString(this.ascentName);
        dest.writeString(this.photoUriString);
        dest.writeLong(this.date.getTime());
        dest.writeInt(this.gradeId);
        dest.writeInt(this.method);
        dest.writeInt(this.score);
        dest.writeParcelable(this.grade, flags);
    }

    /**
     * Compare two ascents to each other based on their date in order to sort them in an ArrayList.
     *
     * @param other - Other ascent to compare to
     * @return - A negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object
     */
    @Override
    public int compareTo(OutdoorAscent other) {
        return this.date.compareTo(other.date);
    }
}
