package caa.climbing.app.Database.DatabaseModels;

import java.util.Date;

/**
 * A class to store information about a (completed) hang board session.
 */
public class HangboardSession extends DatabaseModel implements Comparable<HangboardSession> {
    public int sets;
    public int pauseTime;
    public int reps;
    public int restTime;
    public int hangTime;

    public float maxStrengthLeft;
    public float maxStrengthRight;
    public float bodyWeight;
    public float extraWeight;
    public int testEdgeSize;

    public Date date;
    public Integer id;

    /**
     * Creates a new {@link HangboardSession} object
     *
     * @param id               - ID of the session
     * @param date             - Date of the session
     * @param sets             - Sets
     * @param bodyWeight       - Body weight
     * @param pauseTime        - Pause time in seconds
     * @param extraWeight      - Additional Weight in kg
     * @param maxStrengthLeft  - Finger strength for the right hand in kg
     * @param maxStrengthRight - Finger strength for the left hand in kg
     * @param testEdgeSize     - Size of the test edge in mm
     * @param hangTime         - Hang time in seconds
     * @param restTime         - Rest time in seconds
     * @param reps             - Repetitions
     */
    public HangboardSession(Integer id, Date date, int sets, int pauseTime, int reps, int restTime, int hangTime, float maxStrengthLeft, float maxStrengthRight, int testEdgeSize, float bodyWeight, float extraWeight) {
        this.id = id;
        this.date = date;
        this.sets = sets;
        this.pauseTime = pauseTime;
        this.reps = reps;
        this.restTime = restTime;
        this.hangTime = hangTime;
        this.maxStrengthLeft = maxStrengthLeft;
        this.maxStrengthRight = maxStrengthRight;
        this.testEdgeSize = testEdgeSize;
        this.bodyWeight = bodyWeight;
        this.extraWeight = extraWeight;
    }

    /**
     * Creates a new {@link HangboardSession} object
     *
     * @param date             - Date of the session
     * @param sets             - Sets
     * @param bodyWeight       - Body weight
     * @param pauseTime        - Pause time in seconds
     * @param extraWeight      - Additional Weight in kg
     * @param maxStrengthLeft  - Finger strength for the right hand in kg
     * @param maxStrengthRight - Finger strength for the left hand in kg
     * @param testEdgeSize     - Size of the test edge in mm
     * @param hangTime         - Hang time in seconds
     * @param restTime         - Rest time in seconds
     * @param reps             - Repetitions
     */
    public HangboardSession(Date date, int sets, int pauseTime, int reps, int restTime, int hangTime, float maxStrengthLeft, float maxStrengthRight, int testEdgeSize, float bodyWeight, float extraWeight) {
        this(null, date, sets, pauseTime, reps, restTime, hangTime, maxStrengthLeft, maxStrengthRight, testEdgeSize, bodyWeight, extraWeight);
    }

    /**
     * Creates a new {@link HangboardSession} object
     *
     * @param date         - Date of the session
     * @param sets         - Sets
     * @param pauseTime    - Pause time in seconds
     * @param reps         - Repetitions
     * @param restTime     - Rest time in seconds
     * @param hangTime     - Hang time in seconds
     * @param testEdgeSize - Size of the test edge in mm
     * @param extraWeight  - Additional Weight in kg
     */
    public HangboardSession(Date date, int sets, int pauseTime, int reps, int restTime, int hangTime, int testEdgeSize, float extraWeight) {
        this(null, date, sets, pauseTime, reps, restTime, hangTime, 0, 0, testEdgeSize, 0, extraWeight);
    }

    /**
     * Check if the hangboard session is invalid.
     *
     * @return - True if the session is invalid, false otherwise
     */
    @Override
    public boolean isInvalid() {
        return this.date == null;
    }

    /**
     * Compare two sessions to each other based on their date in order to sort them in an ArrayList.
     *
     * @param other - Other session to compare to
     * @return - A negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object
     */
    @Override
    public int compareTo(HangboardSession other) {
        return this.date.compareTo(other.date);
    }
}
