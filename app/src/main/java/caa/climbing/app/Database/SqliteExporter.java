package caa.climbing.app.Database;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import caa.climbing.app.Database.DataBaseOperations.DatabaseHelper;
import caa.climbing.app.Dialogs.InformDialog.InformDialog;
import caa.climbing.app.HelperClasses.PermissionHandler;
import caa.climbing.app.R;

import java.io.*;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * Can export an SQLite database into a csv file.
 * see https://stackoverflow.com/questions/31367270/exporting-sqlite-database-to-csv-file-in-android
 */
public class SqliteExporter extends Fragment {
    static private final String TAG = "EXPORT";
    private final PermissionHandler permissionHandler;
    private ActivityResultLauncher<Intent> activityResultLauncher;

    private final FragmentManager fragmentManager;

    /**
     * Creates a new {@link SqliteExporter} object.
     *
     * @param activity - Activity the fragment is attached to
     */
    public SqliteExporter(Activity activity) {
        super();
        this.fragmentManager = activity == null ? null : ((FragmentActivity) activity).getSupportFragmentManager();
        this.permissionHandler = new PermissionHandler(getContext(), this.fragmentManager, this::startSaveIntent, PermissionHandler.WRITE);
        this.registerActivityResultLauncher();
    }

    /**
     * Creates a new {@link SqliteExporter} object.
     */
    public SqliteExporter() {
        this(null);
    }

    /**
     * Register the Launcher to handle the result from the folder selection activity.
     */
    private void registerActivityResultLauncher() {
        ActivityResultContracts.StartActivityForResult startActivityForResult = new ActivityResultContracts.StartActivityForResult();
        this.activityResultLauncher = registerForActivityResult(startActivityForResult, this::handleSaveResult);
    }

    /**
     * Select the folder to export to.
     */
    private void startSaveIntent() {
        Intent saveIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        this.activityResultLauncher.launch(saveIntent);
    }

    /**
     * Handle the result of choosing a folder to store the backup file.
     *
     * @param result - Result from the activity
     */
    private void handleSaveResult(ActivityResult result) {
        if (result.getResultCode() == RESULT_OK) {
            boolean success = this.createBackup(result.getData());

            String message = success ? getString(R.string.sqlite_exporter_success) : getString(R.string.sqlite_exporter_failed);
            InformDialog.openDialog(getChildFragmentManager(), message);
        }
    }

    /**
     * Creates a backup file at the designated location.
     *
     * @param data - Data that contains information about where to store the backup file
     * @return - Whether the backup was created successfully
     */
    private boolean createBackup(@Nullable Intent data) {
        boolean success = false;
        Context context = getContext();
        if (data == null || context == null) return false;

        Uri uri = data.getData();
        DocumentFile directory = DocumentFile.fromTreeUri(context, uri);

        if (directory != null) {
            String fileName = createBackupFileName();
            DocumentFile createdFile = directory.createFile("application/vnd.sqlite3, application/x-sqlite3", fileName);

            if (createdFile != null) {
                success = this.streamBackupToFile(context, createdFile.getUri());
            }
        }
        return success;
    }

    /**
     * Stream the contents of the original database file to the backup file.
     *
     * @param context - Context
     * @param destUri - Uri of the file to stream to
     * @return - Whether the streaming was successful
     */
    private boolean streamBackupToFile(Context context, Uri destUri) {
        try {
            ContentResolver contentResolver = context.getContentResolver();
            FileInputStream inputStream = new FileInputStream(DatabaseHelper.getDatabaseFile(context));
            FileOutputStream outputStream = (FileOutputStream) contentResolver.openOutputStream(destUri);

            FileChannel fromChannel = inputStream.getChannel();
            FileChannel toChannel = outputStream.getChannel();

            fromChannel.transferTo(0, fromChannel.size(), toChannel);

            fromChannel.close();
            toChannel.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Export the database.
     */
    public void export() {
        boolean fragmentAlreadyAdded = this.fragmentManager.getFragments().contains(this);

        if (!fragmentAlreadyAdded) {
            this.fragmentManager.beginTransaction().add(this, TAG).commit();
            this.fragmentManager.executePendingTransactions();
        }

        this.permissionHandler.startActionWithPermission();
    }

    /**
     * Create a (unique) name for the backup file.
     *
     * @return - Name for the backup file
     */
    private String createBackupFileName() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HHmm", Locale.ENGLISH);
        return "db_backup_" + sdf.format(new Date()) + ".db";
    }
}