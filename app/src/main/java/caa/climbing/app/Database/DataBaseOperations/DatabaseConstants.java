package caa.climbing.app.Database.DataBaseOperations;

class DatabaseConstants {

    protected static final String ERROR_TAG = "Database error";

    // Resources.Database Info
    protected static final String DATABASE_NAME = "test.db";
    protected static final int DATABASE_VERSION = 1;

    // Table names
    protected static final String TABLE_GRADES = "grades";
    protected static final String TABLE_INDOOR_SESSIONS = "indoor_sessions";
    protected static final String TABLE_OUTDOOR_ASCENTS = "outdoor_ascents";
    protected static final String TABLE_HANGBOARD_SESSIONS = "hangboard_sessions";

    // keys for the respective tables
    // grades table
    protected static final String KEY_GRADES_ID = "id";
    protected static final String KEY_GRADES_SCORE = "score";
    protected static final String KEY_GRADES_FRA_BOULDERS = "fra_boulders";
    protected static final String KEY_GRADES_FRA_ROUTES = "fra_routes";
    protected static final String KEY_GRADES_USA_BOULDERS = "usa_boulders";
    protected static final String KEY_GRADES_UIAA_ROUTES = "uiaa_routes";

    // indoor sessions table
    protected static final String KEY_INDOOR_SESSION_ID = "id";
    protected static final String KEY_INDOOR_SESSION_DATE = "date";
    protected static final String KEY_INDOOR_SESSION_GYM = "gym";
    protected static final String FORMAT_STRING_INDOOR_BOULDER_BRACKETS = "boulder_bracket_%s";
    protected static final String FORMAT_STRING_INDOOR_ROUTE_BRACKETS = "routes_bracket_%s";
    protected static final String KEY_INDOOR_SESSION_BOULDER_BRACKET_0 = "boulder_bracket_0";
    protected static final String KEY_INDOOR_SESSION_BOULDER_BRACKET_1 = "boulder_bracket_1";
    protected static final String KEY_INDOOR_SESSION_BOULDER_BRACKET_2 = "boulder_bracket_2";
    protected static final String KEY_INDOOR_SESSION_BOULDER_BRACKET_3 = "boulder_bracket_3";
    protected static final String KEY_INDOOR_SESSION_BOULDER_BRACKET_4 = "boulder_bracket_4";
    protected static final String KEY_INDOOR_SESSION_ROUTES_BRACKET_0 = "routes_bracket_0";
    protected static final String KEY_INDOOR_SESSION_ROUTES_BRACKET_1 = "routes_bracket_1";
    protected static final String KEY_INDOOR_SESSION_ROUTES_BRACKET_2 = "routes_bracket_2";
    protected static final String KEY_INDOOR_SESSION_ROUTES_BRACKET_3 = "routes_bracket_3";
    protected static final String KEY_INDOOR_SESSION_ROUTES_BRACKET_4 = "routes_bracket_4";
    protected static final String KEY_INDOOR_SESSION_ROUTES_BRACKET_5 = "routes_bracket_5";
    // outdoor session table
    protected static final String KEY_OUTDOOR_ASCENT_ID = "id";
    protected static final String KEY_OUTDOOR_ASCENT_DATE = "date";
    protected static final String KEY_OUTDOOR_ASCENT_CRAG = "crag";
    protected static final String KEY_OUTDOOR_ASCENT_GRADE_ID = "grade_id";
    protected static final String KEY_OUTDOOR_ASCENT_CLIMB_TYPE = "climb_type";
    protected static final String KEY_OUTDOOR_ASCENT_NAME = "name";
    protected static final String KEY_OUTDOOR_ASCENT_METHOD = "method";
    protected static final String KEY_OUTDOOR_ASCENT_IMAGE_URI_STRING = "image_uri_string";
    // hangboard session table
    protected static final String KEY_HANGBOARD_SESSION_ID = "id";
    protected static final String KEY_HANGBOARD_SESSION_DATE = "date";
    protected static final String KEY_HANGBOARD_SESSION_HANG_S = "hang_s";
    protected static final String KEY_HANGBOARD_SESSION_REST_BTW_HANGS_S = "rest_btw_hangs_s";
    protected static final String KEY_HANGBOARD_SESSION_PAUSE_BTW_SETS_S = "pause_btw_sets_s";
    protected static final String KEY_HANGBOARD_SESSION_REPETITIONS = "repetitions";
    protected static final String KEY_HANGBOARD_SESSION_SETS = "sets";
    protected static final String KEY_HANGBOARD_SESSION_BODY_WEIGHT_KG = "body_weight_kg";
    protected static final String KEY_HANGBOARD_SESSION_EXTRA_WEIGHT_KG = "extra_weight_kg";
    protected static final String KEY_HANGBOARD_SESSION_MAX_STRENGTH_LEFT_KG = "max_strength_left_kg";
    protected static final String KEY_HANGBOARD_SESSION_MAX_STRENGTH_RIGHT_KG = "max_strength_right_kg";
    protected static final String KEY_HANGBOARD_SESSION_TEST_EDGE_SIZE_MM = "test_edge_size_mm";


}
