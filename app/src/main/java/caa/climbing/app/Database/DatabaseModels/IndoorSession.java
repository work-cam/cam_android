package caa.climbing.app.Database.DatabaseModels;

import java.util.ArrayList;
import java.util.Date;

/**
 * A class to store information about a completed session indoors.
 */
public class IndoorSession extends DatabaseModel implements Comparable<IndoorSession> {
    public Integer id;
    public String gym;
    public Date date;

    public ArrayList<LabelledInt> boulders;
    public ArrayList<LabelledInt> routes;

    /**
     * Creates a new {@link IndoorSession} object
     *
     * @param id       - Unique ID of the session
     * @param gym      - Gym where the session was held
     * @param date     - Date when the session was held
     * @param boulders - List of boulder ascents
     * @param routes   - List of route ascents
     */
    public IndoorSession(Integer id, String gym, Date date, ArrayList<LabelledInt> boulders, ArrayList<LabelledInt> routes) {
        this.id = id;
        this.gym = gym;
        this.date = date;
        this.boulders = boulders;
        this.routes = routes;
    }

    /**
     * Creates a new {@link IndoorSession} object
     *
     * @param gym      - Gym where the session was held
     * @param date     - Date when the session was held
     * @param boulders - List of boulder ascents
     * @param routes   - List of route ascents
     */
    public IndoorSession(String gym, Date date, ArrayList<LabelledInt> boulders, ArrayList<LabelledInt> routes) {
        this(null, gym, date, boulders, routes);
    }

    /**
     * Check if the indoor session is invalid.
     *
     * @return - True if the session is invalid, false otherwise
     */
    @Override
    public boolean isInvalid() {

        if (this.gym.equals("")) {
            return true;
        }
        if (this.date == null) {
            return true;
        }

        return LabelledInt.allZero(this.boulders) && LabelledInt.allZero(this.routes);
    }

    /**
     * Compare two sessions to each other based on their date in order to sort them in an ArrayList.
     *
     * @param other - Other session to compare to
     * @return - A negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object
     */
    @Override
    public int compareTo(IndoorSession other) {
        return this.date.compareTo(other.date);
    }
}
