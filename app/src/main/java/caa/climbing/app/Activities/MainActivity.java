package caa.climbing.app.Activities;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.ScrollView;

import caa.climbing.app.HelperClasses.LocaleHelper;
import caa.climbing.app.Screens.*;
import caa.climbing.app.Screens.Home.Home;
import caa.climbing.app.Screens.LogBook.LogBook;
import caa.climbing.app.Views.Menu;
import caa.climbing.app.R;
import caa.climbing.app.Views.TopBar;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    final static public String ACTIVE_SCREEN_KEY = "active_screen_key";

    final static private int KEY_HOME = 0;
    final static private int KEY_INDOOR = 1;
    final static private int KEY_OUTDOOR = 2;
    final static private int KEY_HANGBOARD = 3;
    final static private int KEY_LOGBOOK = 4;
    final static private int KEY_PLAN_SESSIONS = 5;
    final static private int KEY_SETTINGS = 6;

    // Screens
    private Home home;
    private Settings settings;
    private HangboardSession hangboardSession;
    private IndoorSession indoorSession;
    private OutdoorSession outdoorSession;
    private PlanSessions planSessions;
    private LogBook logBook;

    private TopBar topBar;
    private Menu menu;
    private NestedScrollView scrollView;

    private HashMap<Integer, String> screens;
    private Integer activeScreen;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.setLocale(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        int activeScreen = getIntent().getIntExtra(ACTIVE_SCREEN_KEY, -1);

        this.topBar = findViewById(R.id.topBar);
        this.menu = findViewById(R.id.menu);
        this.scrollView = findViewById(R.id.main_scrollView);

        this.topBar.setMenuButtonClick((view) -> toggleMenuVisibility());

        this.activeScreen = Math.max(activeScreen, 0);
        this.initScreens();

        this.initMenu();

        this.topBar.setTitle(this.screens.get(this.activeScreen));

        // Screens
        this.home = new Home();
        this.indoorSession = new IndoorSession();
        this.outdoorSession = new OutdoorSession();
        this.hangboardSession = new HangboardSession();
        this.logBook = new LogBook();
        this.planSessions = new PlanSessions();
        this.settings = new Settings();

        this.showActiveScreen();
    }

    /**
     * Initialize the Map for the screens
     */
    private void initScreens() {
        this.screens = new HashMap<>();
        this.screens.put(KEY_HOME, getString(R.string.menu_home));
        this.screens.put(KEY_INDOOR, getString(R.string.menu_new_indoor_session));
        this.screens.put(KEY_OUTDOOR, getString(R.string.menu_new_outdoor_session));
        this.screens.put(KEY_HANGBOARD, getString(R.string.menu_new_hangboard_session));
        this.screens.put(KEY_LOGBOOK, getString(R.string.menu_logbook));
        this.screens.put(KEY_PLAN_SESSIONS, getString(R.string.menu_plan_sessions));
        this.screens.put(KEY_SETTINGS, getString(R.string.menu_settings));
    }

    /**
     * Toggle the visibility of the menu.
     */
    public void toggleMenuVisibility() {
        if (this.menu.isVisible()) {
            this.menu.hide();
        } else {
            this.menu.show();
        }
    }

    /**
     * Initialize the menu.
     */
    private void initMenu() {
        this.menu.setScreens(this.screens);
        this.menu.setActiveScreenKey(this.activeScreen);
        this.menu.setTopReference(this.topBar);
        this.menu.render();
        this.menu.setVisibility(false);
    }

    /**
     * @param activeScreen - Key of the screen to activate
     */
    public void setActiveScreen(int activeScreen, boolean menuTriggered) {
        if (menuTriggered) {
            this.logBook.setActiveTab(0);
        }
        this.activeScreen = activeScreen;
        this.topBar.setTitle(this.screens.get(activeScreen));
        this.showActiveScreen();
    }

    /**
     * @return - The currently active screen
     */
    private Fragment getActiveScreen() {
        switch (this.activeScreen) {
            case KEY_HOME:
                return this.home;
            case KEY_INDOOR:
                return this.indoorSession;
            case KEY_OUTDOOR:
                return this.outdoorSession;
            case KEY_HANGBOARD:
                return this.hangboardSession;
            case KEY_LOGBOOK:
                return this.logBook;
            case KEY_PLAN_SESSIONS:
                return this.planSessions;
            case KEY_SETTINGS:
                return this.settings;
            default:
                return null;
        }
    }

    /**
     *
     */
    private void showActiveScreen() {
        Fragment activeScreen = this.getActiveScreen();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        if (activeScreen == null) {
            // Show an empty screen
            for (Fragment fragment : fragmentManager.getFragments()) {
                transaction.remove(fragment);
            }
        } else {
            // replace the current screen with the active screen
            transaction
                    .replace(R.id.fragment_container_view, activeScreen, null)
                    .setReorderingAllowed(true);
        }

        transaction.commit();
    }

    /**
     * Opens the logbook with a specific tab open.
     *
     * @param activeTab - Which tab of the logbook to open
     */
    public void openLogBook(int activeTab) {
        this.logBook.setActiveTab(activeTab);
        this.menu.setActiveScreenKey(KEY_LOGBOOK);
        this.setActiveScreen(KEY_LOGBOOK, false);
    }

    /**
     * Scrolls the contents of the scroll view to the top.
     */
    public void scrollToTop() {
        this.scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    /**
     * Get the integer key of the currently active screen.
     *
     * @return - Key of the active screen
     */
    public Integer getActiveScreenKey() {
        return this.activeScreen;
    }

    ///////////////

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    ////////////////////////////////////////////////////////////////////////

}
