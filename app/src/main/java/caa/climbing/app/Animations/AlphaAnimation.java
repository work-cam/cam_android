package caa.climbing.app.Animations;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;

/**
 * A class to animate the transparency of a view
 */
public class AlphaAnimation {

    static private final long DURATION = 600;

    /**
     * Creates an animation that animates the alpha channel.
     *
     * @param fromAlpha         - Starting alpha value
     * @param toAlpha           - Destination alpha value
     * @param animationListener - Listener for the animation
     * @return - Fade animation
     */
    private static Animation fadeAnimation(float fromAlpha, float toAlpha, Animation.AnimationListener animationListener) {
        Animation fadeAnimation = new android.view.animation.AlphaAnimation(fromAlpha, toAlpha);
        fadeAnimation.setInterpolator(new DecelerateInterpolator());
        fadeAnimation.setDuration(DURATION);
        fadeAnimation.setAnimationListener(animationListener);

        return fadeAnimation;
    }

    /**
     * Fades in a view from zero opacity and sets the view visible.
     *
     * @param view - View to animate
     */
    static public void fadeIn(final View view) {
        Animation.AnimationListener animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

        Animation fadeInAnimation = fadeAnimation(0, 1, animationListener);

        view.startAnimation(fadeInAnimation);
    }

    /**
     * Fades out a view to zero opacity and sets the view invisible.
     *
     * @param view - View to animate
     */
    static public void fadeOut(final View view) {

        Animation.AnimationListener animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.INVISIBLE);
                view.setAlpha(0);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

        Animation fadeOutAnimation = fadeAnimation(1, 0, animationListener);

        view.startAnimation(fadeOutAnimation);
    }

    /**
     * Fades out a view from full to half opacity.
     *
     * @param view - View to animate
     */
    static public void fadeOutLong(final View view) {
        Animation.AnimationListener animationListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setAlpha(0.5f);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

        Animation fadeOutAnimation = fadeAnimation(1, 0.5f, animationListener);

        view.startAnimation(fadeOutAnimation);
    }
}
