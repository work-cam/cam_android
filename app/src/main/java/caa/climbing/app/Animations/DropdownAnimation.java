package caa.climbing.app.Animations;

import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import androidx.annotation.Nullable;
import caa.climbing.app.HelperClasses.Utils;

/**
 * A class to animate dropdown actions of views.
 */
public class DropdownAnimation {

    static long duration = 400;

    /**
     * Animate the expansion of a view.
     *
     * @param view     - View to expand
     * @param callback - Callback for the animation
     */
    public static void expand(final View view, @Nullable AnimationCallback callback) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) view.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = view.getMeasuredHeight();

        animateExpansion(view, targetHeight, callback);
    }

    /**
     * Animate the expansion of a view.
     *
     * @param view - View to expand
     */
    public static void expand(final View view) {
        expand(view, null);
    }

    /**
     * Animate the expansion of a view so that it fills the remaining space between the top view and the bottom edge of the screen.
     *
     * @param view     - View to expand
     * @param top      - View above the view to expand
     * @param activity - Activity where the animation will be displayed
     */
    public static void expandFull(View view, View top, Activity activity) {
        int targetHeight = Utils.getDistanceToBottomScreen(top, activity);

        animateExpansion(view, targetHeight);
    }

    /**
     * Animate the collapse of a view.
     *
     * @param view     - View to collapse
     * @param callback - Callback for the animation
     */
    public static void collapse(final View view, @Nullable AnimationCallback callback) {
        final int initialHeight = view.getMeasuredHeight();

        Animation animation = new Animation() {
            @Override
            public boolean willChangeBounds() {
                return true;
            }

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 0 && callback != null) {
                    callback.onAnimationStart();
                }

                if (interpolatedTime == 1) {
                    view.setVisibility(View.GONE);
                    if (callback != null) {
                        callback.onAnimationEnd();
                    }
                } else {
                    view.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    view.requestLayout();
                }
            }
        };

        animation.setDuration(duration);
        view.startAnimation(animation);
    }

    /**
     * Animate the collapse of a view.
     *
     * @param view - View to collapse
     */
    public static void collapse(final View view) {
        collapse(view, null);
    }


    /**
     * Expand a view to a certain height and animate that expansion.
     *
     * @param view         - View to expand
     * @param targetHeight - Height to expand to
     * @param callback     - Callback for the animation
     */
    private static void animateExpansion(View view, int targetHeight, @Nullable AnimationCallback callback) {
        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        view.getLayoutParams().height = 1;

        Animation animation = new Animation() {
            @Override
            public boolean willChangeBounds() {
                return true;
            }

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                int currentHeight = (int) (targetHeight * interpolatedTime);

                if (currentHeight > 0) view.setVisibility(View.VISIBLE);
                view.getLayoutParams().height = interpolatedTime == 1
                        ? targetHeight
                        : currentHeight;
                if (callback != null) {
                    if (interpolatedTime == 0) callback.onAnimationStart();
                    else if (interpolatedTime == 1) callback.onAnimationEnd();
                }
                view.requestLayout();
            }
        };

        animation.setDuration(duration);
        view.startAnimation(animation);
    }

    /**
     * Expand a view to a certain height and animate that expansion.
     *
     * @param view         - View to expand
     * @param targetHeight - Height to expand to
     */
    private static void animateExpansion(View view, int targetHeight) {
        animateExpansion(view, targetHeight, null);
    }

    /**
     * Wrapper interface to pass callbacks when the animation starts or ends
     */
    public interface AnimationCallback {
        /**
         * Callback for when the animation ends
         */
        void onAnimationEnd();

        /**
         * Callback for when the animation starts
         */
        void onAnimationStart();
    }
}
