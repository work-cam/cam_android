package caa.climbing.app.Dialogs.ConfirmDialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;

/**
 * A class for displaying a custom {@link AlertDialog}.
 */
public class ConfirmDialog extends DialogFragment {
    public FlatButton cancelBtn, okBtn;
    public TextView dialogTitle;

    private final View.OnClickListener okClick;
    private final View.OnClickListener cancelClick;

    private final String title;
    private final String okText;
    private final String cancelText;

    /**
     * Create a new {@link ConfirmDialog} DialogFragment.
     *
     * @param title       - Title of the dialog
     * @param okText      - Text of the positive button
     * @param cancelText  - Text of the negative button
     * @param okClick     - ClickListener for the positive button
     * @param cancelClick - ClickListener for the negative button
     */
    public ConfirmDialog(String title, @Nullable String okText, @Nullable String cancelText, View.OnClickListener okClick, @Nullable View.OnClickListener cancelClick) {
        this.okClick = okClick;
        this.cancelClick = cancelClick;
        this.title = title;
        this.cancelText = cancelText;
        this.okText = okText;
    }

    /**
     * Create a new {@link ConfirmDialog} DialogFragment.
     *
     * @param title       - Title of the dialog
     * @param okClick     - ClickListener for the positive button
     * @param cancelClick - ClickListener for the negative button
     */
    public ConfirmDialog(String title, View.OnClickListener okClick, @Nullable View.OnClickListener cancelClick) {
        this(title, null, null, okClick, cancelClick);
    }

    /**
     * Override to build a custom Dialog container.
     *
     * @param savedInstanceState - Previous Instance
     * @return - Custom dialog
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(initView());

        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        return dialog;
    }

    /**
     * Initializes the view of the dialog
     *
     * @return - Custom view for the dialog
     */
    private View initView() {
        String cancelText = this.cancelText == null ? getString(R.string.select_dialog_default_cancel_text) : this.cancelText;
        String okText = this.okText == null ? getString(R.string.select_dialog_default_confirm_text) : this.okText;
        String title = this.title == null ? getString(R.string.select_dialog_default_title) : this.title;

        LayoutInflater inflater = requireActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.confirm_dialog, null);

        this.dialogTitle = rootView.findViewById(R.id.dialog_title);
        this.okBtn = rootView.findViewById(R.id.ok_btn);
        this.cancelBtn = rootView.findViewById(R.id.cancel_btn);

        this.dialogTitle.setText(title);
        this.okBtn.setText(okText);
        this.cancelBtn.setText(cancelText);

        this.addEventListener();

        return rootView;
    }

    /**
     * Adds Listener to the positive and negative buttons.
     */
    private void addEventListener() {
        this.okBtn.setOnClickListener((view) -> {
            if (okClick != null) okClick.onClick(view);
            dismiss();
        });

        this.cancelBtn.setOnClickListener((view) -> {
            if (cancelClick != null) cancelClick.onClick(view);
            dismiss();
        });
    }

}
