package caa.climbing.app.Dialogs.InformDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import caa.climbing.app.HelperClasses.Utils;
import caa.climbing.app.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

/**
 * A class for displaying a custom {@link AlertDialog}.
 */
public class InformDialog extends DialogFragment {
    private final String message;

    public InformDialog(String message) {
        this.message = message;
    }

    /**
     * Override to build a custom Dialog container.
     *
     * @param savedInstanceState - Previous Instance
     * @return - Custom dialog
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View rootView = this.initView();
        int verticalPadding = rootView.getPaddingBottom() + rootView.getPaddingTop();

        builder.setView(rootView);

        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        this.setDialogPosition(dialog, verticalPadding);

        return dialog;
    }

    /**
     * Initializes the view of the dialog
     *
     * @return - Custom view for the dialog
     */
    private View initView() {
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.inform_dialog, null);

        TextView textView = rootView.findViewById(R.id.dialog_message);

        textView.setText(this.message);

        return rootView;
    }

    /**
     * Set the position of the dialog on the screen.
     *
     * @param dialog          - Dialog for which to set the position
     * @param verticalPadding - Total vertical padding (top + bottom) of the dialog view
     */
    private void setDialogPosition(AlertDialog dialog, int verticalPadding) {

        WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();

        Activity activity = getActivity();
        if (activity != null) {
            // get window dimensions
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int windowHeight = displayMetrics.heightPixels;

            float scale = getActivity().getResources().getDisplayMetrics().density;
            float textSize = Utils.sp2px(getContext(), getResources().getDimension(R.dimen.fontsize_standard));
            layoutParams.y = (int) (1.0 / 3 * windowHeight - 0.5 * verticalPadding / scale - textSize);
            layoutParams.gravity = Gravity.TOP;
            dialog.getWindow().setAttributes(layoutParams);
        }
    }

    /**
     * Opens a simple {@link InformDialog} in order to display a message.
     *
     * @param fragmentManager - Fragment manager to use for displaying the dialog
     * @param message         - Message to display
     * @param durationMillis  - How long to display the dialog
     * @param closeCallBack   - Callback to execute after the dialog is closed
     */
    static public void openDialog(FragmentManager fragmentManager, String message, long durationMillis, @Nullable CloseCallBack closeCallBack) {
        final InformDialog dialog = new InformDialog(message);
        dialog.show(fragmentManager, "Dialog");

        Handler handler = new Handler();
        handler.postDelayed(() -> dismissWithCallBack(dialog, closeCallBack), durationMillis);
    }

    /**
     * Opens a simple {@link InformDialog} in order to display a message.
     *
     * @param fragmentManager - Fragment manager to use for displaying the dialog
     * @param message         - Message to display
     * @param durationMillis  - How long to display the dialog
     */
    static public void openDialog(FragmentManager fragmentManager, String message, long durationMillis) {
        openDialog(fragmentManager, message, durationMillis, null);
    }

    /**
     * Opens a simple {@link InformDialog} in order to display a message.
     *
     * @param fragmentManager - Fragment manager to use for displaying the dialog
     * @param message         - Message to display
     */
    static public void openDialog(FragmentManager fragmentManager, String message) {
        openDialog(fragmentManager, message, 1200);
    }

    /**
     * Opens a simple {@link InformDialog} in order to display a message.
     *
     * @param fragmentManager - Fragment manager to use for displaying the dialog
     * @param message         - Message to display
     */
    static public void openDialog(FragmentManager fragmentManager, String message, @Nullable CloseCallBack closeCallBack) {
        openDialog(fragmentManager, message, 1200, closeCallBack);
    }

    /**
     * Close the dialog and execute an callback after it is closed.
     *
     * @param dialog        - Dialog to close
     * @param closeCallBack - Callback to execute
     */
    static private void dismissWithCallBack(InformDialog dialog, @Nullable CloseCallBack closeCallBack) {
        dialog.dismiss();
        if (closeCallBack != null) closeCallBack.callBack();
    }

    /**
     * Interface to execute callbacks after a dialog is closed.
     */
    public interface CloseCallBack {
        void callBack();
    }
}
