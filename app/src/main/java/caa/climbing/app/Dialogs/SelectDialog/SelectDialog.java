package caa.climbing.app.Dialogs.SelectDialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import caa.climbing.app.Database.DatabaseModels.LabelledInt;
import caa.climbing.app.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import caa.climbing.app.Views.Buttons.FlatButton;

/**
 * A class for displaying a custom {@link AlertDialog} from which an option can be selected.
 */
public class SelectDialog extends DialogFragment {

    private NumberPicker optionSelector;
    private final ArrayList<LabelledInt> options;
    private String[] displayedValues;
    private final int preSelect;

    public FlatButton cancelBtn, okBtn;
    public TextView dialogTitle;

    private View.OnClickListener okClick;
    private final View.OnClickListener cancelClick;

    private final String title;
    private final String okText;
    private final String cancelText;

    /**
     * Create a new {@link SelectDialog} DialogFragment.
     *
     * @param title       - Title of the dialog
     * @param options     - Options to pick from
     * @param preSelect   - Value of the initially selected option
     * @param okText      - Text of the positive button
     * @param cancelText  - Text of the negative button
     * @param okClick     - ClickListener for the positive button
     * @param cancelClick - ClickListener for the negative button
     */
    public SelectDialog(String title, ArrayList<LabelledInt> options, int preSelect, String okText, String cancelText, @Nullable View.OnClickListener okClick, @Nullable View.OnClickListener cancelClick) {
        this.okClick = okClick;
        this.cancelClick = cancelClick;
        this.title = title;
        this.cancelText = cancelText;
        this.okText = okText;
        this.options = options;
        this.preSelect = preSelect;
    }

    /**
     * Create a new {@link SelectDialog} DialogFragment.
     *
     * @param title     - Title of the dialog
     * @param preSelect - Value of the initially selected option
     * @param options   - Options to pick from
     */
    public SelectDialog(String title, int preSelect, ArrayList<LabelledInt> options) {
        this(title, options, preSelect, null, null, null, null);
    }

    /**
     * Set the Listener for the positive button.
     *
     * @param clickListener - New Listener for the button
     */
    public void setOkClick(View.OnClickListener clickListener) {
        this.okClick = clickListener;
    }

    /**
     * Override to build a custom Dialog container.
     *
     * @param savedInstanceState - Previous Instance
     * @return - Custom dialog
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(initView());

        AlertDialog alert = builder.create();
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alert.show();

        return alert;
    }

    /**
     * Initializes the view of the dialog
     *
     * @return - Custom view for the dialog
     */
    private View initView() {
        String cancelText = this.cancelText == null ? getString(R.string.select_dialog_default_cancel_text) : this.cancelText;
        String okText = this.okText == null ? getString(R.string.select_dialog_default_confirm_text) : this.okText;
        String title = this.title == null ? getString(R.string.select_dialog_default_title) : this.title;

        LayoutInflater inflater = requireActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.select_dialog, null);

        this.dialogTitle = rootView.findViewById(R.id.dialog_title);
        this.okBtn = rootView.findViewById(R.id.ok_btn);
        this.cancelBtn = rootView.findViewById(R.id.cancel_btn);
        this.optionSelector = rootView.findViewById(R.id.option_selector);

        this.dialogTitle.setText(title);
        this.okBtn.setText(okText);
        this.cancelBtn.setText(cancelText);

        this.displayedValues = new String[this.options.size()];
        for (int i = 0; i < this.options.size(); i++) {
            displayedValues[i] = this.options.get(i).label;
        }

        this.optionSelector.setMinValue(0);
        this.optionSelector.setMaxValue(Math.max(options.size() - 1, 0));
        this.optionSelector.setDisplayedValues(displayedValues);

        int preselectedIndex = LabelledInt.getIndexByValue(this.options, this.preSelect);
        if (preselectedIndex > 0) {
            this.optionSelector.setValue(preselectedIndex);
        }

        if (getContext() != null) {
            setDividerColor(this.optionSelector, ContextCompat.getColor(getContext(), R.color.white));
        }

        this.addEventListener();

        return rootView;
    }

    /**
     * Adds Listener to the positive and negative buttons.
     */
    private void addEventListener() {
        this.okBtn.setOnClickListener((view) -> {
            if (okClick != null) okClick.onClick(view);
            dismiss();
        });

        this.cancelBtn.setOnClickListener((view) -> {
            if (cancelClick != null) cancelClick.onClick(view);
            dismiss();
        });
    }

    /**
     * Sets the color of the divider between the options in the NumberPicker.
     * https://stackoverflow.com/a/44518134/12755273
     *
     * @param picker - NumberPicker to style
     * @param color  - Color of the divider
     */
    private void setDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException | Resources.NotFoundException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    /**
     * Get the selected option from the NumberPicker
     *
     * @return - The selected option
     */
    public LabelledInt getSelectedOption() {
        int idx = this.optionSelector.getValue();
        String label = this.displayedValues[idx];
        return LabelledInt.getLabelledInt(label, this.options);
    }
}
