package caa.climbing.app.Dialogs.CalendarEventDialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import caa.climbing.app.Adapters.RecyclerAdapter_CalendarEventDetail;
import caa.climbing.app.R;
import caa.climbing.app.Views.Buttons.FlatButton;
import caa.climbing.app.Views.EventCalendar.CalendarEvent;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * A class for displaying a custom {@link AlertDialog}.
 */
public class CalendarEventDialog extends DialogFragment {
    public FlatButton closeButton;
    public TextView dialogTitle;
    private RecyclerView eventDetailList;

    final private View.OnClickListener buttonClick;
    final private String title;
    final private String buttonText;
    final private ArrayList<CalendarEvent> events;
    final private Consumer<CalendarEvent> deleteCallback;

    /**
     * Create a new {@link CalendarEventDialog} DialogFragment.
     *
     * @param title       - Title of the dialog
     * @param buttonClick - ClickListener for the close button
     * @param events      - List of events to display
     */
    public CalendarEventDialog(String title, @Nullable String buttonText, @Nullable View.OnClickListener buttonClick, ArrayList<CalendarEvent> events, Consumer<CalendarEvent> deleteCallback) {
        this.buttonClick = buttonClick;
        this.title = title;
        this.buttonText = buttonText;
        this.events = events;
        this.deleteCallback = deleteCallback;
    }

    /**
     * Create a new {@link CalendarEventDialog} DialogFragment.
     *
     * @param title  - Title of the dialog
     * @param events - List of events to display
     */
    public CalendarEventDialog(String title, ArrayList<CalendarEvent> events, Consumer<CalendarEvent> deleteCallback) {
        this(title, null, null, events, deleteCallback);
    }

    /**
     * Override to build a custom Dialog container.
     *
     * @param savedInstanceState - Previous Instance
     * @return - Custom dialog
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(initView());

        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        return dialog;
    }

    /**
     * Initializes the view of the dialog
     *
     * @return - Custom view for the dialog
     */
    private View initView() {
        String buttonText = this.buttonText == null ? getString(R.string.select_dialog_default_confirm_text) : this.buttonText;
        String title = this.title == null ? getString(R.string.calendar_dialog_title__default) : this.title;

        LayoutInflater inflater = requireActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.calendar_event_dialog, null);

        this.dialogTitle = rootView.findViewById(R.id.dialog_title);
        this.closeButton = rootView.findViewById(R.id.close_button);
        this.eventDetailList = rootView.findViewById(R.id.event_detail_list);

        this.dialogTitle.setText(title);
        this.closeButton.setText(buttonText);

        this.initEventList();

        this.addEventListener();

        return rootView;
    }

    /**
     * Adds Listener to the positive and negative buttons.
     */
    private void addEventListener() {
        this.closeButton.setOnClickListener((view) -> {
            if (this.buttonClick != null) this.buttonClick.onClick(view);
            dismiss();
        });
    }

    /**
     * Initialize the RecyclerView to display the list of events.
     */
    private void initEventList() {
        Context context = getContext();
        if (context == null) return;

        RecyclerAdapter_CalendarEventDetail adapter = new RecyclerAdapter_CalendarEventDetail(context, this.events, this.deleteCallback, this);
        // use linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        this.eventDetailList.setLayoutManager(layoutManager);

        DividerItemDecoration itemDecoration = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.empty_divider_small, null);
        if (drawable != null) {
            itemDecoration.setDrawable(drawable);
            this.eventDetailList.addItemDecoration(itemDecoration);
        }

        this.eventDetailList.setHasFixedSize(true);
        this.eventDetailList.setAdapter(adapter);
    }

}
