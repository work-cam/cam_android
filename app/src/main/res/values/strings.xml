<?xml version="1.0" encoding="utf-8"?><!DOCTYPE resources [<!ENTITY inputSelector "data-selector=input">]>

<resources>
    <string name="app_name" translatable="false">Climbing App</string>
    <string name="account_name" translatable="false">caa.climbing.app</string>

    <!--  Selectors  -->
    <string name="selector_input" translatable="false">&inputSelector;</string>
    <string name="selector_input_minutes" translatable="false">&inputSelector;.minutes</string>
    <string name="selector_input_seconds" translatable="false">&inputSelector;.seconds</string>

    <!-- General -->
    <string name="general_climb_type_boulders">Boulders</string>
    <string name="general_climb_type_routes">Routes</string>

    <!-- Home Screen -->
    <string name="indoor_statistics_title">Indoor Sessions</string>
    <string name="indoor_cumulative_display">Cumulative Display</string>
    <string name="outdoor_statistics_title">Outdoor Sessions</string>
    <string name="outdoor_chart_title_outdoor_score">Cumulative Score</string>
    <string name="hangboard_statistics_title">Hangboard Sessions</string>
    <string name="hangboard_chart_title_max_strength">Max Strength %1$d mm Edge</string>
    <string name="hangboard_chart_strength_left">Left</string>
    <string name="hangboard_chart_strength_right">Right</string>
    <string name="hangboard_chart_title_relative_strength">Body Weight : Pulling Force</string>

    <!--  Menu  -->
    <string name="menu_home">Home</string>
    <string name="menu_new_indoor_session">New Indoor Session</string>
    <string name="menu_new_outdoor_session">New Outdoor Session</string>
    <string name="menu_new_hangboard_session">New Hangboard Session</string>
    <string name="menu_logbook">Logbook</string>
    <string name="menu_plan_sessions">Plan Sessions</string>
    <string name="menu_settings">Settings</string>

    <!--  Dialogs  -->
    <string name="confirm_dialog_default_title">Confirm.</string>
    <string name="confirm_dialog_default_cancel_text">Cancel</string>
    <string name="confirm_dialog_default_confirm_text">OK</string>

    <string name="select_dialog_default_title">Please select.</string>
    <string name="select_dialog_default_cancel_text">Cancel</string>
    <string name="select_dialog_default_confirm_text">OK</string>

    <!--  Filter  -->
    <string name="filter_button_text">Filters</string>

    <string name="indoor_filter_boulder_checkbox">Only Boulders</string>
    <string name="indoor_filter_route_checkbox">Only Routes</string>
    <string name="indoor_filter_gym_search_field">Search for gym</string>
    <string name="indoor_filter_order_by_date">Order by date</string>

    <!-- Hangboard  -->
    <string name="hangboard_max_strength_test">Max Strength Test</string>
    <string name="hangboard_strength_left">Left / kg</string>
    <string name="hangboard_strength_right">Right / kg</string>
    <string name="hangboard_test_edge_size">Test Edge Size / mm</string>
    <string name="hangboard_body_weight">Body Weight / kg</string>
    <string name="hangboard_extra_weight">Extra Weight / kg</string>

    <string name="hangboard_confirm_delete">Are you sure you want to delete this session?</string>
    <string name="hangboard_update_failed">Could not update the session.</string>

    <string name="hangboard_sets">Sets</string>
    <string name="hangboard_reps">Reps</string>

    <string name="hangboard_session_info_date_required">Date is required to store the session.</string>
    <string name="hangboard_session_info_save_successful">Successfully saved Hangboard Session.</string>
    <string name="hangboard_session_info_save_failed">Error while saving Hangboard Session.</string>

    <string name="hangboard_session_info_hang_required">Hang time must not be empty.</string>
    <string name="hangboard_session_info_rest_required">For more than 1 repetition, the rest time must not be empty.
    </string>
    <string name="hangboard_session_info_repetitions_required">Repetitions must not be empty.</string>
    <string name="hangboard_session_info_pause_required">For more than 1 set, the pause time must not be empty.</string>
    <string name="hangboard_session_info_sets_required">Sets must not be zero.</string>

    <string name="hangboard_session_message_get_ready">Get ready</string>
    <string name="hangboard_session_message_hang">Hang</string>
    <string name="hangboard_session_message_rest">Rest</string>
    <string name="hangboard_session_message_pause">Pause</string>
    <string name="hangboard_session_message_finished">Finished</string>

    <!--  Indoor Session  -->
    <string name="indoor_gym_hint">Gym</string>

    <string name="indoor_session_update_failed">Could not update the session.</string>
    <string name="indoor_session_confirm_delete">Are you sure you want to delete this session?</string>

    <string name="indoor_session_info_gym_required">The gym must not be empty.</string>
    <string name="indoor_session_info_date_required">Date is required to store the session.</string>
    <string name="indoor_session_info_nothing_to_save">There is nothing to save. Enter at least one boulder or route.
    </string>
    <string name="indoor_session_info_save_successful">Successfully saved session.
    </string>

    <!-- Outdoor  -->
    <string name="outdoor_session_boulder_header">Boulder</string>
    <string name="outdoor_session_route_header">Routes</string>

    <string name="outdoor_ascent_name">Name</string>
    <string name="outdoor_ascent_crag">Crag</string>
    <string name="outdoor_ascent_image_description">Image of the ascent</string>
    <string name="outdoor_ascent_method">Method</string>
    <string name="outdoor_ascent_grade">Grade</string>

    <string name="outdoor_ascent_update_failed">Could not update the ascent.</string>
    <string name="outdoor_ascent_confirm_delete">Are you sure you want to delete this ascent?</string>
    <string name="outdoor_ascent_confirm_delete_image">Are you sure you want to delete the image from this ascent? The
        photo will not be deleted from your device.
    </string>
    <string name="outdoor_ascent_select_grade">Please select a grade.</string>
    <string name="outdoor_ascent_select_method">Please select how you topped the problem.</string>

    <string name="outdoor_ascent_method_on_sight">On-Sight</string>
    <string name="outdoor_ascent_method_flash">Flash</string>
    <string name="outdoor_ascent_method_red_point">Red-Point</string>

    <string name="outdoor_session_info_nothing_to_save">Nothing to save.</string>
    <string name="outdoor_session_info_save_successful">Successfully saved Outdoor Session.</string>
    <string name="outdoor_session_info_save_failed">Could not save Outdoor Session.</string>
    <string name="outdoor_session_info_save_failed_boulders">Could not save Boulder Ascents.</string>
    <string name="outdoor_session_info_save_failed_routes">Could not save Route Ascents.</string>

    <!--  Sessions general -->
    <string name="start_session">Start Session</string>
    <string name="save_session">Save Session</string>
    <string name="finish_session">Finish Session</string>

    <!--  Settings  -->
    <string name="settings_grades_boulder">Grades Boulder</string>
    <string name="settings_grades_routes">Grades Routes</string>
    <string name="settings_language">Language</string>
    <string name="settings_save">Save Settings</string>
    <string name="settings_info_saving">Saving Settings…</string>
    <string name="settings_export_database">Export Database</string>

    <!--  Inputs  -->
    <string name="time_input_separator">:</string>
    <string name="time_input_hint_minutes">mm</string>
    <string name="time_input_hint_seconds">ss</string>

    <!--  Session planning  -->
    <string name="plan_session_select_title">Which session do you want to plan?</string>
    <string name="plan_session_indoor">Indoor Session</string>
    <string name="plan_session_hangboard">Hangboard Session</string>
    <string name="plan_session_outdoor">Outdoor Session</string>

    <!--  LogBook  -->
    <string name="logbook_no_entries">No entries to display</string>
    <string name="logbook_tab_outdoor">Outdoor Sessions</string>
    <string name="logbook_tab_indoor">Indoor Sessions</string>
    <string name="logbook_tab_hangboard">Hangboard Sessions</string>
    <string name="logbook_outdoor_table_header_boulders">Boulder (%1$d)</string>
    <string name="logbook_outdoor_table_header_routes">Routes (%1$d)</string>

    <!-- Calendar -->
    <string name="calendar_short_monday">MO</string>
    <string name="calendar_short_tuesday">TU</string>
    <string name="calendar_short_wednesday">WE</string>
    <string name="calendar_short_thursday">TH</string>
    <string name="calendar_short_friday">FR</string>
    <string name="calendar_short_saturday">SA</string>
    <string name="calendar_short_sunday">SU</string>
    <string name="calendar_select_year_dialog_title">Please select a year.</string>
    <string name="calendar_dialog_title">Planned sessions for %1$s</string>
    <string name="calendar_dialog_title__default">Planned sessions</string>
    <string name="calendar_confirm_delete_event">Are you sure you want to delete this session from the calendar?
    </string>

    <!-- Grades  -->
    <string name="grade_system_boulder_fb">fb</string>
    <string name="grade_system_boulder_v">V</string>

    <string name="grade_system_routes_french">French</string>
    <string name="grade_system_routes_uiaa">UIAA</string>

    <!-- Intent Fragments -->
    <string name="camera_missing_permission">You cannot take photos unless you grant the permissions to do so.</string>
    <string name="gallery_choose_prompt">Select a picture</string>
    <string name="gallery_missing_permission">You cannot select and/or show photos unless you grant the permission to do
        so.
    </string>
    <string name="storage_missing_permission">You cannot save files unless you grant the permission to do so.</string>
    <string name="calendar_missing_permission">You cannot plan sessions unless you grant the permissions to access the
        device\'s calendar.
    </string>

    <!-- SQLiteExporter -->
    <string name="sqlite_exporter_choose_folder">Please Select a folder to export the database to.</string>
    <string name="sqlite_exporter_success">Successfully exported database to the designated folder.</string>
    <string name="sqlite_exporter_failed">Database could not be exported.</string>
</resources>
